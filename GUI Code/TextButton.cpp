/*
 * This file implements a class
 * that is a clickable button with
 * a text on it
 *
 * Author:      Stuart Rudderham
 * Created:     March 10, 2012
 * Last Edited: March 10, 2012
 */

#include "GUI/TextButton.hpp"

using namespace sf;
using namespace std;

TextButton::TextButton( const ActionButtonInfo& buttonInfo, const LabelInfo& labelInfo, const ClickableElementInfo& clickableInfo, const BaseElementInfo& baseInfo ) : ActionButton( buttonInfo, clickableInfo, baseInfo ),
                                                                                                                                                                       text( labelInfo, baseInfo )
{
    text.ChangeAlignment( CENTER_ALIGN, CENTER_ALIGN );
}

void TextButton::OnHover() {

    element.setFillColor( fillColor * ClickableElement::mouseOverTint );
    text.SetColor( text.color * ClickableElement::mouseOverTint );
}

void TextButton::DefaultState() {
    element.setFillColor( fillColor );

    text.SetColor( text.color );
    //text.ClearColor();
}

void TextButton::ChildClassDraw( sf::RenderTarget& Target ) {

    // Draw the button however the base class does it
    Target.draw( element );
    text.Draw( Target );
}
