/*
 * This file implements a base
 * class for for all GUI elements
 *
 * Author:      Stuart Rudderham
 * Created:     March 6, 2012
 * Last Edited: March 6, 2012
 */



// Constructor
template<typename T>
BaseElement<T>::BaseElement( const BaseElementInfo& info ) : name( info.name ),
                                                             position( info.position ),
                                                             horizontalAlignment( info.horizontalAlignment ),
                                                             verticleAlignment( info.verticleAlignment ),
                                                             isVisible ( info.isVisible )
{
    // Set the position
    element.setPosition( position );

    // No point in setting up the origin because the actual GUI element
    // is initialized in the child classes so we don't know the size yet
    //SetOrigin();
}

template<typename T>
void BaseElement<T>::SetOrigin() {

    // Get the dimensions of the element
    const sf::FloatRect boundingBox = element.getGlobalBounds();

    // Set the origin of the element depending on its alignment
    element.setOrigin( static_cast<int>(horizontalAlignment) * boundingBox.width / 2.0f,
                       static_cast<int>(verticleAlignment) * boundingBox.height / 2.0f );
}

// draw function
template<typename T>
void BaseElement<T>::Draw( sf::RenderTarget& Target ) {

    // if the element is "visible" then call the draw function of the child class
    if( isVisible ) {
        ChildClassDraw( Target );
    }
}

// make the element visible
template<typename T>
void BaseElement<T>::MakeVisible() {
    isVisible = true;
}

// make the element invisible
template<typename T>
void BaseElement<T>::MakeInvisible() {
    isVisible = false;
}

// change the alignment of the element
template<typename T>
void BaseElement<T>::ChangeAlignment( Alignment hAlign, Alignment vAlign ) {

    // Set the new alignment
    horizontalAlignment = hAlign;
    verticleAlignment = vAlign;

    // Update the location of the origin
    SetOrigin();

}

template<typename T>
void BaseElement<T>::SetColor( sf::Color color ) {
    element.setColor( color );
}



template<typename T>
void BaseElement<T>::ClearColor() {
    element.setColor( sf::Color::White );
}



/*

// move the element to a new location
template<typename T>
void BaseElement<T>::MoveTo( Point2f newPos ) {

    // Move the element by the difference between the current position and the desired position
    element.Move( GetDirectionVector( element.GetPosition(), newPos ) );
}

// move the element by the displacement vector
template<typename T>
void BaseElement<T>::MoveBy( Point2f displacement ) {

    element.Move( displacement );
}

*/
