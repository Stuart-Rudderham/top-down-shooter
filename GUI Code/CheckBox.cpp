/*
 * This file implements a checkbox
 * class for toggling a boolean
  *
 * Author:      Stuart Rudderham
 * Created:     March 3, 2012
 * Last Edited: March 3, 2012
 */

#include "GUI/CheckBox.hpp"
#include "MediaCache/MediaCache.hpp"

using namespace sf;
using namespace std;


// Constructor
CheckBox::CheckBox( const CheckBoxInfo& checkInfo, const PictureInfo& pictureInfo, const ClickableElementInfo& clickableInfo, const BaseElementInfo& baseInfo ) : ClickableElement( clickableInfo, baseInfo ),
                                                                                                                                                                  optionToToggle( checkInfo.optionToToggle ),
                                                                                                                                                                  image( pictureInfo, baseInfo )
{
    image.ChangeAlignment( CENTER_ALIGN, CENTER_ALIGN );
    image.SetSize( clickableInfo.width, clickableInfo.height );
}


void CheckBox::ChildClassDraw( RenderTarget& Target ) {

    if( optionToToggle == true ) {
        image.SetFrameNum( 1 );
    } else {
        image.SetFrameNum( 0 );
    }

    // Draw the button however the base class does it
    Target.draw( element );
    image.Draw( Target );
}

void CheckBox::OnClick() {

    // toggle the option when the check box is clicked
    optionToToggle = !optionToToggle;
}

// this function is called when the mouse is over the button but not pressed
void CheckBox::OnHover() {

    element.setFillColor( fillColor * ClickableElement::mouseOverTint );
    image.SetColor( ClickableElement::mouseOverTint );
}

// this function is called when the mouse is not over the button at all
void CheckBox::DefaultState() {
    element.setFillColor( fillColor );
    image.ClearColor();
}
