/*
 * This file implements a checkbox
 * class for toggling a boolean
  *
 * Author:      Stuart Rudderham
 * Created:     March 3, 2012
 * Last Edited: March 3, 2012
 */

#include "GUI/ActionButton.hpp"

using namespace sf;
using namespace std;

ActionButton::ActionButton( const ActionButtonInfo& buttonInfo, const ClickableElementInfo& clickableInfo, const BaseElementInfo& baseInfo ) : ClickableElement( clickableInfo, baseInfo )
{
    action = buttonInfo.action;
}

void ActionButton::OnClick() {

    // Do the action when the button is clicked
    action();
}
