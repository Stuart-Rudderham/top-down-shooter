/*
 * This header defines a class
 * for displaying a picture on
 * the screen
 *
 * Author:      Stuart Rudderham
 * Created:     March 4, 2012
 * Last Edited: March 4, 2012
 */

#ifndef PICTURE_HPP_INCLUDED
#define PICTURE_HPP_INCLUDED

#include "GUI/BaseElement.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

// This is a struct that holds all the necessary info to initialize a Picture
struct PictureInfo {
    const sf::Texture& texture;

    unsigned int numFrames;
    unsigned int currentFrame;

    float width;
    float height;

    // Constructor with appropriate default values
    PictureInfo( const sf::Texture& pictureTexture, unsigned int nFrames = 1, unsigned int cFrame = 0, float w = -1.0f, float h = -1.0f ) : texture( pictureTexture ),
                                                                                                                                      numFrames( nFrames ),
                                                                                                                                      currentFrame( cFrame ),
                                                                                                                                      width( w ),
                                                                                                                                      height( h )
    {

    }
};

class Picture : public BaseElement<sf::Sprite> {
    private:
        unsigned int currentFrame;
        const unsigned int numFrames;

        void ChildClassDraw( sf::RenderTarget& Target );                    // the draw function implemented by the child of the BaseElement class

    public:
        Picture( const PictureInfo& pictureInfo,
                 const BaseElementInfo& baseInfo );

        void SetFrameNum( unsigned int frameNum );
        void SetSize( float width, float height );
};

#endif // PICTURE_HPP_INCLUDED
