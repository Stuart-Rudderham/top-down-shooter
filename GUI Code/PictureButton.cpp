/*
 * This file implements a class
 * that is a clickable button with
 * a texture on it
 *
 * Author:      Stuart Rudderham
 * Created:     March 10, 2012
 * Last Edited: March 10, 2012
 */

#include "GUI/PictureButton.hpp"

using namespace sf;
using namespace std;

PictureButton::PictureButton( const ActionButtonInfo& buttonInfo, const PictureInfo& pictureInfo, const ClickableElementInfo& clickableInfo, const BaseElementInfo& baseInfo ) : ActionButton( buttonInfo, clickableInfo, baseInfo ),
                                                                                                                                                                                 image( pictureInfo, baseInfo )
{
    image.ChangeAlignment( CENTER_ALIGN, CENTER_ALIGN );
    image.SetSize( clickableInfo.width, clickableInfo.height );
}

void PictureButton::OnHover() {

    element.setFillColor( fillColor * ClickableElement::mouseOverTint );
    image.SetColor( ClickableElement::mouseOverTint );
}

void PictureButton::DefaultState() {
    element.setFillColor( fillColor );
    image.ClearColor();
}

void PictureButton::ChildClassDraw( sf::RenderTarget& Target ) {

    // Draw the button however the base class does it
    Target.draw( element );
    image.Draw( Target );
}
