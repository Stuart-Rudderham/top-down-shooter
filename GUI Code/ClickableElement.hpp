/*
 * This header defines a base
 * class for GUI elements that
 * can be clicked on
 *
 * Author:      Stuart Rudderham
 * Created:     March 5, 2012
 * Last Edited: March 5, 2012
 */

#ifndef CLICKABLEELEMENT_HPP_INCLUDED
#define CLICKABLEELEMENT_HPP_INCLUDED

#include "GUI/BaseElement.hpp"
#include "UpgradesToSFML/Input.hpp"

#include <SFML/Graphics/RectangleShape.hpp>

// This is a struct that holds all the necessary info to initialize a ClickableElement
struct ClickableElementInfo {

    float width;
    float height;

    sf::Color fillColor;
    sf::Color outlineColor;
    float outlineThickness;

    // Constructor with appropriate default values
    ClickableElementInfo( float w, float h, sf::Color fill = sf::Color::Transparent, sf::Color outline = sf::Color::Transparent, float thickness = 0.0f ) : width( w ),
                                                                                                                                                            height( h ),
                                                                                                                                                            fillColor( fill ),
                                                                                                                                                            outlineColor( outline ),
                                                                                                                                                            outlineThickness( thickness )
    {

    }
};

// defines whether the button is "clicked" when the mouse is pressed or when it is released
enum ActivationMode
{
    ON_PRESS,
    ON_RELEASE
};


class ClickableElement : public BaseElement<sf::RectangleShape> {

    // marked as "protected" rather than "private" so that
    // child classes can access them
    protected:
        static sf::Color mouseOverTint;                                         // the color used to tint the button when the mouse is over it

        static ActivationMode activationMode;                                   // defines whether the button is "clicked" when the mouse is pressed
                                                                                // or when it is released

        sf::Color fillColor;                                              // the color of the button
        sf::Color outlineColor;
        float outlineThickness;



        virtual void OnClick() = 0;                                             // this function is called when the button is clicked. It is implemented
                                                                                // by the child classes

        virtual void OnHover();                                                 // this function is called when the mouse is over the button but not pressed
        virtual void OnPress();                                                 // this function is called when the mouse is clicked but not released
        virtual void DefaultState();                                            // this function is called when the mouse is not over the button at all

    public:
        ClickableElement( const ClickableElementInfo& clickableInfo,
                          const BaseElementInfo& baseInfo );

        void Update( MouseState& mouse );                                       // this updates the button's state based on the mouse input

        void SetColor( sf::Color color );                                   // set the global color of the element.
        void ClearColor();                                                  // reset the global color back to white
};

#endif // CLICKABLEELEMENT_HPP_INCLUDED
