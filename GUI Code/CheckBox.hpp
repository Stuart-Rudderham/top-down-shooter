/*
 * This header defines a checkbox
 * class for toggling a boolean
 *
 * Author:      Stuart Rudderham
 * Created:     March 3, 2012
 * Last Edited: March 3, 2012
 */

#ifndef CHECKBOX_HPP_INCLUDED
#define CHECKBOX_HPP_INCLUDED

#include "GUI/ClickableElement.hpp"
#include "GUI/Picture.hpp"


// This is a struct that holds all the necessary info to initialize a CheckBox
struct CheckBoxInfo {

    bool& optionToToggle;

    // Constructor with appropriate default values
    CheckBoxInfo( bool& option ) : optionToToggle( option )
    {

    }
};

class CheckBox : public ClickableElement {
    private:
        bool& optionToToggle;                                                   // the option the checkbox toggles
        Picture image;

        void OnClick();                                                         // what we want to do when the checkbox is clicked
        void OnHover();                                                 // this function is called when the mouse is over the button but not pressed
        void DefaultState();

        void ChildClassDraw( sf::RenderTarget& Target );

    public:
        CheckBox( const CheckBoxInfo& checkInfo,
                  const PictureInfo& pictureInfo,
                  const ClickableElementInfo& clickableInfo,
                  const BaseElementInfo& baseInfo );
};

#endif // CHECKBOX_HPP_INCLUDED
