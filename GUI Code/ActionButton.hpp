/*
 * This header defines a button
 * class that calls a function
 * when you click it
 *
 * Author:      Stuart Rudderham
 * Created:     March 3, 2012
 * Last Edited: March 3, 2012
 */

#ifndef ACTIONBUTTON_HPP_INCLUDED
#define ACTIONBUTTON_HPP_INCLUDED

#include "GUI/ClickableElement.hpp"

// This is a struct that holds all the necessary info to initialize a CheckBox
struct ActionButtonInfo {

    void (*action)();

    // Constructor with appropriate default values
    ActionButtonInfo( void (*f)() )
    {
        action = f;
    }
};

class ActionButton : public ClickableElement {
    private:
        void (*action)();                                                       // a pointer to the function that's called when
                                                                                //the button is clicked. The function takes no
                                                                                //parameters and returns void

        void OnClick();                                                         // what we want to do when the checkbox is clicked
        virtual void OnHover() = 0;                                                         // this function is called when the mouse is over the button but not pressed
        virtual void DefaultState() = 0;

        virtual void ChildClassDraw( sf::RenderTarget& Target ) = 0;                        // draw the checkbox onto the screen

    public:
        ActionButton( const ActionButtonInfo& buttonInfo,
                      const ClickableElementInfo& clickableInfo,
                      const BaseElementInfo& baseInfo );
};

#endif // ACTIONBUTTON_HPP_INCLUDED
