/*
 * This header defines a class
 * that is a clickable button with
 * a texture on it
 *
 * Author:      Stuart Rudderham
 * Created:     March 10, 2012
 * Last Edited: March 10, 2012
 */

#ifndef PICTUREBUTTON_HPP_INCLUDED
#define PICTUREBUTTON_HPP_INCLUDED

#include "GUI/ActionButton.hpp"
#include "GUI/Picture.hpp"

class PictureButton: public ActionButton {

    // marked as "protected" rather than "private" so that
    // child classes can access them
    protected:
        Picture image;

        void OnHover();                                                 // this function is called when the mouse is over the button but not pressed
        void DefaultState();

        void ChildClassDraw( sf::RenderTarget& Target );

    public:
        PictureButton( const ActionButtonInfo& buttonInfo,
                       const PictureInfo& pictureInfo,
                       const ClickableElementInfo& clickableInfo,
                       const BaseElementInfo& baseInfo );
};

#endif // PICTUREBUTTON_HPP_INCLUDED
