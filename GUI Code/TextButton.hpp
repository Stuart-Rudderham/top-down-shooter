/*
 * This header defines a class
 * that is a clickable button with
 * a text on it
 *
 * Author:      Stuart Rudderham
 * Created:     March 10, 2012
 * Last Edited: March 10, 2012
 */

#ifndef TEXTBUTTON_HPP_INCLUDED
#define TEXTBUTTON_HPP_INCLUDED

#include "GUI/ActionButton.hpp"
#include "GUI/Label.hpp"

class TextButton : public ActionButton {

    // marked as "protected" rather than "private" so that
    // child classes can access them
    protected:
        Label text;

        void OnHover();                                                 // this function is called when the mouse is over the button but not pressed
        void DefaultState();

        void ChildClassDraw( sf::RenderTarget& Target );

    public:
        TextButton( const ActionButtonInfo& buttonInfo,
                    const LabelInfo& labelInfo,
                    const ClickableElementInfo& clickableInfo,
                    const BaseElementInfo& baseInfo );
};

#endif // TEXTBUTTON_HPP_INCLUDED
