/*
 * This header defines a base
 * class for all GUI elements
 *
 * Author:      Stuart Rudderham
 * Created:     March 6, 2012
 * Last Edited: March 6, 2012
 */

#ifndef BASEELEMENT_HPP_INCLUDED
#define BASEELEMENT_HPP_INCLUDED

#include "Math/MathFunctions.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

// This enum defines the way that an element can be justified
// The reason that we have multiple entries with the same value
// is so that both verticle and horizontal alignment can use
// names that make sence
enum Alignment
{
    LEFT_ALIGN = 0,
    TOP_ALIGN = 0,
    CENTER_ALIGN = 1,
    RIGHT_ALIGN = 2,
    BOTTOM_ALIGN = 2
};


// This is a struct that holds all the necessary info to initialize a BaseElement
struct BaseElementInfo {

    const char* name;                       // the name given to the element

    Point2f position;                       // the position the element is drawn at

    Alignment horizontalAlignment;          // its alignment relative to that position
    Alignment verticleAlignment;

    bool isVisible;                         // whether or not the element is visible

    // Constructor with appropriate default values
    BaseElementInfo( const char* elementName, Point2f pos, Alignment hAlign = CENTER_ALIGN, Alignment vAlign = CENTER_ALIGN, bool visible = true ) : name( elementName ),
                                                                                                                                                     position( pos ),
                                                                                                                                                     horizontalAlignment( hAlign ),
                                                                                                                                                     verticleAlignment( vAlign ),
                                                                                                                                                     isVisible( visible )
    {

    }
};

template<typename T>
class BaseElement {

    // marked as "protected" rather than "private" so that
    // child classes can access them
    protected:
        T element;                                                          // the element that is drawn onto the screen

        const char* name;

        Point2f position;                                                   // the position to draw the element at

        Alignment horizontalAlignment;                                      // the alignment of the element. The alignment
        Alignment verticleAlignment;                                        // dictates the location of the element's origin

        bool isVisible;                                                     // if this bool is false the element is not drawn onto the screen

        void SetOrigin();                                                   // set the origin of the element

        virtual void ChildClassDraw( sf::RenderTarget& Target ) = 0;        // the actual draw function implemented by the child class

    public:
        BaseElement( const BaseElementInfo& info );                         // constructor

        void Draw( sf::RenderTarget& Target );                              // if the element is "visible" then call the draw function
                                                                            // of the child class

//        virtual void MoveTo( Point2f newPos );                              // move the element to a new location
//        virtual void MoveBy( Point2f displacement );                        // move the element by the displacement vector

        virtual void ChangeAlignment( Alignment hAlign,                     // change the alignment of the element
                                      Alignment vAlign );                   // This updates the origin

        void MakeVisible();                                         // set the visibility state of the element
        void MakeInvisible();

        void SetColor( sf::Color color );                                   // set the global color of the element.
        void ClearColor();                                                  // reset the global color back to white
};

#include "GUI/BaseElement.inl"

#endif // BASEELEMENT_HPP_INCLUDED
