/*
 * This file implements a class
 * for displaying a string as
 * text on the screen
 *
 * Author:      Stuart Rudderham
 * Created:     March 3, 2012
 * Last Edited: March 4, 2012
 */

#include "GUI/Label.hpp"

#include "Debug/DebugTools.hpp"

using namespace sf;

// Constructor
Label::Label( const LabelInfo& labelInfo, const BaseElementInfo& baseInfo ) : BaseElement( baseInfo )
{
    color = labelInfo.color;

    // Set up the text with the given font, size, and color
    element.setFont( labelInfo.font );
    element.setCharacterSize( labelInfo.size );
    element.setColor( color );

    SetText( labelInfo.text );
}

void Label::SetOffset() {

    // Get the dimensions of the text
    const FloatRect boundingBox = element.getLocalBounds();

    // Need to offset the position a bit because of the way that SFML draws text
    offset = Vector2f( boundingBox.left, boundingBox.top );
}

void Label::SetText( const char* labelText ) {

    // Change the string
    element.setString( labelText );

    // Recalculate the center, origin, and position because
    // the bounding rectangle of the text has changed
    SetOrigin();
    SetOffset();
    element.setPosition( position - offset );
}

void Label::ChildClassDraw( RenderTarget& Target ) {

    // Draw the text onto the screen
    Target.draw( element );
}

/*
void Label::ClearColor() {
    element.SetColor( color );
}
*/
