/*
 * This file implements a class
 * for displaying a string as
 * text on the screen
 *
 * Author:      Stuart Rudderham
 * Created:     March 3, 2012
 * Last Edited: March 3, 2012
 */

#include "GUI/ClickableElement.hpp"

using namespace sf;
using namespace std;

// Set the static member variables
Color ClickableElement::mouseOverTint( 128, 128, 128 );             // grey

ActivationMode ClickableElement::activationMode = ON_PRESS;

// constructor for a colored button
ClickableElement::ClickableElement( const ClickableElementInfo& clickableInfo, const BaseElementInfo& baseInfo  ) : BaseElement( baseInfo )
{


    fillColor = clickableInfo.fillColor;                                              // the color of the button
    outlineColor = clickableInfo.outlineColor;
    outlineThickness = clickableInfo.outlineThickness;


    element.setFillColor( fillColor );
    element.setOutlineColor( outlineColor );
    element.setOutlineThickness( outlineThickness );


    element.setSize( Vector2f(clickableInfo.width, clickableInfo.height) );

    SetOrigin();

    element.setPosition( position + Vector2f(outlineThickness, outlineThickness) );
}

void ClickableElement::Update( MouseState& mouse ) {

    // Don't do anything if the button is invisible
    if( isVisible == true ) {
        // Get the bounding box of the button
        FloatRect boundingBox = element.getGlobalBounds();

        // If the mouse cursor is over the button
        if( boundingBox.contains( mouse.GetPosition() ) ) {

            // if the mouse button is pressed
            if( ( activationMode == ON_PRESS ) && ( mouse.IsJustPressed() ) ) {
                OnClick();
            } else if( ( activationMode == ON_RELEASE ) && ( mouse.IsJustReleased() ) ) {
                OnClick();
            } else if( mouse.IsDown() ) {
                OnPress();
            } else {
                OnHover();
            }

        // Otherwise untint the button
        } else {
            DefaultState();
        }
    }
}


void ClickableElement::OnHover() {
    // default behaviour is do nothing
}

void ClickableElement::OnPress() {
    // default behaviour is do nothing
}

void ClickableElement::DefaultState() {
    // default behaviour is do nothing
}


void ClickableElement::SetColor( sf::Color color ) {
    element.setFillColor( color );
}

void ClickableElement::ClearColor() {
    element.setFillColor( sf::Color::White );
}
