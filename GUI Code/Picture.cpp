/*
 * This file implements a class
 * for displaying a picture on
 * the screen
 *
 * Author:      Stuart Rudderham
 * Created:     March 4, 2012
 * Last Edited: March 4, 2012
 */

#include "GUI/Picture.hpp"

#include "UpgradesToSFML/Drawing.hpp"
#include "Debug/DebugTools.hpp"

using namespace sf;

// Constructor
Picture::Picture( const PictureInfo& pictureInfo, const BaseElementInfo& baseInfo ) : BaseElement( baseInfo ),
                                                                                      numFrames( pictureInfo.numFrames )
{
    // Set the picture's image
    element.setTexture( pictureInfo.texture );

    SetFrameNum( pictureInfo.currentFrame );

    SetOrigin();

    // Resize it if necessary
    if( pictureInfo.width > 0.0f && pictureInfo.height > 0.0f ) {
        SetSize( pictureInfo.width, pictureInfo.height );
    }
}

void Picture::SetFrameNum( unsigned int frameNum ) {
    dbAssert( frameNum < numFrames, "Desired frame was out of range. Value given was ", frameNum );

    currentFrame = frameNum;

    // Set the texture of the CheckBox depending on whether or not the
    // boolean is set to true or false
    IntRect textureRect;

    textureRect.left = 0;
    textureRect.width = element.getTexture()->getSize().x;
    textureRect.height = element.getTexture()->getSize().y / numFrames;
    textureRect.top = currentFrame * textureRect.height;

    element.setTextureRect(textureRect);
}

void Picture::SetSize( float width, float height ) {

    dbAssert( width > 0.0f, "Width was not positive. Value given was ", width );
    dbAssert( height > 0.0f, "Height was not positive. Value given was ", height );

    ResizeSprite( element, width, height );
}

void Picture::ChildClassDraw( RenderTarget& Target ) {

    // Draw the picture onto the screen
    Target.draw( element );
}
