/*
 * This header defines a class
 * for displaying a string as
 * text on the screen
 *
 * Author:      Stuart Rudderham
 * Created:     March 3, 2012
 * Last Edited: March 4, 2012
 */

#ifndef LABEL_HPP_INCLUDED
#define LABEL_HPP_INCLUDED

#include "GUI/BaseElement.hpp"

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>

// This is a struct that holds all the necessary info to initialize a Label
struct LabelInfo {
    const char* text;
    const sf::Font& font;
    unsigned int size;
    sf::Color color;

    // Constructor with appropriate default values
    LabelInfo( const char* labelText, const sf::Font& labelFont, unsigned int textSize, sf::Color textColor = sf::Color::Black ) : text( labelText ),
                                                                                                                             font( labelFont ),
                                                                                                                             size( textSize ),
                                                                                                                             color( textColor )
    {

    }
};

class Label : public BaseElement<sf::Text> {
    private:
        sf::Vector2f offset;                                                // an offset because SFML draws text in a weird way

        sf::Color color;

        void SetOffset();

        void ChildClassDraw( sf::RenderTarget& Target );                    // the draw function implemented by the child of the BaseElement class

    public:
        Label( const LabelInfo& labelInfo,                                        // constructor
               const BaseElementInfo& baseInfo );

        void SetText( const char* labelText );
        //void ClearColor();

    friend class TextButton;
};

#endif // LABEL_HPP_INCLUDED
