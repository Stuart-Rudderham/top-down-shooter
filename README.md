# Controls: #

| Action         | Key         |
|----------------|-------------|
| Move           | WASD        |
| Shoot          | Left click  |
| Switch Weapons | E           |
| Reload         | R           |
| Zoom in/out    | Mouse wheel |

# Screenshot #

![](./screenshot.png "Screenshot")
