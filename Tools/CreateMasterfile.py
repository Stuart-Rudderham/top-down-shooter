# This script searches through the Source directory and 
# creates a masterfile from all of the C++ source files found
import os
import datetime

# Global variables
masterfileName = '../Source/Masterfile.cpp'
startingFolder = '../Source'
currentTime = datetime.datetime.now()
useMasterfileMacro = 'USE_MASTERFILE'

def main():

	# Open the masterfile and walk the Source directory
	# If we find a .cpp file then add it to the masterfile
	with open( masterfileName, 'w' ) as masterfile:
	
		masterfile.write( '/* \n' )
		masterfile.write( ' * This is the masterfile for the Top Down Shooter program \n' )
		masterfile.write( ' * \n' )
		masterfile.write( ' * It is used to speed up build time and increase program \n' )
		masterfile.write( ' * efficiency by using the pre-processor to concatenate all \n' )
		masterfile.write( ' * the C++ source files together and then compiling the \n' )
		masterfile.write( ' * resulting masterfile \n' )
		masterfile.write( ' * \n' )
		masterfile.write( ' * For more information see -> http://buffered.io/posts/the-magic-of-unity-builds \n' )
		masterfile.write( ' * \n' )
		masterfile.write( ' * Generated on ' + currentTime.strftime( '%B %d, %Y' ) + ' at ' + currentTime.strftime( '%H:%M' ) + '\n' )
		masterfile.write( ' */ \n' )
		
		masterfile.write( '\n' )
		masterfile.write( '#if USE_MASTERFILE \n' )
		masterfile.write( '\n' )
	
		for root, dirs, files in os.walk( startingFolder ):
			for f in files:																					# for every file
				if f.endswith( '.cpp' ) and f.find( 'Masterfile.cpp' ) == -1:								# if it's a C++ source file that isn't the masterfile itself
					includeFile = os.path.relpath( os.path.join( root, f ), startingFolder )				# get the correct relative path
					includeFile = includeFile.replace( '\\', '/' )											# fix the file paths
					print includeFile		
					masterfile.write( '#include "' + includeFile + '"\n' )									# add it to the master file

		masterfile.write( '\n' )
		masterfile.write( '#endif // ' + useMasterfileMacro + '\n' )
		
main()

print ''
print '----------------------------------'
print 'Masterfile Created!'
print ''
raw_input( 'Press Enter to exit' )
