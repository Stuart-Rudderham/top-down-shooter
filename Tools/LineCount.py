# This script searches through all of the source files and 
# counts the number of lines of code
import os

def CountLines( filename ):
    lines = 0
    for line in open(filename):
        lines += 1
    return lines

def main():

    LOC = 0
    numberOfFiles = 0;
    startingFolder = os.path.normpath( '../Source/' )
    fileExtensions = tuple( [ '.inl', '.hpp', '.cpp' ] )        # this is a list of all the types of files we care about

    # Walk the filesystem and get a list of all the files we care about
    for root, dirs, files in os.walk( startingFolder ):
        for f in files:
            if f.endswith( fileExtensions ):
                print os.path.join( root, f )
                LOC += CountLines( os.path.join( root, f ) )
                numberOfFiles += 1

    print ''
    print 'LOC        - ' + str( LOC )
    print '# Of Files - ' + str( numberOfFiles )

main()

print ''
print '----------------------------------'
print ''
raw_input( 'Press Enter to exit' )
