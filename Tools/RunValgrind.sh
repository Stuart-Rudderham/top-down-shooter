#!/bin/bash

# This script will run Valgrind on the executable and archive the resulting log file
# It is only intended to work on a Unix-like operating system
# It is intended to be run from the Tools directory
# It expects that the executable is called "TopDownShooter"
# It will also print out a summary of the Valgrind results as well as memory leak checks from the executable

# Run valgrind on program
cd ..
valgrind --leak-check=full --show-reachable=yes "./TopDownShooter" &> ValgrindOutput.txt

# Archive the results
now=`date "+ (%H-%M-%S%P, %d-%m-%Y)"`
cp ValgrindOutput.txt "Tools/ValgrindArchive/ValgrindOutput$now.txt"

# Print out results
echo ""
echo "ValgrindOutput"
grep "Leaked Memory:"   ValgrindOutput.txt
grep "definitely lost:" ValgrindOutput.txt
grep "indirectly lost:" ValgrindOutput.txt
grep "possibly lost:"   ValgrindOutput.txt
grep "still reachable:" ValgrindOutput.txt
grep "suppressed:"      ValgrindOutput.txt