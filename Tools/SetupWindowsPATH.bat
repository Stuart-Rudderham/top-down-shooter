@echo off 

:: This script will add Python and the MinGW compiler to the PATH environment variables
:: It is only intended to work on Windows
:: It assumes that Pythong and MinGW are installed in the default locations
:: It assumes the Python version is 2.7
:: It should only be run once, otherwise the PATH variable will have duplicates

setx PATH "%PATH%;C:\Python27;C:\MinGW\bin"