#!/bin/bash

# This script will download and install all the libraries that SFML needs to compile
# It is only intended to work on a Unix-like operating system
# It should only ever have to be run once, when you want to build SFML for the first time

# Want to be root user so not always asking for password
su

yes | apt-get install cmake
yes | apt-get install libx11-dev
yes | apt-get install libxrandr-dev
yes | apt-get install libfreetype6-dev
yes | apt-get install libglew1.5-dev
yes | apt-get install libjpeg62-dev
yes | apt-get install libopenal-dev
yes | apt-get install libsndfile1-dev
