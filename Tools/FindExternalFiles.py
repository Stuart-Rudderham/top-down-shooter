# This script searches through the Content directory and 
# creates list of paths to all of the files found
import os
import datetime
import sys
import shutil

# Global Variables
currentTime = datetime.datetime.now()

rootDirectory  = '..'                                                                   # the root folder ( /top-down-shooter )
contentFolder  = '../Content'                                                           # where all the content is stored
outputFilename = '../Source/ExternalFiles.hpp'                                          # where we want to create the source file

# This is a list of all the types of files we care about
# The key is the file extension and the value is what type of media it is
fileExtensions = {
                    '.png'  : 'Image',
                    '.bmp'  : 'Image',
                    '.jpg'  : 'Image',
                    '.jpeg' : 'Image',
                    '.wav'  : 'Sound',
                    '.ogg'  : 'Sound',
                    '.ttf'  : 'Font',
                    '.xml'  : 'Map',
                 }

# Let the user know what's up
print ''
print 'Getting content from:   ' + os.path.relpath( contentFolder, rootDirectory ) + os.sep
print 'Putting generated file: ' + os.path.relpath( outputFilename, rootDirectory ) + os.sep
print ''

with open( outputFilename, 'w' ) as outputFile:
    
    # The header at the top of the generated file
    outputFile.write( '/* \n' )
    outputFile.write( ' * This file holds all the external files used \n' )
    outputFile.write( ' * for storing media within an executable file \n' )
    outputFile.write( ' * \n' )
    outputFile.write( ' * Generated on ' + currentTime.strftime( '%B %d, %Y' ) + ' at ' + currentTime.strftime( '%H:%M' ) + '\n' )
    outputFile.write( ' */ \n' )
    outputFile.write( '\n' )

    # Walk the filesystem and get a list of all the files we care about
    print 'Files found:'
    for root, dirs, files in os.walk( contentFolder ):
        for f in files:
            if f.endswith( tuple( fileExtensions.keys() ) ):
                filePath = os.path.relpath( os.path.join(root, f), rootDirectory )
                print '\t' + filePath

                extensionSuffix = fileExtensions[ os.path.splitext( filePath )[1] ]                     # based on the file extension find a string representing 
                                                                                                        # what type of file it is (e.g. ".png" -> "Image")

                variableName = os.path.basename( filePath );                                            # the variable name is the file name
                variableName = os.path.splitext( variableName )[0]                                      # minus the file extension
                variableName += extensionSuffix;                                                        # with the extension suffix string
                variableName = variableName.replace( ' ', '' )                                          # and no spaces

                filePath = filePath.replace( '\\', '/' )                                                # replace single backslash with single forward slash so C++ compiler is happy

                outputFile.write( 'const ExternalFile ' + variableName + ' = "' + filePath + '"; \n' )        # write to output file
    print ''

print ''
print '----------------------------------'
print ''
raw_input( 'Press Enter to exit' )
