CCCCCCCC	Used by Microsoft's C++ debugging runtime library to mark uninitialised stack memory
CDCDCDCD	Used by Microsoft's C++ debugging runtime library to mark uninitialised heap memory
DDDDDDDD	Used by Microsoft's C++ debugging heap to mark freed heap memory
FEEEFEEE	Used by Microsoft's HeapFree() to mark freed heap memory
FDFDFDFD	Used by Microsoft's C++ debugging heap to mark "no man's land" guard bytes before and after allocated heap memory
ABABABAB	Used by Microsoft's HeapAlloc() to mark "no man's land" guard bytes after allocated heap memory
BAADF00D	Used by Microsoft's LocalAlloc(LMEM_FIXED) to mark uninitialised allocated heap memory
DEADDEAD	A Microsoft Windows STOP Error code used when the user manually initiates the crash.