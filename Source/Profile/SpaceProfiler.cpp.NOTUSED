/*
 * This header implements a class that can
 * be used to profile how much memory
 * something takes
 *
 * Author:      Stuart Rudderham
 * Created:     February 9, 2012
 * Last Edited: February 9, 2012
 */

#include "Profile/SpaceProfiler.hpp"
#include "Debug/DebugTools.hpp"

using namespace std;
using namespace sf;

const char* categoryNames[] = { "Other: ",
                                "AABB: ",
                                "Circle: " };
// constructor
SpaceProfiler::SpaceProfiler( const char* logfileName ) : numFramesCompleted( 0 ),
                                                          totalTime(),
                                                          currentMemoryUse( 0 ),
                                                          maxMemoryUse( 0 ),
                                                          memoryUseByCategory(NUM_SPACE_PROFILER_CATEGORIES, 0)
{



    // If a filename have been provided then open the file
    if( logfileName != NULL ) {
        loggingEnabled = true;

        // open the file
        log.open( logfileName );

        // Make sure it actually opened
        dbAssert( log.fail() == false, "SpaceProfiler::SpaceProfiler() - log file failed to open. Given name was ", logfileName );
    }
}

// Destructor
SpaceProfiler::~SpaceProfiler() {

    // Close the log file if we are using it
    if( loggingEnabled == true ) {
        log.close();
    }
}
                                                           // destructor, close the log file if it is open

// Increase the memory use count
void SpaceProfiler::MemoryAllocated( Int32 size, SpaceProfilerCategory category ) {

    // Error checking
    dbAssert( category < NUM_SPACE_PROFILER_CATEGORIES, "SpaceProfiler::MemoryAllocated - passed an invalid category" );

    currentMemoryUse += size;                                       // keep track of the allocation
    maxMemoryUse = max( maxMemoryUse, currentMemoryUse );           // keep track of the max seen so far

    memoryUseByCategory[category] += size;                          // keep track of which category we are dealing with
}

// Decrease the memory use count
void SpaceProfiler::MemoryDeallocated( Int32 size, SpaceProfilerCategory category ) {

    // Error checking
    dbAssert( category < NUM_SPACE_PROFILER_CATEGORIES, "SpaceProfiler::MemoryDeallocated - passed an invalid category" );
    dbAssert( currentMemoryUse >= size, "SpaceProfiler::MemoryDeallocated - deallocated more memory than has allocated" );
    dbAssert( memoryUseByCategory[category] >= size, "SpaceProfiler::MemoryDeallocated - deallocating more memory for a category than has been allocated. Category is ", categoryNames[category] );

    currentMemoryUse -= size;                                       // keep track of the deallocation
    memoryUseByCategory[category] -= size;
}

// Keep track of the passing of frames
void SpaceProfiler::FrameDone() {
    numFramesCompleted++;                                           // keep track of number of frames completed

    totalTime += timer.GetElapsedTime();                            // keep track of total time

    timer.Restart();
}

// sends the stats to the debug string
void SpaceProfiler::PrintStatsToScreen( bool printCategories ) {
    dbPrintToScreen( "Memory use: ", currentMemoryUse );
    dbPrintToScreen( "Max use: ", maxMemoryUse );

    if( printCategories ) {
        for( int i = 0; i < NUM_SPACE_PROFILER_CATEGORIES; i++ ) {
            dbPrintToScreen( categoryNames[i], memoryUseByCategory[i] );
        }
    }
}

// sends the stats to the log file
void SpaceProfiler::PrintStatsToLog() {
    // Make sure we have a log file to print to
    dbAssert( loggingEnabled == true, "SpaceProfiler::PrintStatsToLog() - tried to print to log but loggingEnabled is false" );

    static bool printedHeader = false;
    const char delimitingCharacter = ',';

    // Print the header to the CSV file, but only once
    if( printedHeader == false ) {
        log << "Frame Number" << delimitingCharacter;
        log << "Frame Time" << delimitingCharacter;

        log << "Current Memory Use" << delimitingCharacter;

        for( int i = 0; i < NUM_SPACE_PROFILER_CATEGORIES; i++ ) {
            log << categoryNames[i] << delimitingCharacter;
        }

        log << "\n";

        printedHeader = true;
    }

    // Print the stats
    log << numFramesCompleted << delimitingCharacter;
    log << totalTime.AsSeconds() << delimitingCharacter;

    log << currentMemoryUse << delimitingCharacter;

    for( int i = 0; i < NUM_SPACE_PROFILER_CATEGORIES; i++ ) {
        log << memoryUseByCategory[i] << delimitingCharacter;
    }

    log << "\n";
}
