/*
 * This file implements all the functions
 * that need to be called when the program
 * starts
 *
 * Author:      Stuart Rudderham
 * Created:     March 26, 2012
 * Last Edited: December 11, 2012
 */

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "Startup.hpp"
#include "MediaCache/MediaCache.hpp"

#include "Math/Geometry/BaseShape.hpp"
#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"
#include "Math/Geometry/ConvexPolygon.hpp"
#include "Math/Geometry/LineSegment.hpp"

#include "Interfaces/GameObject.hpp"
#include "Interfaces/Movement/MoveableObject.hpp"
#include "Interfaces/Collision/CollidableObject.hpp"
#include "Interfaces/Collision/RotatableObject.hpp"

#include "DebugTools/ValueLogger.hpp"
#include "Profile/TimeProfiler.hpp"

#include "Managers/Movement/MovementManager.hpp"
#include "Managers/Collision/CollisionManager.hpp"
#include "Managers/Drawing/DrawingManager.hpp"
#include "Managers/Intelligence/IntelligenceManager.hpp"

#include "GameObjects/Characters/Character.hpp"
#include "GameObjects/Characters/Soldier.hpp"
#include "GameObjects/Characters/Zombie.hpp"
#include "GameObjects/AI/FieldOfView.hpp"
#include "GameObjects/AI/LineOfSight.hpp"
#include "GameObjects/MapObjects/StaticGeo.hpp"
#include "GameObjects/MapObjects/Scenery.hpp"
#include "GameObjects/MapObjects/SpawnPoint.hpp"

#include "GameObjects/Characters/Weapons/Guns/Interfaces/BaseGun.hpp"
#include "GameObjects/Characters/Weapons/Guns/Interfaces/Reloading/MagazineFed.hpp"
#include "GameObjects/Characters/Weapons/Guns/Interfaces/Shooting/FiresSlowBullets.hpp"
#include "GameObjects/Characters/Weapons/Guns/Specifics/Pistol.hpp"

#include "GameObjects/Projectiles/SlowBullet.hpp"

void PrintClassSizes() {

    // System Stuff
    dbPrintToConsole( "Basic"                                                                     );
    dbPrintToConsole( "\t""bool                           : ", sizeof( bool                     ) );
    dbPrintToConsole( "\t""char                           : ", sizeof( char                     ) );
    dbPrintToConsole( "\t""int                            : ", sizeof( int                      ) );
    dbPrintToConsole( "\t""long                           : ", sizeof( long                     ) );
    dbPrintToConsole( "\t""float                          : ", sizeof( float                    ) );
    dbPrintToConsole( "\t""char*                          : ", sizeof( char*                    ) );
    dbPrintToConsole( "\t""int*                           : ", sizeof( int*                     ) );
    dbPrintToConsole( "\t""void*                          : ", sizeof( void*                    ) );
    dbPrintToConsole( "\t""vector<GameObject*>            : ", sizeof( std::vector<GameObject*> ) );
    dbPrintToConsole( "" );

    // SFML Stuff
    dbPrintToConsole( "SFML"                                                                );
    dbPrintToConsole( "\t""Vector2f                       : ", sizeof( sf::Vector2f       ) );
    dbPrintToConsole( "\t""Sprite                         : ", sizeof( sf::Sprite         ) );
    dbPrintToConsole( "\t""RectangleShape                 : ", sizeof( sf::RectangleShape ) );
    dbPrintToConsole( "\t""CircleShape                    : ", sizeof( sf::CircleShape    ) );
    dbPrintToConsole( "\t""ConvexShape                    : ", sizeof( sf::ConvexShape    ) );
    dbPrintToConsole( "\t""Vertex                         : ", sizeof( sf::Vertex         ) );
    dbPrintToConsole( "\t""IntRect                        : ", sizeof( sf::IntRect        ) );
    dbPrintToConsole( "\t""Drawable                       : ", sizeof( sf::Drawable       ) );
    dbPrintToConsole( "\t""Transformable                  : ", sizeof( sf::Transformable  ) );
    dbPrintToConsole( "\t""Int8                           : ", sizeof( sf::Int8           ) );
    dbPrintToConsole( "\t""Int16                          : ", sizeof( sf::Int16          ) );
    dbPrintToConsole( "\t""Int32                          : ", sizeof( sf::Int32          ) );
    dbPrintToConsole( "\t""Int64                          : ", sizeof( sf::Int64          ) );
    dbPrintToConsole( "" );

    // Math
    dbPrintToConsole( "Math"                                                           );
    dbPrintToConsole( "\t""BaseShape                      : ", sizeof( BaseShape     ) );
    dbPrintToConsole( "\t""AABB                           : ", sizeof( AABB          ) );
    dbPrintToConsole( "\t""Circle                         : ", sizeof( Circle        ) );
    dbPrintToConsole( "\t""ConvexPolygon                  : ", sizeof( ConvexPolygon ) );
    dbPrintToConsole( "\t""LineSegment                    : ", sizeof( LineSegment   ) );
    dbPrintToConsole( "\t""Point2f                        : ", sizeof( Point2f       ) );
    dbPrintToConsole( "" );

    // Interfaces
    dbPrintToConsole( "Interfaces"                                                         );
    dbPrintToConsole( "\t""GameObject                     : ", sizeof( GameObject        ) );
    dbPrintToConsole( "\t""MoveableObject                 : ", sizeof( MoveableObject    ) );
    dbPrintToConsole( "\t""DrawableObject                 : ", sizeof( DrawableObject    ) );
    dbPrintToConsole( "\t""DrawableSprite                 : ", sizeof( DrawableSprite    ) );
    dbPrintToConsole( "\t""DrawableShape                  : ", sizeof( DrawableShape     ) );
    dbPrintToConsole( "\t""CollidableObject               : ", sizeof( CollidableObject  ) );
    dbPrintToConsole( "\t""RotatableObject                : ", sizeof( RotatableObject   ) );
    dbPrintToConsole( "\t""IntelligentObject              : ", sizeof( IntelligentObject ) );
    dbPrintToConsole( "\t""MortalObject                   : ", sizeof( MortalObject      ) );
    dbPrintToConsole( "" );

    // Managers
    dbPrintToConsole( "Managers"                                                                        );
    dbPrintToConsole( "\t""CollisionManager               : ", sizeof( CollisionManager               ) );
    dbPrintToConsole( "\t""DrawingManager                 : ", sizeof( DrawingManager                 ) );
    dbPrintToConsole( "\t""MovementManager                : ", sizeof( MovementManager                ) );
    dbPrintToConsole( "\t""IntelligenceManager            : ", sizeof( IntelligenceManager            ) );
    dbPrintToConsole( "\t""ArrayOfGameObjects<GameObject> : ", sizeof( ArrayOfGameObjects<GameObject> ) );
    dbPrintToConsole( "\t""ArrayOfCollidableObjects       : ", sizeof( ArrayOfCollidableObjects       ) );
    dbPrintToConsole( "" );

    // Logging
    dbPrintToConsole( "Logging"                                                         );
    dbPrintToConsole( "\t""LoggableObject                 : ", sizeof( LoggableObject ) );
    dbPrintToConsole( "\t""ValueLogger                    : ", sizeof( ValueLogger    ) );
    dbPrintToConsole( "\t""TimeProfiler                   : ", sizeof( TimeProfiler   ) );
    dbPrintToConsole( "" );

    // GameObjects
    dbPrintToConsole( "GameObjects"                                                         );
    dbPrintToConsole( "\t""Character                      : ", sizeof( Character          ) );
    dbPrintToConsole( "\t""CharacterConstData             : ", sizeof( CharacterConstData ) );
    dbPrintToConsole( "\t""Soldier                        : ", sizeof( Soldier            ) );
    dbPrintToConsole( "\t""SoldierConstData               : ", sizeof( SoldierConstData   ) );
    dbPrintToConsole( "\t""Zombie                         : ", sizeof( Zombie             ) );
    dbPrintToConsole( "\t""ZombieConstData                : ", sizeof( ZombieConstData    ) );
    dbPrintToConsole( "\t""FieldOfView                    : ", sizeof( FieldOfView        ) );
    dbPrintToConsole( "\t""LineOfSight                    : ", sizeof( LineOfSight        ) );
    dbPrintToConsole( "\t""StaticGeo                      : ", sizeof( StaticGeo          ) );
    dbPrintToConsole( "\t""Scenery                        : ", sizeof( Scenery            ) );
    dbPrintToConsole( "\t""SpawnPoint                     : ", sizeof( SpawnPoint         ) );
    dbPrintToConsole( "\t""SlowBullet                     : ", sizeof( SlowBullet         ) );
    dbPrintToConsole( "" );

    // Weapons
    dbPrintToConsole( "Weapons"                                                                      );
    dbPrintToConsole( "\t""BaseGun                        : ", sizeof( BaseGun                     ) );
    dbPrintToConsole( "\t""BaseGun::ConstData             : ", sizeof( BaseGun::ConstData          ) );
    dbPrintToConsole( "\t""MagazineFed                    : ", sizeof( MagazineFed                 ) );
    dbPrintToConsole( "\t""MagazineFed::ConstData         : ", sizeof( MagazineFed::ConstData      ) );
    dbPrintToConsole( "\t""FiresSlowBullets               : ", sizeof( FiresSlowBullets            ) );
    dbPrintToConsole( "\t""FiresSlowBullets::ConstData    : ", sizeof( FiresSlowBullets::ConstData ) );
    dbPrintToConsole( "\t""Pistol                         : ", sizeof( Pistol                      ) );
    dbPrintToConsole( "\t""Pistol::ConstData              : ", sizeof( Pistol::ConstData           ) );
    dbPrintToConsole( "" );
}
