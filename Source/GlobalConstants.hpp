/*
 * This header defines all the global
 * constants for the project.
 *
 * Author:      Stuart Rudderham
 * Created:     April 4, 2012
 * Last Edited: January 10, 2013
 */

#ifndef GLOBALCONSTANTS_HPP_INCLUDED
#define GLOBALCONSTANTS_HPP_INCLUDED

// Macro to tell what compiler you are using
#if defined( _MSC_VER )
    #define USING_GCC   0
    #define USING_VS    1

#elif defined( __GNUC__ )
    #define USING_GCC   1
    #define USING_VS    0
#endif

// Macros to define what mode we're compiling in
#if defined( DEBUG )
    #define DEBUG_MODE                  1
    #define RELEASE_MODE                0

    #define ENABLE_ASSERTS              1
    #define ENABLE_WARNINGS             1
    #define ENABLE_LOGGING              1
    #define ENABLE_CONSOLE_PRINTING     1
    #define ENABLE_SCREEN_PRINTING      1
    #define ENABLE_CHECKED_CAST         1
    #define ENABLE_MEMORY_CLEARING      1
    #define USE_ARENA_ALLOCATOR         1

#elif defined( RELEASE )
    #define DEBUG_MODE                  0
    #define RELEASE_MODE                1

    #define ENABLE_ASSERTS              0
    #define ENABLE_WARNINGS             0
    #define ENABLE_LOGGING              1
    #define ENABLE_CONSOLE_PRINTING     1
    #define ENABLE_SCREEN_PRINTING      1
    #define ENABLE_CHECKED_CAST         0
    #define ENABLE_MEMORY_CLEARING      1
    #define USE_ARENA_ALLOCATOR         1

#else
    #error "Please specify either Debug (-DDEBUG) or Release (-DRELEASE) mode"

#endif

// Define common macro to print out a function's name
#if USING_GCC
    #define FUNCTION_NAME __PRETTY_FUNCTION__
#elif USING_VS
    #define FUNCTION_NAME __FUNCSIG__
#endif

// Macros to define functionality
#define USE_MASTERFILE 0

// Macros to easily make a class non-copyable or non-assignable
#define NO_COPY(Type)                       Type( const Type& original );
#define NO_ASSIGN(Type)                     Type& operator= ( const Type &original );
#define TEMPLATE_NO_COPY(Type)              Type( const Type<T>& original );
#define TEMPLATE_NO_ASSIGN(Type)            Type& operator= ( const Type<T> &original );

#define NO_COPY_OR_ASSIGN(Type)             NO_COPY(Type);          NO_ASSIGN(Type);
#define TEMPLATE_NO_COPY_OR_ASSIGN(Type)    TEMPLATE_NO_COPY(Type); TEMPLATE_NO_ASSIGN(Type);

// Includes
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Config.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>
#include <limits>

// Forward Declarations
class TimeProfiler;
template<typename T> class MediaCache;

namespace sf {
    class Texture;
    class SoundBuffer;
    class Font;
}

// Typedefs
typedef sf::Vector2f Point2f;                                       // a point in the 2D plane with floating point values
typedef sf::Vector2i Point2i;                                       // a point in the 2D plane with integer values
typedef sf::Vector2f Pair;                                          // a pair of floating point numbers
typedef unsigned char DrawLevel;                                    // used to decide what order things are drawn in
typedef unsigned char CollisionPriority;                            // used to resolve collisions
typedef const char* ExternalFile;                                   // the path to an external file used by the program
typedef MediaCache<sf::Texture>     TextureCache;                   // The class that caches loaded textures
typedef MediaCache<sf::SoundBuffer> SoundCache;                     // The class that caches loaded sounds
typedef MediaCache<sf::Font>        FontCache;                      // The class that caches loaded fonts

// This enum defines the different types the CollidableObject can actually be
// It should really be located in CollidableObject.hpp but due to circular
// dependency issues with CollisionManager and ArrayOfCollidableObject it is
// just easier to put it here
namespace CollidableObjectType {
    enum Enum : unsigned char {
        Character,
        StaticGeo,
        Projectile,
        FieldOfView,
        LineOfSight,
        MiniMap,
        Max
    };
};

// Global Constants and Variables
namespace MathConstants {
    const float PI                  = 3.14159265358979323846f;
    const float PI_2                = PI / 2.0f;

    const float EpsilonAroundOne    = 0.0001f;
    const float EpsilonAroundZero   = 0.0001f;

    const float BigFloat            = std::numeric_limits<float>::infinity();
    const sf::Vector2f BigVector    = sf::Vector2f( BigFloat, BigFloat );
    const int BigInt                = std::numeric_limits<int>::max();

    const sf::Vector2f ZeroVector   = sf::Vector2f( 0.0f, 0.0f );
    const Point2f Origin            = Point2f( 0.0f, 0.0f );
    const sf::Vector2f xAxis        = sf::Vector2f( 1.0f, 0.0f );
    const sf::Vector2f yAxis        = sf::Vector2f( 0.0f, 1.0f );
}

namespace WindowOptions {
    extern const char GameWindowName[];             // The title of the main game window
    extern const char DebugWindowName[];            // The title of the debugging/profiling window

    extern const bool UseVSync;                     // True if we want to enable v-sync

    extern const bool FullScreen;                   // True if we want to make the main game window full screen

    extern const unsigned int AntiAliasingLevel;    // The anti-aliasing level for the windows

    extern const sf::Vector2u GameWindowSize;       // The dimensions of the main game window
    extern const sf::Vector2u DebugWindowSize;      // The dimensions of the debugging/profiling window
}

namespace DebugOptions {
    extern const sf::Color TextColor;               // The color of the graphical debug string that gets drawn to the screen
    extern const unsigned int TextSize;             // The size of the graphical debug string that gets drawn to the screen

    extern const unsigned int TextSpaceWidth;       // How wide the space for drawing the debug text is

    const unsigned int LoggerBufferSize = 60;       // Sets how big the buffer size is for the loggers
                                                    // We declare it in the this file so that ValueLogger can use it
                                                    // Otherwise get "field initializer is not constant" error

    extern const sf::Vector2f GraphSize;            // The size of the profiling graphs

    extern const bool DrawLineGraph;                // True if we want the profiling graphs to be line graphs, false if we want them to be bar graphs
}

namespace Drawing {
    extern const bool DrawCollisionOutline;         // True if we want to draw all collision outlines (used for testing)

    extern const sf::Color CollisionOutlineColor;   // The color we want to draw the collision outlines with
    extern const sf::Color BoundingBoxColor;        // The color we want to draw the bounding boxes with

    extern const bool DrawSpawnPoints;              // True if we want to draw the location of SpawnPoints (used for testing)

    extern const bool DrawCharacterPicture;         // True if we want the Character's sprite to be drawn (used for testing)

    extern const bool UseImageAntiAliasing;         // If we want all sprites/textures to be drawn with anti-aliasing

    namespace Level {
        extern const DrawLevel Background;          // The different levels that something can be drawn at
        extern const DrawLevel Midground;
        extern const DrawLevel Foreground;
    }
}

namespace Physics {
    extern const bool EnableStressTest;             // True if we want all Characters to spawn in the same spot

    extern const unsigned int IterationsPerFrame;   // How many times the physics engine will move objects and then resolve collisions
                                                    // each frame. More iterations provide better resolution

    extern const bool UseFixedTimeStep;             // True if we want the physics timestep to be independent of frame rate

    extern float DeltaT;                            // The physics timestep, in percentage of 1 second (e.g. 0.5 -> 1/2 a second has passed)
    extern const float FixedDeltaT;                 // A constant value for the timestep based on the target framerate (e.g. 60 FPS -> 0.06

    extern const bool ClampDeltaT;                  // True if we want to clamp the physics timestep to a given range

    extern const float MaxDeltaT;                   // The minimum and maximum values for the timestep if we are clamping it
    extern const float MinDeltaT;

    namespace Priority {
        extern const CollisionPriority Immovable;   // The collision priority for any immovable object (e.g. StaticGeo)
        extern const CollisionPriority Phantom;     // The collision priority for any phantom object (e.g. FieldOfView)
        extern const CollisionPriority Character;   // The collision priority for Characters
        extern const CollisionPriority SlowBullet;  // The collision priority for SlowBullets
    }
}

namespace Game {
    extern sf::Clock GameTimer;                     // The global game timer. Starts timing when the game starts and never stops
    extern sf::Time CurrentTime;                    // The current time. Updated only at the end of each frame, so everything sees the same time
    extern sf::Time DeltaT;                         // How long the last frame took to complete

    extern const unsigned int FPS;                  // The target framerate for the game

    extern const sf::Time IntentionalDelay;         // The amount of time to pause for if we want an intentional delay. Useful for checking if things are framerate dependent

    extern const unsigned int NumCharacters;        // How many characters we want to spawn                                   (used for testing)
    extern const bool SpawnZombies;                 // True if we want to spawn Zombies                                       (used for testing)
    extern const bool SpawnSoldiers;                // True if we want to spawn Soldiers                                      (used for testing)
    extern const bool MakeSoldiersZombies;          // True if we want to spawned Soldiers and Zombies to be on the same team (used for testing)

    extern const sf::Time CharacterRespawnTime;     // How long we wait before respawning dead Characters
    extern const sf::Time DeadBodyDespawnTime;      // How long we wait before deleting dead bodies

    namespace Camera {
        extern const sf::Vector2f ViewportSize;     // How much area the camera can see
        extern const float ZoomSpeed;               // How fast the camera zooms
    }

    extern const float MiniMapRange;                // the radius of the collision outline of the minimap
}

namespace HUDOptions {
    namespace MiniMap {
        extern const float Size;                    // the radius of the mini-map when drawn to the screen
        extern const Point2f Position;              // the position it's drawn at
        extern const sf::Color Color;               // the background color
        extern const float BezelThickness;          // how thick the outline is

        extern const float CharacterDotSize;        // the radius of the dots representing each Character
        extern const sf::Color SelfColor;           // the color of the dot representing the main Character
        extern const sf::Color AllyColor;           // the color of the dot representing allied Characters
        extern const sf::Color EnemyColor;          // the color of the dot representing enemy Characters
    }

    namespace GunPicture {
        extern const sf::Vector2f Size;             // the size the the picture of the current Gun
        extern const Point2f Position;              // where the picture is drawn
        extern const sf::Color BackgroundColor;     // the color of the background
    }
};

// Declared as references so that we don't have to include TimeProfiler.hpp in this file
// They reference static variables created in GlobalConstants.cpp
namespace Profile {
    extern TimeProfiler&                    GlobalFramerate;
    extern TimeProfiler&                    FieldOfViewCollision;
    extern TimeProfiler&                    LineOfSightCollision;
    extern TimeProfiler&                    ControllerUpdate;
    extern TimeProfiler&                    PhysicsUpdate;
    extern TimeProfiler&                    Drawing;
    extern TimeProfiler&                    Displaying;
    extern TimeProfiler&                    Cleanup;
}

#endif // GLOBALCONSTANTS_HPP_INCLUDED
