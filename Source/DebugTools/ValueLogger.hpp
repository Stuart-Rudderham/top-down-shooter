/*
 * This header defines a circular buffer for storing
 * numbers. When a new value is added an old value
 * is discarded. Also provided stat functions (e.g.
 * to get the average of all the numbers)
 *
 * Author:      Stuart Rudderham
 * Created:     August 21, 2012
 * Last Edited: November 22, 2012
 */

#ifndef VALUELOGGER_HPP_INCLUDED
#define VALUELOGGER_HPP_INCLUDED

#include "DebugTools/Debug.hpp"
#include <SFML/Graphics/RenderTarget.hpp>
#include <deque>

// This enum defines the different types of ValueLoggers
// that can exist. This lets us selectively turn off
// drawing certain graphs. The graphs are also ordered
// by LogType when drawn to the screen
namespace LogType {
    enum Enum : unsigned char {
        Timer,
        Manager,
        Factory,
        Pool,
        Max
    };
};

class ValueLogger {
    private:
        static const unsigned int MaxBufferSize = DebugOptions::LoggerBufferSize;

        std::deque<sf::Int64> buffer;                                               // the buffer

        std::string name;                                                           // a decription of what the logger is logging (e.g. "Global Framerate")
        sf::Int64 graphScale;                                                       // the max value displayable in the graph

        mutable bool updatedSinceLastGraphed;                                       // used to confirm that the logger is actually logging values
                                                                                    // makes sure I don't forget to update any loggers

        unsigned int numValuesAdded;                                                // the total number of values that have been added to the buffer

        float average;                                                              // Stat variables. Average is an arithmetic average
        float variance;

        sf::Int64 summedValues;                                                     // these are intermediate values used to calculate the stats
        sf::Int64 summedSquaredValues;

        sf::Int64 minimumValue;                                                     // min/max value in the buffer
        sf::Int64 maximumValue;

        LogType::Enum type;                                                         // the type of the ValueLogger

        void DrawLineGraph( sf::RenderTarget& Target, Point2f topLeft, sf::Vector2f size ) const;
        void DrawBarGraph ( sf::RenderTarget& Target, Point2f topLeft, sf::Vector2f size ) const;

    public:
        ValueLogger( const char* loggerName,                                        // ctor
                     LogType::Enum logType,
                     sf::Int64 scale );

        void AddNewValue( sf::Int64 newValue );                                     // add a new value to the logger


        // Draw a graph of the values in the logger
        void DrawGraph( sf::RenderTarget& Target, Point2f topLeft, sf::Vector2f size ) const;


        inline const std::string& GetName() const { return name; }

        // Get a value from the buffer
        inline sf::Int64 GetValueAt( unsigned int i ) const {
            dbAssert( i < buffer.size(), "Tried to get value at invalid index" );
            return buffer[i];
        }

        // Get the type
        LogType::Enum GetLogType() const { return type; }

        // Getters for the stats
        inline sf::Int64    GetMostRecentValue() const { return buffer.empty() ? std::numeric_limits<sf::Int64>::quiet_NaN() : buffer.back(); }
        inline unsigned int GetNumValuesAdded()  const { return numValuesAdded; }
        inline float        GetAverage()         const { return average;        }
        inline float        GetVariance()        const { return variance;       }
        inline sf::Int64    GetMinimumValue()    const { return minimumValue;   }
        inline sf::Int64    GetMaximumValue()    const { return maximumValue;   }
};

#endif // VALUELOGGER_HPP_INCLUDED
