/*
 * This header implements a circular buffer for storing
 * numbers. When a new value is added an old value
 * is discarded. Also provided stat functions (e.g.
 * to get the average of all the numbers)
 *
 * Author:      Stuart Rudderham
 * Created:     August 21, 2012
 * Last Edited: November 22, 2012
 */

#include "DebugTools/ValueLogger.hpp"
#include "Managers/Logging/LoggingManager.hpp"
#include "MediaCache/MediaCache.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>

#include <algorithm>
#include <sstream>
using namespace sf;

static Color const &  graphDataColor            = Color::Green;
static Color const &  overflowGraphDataColor    = Color::Red;
static Color const &  graphBackgroundColor      = Color::Black;


// Constructor
ValueLogger::ValueLogger( const char* loggerName, LogType::Enum logType, Int64 scale )
    : name( loggerName ),
      graphScale( scale ),
      updatedSinceLastGraphed( true ),
      numValuesAdded( 0u ),
      average( std::numeric_limits<float>::quiet_NaN() ),
      variance( std::numeric_limits<float>::quiet_NaN() ),
      summedValues( 0 ),
      summedSquaredValues( 0 ),
      minimumValue( std::numeric_limits<Int64>::max() ),
      maximumValue( std::numeric_limits<Int64>::min() ),
      type( logType )
{
    // Add the ValueLogger to the LoggingManger
    LoggingManager::Get().AddValueLogger( this );
}


// add a new value to the logger
void ValueLogger::AddNewValue( Int64 newValue ) {

    // update bookkeeping
    numValuesAdded++;
    updatedSinceLastGraphed = true;

    // Update the min/max values if necessary
    minimumValue = std::min( minimumValue, newValue );
    maximumValue = std::max( maximumValue, newValue );

    // Add the new value to the buffer
    buffer.push_back( newValue );

    // and update the intermediate calculations with the new value
    summedValues += newValue;
    summedSquaredValues += newValue * newValue;

    // if the buffer is full then we have to remove an old value
    if( buffer.size() > MaxBufferSize ) {
        Int64 oldValue = buffer.front();

        summedValues -= oldValue;
        summedSquaredValues -= oldValue * oldValue;

        buffer.pop_front();

        // If the element we removed was the max/min element then we need to find the new max/min
        if( oldValue == minimumValue ) {
            minimumValue = *std::min_element( buffer.begin(), buffer.end() );
        }

        if( oldValue == maximumValue ) {
            maximumValue = *std::max_element( buffer.begin(), buffer.end() );
        }
    }

    // Recalculate stats with new value
    float numElements = static_cast<float>( buffer.size() );            // cast to float to make sure division is always using floating point, not ints

    average = summedValues / numElements;

    variance = ( summedSquaredValues - ( (summedValues * summedValues) / numElements ) ) / numElements;

}


// Draw a line graph plotting the values of the logger within the given area
// This only draws the graph, no title, and the graph takes up the entire area
void ValueLogger::DrawLineGraph( RenderTarget& Target, Point2f topLeft, Vector2f size ) const {

    // Setup the graph
    VertexArray graphPoints( LinesStrip );

    // for each value logged
    for( unsigned int i = 0; i < buffer.size(); ++i ) {

        Int64 cappedValue = std::min( buffer[i], graphScale );                                              // cap the value so we don't draw off the graph
        Color pointColor = ( buffer[i] > graphScale ) ? overflowGraphDataColor : graphDataColor;            // and change the line color if the value is too big

        Point2f pointPosition;                                                                              // calculate the position of the point
        pointPosition.x = i * size.x / (MaxBufferSize - 1);
        pointPosition.y = size.y - ( std::min( cappedValue, graphScale ) * size.y / graphScale );

        graphPoints.append( Vertex( topLeft + pointPosition, pointColor ) );
    }

    Target.draw( graphPoints );                                                                             // draw all the points
}

// Draw a bar graph plotting the values of the logger within the given area
// This only draws the graph, no title, and the graph takes up the entire area
void ValueLogger::DrawBarGraph( RenderTarget& Target, Point2f topLeft, Vector2f size ) const {

    // Setup the graph
    VertexArray graphPoints( Quads );

    // for each value logged
    for( unsigned int i = 0; i < buffer.size(); ++i ) {

        Int64 cappedValue = std::min( buffer[i], graphScale );                                              // cap the value so we don't draw off the graph
        Color barColor = ( buffer[i] > graphScale ) ? overflowGraphDataColor : graphDataColor;              // and change the bar color if the value is too big

        Vector2f barSize;
        barSize.x = size.x / MaxBufferSize;
        barSize.y = cappedValue * size.y / graphScale;

        Point2f barPosition;
        barPosition.x = topLeft.x + (barSize.x * i);
        barPosition.y = topLeft.y + size.y;

        graphPoints.append( Vertex( Point2f( barPosition.x,             barPosition.y             ), barColor ) );
        graphPoints.append( Vertex( Point2f( barPosition.x,             barPosition.y - barSize.y ), barColor ) );
        graphPoints.append( Vertex( Point2f( barPosition.x + barSize.x, barPosition.y - barSize.y ), barColor ) );
        graphPoints.append( Vertex( Point2f( barPosition.x + barSize.x, barPosition.y             ), barColor ) );
    }

    Target.draw( graphPoints );                                                                             // draw all the points
}

void ValueLogger::DrawGraph( RenderTarget& Target, Point2f topLeft, Vector2f size ) const {

    dbWarn( updatedSinceLastGraphed == true, "Logger hasn't been updated since it was last drawn. Name is ", name );

    // Variables to control size and spacing
    float graphBezel = 10.0f;
    float textBezel = 3.0f;
    unsigned int textSize = 15;


    // GRAPH OUTLINE
    RectangleShape graphOutline;

    graphOutline.setPosition( topLeft );
    graphOutline.setSize( Vector2f( size.x, size.y ) );
    graphOutline.setFillColor( Color::White );
    graphOutline.setOutlineColor( Color::Black );
    graphOutline.setOutlineThickness( -1.0f );

    Target.draw( graphOutline );


    // TITLE
    Text graphTitle( name, FontCache::Get().Load( DebugTextFont ), textSize );
    FloatRect graphTitleBoundingBox = graphTitle.getLocalBounds();

    graphTitle.setOrigin( graphTitleBoundingBox.width / 2.0f, 0 );
    graphTitle.setColor( Color::Black );
    graphTitle.setPosition( topLeft.x - graphTitleBoundingBox.left + (size.x / 2.0f), topLeft.y - graphTitleBoundingBox.top + textBezel );

    Target.draw( graphTitle );


    // GRAPH BACKGROUND
    RectangleShape graphBackground;

    graphBackground.setPosition( Vector2f( topLeft.x + graphBezel, topLeft.y + graphTitleBoundingBox.height + (2 * textBezel) ) );
    graphBackground.setSize( Vector2f( size.x - (2 * graphBezel), size.y - graphBezel - ( graphBackground.getPosition().y - topLeft.y ) ) );
    graphBackground.setFillColor( graphBackgroundColor );
    graphBackground.setOutlineThickness( 0.0f );

    Target.draw( graphBackground );


    // GRAPH VALUES
    if( DebugOptions::DrawLineGraph == true ) {
        DrawLineGraph( Target, graphBackground.getPosition(), graphBackground.getSize() );
    } else {
        DrawBarGraph( Target, graphBackground.getPosition(), graphBackground.getSize() );
    }

    // CURRENT VALUE
    std::stringstream converter;
    converter << GetMostRecentValue();

    Text valueText( converter.str(), FontCache::Get().Load( DebugTextFont ), textSize * 2 );
    FloatRect valueTextBoundingBox = valueText.getLocalBounds();

    valueText.setOrigin( valueTextBoundingBox.width / 2.0f, valueTextBoundingBox.height / 2.0f );
    valueText.setColor( Color::White );
    valueText.setPosition( graphBackground.getPosition() + graphBackground.getSize() / 2.0f );

    Target.draw( valueText );


    updatedSinceLastGraphed = false;
}
