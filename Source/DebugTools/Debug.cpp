/*
 * This file implements of all the variables, functions
 * and macros that are used for debugging purposes
 *
 * Author:      Stuart Rudderham
 * Created:     January 29, 2012
 * Last Edited: January 30, 2013
 */

#include "DebugTools/Debug.hpp"
#include "MediaCache/MediaCache.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <limits>

using namespace std;
using namespace sf;

static Text         debugText;                                                  // the graphical text used to draw the debug string

stringstream        debugString;                                                // the string that all debugging text is added to
                                                                                // it can't be static because the templated Print functions
                                                                                // need to be able to write to it

void Debug::DrawDebugText( RenderTarget& Target, Vector2f pos )
{
    // These functions check if the value we're setting is new
    // so it's OK that we keep setting them to the same value
    debugText.setColor( DebugOptions::TextColor );                              // set the color of the text
    debugText.setFont( FontCache::Get().Load( DebugTextFont ) );                      // set the font of the text
    debugText.setCharacterSize( DebugOptions::TextSize );                       // set the size of the text

    debugText.setString( debugString.str() );                                   // set the text string to display the debug string
    debugText.setPosition( pos );                                               // set the position of the text

    // get the bounding rectangle of the text
    FloatRect boundary = debugText.getGlobalBounds();
    boundary.width += boundary.left - pos.x;                                     // getting the bounding rectangle for
    boundary.height += boundary.top - pos.y;                                     // text can be strange, this just makes
    boundary.left = pos.x;                                                       // sure that the top-left corner of the rectangle
    boundary.top = pos.y;                                                        // is where we actually want it to be

    RectangleShape debugTextBackground;
    debugTextBackground.setPosition( 0, 0 );
    debugTextBackground.setSize( Vector2f(DebugOptions::TextSpaceWidth, boundary.height) );
    debugTextBackground.setFillColor( Color::White );
    debugTextBackground.setOutlineColor( Color::Black );
    debugTextBackground.setOutlineThickness( 1.0f );

    Target.draw( debugTextBackground );                                         // draw the background to make sure the text can be read
    Target.draw( debugText );                                                   // draw the text

    debugString.str( "" );                                                      // clear the debug string
}
