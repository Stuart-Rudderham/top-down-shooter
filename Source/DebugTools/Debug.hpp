/*
 * This header defines all the variables, functions, and macros that
 * are used for debugging purposes
 *
 * Author:      Stuart Rudderham
 * Created:     January 29, 2012
 * Last Edited: January 30, 2013
 */

#ifndef DEBUGTOOLS_HPP_INCLUDED
#define DEBUGTOOLS_HPP_INCLUDED

#include "UpgradesToSFML/Printing.hpp"

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Color.hpp>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>

// Macros to turn off debugging in release

#if ENABLE_ASSERTS
    #define dbAssert( condition, message, ... )     Debug::Test( condition, true,  __FILE__, __LINE__, FUNCTION_NAME, #condition, message, ##__VA_ARGS__ )
#else
    #define dbAssert( condition, message, ... )
#endif


#if ENABLE_WARNINGS
    #define dbWarn( condition, message, ... )       Debug::Test( condition, false, __FILE__, __LINE__, FUNCTION_NAME, #condition, message, ##__VA_ARGS__ )
#else
    #define dbWarn( condition, message, ... )
#endif


#if ENABLE_CONSOLE_PRINTING
    #define dbPrintToConsole( message, ... )        Debug::Print( std::cerr, message, ##__VA_ARGS__ )
#else
    #define dbPrintToConsole( message, ... )
#endif


#if ENABLE_SCREEN_PRINTING
    #define dbPrintToScreen( message, ... )         Debug::Print( debugString, message, ##__VA_ARGS__ )
    #define dbDrawDebugText( target, ... )          Debug::DrawDebugText( target, ##__VA_ARGS__ )
#else
    #define dbPrintToScreen( message, ... )
    #define dbDrawDebugText( target, ... )
#endif

#define dbCrash( message, ... )                     Debug::Test( false, true, __FILE__, __LINE__, FUNCTION_NAME, "Crashing", message, ##__VA_ARGS__ )
#define dbPause( message )                          Debug::Pause( message );

extern std::stringstream debugString;

namespace Debug
{
    // A function to print string to an output stream
    inline void Print( std::ostream& os )                               // Base case
    {
        os << std::endl;                                                // only print out a newline
    }

    template<typename T, typename... Args>
    inline void Print( std::ostream& os, T& value, Args&&... args )     // Recursive case
    {
        os << std::noskipws;                                            // don't skip leading whitespace
        os << std::fixed << std::setprecision( 2 );             		// print out 2 digits after the decimal place
        os << std::boolalpha;                                           // print True/False instead of 1/0

        os << value;
        Print( os, args... );
    }

    // A function that inserts a breakpoint into the program
    inline void Pause( const char* message )
    {
        Debug::Print( std::cerr, "PAUSE - ", message );

        // Add a breakpoint
        #if USING_GCC
            __asm__( "int $0x3" );
        #elif USING_VS
            __asm{ int 3 }
        #endif
    }

    // A function to crash the program with hooks for a debugger
    inline void Crash()
    {
        Debug::Pause( "CRASHING" );     // Add a breakpoint
        exit( 1 );                      // Then DIE
    }

    // A custom assert function. Triggers if the condition is false
    // Shouldn't really be called directly, use the macro dbAssert or
    // dbWarn to automatically fill out the correct file, line number, etc...
    template<typename... Args>
    void Test( bool condition, bool crashIfFail, const char* file, int line, const char* function, const char* conditionString, const char* message, Args&&... args )
    {
        // If the assert triggers print out an error message
        if( condition == false ) {
            Debug::Print( std::cerr, "ASSERT" );
            Debug::Print( std::cerr, "    Message:   ", message, args... );
            Debug::Print( std::cerr, "    File:      ", file             );
            Debug::Print( std::cerr, "    Line:      ", line             );
            Debug::Print( std::cerr, "    Function:  ", function         );
            Debug::Print( std::cerr, "    Condition: ", conditionString  );
            Debug::Print( std::cerr, "" );

            // Then crash the program (if desired)
            if( crashIfFail ) {
                Debug::Crash();
            }
        }
    }

    // A function to draw the debug text onto the screen
    void DrawDebugText( sf::RenderTarget& Target, sf::Vector2f pos = sf::Vector2f(0, 0) );
}

// A casting function that does a more expensive but safer dynamic_cast
// in debug mode and a static_cast in release mode
// Idea from here -> http://aegisknight.livejournal.com/101698.html

#if ENABLE_CHECKED_CAST
template<typename ToType, typename FromType>
ToType checked_cast( FromType variable )
{
    ToType testCast = dynamic_cast<ToType>( variable );                                                 // do the dynamic_cast
    dbAssert( testCast != nullptr, "checked_cast failed to convert to ", typeid(ToType).name() );       // make sure it worked
    return testCast;                                                                                    // return the casted object
}
#else
    #define checked_cast static_cast                                                                    // just replace checked_cast with static_cast
#endif


#endif // DEBUGTOOLS_HPP_INCLUDED
