/*
 * This file implements an abstract
 * pool class that is used to
 * hold and recycle objects rather
 * than creating and deleting them
 * repeatedly
 *
 * Author:      Stuart Rudderham
 * Created:     October 10, 2012
 * Last Edited: Decembet 16, 2012
 */

#include <algorithm>
#include <cstdlib>
#include <cstring>

// Constructor
template<typename T, unsigned int N>
ObjectPool<T, N>::ObjectPool( const std::string& poolName ) : factory( poolName + " Factory" ),
                                                              activeObjectLogger( poolName.c_str(), LogType::Pool, N )
{
}


// Destructor
template<typename T, unsigned int N>
ObjectPool<T, N>::~ObjectPool() {

    // Make sure we have no active objects
    if( HaveActiveObjects() == true ) {
        dbCrash( "Leaking memory in ", activeObjectLogger.GetName().c_str() );
    }

    // Make sure all constructed objects (both active and inactive) are destructed
    DestroyAllObjects();
}


// returns a memory address where an object of type T already exists but is unused
template<typename T, unsigned int N>
T* ObjectPool<T, N>::GetObject() {

    T* object;

    // If there are no inactive objects then we have to make a new one using the default constructor
    if( inactiveObjects.empty() ) {
        object = new( factory.GetNewObjectAddress() ) T();

    // Otherwise grab the already constructed object with the lowest address
    } else {
        object = inactiveObjects.top();
        inactiveObjects.pop();
    }

    // Add the address to the list of active, construted objects
    activeObjects.insert( object );

    return object;
}

// Marks the address as unused, but does *not* destruct the object
template<typename T, unsigned int N>
void ObjectPool<T, N>::ReturnObject( T* object ) {

    // If address is nullptr then just exit
    if( object == nullptr ) {
        dbPrintToConsole( "Just tried to return a nullptr pointer to the pool. Might want to look into why. Pool is ", activeObjectLogger.GetName().c_str() );
        return;
    }

    // Make sure object belongs to this pool
    dbAssert( factory.Created( object ), "Given object doesn't belong to this pool. Pool is ", activeObjectLogger.GetName().c_str() );

    // Make sure the object is currently active
    dbAssert( activeObjects.count( object ) == 1, "Tried to destruct an object that isn't currently active" );

    // Remove the address from the list of constructed objects
    activeObjects.erase( object );

    // Don't destruct the object, just add the address on the list of unused objects
    inactiveObjects.push( object );
}

// frees the memory associated with each object created by the factory
template<typename T, unsigned int N>
unsigned int ObjectPool<T, N>::DestroyAllObjects() {
    unsigned int numObjectsDestroyed = activeObjects.size() + inactiveObjects.size();

    // Destroy all active objects
    for( auto i = activeObjects.begin(); i != activeObjects.end(); ++i ) {
        factory.Destroy( *i );
    }
    activeObjects.clear();

    // Destroy all inactive
    while( inactiveObjects.empty() == false ) {
        factory.Destroy( inactiveObjects.top() );
        inactiveObjects.pop();
    }

    return numObjectsDestroyed;
}

template<typename T, unsigned int N>
void ObjectPool<T, N>::UpdateLoggers() {
    activeObjectLogger.AddNewValue( activeObjects.size() );
}
