/*
 * This file implements an abstract
 * factory class that is used
 * to create and delete objects of
 * a given type from a memory pool
 *
 * Author:      Stuart Rudderham
 * Created:     August 5, 2012
 * Last Edited: January 5, 2012
 */

#include <algorithm>
#include <cstdlib>
#include <cstring>

// Constructor
template<typename T, unsigned int N>
GenericFactory<T, N>::GenericFactory( const std::string& factoryName ) : memoryPool( nullptr ),
                                                                         maxNumCreatedObjects( 0 ),
                                                                         createdObjectLogger( factoryName.c_str(), LogType::Factory, N )
{
    // Create the memory pool
    // Uses "::operator new" since objects may not have a default constructor
    memoryPool = static_cast<T*>(::operator new( N * sizeof(T) ) );


#if ENABLE_MEMORY_CLEARING
    // Set the entire pool to an invalid bit-pattern
    memset( memoryPool, 0xCD, N * sizeof(T) );
#endif

    // Populate the list of free addresses with all the addresses in the memory pool
    for( unsigned int i = 0; i < N; ++i ) {
        freeSlots.push( memoryPool + i );
    }

    // Print out creation stats
    PrintStatsToConsole();
}


// Destructor
template<typename T, unsigned int N>
GenericFactory<T, N>::~GenericFactory() {

    // Print out destruction stats
    PrintStatsToConsole();

    // Make sure we don't have a memory leak
    if( LeakedMemory() == true ) {
        dbCrash( "Leaking memory in ", createdObjectLogger.GetName().c_str() );
    }

    // If we haven't leaked any memory then there should
    // be no objects we need to destroy
    // DestroyAllObjects();

    // Free the memory associated with the memory pool
    // Used "::operator delete" since pool was created with "::operator new"
    ::operator delete( memoryPool );
}

// Interesting idea to do everything in GenericFactory
// Idea from here -> http://cpptruths.blogspot.ca/2012/06/perfect-forwarding-of-parameter-groups.html
template<typename T, unsigned int N>
template<typename... Args>
T* GenericFactory<T, N>::Create( Args&&... args ) {
    T* address = GetNewObjectAddress();
    return new (address) T( std::forward<Args>(args)... );
}

// returns a free memory address where an object of type T can be created
template<typename T, unsigned int N>
T* GenericFactory<T, N>::GetNewObjectAddress() {

    // If we don't have any free addresses then crash
    if( freeSlots.empty() ) {
        dbCrash( "No free space in ", createdObjectLogger.GetName().c_str() );
    }

    // Remove the lowest free address from the free list
    T* returnAddress = freeSlots.top();
    freeSlots.pop();

    // and add it to the list of used, construted objects
    createdObjects.insert( returnAddress );

    // Update bookkeeping
    maxNumCreatedObjects = std::max( maxNumCreatedObjects, createdObjects.size() );

    // And return it
    return returnAddress;
}

// Destructs the object but *doesn't* modify the different lists of object addresses
template<typename T, unsigned int N>
void GenericFactory<T, N>::InternalDestroy( T* object ) {

    // If address is nullptr then just exit
    if( object == nullptr ) {
        dbPrintToConsole( "Just tried to delete a nullptr pointer. Might want to look into why. Factory is ", createdObjectLogger.GetName().c_str() );
        return;
    }

    // Make sure object belongs to this pool
    dbAssert( Created( object ), "Given object wasn't created by this factory" );

    // Destroy the object
    object->~T();

#if ENABLE_MEMORY_CLEARING
    // Set the object's memory to an invalid bit-pattern
    memset( object, 0xCD, sizeof(T) );
#endif
}

// Destructs the object and marks that location in the memory pool as free
template<typename T, unsigned int N>
void GenericFactory<T, N>::Destroy( T* object ) {

    // Destroy the object
    InternalDestroy( object );

    // Remove the address from the list of constructed objects
    createdObjects.erase( object );

    // put the address of the object back onto the free list
    freeSlots.push( object );
}

template<typename T, unsigned int N>
bool GenericFactory<T, N>::Created( T* object ) {
    return ( object >= memoryPool )              &&     // make sure address is valid
           ( object <= memoryPool + N - 1 )      &&
           ( createdObjects.count( object ) == 1 );     // make sure object has actually been constructed
}

// Apply the function Fn to each created object
template<typename T, unsigned int N>
void GenericFactory<T, N>::ForEach( std::function< void( T* ) > Fn ) {
    for( auto i = createdObjects.begin(); i != createdObjects.end(); ++i ) {
        Fn( *i );
    }
}

// frees the memory associated with all constructed objects that are marked for deletion
template<typename T, unsigned int N>
unsigned int GenericFactory<T, N>::DestroyAllMarkedObjects() {

    unsigned int oldSize = createdObjects.size();

    // Destory all invalid constructed objects that are currently in use
    for( auto i = createdObjects.begin(); i != createdObjects.end(); ) {
        if( (**i).ShouldDelete() ) {
            InternalDestroy( *i );                  // destroy the object
            freeSlots.push( *i );                   // put the address of the object back onto the free list
            i = createdObjects.erase( i );          // Remove the address from the list of constructed objects
        } else {
            ++i;
        }
    }

    return oldSize - createdObjects.size();         // return how many objects were destroyed
}


// frees the memory associated with each object created by the factory
template<typename T, unsigned int N>
unsigned int GenericFactory<T, N>::DestroyAllObjects() {

    // Destroy all constructed objects, adding the address back onto the free list
    for( auto i = createdObjects.begin(); i != createdObjects.end(); ++i ) {
        InternalDestroy( *i );
        freeSlots.push( *i );
    }

    unsigned int numObjectsDestroyed = createdObjects.size();
    createdObjects.clear();

    return numObjectsDestroyed;
}

template<typename T, unsigned int N>
void GenericFactory<T, N>::PrintStatsToConsole() {

    dbPrintToConsole( createdObjectLogger.GetName().c_str()                               );
    dbPrintToConsole( "\tObject Size:                  ", sizeof(T)                       );
    dbPrintToConsole( "\tMemoty Pool Size:             ", N                               );
    dbPrintToConsole( "\tMemory Used (Bytes):          ", N * sizeof(T)                   );
    dbPrintToConsole( "\tMemory Used (KB):             ", ( N * sizeof(T) ) / 1024.f      );
    dbPrintToConsole( "\tNumber Of Free Slots:         ", freeSlots.size()                );
    dbPrintToConsole( "\tNumber Of Active Objects:     ", createdObjects.size()           );
    dbPrintToConsole( "\tMax Number Of Active Objects: ", maxNumCreatedObjects            );
    dbPrintToConsole( "\tWasted Number Of Slots        ", N - maxNumCreatedObjects        );
    dbPrintToConsole( "\tLeaked Memory:                ", LeakedMemory()                  );
    dbPrintToConsole( ""                                                                  );
}

template<typename T, unsigned int N>
void GenericFactory<T, N>::UpdateLoggers() {
    createdObjectLogger.AddNewValue( createdObjects.size() );
}
