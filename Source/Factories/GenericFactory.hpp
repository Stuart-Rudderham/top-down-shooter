/*
 * This header defines an abstract
 * factory class that is used
 * to create and delete objects of
 * a given type from a memory pool
 *
 * Author:      Stuart Rudderham
 * Created:     August 5, 2012
 * Last Edited: October 10, 2012
 */

#ifndef GENERICFACTORY_HPP_INCLUDED
#define GENERICFACTORY_HPP_INCLUDED

#include "Interfaces/Logging/LoggableObject.hpp"
#include "DebugTools/ValueLogger.hpp"

#include <functional>
#include <unordered_set>
#include <queue>

template<typename T, unsigned int N>
class GenericFactory : public LoggableObject {
    private:
        T* memoryPool;                                                                  // The memory pool of the Factory. Is an array that can hold N objects of type T

        std::priority_queue< T*, std::vector<T*>, std::greater<T*> > freeSlots;         // A min-heap of unused addresses in the memory pool
        std::unordered_set<T*> createdObjects;                                          // A hashtable of currently used addresses

        unsigned int maxNumCreatedObjects;                                              // Holds the highest number of created objects in use at the same time
        ValueLogger createdObjectLogger;                                                // Keeps track of the number of created objects over time

        inline bool LeakedMemory() const { return createdObjects.size() != 0; }         // Sanity check, make sure no memory leaks

        void InternalDestroy( T* object );                                              // Destructs the object but *doesn't* modify the different lists of object addresses
                                                                                        // This is because different circumstances require the object address be
                                                                                        // removed from the active object list in different ways

    public:
        GenericFactory( const std::string& factoryName );                               // Ctor
        ~GenericFactory();                                                              // Dtor. Calls destructors of all created objects (both active and inactive)

        template<typename... Args>                                                      // Create an object of type T using the given parameters. If the parameters
        T* Create( Args&&... args );                                                    // don't correspond to a constructor then there will be a compile error

        T* GetNewObjectAddress();                                                       // Returns a free memory address where an object of type T can be created
        void Destroy( T* object );                                                      // Destructs the object and marks that location in the memory pool as free

        bool Created( T* object );                                                      // Return true if the object was created by this Factory

        void ForEach( std::function< void( T* ) > Fn );                                 // Apply the function Fn to each created object

        unsigned int DestroyAllMarkedObjects();                                         // Frees the memory associated with all currently invalid objects created by the factory
                                                                                        // The class needs to have the function ShouldDelete() defined

        unsigned int DestroyAllObjects();                                               // Frees the memory associated with all the objects created by the factory
                                                                                        // Does not given other objects time to react

        void PrintStatsToConsole();                                                     // Print stats about the Factory to the console
        void UpdateLoggers() override;                                                  // Update the logging for the Factory
};

#include "Factories/GenericFactory.inl"

#endif // GENERICFACTORY_HPP_INCLUDED
