/*
 * This header defines an abstract
 * pool class that is used to
 * hold and recycle objects rather
 * than creating and deleting them
 * repeatedly
 *
 * Author:      Stuart Rudderham
 * Created:     October 10, 2012
 * Last Edited: December 16, 2012
 */

#ifndef OBJECTPOOL_HPP_INCLUDED
#define OBJECTPOOL_HPP_INCLUDED

#include "Interfaces/Logging/LoggableObject.hpp"
#include "DebugTools/ValueLogger.hpp"

#include "Factories/GenericFactory.hpp"

// The abstract manager class
template<typename T, unsigned int N>
class ObjectPool : public LoggableObject {
    private:
        GenericFactory<T, N> factory;                                                   // The Factory that creates new objects for the ObjectPool

        std::priority_queue< T*, std::vector<T*>, std::greater<T*> > inactiveObjects;   // A min-heap of constructed objects that are inactive (ordered by memory address)
        std::unordered_set<T*> activeObjects;                                           // A hashtable of constructed objects that are active

        ValueLogger activeObjectLogger;                                                 // Keeps track of the number of active objects over time

        inline bool HaveActiveObjects() const { return activeObjects.size() != 0; }     // Sanity check, used to check for memory leaks

    public:
        ObjectPool( const std::string& poolName );                                      // Ctor
        ~ObjectPool();                                                                  // Dtor. Calls destructors of all objects in the pool (both active and inactive)

        T* GetObject();                                                                 // Gets a (possible newly created) object from the pool
        void ReturnObject( T* object );                                                 // Returns the object to the pool but does *not* destruct the object

        unsigned int DestroyAllObjects();                                               // Destroy all the objects in the pool

        void UpdateLoggers() override;                                                  // Update the logging for the ObjectPool
};

#include "Factories/ObjectPool.inl"

#endif // OBJECTPOOL_HPP_INCLUDED
