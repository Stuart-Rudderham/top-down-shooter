/*
 * This header defines all the functions
 * that need to be called when the program
 * starts
 *
 * Author:      Stuart Rudderham
 * Created:     March 26, 2012
 * Last Edited: March 26, 2012
 */

#ifndef STARTUP_HPP_INCLUDED
#define STARTUP_HPP_INCLUDED

void PrintClassSizes();

#endif // STARTUP_HPP_INCLUDED
