/*
 * This file implements the particle
 * engine
 *
 * Author:      Stuart Rudderham
 * Created:     February 26, 2012
 * Last Edited: February 26, 2012
 */

#include "ParticleEffect/ParticleEngine.hpp"
#include <SFML/Graphics/PrimitiveType.hpp>
#include "DebugTools/Debug.hpp"

using namespace std;
using namespace sf;

// Constructor
ParticleEngine::ParticleEngine( unsigned int N ) : maxParticles( N ),
                                                   numParticles( 0 )
{
    // Create the arrays
    particleVertex = new Vertex[maxParticles];
    particleVelocity = new Vector2f[maxParticles];
    particleTimeLeft = new Int32[maxParticles];
}

// Destructor
ParticleEngine::~ParticleEngine() {
    delete [] particleVertex;                 // delete the arrays
    delete [] particleVelocity;
    delete [] particleTimeLeft;
}

// create a new particle with the given velocity vector
void ParticleEngine::CreateParticle( Point2f pos, Vector2f vel, Color color, Int32 timeToLive ) {

    // If there is a free spot
    if( numParticles != maxParticles ) {

        // Then create the particle in that spot
        particleVertex[numParticles].position = pos;
        particleVertex[numParticles].color = color;

        particleVelocity[numParticles] = vel;

        particleTimeLeft[numParticles] = timeToLive;

        // and increase the size of the array by 1
        numParticles++;

    // Otherwise overwrite an existing particle
    } else {
        static int overwriteIndex = 0;

        particleVertex[overwriteIndex].position = pos;
        particleVertex[overwriteIndex].color = color;

        particleVelocity[overwriteIndex] = vel;

        particleTimeLeft[overwriteIndex] = timeToLive;

        overwriteIndex = (overwriteIndex + 1) % maxParticles;
    }
}

// create a new particle with the given direction and speed
void ParticleEngine::CreateParticle( Point2f pos, float angleDirection, float moveSpeed, Color color, Int32 timeToLive ) {
    CreateParticle( pos, Vector2f( cos(DegreeToRadian(angleDirection)) * moveSpeed, sin(DegreeToRadian(angleDirection)) * moveSpeed ), color, timeToLive );
}


// update the position of all the particles and remove any that should be destroyed
void ParticleEngine::Update( Int32 millisecondsSinceLastFrame, float deltaT ) {
    for( unsigned int i = 0; i < numParticles; ++i ) {

        // The particle's speed is given in units per second
        particleVertex[i].position += particleVelocity[i] * deltaT;             // Update all the particles' positions

        particleTimeLeft[i] -= millisecondsSinceLastFrame;                      // Update how much time the particle has left
    }

    // Clean up any particles that should be destroyed
    // We go backwards through the array so that we know that the last element in the
    // array should not be destroyed, so we can move it when we need to

    // First check if there are any particles at all
    if( numParticles > 0 ) {
        unsigned int i = numParticles;

        do {
            i--;

            // If the particle should be destroyed
            if( particleTimeLeft[i] <= 0 ) {

                // Overwrite it with the particle in the last spot of the
                // array (which we know doesn't have to be destroyed)
                particleVertex[i] = particleVertex[numParticles - 1];
                particleVelocity[i] = particleVelocity[numParticles - 1];
                particleTimeLeft[i] = particleTimeLeft[numParticles - 1];

                // And reduce the size of the array by 1
                numParticles--;
            }

        } while( i != 0 );
    }

    dbPrintToScreen( "Num Particles: ", numParticles );
}

// draw all the particles
void ParticleEngine::Draw( RenderTarget& Target ) {

    // draw the particles
    Target.draw( particleVertex, numParticles, Points );
}
