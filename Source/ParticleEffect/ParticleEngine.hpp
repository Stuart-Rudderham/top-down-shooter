/*
 * This header defines the particle
 * engine
 *
 * Author:      Stuart Rudderham
 * Created:     February 26, 2012
 * Last Edited: February 26, 2012
 */

#ifndef PARTICLEENGINE_HPP_INCLUDED
#define PARTICLEENGINE_HPP_INCLUDED

#include "Math/MathFunctions.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/System/Clock.hpp>

class ParticleEngine {
    private:

        // These are separate arrays rather than an array
        // of structs so that we can separate out the
        // vertex array for the OpenGL render call
        sf::Vertex* particleVertex;                                             // the vertex used for the OpenGL call. Stores a position and color
        sf::Vector2f* particleVelocity;                                         // the velocity of the particles
        sf::Int32* particleTimeLeft;                                            // the amount of time left until the particle is destroyed

        unsigned int maxParticles;                                              // the maximum number of particles the engine can manage
        unsigned int numParticles;                                              // the current number of particles the engine is managing

    public:
        ParticleEngine( unsigned int N );                                       // constructor. Passed the max number of
                                                                                // particles the engine can manage

        ~ParticleEngine();                                                      // Destructor

        void CreateParticle( Point2f pos,                                       // create a new particle with the given velocity vector
                             sf::Vector2f vel,
                             sf::Color color,
                             sf::Int32 timeToLive );

        void CreateParticle( Point2f pos,                                       // create a new particle with the given direction and speed
                             float angleDirection,
                             float moveSpeed,
                             sf::Color color,
                             sf::Int32 timeToLive );

        void Update( sf::Int32 millisecondsSinceLastFrame, float deltaT );      // update the position of all the particles
                                                                                // and remove any particles that should be destroyed

        void Draw( sf::RenderTarget& Target );                                  // draw all the particles
};

#endif // PARTICLEENGINE_HPP_INCLUDED
