/*
 * This is the masterfile for the Top Down Shooter program
 *
 * It is used to speed up build time and increase program
 * efficiency by using the pre-processor to concatenate all
 * the C++ source files together and then compiling the
 * resulting masterfile
 *
 * For more information see -> http://buffered.io/posts/the-magic-of-unity-builds
 *
 * Generated on September 03, 2012 at 20:39
 */

#if USE_MASTERFILE

#endif // USE_MASTERFILE
