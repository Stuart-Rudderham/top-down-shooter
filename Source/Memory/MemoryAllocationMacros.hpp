/*
 * This header defines macros that
 * are used to override operator new
 * and operator delete so that a custom
 * allocator can be used.
 *
 * Author:      Stuart Rudderham
 * Created:     January 30, 2012
 * Last Edited: January 30, 2013
 */

#ifndef ALLOCATOR_HPP_INCLUDED
#define ALLOCATOR_HPP_INCLUDED

#include "Memory/ObjectTracker.hpp"
#if USE_ARENA_ALLOCATOR
    #include "Memory/ArenaAllocator.hpp"
#endif

// These macros define an easy way override "operator new" and "operator delete"
// so that an class can use the ArenaAllocator rather than "::operator new"
// It also allows object tracking, so that marked objects can be collected at
// the end of the frame
// Use them so you aren't copy/pasting everywhere

// Either use the ArenaAllocator or "::operator new" to allocate addresses
// based on the value of a global #define
#if USE_ARENA_ALLOCATOR
    #define GET_ADDRESS(Type, size)                                                 \
        ArenaAllocator<Type>::Get().Allocate();

    #define RETURN_ADDRESS(Type, object)                                            \
        ArenaAllocator<Type>::Get().Deallocate( static_cast<Type*>(object) );
#else
    #define GET_ADDRESS(Type, size)                                                 \
        static_cast<Type*>( ::operator new(size) );

    #define RETURN_ADDRESS(Type, object)                                            \
        ::operator delete( object );
#endif

// Overload the new and delete operators
//
// Will either use the ArenaAllocator or "::operator new" based on the
// value of the macro USE_ARENA_ALLOCATOR
//
// Can optionally track the object based on the value
// of the bool ShouldTrack
//
// Define but don't implement new[] and delete[], as I don't
// want to implement them right now and we don't want to
// accidently the default ones
#define CUSTOM_NEW_AND_DELETE(Type, ShouldTrack)                                    \
    void* operator new( size_t size ) {                                             \
        dbAssert( size == sizeof(Type), "Wrong size" );                             \
        (void)size;                                                                 \
                                                                                    \
        Type* address = GET_ADDRESS(Type, size);                                    \
                                                                                    \
        if( ShouldTrack ) {                                                         \
            ObjectTracker<Type>::Get().StartTracking( address );                    \
        }                                                                           \
                                                                                    \
        return address;                                                             \
    }                                                                               \
                                                                                    \
    void operator delete( void* object ) {                                          \
        RETURN_ADDRESS(Type, object);                                               \
                                                                                    \
        if( ShouldTrack ) {                                                         \
            ObjectTracker<Type>::Get().StopTracking( static_cast<Type*>(object) );  \
        }                                                                           \
    }                                                                               \
                                                                                    \
    void* operator new[]( size_t size );                                            \
    void operator delete[](void* array);

// Convience macros for tracked/untracked allocations
#define   TRACKED_NEW_AND_DELETE(Type)  CUSTOM_NEW_AND_DELETE(Type, true);
#define UNTRACKED_NEW_AND_DELETE(Type)  CUSTOM_NEW_AND_DELETE(Type, false);

// The ArenaAllocator will use this function to set its name (which is
// used for debugging/printing purposes)
#define SET_ALLOCATOR_NAME(Name)                                                    \
    static const char* GetArenaAllocatorName() {                                    \
        return #Name" Allocator";                                                   \
    }

// The ArenaAllocator will use this function to set its size (which is
// the maximum number of addresses that can be allocated simultaneously)
#define SET_ALLOCATOR_SIZE(Size)                                                    \
    static unsigned int GetArenaAllocatorSize() {                                   \
        return Size;                                                                \
    }

// Macro to wrap calls to "new" and "delete" with static functions Create(...) and
// Destroy() so we can easily search for where we are allocating memory (new and
// delete are very common words)
#define USE_WRAPPED_MEM_CALLS(Type)                                                 \
    template<typename... Args>                                                      \
    static Type* Create( Args&&... args ) {                                         \
        return new Type( std::forward<Args>(args)... );                             \
    }                                                                               \
                                                                                    \
    static void Destroy( Type* object ) {                                           \
        delete object;                                                              \
    }

// If using tracked allocations this will define static for doing a
// batch deletion of all "marked" objects
//#define IS_TRACKED_OBJECT(Type)
#define IS_TRACKED_OBJECT(Type)                                                     \
    static void DestroyAll() {                                                      \
        ObjectTracker<Type>::Get().DestroyAll();                                    \
    }                                                                               \
                                                                                    \
    static void DestroyAllMarked() {                                                \
        ObjectTracker<Type>::Get().DestroyAllMarked();                              \
    }

// An example would be:
//
// class Foo : public GameObject {
//     public:
//         IS_TRACKED_OBJECT(Foo)       // Add functions for garbage collecting
//         SET_ALLOCATOR_NAME(Foo)      // Set the name and size of the ArenaAllocator
//         SET_ALLOCATOR_SIZE(10)
//         USE_WRAPPED_MEM_CALLS(Foo)   // Wrap calls to "new" and "delete" with static functions
//                                      // Create(...) and Destroy() for easy grepping
//     private:
//         TRACKED_NEW_AND_DELETE(Foo)  // Override "new" and "delete" to use the ArenaAllocator
//                                      // Marked as private because we want to force using the wrapped calls
// }

#endif // ALLOCATOR_HPP_INCLUDED
