/*
 * This header defines an allocator
 * class that is used to allocate
 * and deallocate memory for a *single*
 * type. It does not  handle construction
 * or destruction of objects, it only
 * handles memory addresses. It is not
 * meant to be a general purpose allocator
 *
 * Author:      Stuart Rudderham
 * Created:     August 5, 2012
 * Last Edited: January 30, 2013
 */

#ifndef ARENAALLOCATOR_HPP_INCLUDED
#define ARENAALLOCATOR_HPP_INCLUDED

#include "Singleton.hpp"
#include "Interfaces/Logging/LoggableObject.hpp"
#include "DebugTools/ValueLogger.hpp"

#include <unordered_set>
#include <queue>

template<typename T>
class ArenaAllocator : public LoggableObject, public Singleton<ArenaAllocator<T>> {
    private:
        const char* name;                                           // The name of the ArenaAllocator. Used for debugging purposes

        const unsigned int size;                                    // The maximum number of addresses that can be allocated simultaneously

        T* memoryPool;                                              // A pointer to the big block of memory the ArenaAllocator allocates from

        T* highestAllocatedAddress;                                 // The highest address that has ever been allocated (the high-water mark)
                                                                    // If no addresses have been allocated it points to the address right before
                                                                    // the start of the memory pool

        std::vector<T*> freeList;                                   // A min-heap of free addresses in the memory pool
                                                                    // We don't use std::priority_queue because you can't iterate over
                                                                    // it and we need that ability to check for double-frees

        ValueLogger numAllocatedAddressesLogger;                    // Keeps track of the number of allocated addresses over time

        inline unsigned int MaxAllocatedAddresses() const {         // Calculates the highest number of simultaneously allocated addresses (the high-water mark)
            return highestAllocatedAddress + 1 - memoryPool;        // The "+ 1" is because highestAllocatedAddress is equal to "memoryPool - 1" initially
        }

        inline unsigned int NumAllocatedAddresses() const {         // Calculates the current number of allocated addresses
            return MaxAllocatedAddresses() - freeList.size();
        }

        inline bool LeakedMemory() const {                          // Sanity check, make sure no memory leaks
            return NumAllocatedAddresses() != 0;
        }

    public:
        ArenaAllocator();                                           // Ctor
        ~ArenaAllocator();                                          // Dtor. Checks for memory leaks

        T* Allocate();                                              // Returns a free memory address where an object of type T can be created
                                                                    // Does *not* construct an object of type T

        void Deallocate( T* address );                              // Marks the given address in the memory pool as free. Does *not* destruct the
                                                                    // object located at that address.

        void PrintStatsToConsole();                                 // Print stats about the ArenaAllocator to the console
        void UpdateLoggers() override;                              // Update the logging for the ArenaAllocator
};

#include "Memory/ArenaAllocator.inl"

#endif // ARENAALLOCATOR_HPP_INCLUDED
