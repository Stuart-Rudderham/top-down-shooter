/*
 * This file implements a tracker
 * class that is used as a pseudo-
 * garbage collector for a single type.
 * It keeps track of created objects and
 * destructs any "marked" objects at the
 * end of the frame. It is independent of
 * the memory allocator used
 *
 * Author:      Stuart Rudderham
 * Created:     January 11, 2013
 * Last Edited: January 12, 2013
 */

#include <algorithm>

// Constructor
template<typename T>
ObjectTracker<T>::ObjectTracker() : name( T::GetArenaAllocatorName() )
{

}

// Destructor
template<typename T>
ObjectTracker<T>::~ObjectTracker() {

    // Make sure we don't have a memory leak
    if( LeakedMemory() == true ) {
        dbCrash( "Leaking memory in ", name );
    }

    // If we haven't leaked any memory then there should
    // be no objects we need to destroy
    //DestroyAll();
}

template<typename T>
void ObjectTracker<T>::StartTracking( T* object ) {
    dbAssert( trackedObjects.find( object ) == trackedObjects.end(), "Already tracking object" );
    trackedObjects.insert( object );
}

template<typename T>
void ObjectTracker<T>::StopTracking( T* object ) {
    dbAssert( trackedObjects.find( object ) != trackedObjects.end(), "Not tracking object" );
    trackedObjects.erase( object );
}

template<typename T>
void ObjectTracker<T>::DestroyAllMarked() {
    auto currentIndex = trackedObjects.begin();

    while( currentIndex != trackedObjects.end() ) {
        if( (**currentIndex).ShouldDelete() ) {
            T::Destroy( *(currentIndex++) );
        } else {
            ++currentIndex;
        }
    }
}

template<typename T>
void ObjectTracker<T>::DestroyAll() {
    while( trackedObjects.empty() == false ) {
        T::Destroy( *trackedObjects.begin() );
    }
}
