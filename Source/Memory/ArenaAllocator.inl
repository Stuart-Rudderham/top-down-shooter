/*
 * This file implements an abstract
 * factory class that is used
 * to create and delete objects of
 * a given type from a memory pool
 *
 * Author:      Stuart Rudderham
 * Created:     August 5, 2012
 * Last Edited: January 7, 2012
 */

#include <algorithm>
#include <cstring>      // for memset
#include <functional>   // for greater<T>

// Constructor
template<typename T>
ArenaAllocator<T>::ArenaAllocator() : name( T::GetArenaAllocatorName() ),
                                      size( T::GetArenaAllocatorSize() ),
                                      memoryPool( nullptr ),
                                      highestAllocatedAddress( nullptr ),
                                      numAllocatedAddressesLogger( name, LogType::Factory, size )
{
    // Create the memory pool
    // Uses "::operator new" since we don't actually want to
    // construct any objects, just allocate a block of memory
    memoryPool = static_cast<T*>(::operator new( size * sizeof(T) ) );

    #if ENABLE_MEMORY_CLEARING
        // Set the object's memory to an invalid bit-pattern
        memset( memoryPool, 0xCD, size * sizeof(T) );
    #endif

    // We haven't allocated anything yet
    highestAllocatedAddress = memoryPool - 1;

    // Since we know the max size reserve that much
    freeList.reserve(size);
}

// Destructor
template<typename T>
ArenaAllocator<T>::~ArenaAllocator() {

    // Print out stats
    PrintStatsToConsole();

    // Make sure we don't have a memory leak
    if( LeakedMemory() == true ) {
        dbCrash( "Leaking memory in ", name );
    }

    // Free the memory pool
    // Used "::operator delete" since pool was created with "::operator new"
    ::operator delete( memoryPool );
}

// returns a free memory address where an object of type T can be created
template<typename T>
T* ArenaAllocator<T>::Allocate() {

    // If we have a free address within the high-water mark bounds then use that
    // We use the lowest free address to try and keep objects contiguous in memory
    // Need to test if it is actually worth the cost of updating the heap (as opposed to a stack or queue)
    if( freeList.empty() == false ) {
        T* returnAddress = freeList.front();                                                // look at the top of the heap
        std::pop_heap( freeList.begin(), freeList.end(), std::greater<T*>() );              // rearange the heap so the top element is now at the back
        freeList.pop_back();                                                                // and then pop it off
        return returnAddress;
    }

    // Otherwise we have no free addresses, so we have to raise the high-water mark
    highestAllocatedAddress++;
    T* returnAddress = highestAllocatedAddress;

    // If there's no space left then we can only crash
    if( returnAddress >= memoryPool + size ) {
        dbCrash( "No free space in ", name );
    }

    return returnAddress;
}

 // Marks the given address in the memory pool as free. Does *not* destruct the object
template<typename T>
void ArenaAllocator<T>::Deallocate( T* address ) {

    dbAssert( address >= memoryPool              &&                                         // make sure address is in valid range
              address <= highestAllocatedAddress &&
              std::find( freeList.begin(), freeList.end(), address ) == freeList.end(),     // make sure address is actually allocated
              "Given address wasn't allocated by this allocator" );

    #if ENABLE_MEMORY_CLEARING
        memset( address, 0xCD, sizeof(T) );                                                 // Set the object's memory to an invalid bit-pattern
    #endif

    freeList.push_back( address );                                                          // add the address to the back of the free-list
    std::push_heap( freeList.begin(), freeList.end(), std::greater<T*>() );                 // and restore the heap property
}

template<typename T>
void ArenaAllocator<T>::PrintStatsToConsole() {
    dbPrintToConsole( name                                                                    );
    dbPrintToConsole( "\tObject Size:                       ", sizeof(T)                      );
    dbPrintToConsole( "\tMemory Pool Size:                  ", size                           );
    dbPrintToConsole( "\tMemory Used (Bytes):               ", size * sizeof(T)               );
    dbPrintToConsole( "\tMemory Used (KB):                  ", ( size * sizeof(T) ) / 1024.f  );
    dbPrintToConsole( "\tCurrent # Of Allocated Addresses:  ", NumAllocatedAddresses()        );
    dbPrintToConsole( "\tHigh-Water Mark:                   ", MaxAllocatedAddresses()        );
    dbPrintToConsole( "\tNumber Of Wasted Addresses         ", size - MaxAllocatedAddresses() );
    dbPrintToConsole( "\tLeaked Memory:                     ", LeakedMemory()                 );
    dbPrintToConsole( ""                                                                      );
}

template<typename T>
void ArenaAllocator<T>::UpdateLoggers() {
    numAllocatedAddressesLogger.AddNewValue( NumAllocatedAddresses() );
}
