/*
 * This header defines a tracker
 * class that is used as a pseudo-
 * garbage collector for a single type.
 * It keeps track of created objects and
 * destructs any "marked" objects at the
 * end of the frame. It is independent of
 * the memory allocator used
 *
 * Author:      Stuart Rudderham
 * Created:     January 11, 2013
 * Last Edited: January 12, 2013
 */

#ifndef OBJECTTRACKER_HPP_INCLUDED
#define OBJECTTRACKER_HPP_INCLUDED

#include "Singleton.hpp"
#include <set>

template<typename T>
class ObjectTracker : public Singleton<ObjectTracker<T>> {
    private:
        const char* name;                                           // The name of the class being tracked. Used for debugging purposes
                                                                    // Class needs to have the function

        std::set<T*> trackedObjects;								// A hashtable of all the objects currently being tracked
																	// Visual Studio dies if we try and make this an unordered_set so
																	// we have to make due with O(logn) operations :(

        inline bool LeakedMemory() const {                          // Sanity check, make sure no memory leaks
            return trackedObjects.empty() == false;
        }

    public:
        ObjectTracker();                                            // Ctor.
        ~ObjectTracker();                                           // Dtor. Checks for memory leaks

        void StartTracking( T* object );                            // Add the object to the list of active objects
        void StopTracking( T* object );                             // Remove the object from the list of active objects

        void DestroyAllMarked();                                    // Calls "delete" on all "marked" objects
                                                                    // The object needs to have the function ShouldDelete() defined

        void DestroyAll();                                          // Calls "delete" on all tracked objects
                                                                    // Does not given other objects (or Managers) time to react
};

#include "Memory/ObjectTracker.inl"

#endif // OBJECTTRACKER_HPP_INCLUDED
