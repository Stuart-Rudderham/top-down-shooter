/*
 * This header defines a class
 * that holds a list of expected
 * tags to be read while loading a map
 * in XML format
 *
 * Author:      Stuart Rudderham
 * Created:     August 31, 2012
 * Last Edited: August 31, 2012
 */

#ifndef LISTOFPOSSIBLEMAPTAGS_HPP_INCLUDED
#define LISTOFPOSSIBLEMAPTAGS_HPP_INCLUDED

// Forward Declarations
class BaseTag;

#include "Map/Loading/PossibleMapTag.hpp"
#include "DebugTools/Debug.hpp"
#include "TinyXML/tinyxml.h"
#include <vector>

class ListOfPossibleMapTags {
    private:
        std::vector<BaseTag*> possibleTags;                         // holds all possible tags we want to read
                                                                    // DOES NOT FREE THE MEMORY ASSOCIATED WITH THE POINTERS

    public:

        // take in a node and read all the tags, comparing
        // them against the list of possible tags
        void ReadTags( TiXmlNode* startNode ) {

            // Loop through all the tags
            TiXmlNode* childNode = startNode->IterateChildren( nullptr );
            while( childNode != nullptr ) {

                bool readUnknownTag = true;

                // Check the current tag against all valid tags. If we find a valid tag then
                // process it and move on to the next tag
                for( auto i = possibleTags.begin(); i != possibleTags.end(); ++i ) {
                    if( (**i).HandleTag( childNode ) ) {
                        readUnknownTag = false;
                        break;
                    }
                }

                dbAssert( readUnknownTag == false, "Read unknown tag. Tag was ", childNode->Value() );
                (void)readUnknownTag;

                childNode = startNode->IterateChildren( childNode );
            }

            // Make sure we have the right number of each possible tag
            for( auto i = possibleTags.begin(); i != possibleTags.end(); ++i ) {
                (**i).ConfirmCorrectNumberOfOccurances();
            }
        }


        // add a new tag to the list
        inline void AddNewPossibleTag( BaseTag* newTag ) {
            possibleTags.push_back( newTag );
        }
};

// Because of the way that PossibleMapTag and ListOfPossibleMapTags use each other
// we have to define the entire ListOfPossibleMapTags class in the header file
// and then include PossibleMapTag.inl at the end
#include "Map/Loading/PossibleMapTag.inl"

#endif // LISTOFPOSSIBLEMAPTAGS_HPP_INCLUDED
