/*
 * This header defines the class
 * that represents one of the expected
 * tags to be read while loading a map
 * in XML format
 *
 * Author:      Stuart Rudderham
 * Created:     August 31, 2012
 * Last Edited: August 31, 2012
 */

#ifndef POSSIBLEMAPTAG_HPP_INCLUDED
#define POSSIBLEMAPTAG_HPP_INCLUDED

#include "DebugTools/Debug.hpp"
#include "TinyXML/tinyxml.h"
#include <string>
#include <vector>

// Forward Declarations
class ListOfPossibleMapTags;


// This is a base class that all types of possible tags inherit from
// It allows us to have lists of different types of possible tags
class BaseTag {
    public:
        virtual ~BaseTag() {}

        virtual bool HandleTag( TiXmlNode* node ) = 0;                  // Given a tag, read the data if it's the tag we are
                                                                        // watching for. Do nothing otherwise

        virtual void ConfirmCorrectNumberOfOccurances() const = 0;      // Make sure we read the tag the corrent number of times
};


// This is the specialized child class for reading different types of tag data (ints, points, strings, etc...)
template<typename T>
class PossibleMapTag : public BaseTag {
    private:
        std::string name;                                       // the tag name we are looking for

        std::vector<T> data;                                    // a vector to hold the tag values

        unsigned int minNumberOfTimesToRead;                    // the minimum number of times we should read the tag
        unsigned int maxNumberOfTimesToRead;                    // the maximum number of times we should read the tag


    public:

        // ctor. Adds the tag to the parent list
        PossibleMapTag( std::string tagName,
                        ListOfPossibleMapTags& parentList,
                        unsigned int minTimes,
                        unsigned int maxTimes );

        // Given a tag, read the data if it's the tag we are
        // watching for. Do nothing otherwise
        bool HandleTag( TiXmlNode* node );

        // This confirms that we read the tag the expected number of times
        inline void ConfirmCorrectNumberOfOccurances() const {
            dbAssert( data.size() >= minNumberOfTimesToRead, "Didn't read enough occurrences of tag ", name );
            dbAssert( data.size() <= maxNumberOfTimesToRead, "Read too many occurrences of tag ", name );
        }

        // Allows access to the data
        inline const std::vector<T>& GetVector() const {
            return data;
        }
};

#endif // POSSIBLEMAPTAG_HPP_INCLUDED
