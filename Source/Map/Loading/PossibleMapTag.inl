/*
 * This file implements the class
 * that represents one of the expected
 * tags to be read while loading a map
 * in XML format
 *
 * Author:      Stuart Rudderham
 * Created:     August 31, 2012
 * Last Edited: August 31, 2012
 */

#include "UpgradesToSFML/Input.hpp"

// ctor
template<typename T>
PossibleMapTag<T>::PossibleMapTag( std::string tagName, ListOfPossibleMapTags& parentList, unsigned int minTimes, unsigned int maxTimes ) : name( tagName ),
                                                                                                                                            minNumberOfTimesToRead( minTimes ),
                                                                                                                                            maxNumberOfTimesToRead( maxTimes )
{
    dbAssert( maxNumberOfTimesToRead >= minNumberOfTimesToRead, "min > max, that's not right" );
    parentList.AddNewPossibleTag( this );       // add tag to a list for easy iteration
}


// This function take in a tag and if it's the tag the struct is watching for it processes it
template<typename T>
bool PossibleMapTag<T>::HandleTag( TiXmlNode* node ) {

    // Check and see if it's the type of tag we care about
    if( node->Value() != name ) {

        // If not then return false
        return false;

    // Otherwsie
    } else {

        // get the tag's text
        std::stringstream tagText( node->ToElement()->GetText() );

        // Read and store the value
        T newValue;
        tagText >> newValue;
        data.push_back( newValue );

        return true;
    }
}
