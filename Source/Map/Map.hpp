/*
 * This header defines the class
 * that represents the level map
 *
 * Author:      Stuart Rudderham
 * Created:     March 18, 2012
 * Last Edited: March 18, 2012
 */

#ifndef MAP_HPP_INCLUDED
#define MAP_HPP_INCLUDED

#include "DebugTools/Debug.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include <vector>

// Forward Declaration
class StaticGeo;
class Scenery;
class TiXmlNode;

class Map {
    private:
        Scenery* backgroundImage;                                                   // the background image of the map. We need this
                                                                                    // so that we can manipulate the background image
                                                                                    // after the entire map has been loaded

        Point2f minimumPoint;                                                       // the two extreme points of the map
        Point2f maximumPoint;

        // Functions to read data from tags
        void HandleSceneryTag               ( TiXmlNode* startNode );               // read contents of <Scenery> tag
        void HandleSpawnPointTag            ( TiXmlNode* startNode );               // read contents of <SpawnPoint> tag
        void HandleBackgroundImageTag       ( TiXmlNode* startNode );               // read contents of <BackgroundImage> tag
        void HandleStaticGeoAABBTag         ( TiXmlNode* startNode );               // read contents of <StaticGeo Type="AABB"> tag
        void HandleStaticGeoCircleTag       ( TiXmlNode* startNode );               // read contents of <StaticGeo Type="Circle"> tag
        void HandleStaticGeoNGonTag         ( TiXmlNode* startNode );               // read contents of <StaticGeo Type="NGon"> tag
        void HandleStaticGeoConvexPolygonTag( TiXmlNode* startNode );               // read contents of <StaticGeo Type="ConvexPolygon"> tag

        // Functions to add new objects to the map
        void UpdateMapDimensions( StaticGeo* newGeo );                              // when creating new StaticGeo we have to update the attributes
                                                                                    // of the map such as width, height and center point


    public:
        Map();

        void Load( const ExternalFile& map );

        // Getters
        inline unsigned int GetWidth() const {
            dbAssert( minimumPoint != MathConstants::BigVector, "Map has no objects, cannot get width" );
            return maximumPoint.x - minimumPoint.x;
        }

        inline unsigned int GetHeight() const {
            dbAssert( minimumPoint != MathConstants::BigVector, "Map has no objects, cannot get height" );
            return maximumPoint.y - minimumPoint.y;
        }

        inline Point2f GetCenter() const {
            dbAssert( minimumPoint != MathConstants::BigVector, "Map has no objects, cannot get center point" );
            return ( minimumPoint + maximumPoint ) / 2.0f;
        }
};

#endif // MAP_HPP_INCLUDED
