/*
 * This file implements the class
 * that represents the level map
 *
 * Author:      Stuart Rudderham
 * Created:     March 18, 2012
 * Last Edited: January 12, 2013
 */

#include "Map/Map.hpp"
#include "Map/Loading/ListOfPossibleMapTags.hpp"

#include "GameObjects/MapObjects/StaticGeo.hpp"
#include "GameObjects/MapObjects/Scenery.hpp"
#include "GameObjects/MapObjects/SpawnPoint.hpp"

#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"
#include "Math/Geometry/ConvexPolygon.hpp"

#include "MediaCache/MediaCache.hpp"

#include <string>
using namespace std;
using namespace sf;

// These are all the accepted tag names
static string aabbTag               ( "AABB"            );
static string backgroundImageTag    ( "BackgroundImage" );
static string circleTag             ( "Circle"          );
static string convexPolyTag         ( "ConvexPolygon"   );
static string drawModeTag           ( "DrawMode"        );
static string imageNameTag          ( "ImageName"       );
static string mapTag                ( "Map"             );
static string ngonTag               ( "NGon"            );
static string numPointsTag          ( "NumberOfPoints"  );
static string pointTag              ( "Point"           );
static string positionTag           ( "Position"        );
static string radiusTag             ( "Radius"          );
static string sceneryTag            ( "Scenery"         );
static string sizeTag               ( "Size"            );
static string spawnPointTag         ( "SpawnPoint"      );
static string staticGeoTag          ( "StaticGeo"       );



// Constructor
Map::Map() : backgroundImage( nullptr ),
             minimumPoint( MathConstants::BigVector ),
             maximumPoint( MathConstants::BigVector * -1.0f )
{ }

// Add static geo to the map
void Map::UpdateMapDimensions( StaticGeo* newGeo ) {
    Point2f topLeftCorner = newGeo->GetBoundingBox()->GetTopLeftCorner();
    Point2f bottomRightCorner = newGeo->GetBoundingBox()->GetBottomRightCorner();

    minimumPoint.x = min( minimumPoint.x, topLeftCorner.x );
    minimumPoint.y = min( minimumPoint.y, topLeftCorner.y );

    maximumPoint.x = max( maximumPoint.x, bottomRightCorner.x );
    maximumPoint.y = max( maximumPoint.y, bottomRightCorner.y );
}



void Map::Load( const ExternalFile& map ) {

    TiXmlDocument mapFile;

    bool ableToLoad = mapFile.LoadFile( map );                                          // load the file from disk
    dbAssert( ableToLoad == true, "Unable to load file. Given path was ", map );        // make sure it actually loaded
    (void)ableToLoad;

    TiXmlNode* rootNode = mapFile.RootElement();                                        // start at the highest level tag
    dbAssert( rootNode->Value() == mapTag, "First element wasn't <map>");               // make sure it's a valid tag

    dbPrintToConsole( "Loading map ", map );

    // loop through all the sub-tags
    TiXmlNode* childNode = rootNode->IterateChildren( nullptr );
    while( childNode != nullptr ) {
        //dbPrintToConsole( "Tag: ", childNode->Value() );

        // if it's the <StaticGeo> tag
    	if( childNode->Value() == staticGeoTag ) {

            TiXmlElement* childNodeElement = childNode->ToElement();
            dbAssert( childNodeElement != nullptr, "StaticGeo tag is not a valid element tag" );

            // Make sure it has an attribute specifying the type
            string geoType;
            int result = childNodeElement->QueryStringAttribute( "Type", &geoType );

            dbAssert( result == TIXML_SUCCESS, "StaticGeo tag doesn't have valid Type attribute" );
            (void)result;

            //dbPrintToConsole( "Type: ", geoType );

            // Based on the attribute Type create the correct type of SaticGeo
            if( geoType == "AABB" ) {
                HandleStaticGeoAABBTag( childNode );

            } else if( geoType == "Circle" ) {
                HandleStaticGeoCircleTag( childNode );

            } else if( geoType == "NGon" ) {
                HandleStaticGeoNGonTag( childNode );

            } else if( geoType == "ConvexPolygon" ) {
                HandleStaticGeoConvexPolygonTag( childNode );

            } else {
                dbCrash( "Invalid Type attribute tag. Type given was ", geoType );
            }

        // if it's the <Scenery> tag
	    } else if( childNode->Value() == sceneryTag ) {
            HandleSceneryTag( childNode );

        // if it's the <BackgroundImage> tag
	    } else if( childNode->Value() == backgroundImageTag ) {
            HandleBackgroundImageTag( childNode );

        // if it's the <SpawnPoint> tag
	    } else if( childNode->Value() == spawnPointTag ) {
            HandleSpawnPointTag( childNode );

	    } else if( childNode->ToComment() != nullptr ) {
            // we ignore comment tags

	    // Otherwise invalid tag
	    } else {
            dbPrintToConsole( "Invalid Tag. Value is ", childNode->Value() );
	    }

        // go to the next tag
        childNode = rootNode->IterateChildren( childNode );
    }

    // If the map has a background image then make it cover the whole map
    if( backgroundImage != nullptr ) {
        backgroundImage->GetDrawingSprite()->setPosition( GetCenter() );
        backgroundImage->SetSize( Vector2f( GetWidth(), GetHeight() ) );
    }
}



// Used to read in the <BackgroundImage> tag
void Map::HandleBackgroundImageTag( TiXmlNode* startNode ) {

    // Make sure we're handling an <BackgroundImage> tag
    dbAssert( startNode->Value() == backgroundImageTag, "Tried to create BackgroundImage with wrong tag. Given tag was ", startNode->Value() );

    // Create the list of possible tags
    ListOfPossibleMapTags possibleTags;
    PossibleMapTag<string> imageName( imageNameTag, possibleTags, 1, 1 );
    PossibleMapTag<string> drawMode ( drawModeTag,  possibleTags, 1, 1 );

    // Read all the tags
    possibleTags.ReadTags( startNode );

    // Use the data in the tags to create the background image
    backgroundImage = Scenery::Create( TextureCache::Get().Load( imageName.GetVector()[0].c_str() ), Drawing::Level::Background );
}



// Used to read in the <Scenery> tag
void Map::HandleSceneryTag( TiXmlNode* startNode ) {

    // Make sure we're handling an <Scenery> tag
    dbAssert( startNode->Value() == sceneryTag, "Tried to create Scenery with wrong tag. Given tag was ", startNode->Value() );

    // Create the list of possible tags
    ListOfPossibleMapTags possibleTags;
    PossibleMapTag<string>       imageName( imageNameTag, possibleTags, 1, 1 );
    PossibleMapTag<Point2f>      position ( positionTag,  possibleTags, 1, 1 );
    PossibleMapTag<sf::Vector2f> size     ( sizeTag,      possibleTags, 0, 1 );


    // Read all the tags
    possibleTags.ReadTags( startNode );

    // Use the data in the tags to create the Scenery
    Scenery* newScenery = Scenery::Create( TextureCache::Get().Load( imageName.GetVector()[0].c_str() ), Drawing::Level::Foreground );
    newScenery->GetDrawingSprite()->setPosition( position.GetVector()[0] );

    // Set the size if there was a <Size> tag
    if( size.GetVector().empty() == false ) {
        newScenery->SetSize( size.GetVector()[0] );
    }
}



// Used to read in the <SpawnPoint> tag
void Map::HandleSpawnPointTag( TiXmlNode* startNode ) {

    // Make sure we're handling an <SpawnPoint> tag
    dbAssert( startNode->Value() == spawnPointTag, "Tried to create SpawnPoint with wrong tag. Given tag was ", startNode->Value() );

    // Create the list of possible tags
    ListOfPossibleMapTags possibleTags;
    PossibleMapTag<Point2f>      position ( positionTag,  possibleTags, 1, 1 );

    // Read all the tags
    possibleTags.ReadTags( startNode );

    // Use the data in the tags to create the Scenery
    SpawnPoint::Create( position.GetVector()[0] );

    // Add a drawable representation if desired
    if( Drawing::DrawSpawnPoints ) {
        Scenery* newScenery = Scenery::Create( TextureCache::Get().Load( SpawnPointImage ), Drawing::Level::Midground );
        newScenery->GetDrawingSprite()->setPosition( position.GetVector()[0] );
    }
}



// read contents of <StaticGeo Type="AABB"> tag
void Map::HandleStaticGeoAABBTag( TiXmlNode* startNode ) {

    // Create the list of possible tags
    ListOfPossibleMapTags possibleTags;
    PossibleMapTag<Point2f> points( pointTag, possibleTags, 2, 2 );

    // Read all the tags
    possibleTags.ReadTags( startNode );

    // Use the data in the tags to create the StaticGeo
    AABB* collisionOutline = AABB::Create( points.GetVector()[0], points.GetVector()[1] );
    StaticGeo* newGeo = StaticGeo::Create( collisionOutline, Color::Black );

    UpdateMapDimensions( newGeo );
}



// read contents of <StaticGeo Type="Circle"> tag
void Map::HandleStaticGeoCircleTag( TiXmlNode* startNode ) {

    // Create the list of possible tags
    ListOfPossibleMapTags possibleTags;
    PossibleMapTag<Point2f> position( positionTag, possibleTags, 1, 1 );
    PossibleMapTag<float>   radius  ( radiusTag,   possibleTags, 1, 1 );

    // Read all the tags
    possibleTags.ReadTags( startNode );

    // Use the data in the tags to create the StaticGeo
    Circle* collisionOutline = Circle::Create( position.GetVector()[0], radius.GetVector()[0] );
    StaticGeo* newGeo = StaticGeo::Create( collisionOutline, Color::Black );

    UpdateMapDimensions( newGeo );
}



// read contents of <StaticGeo Type="NGon"> tag
void Map::HandleStaticGeoNGonTag( TiXmlNode* startNode ) {

    // Create the list of possible tags
    ListOfPossibleMapTags possibleTags;
    PossibleMapTag<Point2f>      position ( positionTag,  possibleTags, 1, 1 );
    PossibleMapTag<float>        radius   ( radiusTag,    possibleTags, 1, 1 );
    PossibleMapTag<unsigned int> numPoints( numPointsTag, possibleTags, 1, 1 );

    // Read all the tags
    possibleTags.ReadTags( startNode );

    // Make sure the NGon has at least 3 points
    dbAssert( numPoints.GetVector()[0] >= 3, "<NumberOfPoints> tag must have value of at least 3, only has ", numPoints.GetVector()[0] );

    // Use the data in the tags to create the StaticGeo
    ConvexPolygon* collisionOutline = ConvexPolygon::Create( numPoints.GetVector()[0], position.GetVector()[0], radius.GetVector()[0] );
    StaticGeo* newGeo = StaticGeo::Create( collisionOutline, Color::Black );

    UpdateMapDimensions( newGeo );
}



// read contents of <StaticGeo Type="ConvexPolygon"> tag
void Map::HandleStaticGeoConvexPolygonTag( TiXmlNode* startNode ) {

    // Create the list of possible tags
    ListOfPossibleMapTags possibleTags;
    PossibleMapTag<Point2f> points ( pointTag, possibleTags, 3, std::numeric_limits<unsigned int>::max() );

    // Read all the tags
    possibleTags.ReadTags( startNode );

    // Use the data in the tags to create the StaticGeo
    ConvexPolygon* collisionOutline = ConvexPolygon::Create( points.GetVector() );
    StaticGeo* newGeo = StaticGeo::Create( collisionOutline, Color::Black );

    UpdateMapDimensions( newGeo );
}
