#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "Startup.hpp"
#include "Testing/UnitTests.hpp"
#include "DebugTools/Debug.hpp"

using namespace std;
using namespace sf;

int main() {
    PrintClassSizes();

    RenderWindow DebugWindow;
    RenderWindow GameWindow;

    DebugWindow.create(
                        VideoMode(WindowOptions::DebugWindowSize.x, WindowOptions::DebugWindowSize.y, VideoMode::getDesktopMode().bitsPerPixel),
                        WindowOptions::DebugWindowName,
                        Style::Default,
                        ContextSettings( 0, 0, WindowOptions::AntiAliasingLevel )
                      );

    DebugWindow.setPosition( Vector2i(0, 0) );

    if( WindowOptions::FullScreen ) {
        GameWindow.create(
                           sf::VideoMode::getFullscreenModes().at(0),
                           WindowOptions::GameWindowName,
                           Style::Fullscreen,
                           ContextSettings( 0, 0, WindowOptions::AntiAliasingLevel )
                         );
    } else {
        GameWindow.create(
                           VideoMode(WindowOptions::GameWindowSize.x, WindowOptions::GameWindowSize.y, VideoMode::getDesktopMode().bitsPerPixel),
                           WindowOptions::GameWindowName,
                           Style::Default,
                           ContextSettings( 0, 0, WindowOptions::AntiAliasingLevel )
                         );

        GameWindow.setPosition( Vector2i(0, 0) );
    }

    GameWindow.setVerticalSyncEnabled( WindowOptions::UseVSync );
    DebugWindow.setVerticalSyncEnabled( WindowOptions::UseVSync );

    GameWindow.setMouseCursorVisible( false );

    //TestShapes( Window, 2 );

    //TestParticles( Window );

    //TestMediaCaching( Window );

    //TestGUI( Window );

    //TestRayCasting( Window );

    TestEntities( GameWindow, DebugWindow );

    //Entity::SanityCheck();

#if USING_VS
    dbPause( "Stupid Visual Studio, always closing the console window ..." );
#endif

    GameWindow.close();
    DebugWindow.close();
    return 0;
}
