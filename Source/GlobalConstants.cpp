/*
 * This header instantiates all the global
 * constants for the project
 *
 * Author:      Stuart Rudderham
 * Created:     April 4, 2012
 * Last Edited: January 10, 2013
 */

#include "GlobalConstants.hpp"

// Profiling
#include "Profile/TimeProfiler.hpp"

// ConstData
#include "GameObjects/Characters/Soldier.hpp"
#include "GameObjects/Characters/Zombie.hpp"

using namespace sf;
using namespace std;

// Initialize all the constants first
namespace WindowOptions {
    const char GameWindowName[]                 = "Top Down Shooter v2.0";
    const char DebugWindowName[]                = "Debug Window";

    const bool UseVSync                         = false;

    const bool FullScreen                       = false;

    const unsigned int AntiAliasingLevel        = 16;

    const sf::Vector2u GameWindowSize           = sf::Vector2u( 800, 450 );
    const sf::Vector2u DebugWindowSize          = sf::Vector2u( sf::VideoMode::getFullscreenModes().at(0).width,        // make the debug window as big as possible
                                                                sf::VideoMode::getFullscreenModes().at(0).height );
}

namespace DebugOptions {
    const sf::Color TextColor                   = sf::Color( 0, 0, 0 );			    // can't use sf::Color::Black because it may not be initialized yet
    const unsigned int TextSize                 = 13;

    const unsigned int TextSpaceWidth           = 200;

    const sf::Vector2f GraphSize                = sf::Vector2f( 200, 200 );
    const bool DrawLineGraph                    = false;
}

namespace Drawing {
    const bool DrawCollisionOutline             = !true;

    const sf::Color CollisionOutlineColor       = sf::Color( 128, 128, 128, 128 );  // slightly transparent grey
    const sf::Color BoundingBoxColor            = sf::Color( 255, 255, 255, 0 );    // transparent

    const bool DrawSpawnPoints                  = true;

    const bool DrawCharacterPicture             = true;

    const bool UseImageAntiAliasing             = false;

    namespace Level {
        const DrawLevel Background              = std::numeric_limits<DrawLevel>::min();
        const DrawLevel Midground               = Background + 1;
        const DrawLevel Foreground              = Midground + 1;
    }
}

namespace Game {
    sf::Clock GameTimer;
    sf::Time  CurrentTime                       = sf::milliseconds(0);
    sf::Time  DeltaT                            = sf::milliseconds(0);

    const unsigned int FPS                      = 60;

    const sf::Time IntentionalDelay             = sf::milliseconds(20);

#if DEBUG_MODE
    const unsigned int NumCharacters            = 10;
#else
    const unsigned int NumCharacters            = 1000;
#endif
    const bool SpawnZombies                     = true;
    const bool SpawnSoldiers                    = !false;
    const bool MakeSoldiersZombies              = false;

    const sf::Time CharacterRespawnTime         = sf::seconds( 5.0f );
    const sf::Time DeadBodyDespawnTime          = sf::seconds( 10.0f );

    namespace Camera {
        const sf::Vector2f ViewportSize         = sf::Vector2f( WindowOptions::GameWindowSize.x, WindowOptions::GameWindowSize.y );
        const float ZoomSpeed                   = 0.1f;
    }

    const float MiniMapRange                    = 500.0f;
}

namespace Physics {
    const bool EnableStressTest                 = !true;

    const bool UseFixedTimeStep                 = false;

    const unsigned int IterationsPerFrame       = 5;                                                // An arbitrary value. Good resolution without taking too long

    const float FixedDeltaT                     = 1.0f / Game::FPS;
    float DeltaT                                = FixedDeltaT;

    const bool ClampDeltaT                      = !true;
    const float MaxDeltaT                       = FixedDeltaT * 5.0f;                               // Maximum of 300 milliseconds (an arbitrary value)
    const float MinDeltaT                       = 0.01f;                                            // Minimum of 10 milliseconds  (an arbitrary value)

    namespace Priority {
        const CollisionPriority Immovable       = std::numeric_limits<CollisionPriority>::max();    // The highest possible priority
        const CollisionPriority Phantom         = std::numeric_limits<CollisionPriority>::min();    // The lowest possible priority
        const CollisionPriority Character       = 10;                                               // An arbitrary value
        const CollisionPriority SlowBullet      = 10;                                               // An arbitrary value
    }
}

namespace HUDOptions {
    namespace MiniMap {
        const float Size                        = 50.0f;
        const Point2f Position                  = Point2f(Size, WindowOptions::GameWindowSize.y - Size);
        const sf::Color Color                   = sf::Color(0, 0, 255, 128);        // light-blue
        const float BezelThickness              = 2;

        const float CharacterDotSize            = 3.0f;
        const sf::Color SelfColor               = sf::Color(255, 255, 0);           // yellow. Can't use sf::Color::Yellow because it may not be initialized yet
        const sf::Color AllyColor               = sf::Color(0, 255, 0);             // green.  Can't use sf::Color::Green  because it may not be initialized yet
        const sf::Color EnemyColor              = sf::Color(255, 0, 0);             // red.    Can't use sf::Color::Red    because it may not be initialized yet
    }

    namespace GunPicture {
        const sf::Vector2f Size                 = sf::Vector2f(250, 100);
        const Point2f Position                  = Point2f(WindowOptions::GameWindowSize.x - Size.x, 0);
        const sf::Color BackgroundColor         = sf::Color(127, 127, 127, 127);    // light grey
    }
};

namespace Profile {
    static TimeProfiler                         StaticGlobalFramerateProfiler( " Global Frame Time" );      // extra space at beginning of name makes sure
    TimeProfiler& GlobalFramerate               = StaticGlobalFramerateProfiler;                            // it gets placed first alphabetically

    static TimeProfiler                         StaticFieldOfViewCollisionProfiler( "FOV Collision Time" );
    TimeProfiler& FieldOfViewCollision          = StaticFieldOfViewCollisionProfiler;

    static TimeProfiler                         StaticLineOfSightCollisionProfiler( "LOS Collision Time" );
    TimeProfiler& LineOfSightCollision          = StaticLineOfSightCollisionProfiler;

    static TimeProfiler                         StaticControllerUpdateProfiler( "Controller Update Time" );
    TimeProfiler& ControllerUpdate              = StaticControllerUpdateProfiler;

    static TimeProfiler                         StaticPhysicsUpdateProfiler( "Physics Update Time" );
    TimeProfiler& PhysicsUpdate                 = StaticPhysicsUpdateProfiler;

    static TimeProfiler                         StaticDrawingProfiler( "Drawing Time" );
    TimeProfiler& Drawing                       = StaticDrawingProfiler;

    static TimeProfiler                         StaticDisplayingProfiler( "Window Displaying Time" );
    TimeProfiler& Displaying                    = StaticDisplayingProfiler;

    static TimeProfiler                         StaticCleanupProfiler( "Object Cleanup Time" );
    TimeProfiler& Cleanup                       = StaticCleanupProfiler;
}

// Const data for characters
const SoldierConstData Soldier::SoldierSpecificConstData;
const ZombieConstData Zombie::ZombieSpecificConstData;
