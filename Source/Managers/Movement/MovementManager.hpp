/*
 * This header defines the manager that
 * handles the movement of objects (i.e.
 * applying acceleration, friction, etc...)
 *
 * Author:      Stuart Rudderham
 * Created:     August 2, 2012
 * Last Edited: October 30, 2012
 */

#ifndef MOVEMENTMANAGER_HPP_INCLUDED
#define MOVEMENTMANAGER_HPP_INCLUDED

#include "Singleton.hpp"
#include "Managers/Containers/ArrayOfGameObjects.hpp"

class MoveableObject;

class MovementManager : public Singleton<MovementManager> {
    private:
        ArrayOfGameObjects<MoveableObject> moveableObjects;     // The list of MoveableObjects

    public:
        MovementManager();

        void AddGameObject( MoveableObject* object );           // Add a new MoveableObject to the Manager
        bool InManager    ( MoveableObject* object );           // Return true if the given MoveableObject is in the Manager. It
                                                                // is only used for debugging/sanity-check purposes so it doesn't
                                                                // matter that it takes O(n) time

        void FrameUpdate( float deltaT );                       // Do updates that should only happen once per frame
        void TimeSliceUpdate( float deltaT );                   // Do updates that should happen once per physics time step

        void Cleanup();                                         // Do any end-of-frame cleanup (such as removing marked GameObjects)
        void RemoveAllGameObjects();                            // Remove all GameObjects from the Manager
};

#endif // MOVEMENTMANAGER_HPP_INCLUDED
