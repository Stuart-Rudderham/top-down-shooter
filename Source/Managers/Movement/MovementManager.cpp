/*
 * This file implements the manager that
 * handles moving objects
 *
 * Author:      Stuart Rudderham
 * Created:     August 2, 2012
 * Last Edited: October 31, 2012
 */

#include "Managers/Movement/MovementManager.hpp"
#include "Interfaces/Movement/MoveableObject.hpp"
#include "Managers/Containers/ArrayOfGameObjects.inl"

MovementManager::MovementManager() : moveableObjects( "MovementManager", 5000 )
{

}

// Add a new MoveableObject to the Manager
void MovementManager::AddGameObject( MoveableObject* object ) {
    moveableObjects.AddGameObject( object );
}

// Return true if the given MoveableObject is in the Manager
bool MovementManager::InManager( MoveableObject* object ) {
    return moveableObjects.InContainer( object );
}

// Do updates that should only happen once per frame
void MovementManager::FrameUpdate( float deltaT ) {
    moveableObjects.ForEach( [=](MoveableObject* Obj){ Obj->FrameUpdate( deltaT ); } );
}

// Do updates that should happen once per physics time step
void MovementManager::TimeSliceUpdate( float deltaT ) {
    moveableObjects.ForEach( [=](MoveableObject* Obj){ Obj->TimeSliceUpdate( deltaT ); } );
}

// Do any end-of-frame cleanup
void MovementManager::Cleanup() {
    moveableObjects.RemoveAllMarkedGameObjects();
}

// Remove all GameObjects from the Manager
void MovementManager::RemoveAllGameObjects() {
    moveableObjects.RemoveAllGameObjects();
}
