/*
 * This file implements the manager that
 * handles respawning objects
 *
 * Author:      Stuart Rudderham
 * Created:     November 1, 2012
 * Last Edited: November 24, 2012
 */

#include "Managers/Respawning/RespawnManager.hpp"
#include "GameObjects/MapObjects/SpawnPoint.hpp"
#include "Interfaces/Health/MortalObject.hpp"
#include "Managers/Containers/ArrayOfGameObjects.inl"
#include "Managers/Containers/DeferredActionList.inl"

RespawnManager::RespawnManager() : spawnPoints( "SpawnPoints", 5000 ),
                                   deadObjects( "RespawnManager", 5000 )
{

}

// Add a new MortalObject to the Manager
// to be respawned after the given time
void RespawnManager::AddRespawnEntry( MortalObject* object, sf::Time respawnTime ) {
    deadObjects.AddGameObject( object, respawnTime );
}

// Return true if the given MortalObject is in the Manager
bool RespawnManager::InManager( MortalObject* object ) {
    return deadObjects.InContainer( object );
}

// Add a new SpawnPoint to the Manager
void RespawnManager::AddGameObject( SpawnPoint* object ) {
    spawnPoints.AddGameObject( object );
}

// Return true if the given SpawnPoint is in the Manager
bool RespawnManager::InManager( SpawnPoint* object ) {
    return spawnPoints.InContainer( object );
}

// Respawn any objects whos time is up
void RespawnManager::RespawnObjects() {
    deadObjects.ApplyAndRemoveExpiredObjects( [&](MortalObject* Obj){ Obj->Respawn(); } );
}

// Remove all GameObjects from the Manager
void RespawnManager::RemoveAllGameObjects() {
    spawnPoints.RemoveAllGameObjects();
    deadObjects.RemoveAllGameObjects();
}
