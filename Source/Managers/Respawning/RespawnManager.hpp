/*
 * This header defines the manager that
 * handles respawning objects
 *
 * Author:      Stuart Rudderham
 * Created:     November 1, 2012
 * Last Edited: November 24, 2012
 */

#ifndef RESPAWNMANAGER_HPP_INCLUDED
#define RESPAWNMANAGER_HPP_INCLUDED

#include "Singleton.hpp"
#include "Managers/Containers/ArrayOfGameObjects.hpp"
#include "Managers/Containers/DeferredActionList.hpp"

class MortalObject;
class SpawnPoint;

class RespawnManager : public Singleton<RespawnManager> {
    private:
        ArrayOfGameObjects<SpawnPoint> spawnPoints;                             // The list of RespawnPoints for the current map

        DeferredActionList<MortalObject> deadObjects;                           // The list of objects waiting to respawn

    public:
        RespawnManager();

        void AddRespawnEntry( MortalObject* object, sf::Time respawnTime );     // Add a new MortalObject to the Manager to be respawned after the given time has expired
        bool InManager      ( MortalObject* object                       );     // Return true if the given MortalObject is in the Manager. It
                                                                                // is only used for debugging/sanity-check purposes so it doesn't
                                                                                // matter that it takes O(n) time

        void AddGameObject( SpawnPoint* object );                               // Add a new SpawnPoint to the Manager
        bool InManager    ( SpawnPoint* object );                               // Return true if the given SpawnPoint is in the Manager. It
                                                                                // is only used for debugging/sanity-check purposes so it doesn't
                                                                                // matter that it takes O(n) time

        void RespawnObjects();                                                  // Respawn any objects whos time is up

        void RemoveAllGameObjects();                                            // Remove all GameObjects from the Manager
};

#endif // RESPAWNMANAGER_HPP_INCLUDED
