/*
 * This file implements a generic
 * container class for holding GameObjects
 * and performing an action on them sometime
 * in the future (e.g. 5 seconds from now)
 *
 * Author:      Stuart Rudderham
 * Created:     November 23, 2012
 * Last Edited: November 23, 2012
 */

// ctor. Takes in a name that describes what's in the list
// as well as a number for the scale of the logging graph
template<typename T>
DeferredActionList<T>::DeferredActionList( const char* listName, sf::Int64 loggerScale ) : sizeLogger( listName, LogType::Manager, loggerScale )
{

}

// dtor. Makes sure the list is empty first
template<typename T>
DeferredActionList<T>::~DeferredActionList() {
    if( GetSize() != 0 ) {
        dbCrash( "Tried to destruct a non-empty DeferredActionList.  Name is ", sizeLogger.GetName() );
    }
}

// get how many objects the list has
 template<typename T>
unsigned int DeferredActionList<T>::GetSize() {
    return objects.size();
}

// Predicate function to check if a given GameObject is currently in the list
template<typename T>
bool DeferredActionList<T>::InContainer( T* object ) {
    for( auto i = objects.begin(); i != objects.end(); ++i ) {
        if( i->second == object ) {
            return true;
        }
    }

    return false;
}

// Add a GameObject to the list
template<typename T>
void DeferredActionList<T>::AddGameObject( T* object, sf::Time timeUntilAction ) {
    dbAssert( InContainer( object ) == false, "GameObject already in container" );
    objects.insert( std::make_pair( Game::CurrentTime + timeUntilAction, object ) );
}

// Clear the list
template<typename T>
void DeferredActionList<T>::RemoveAllGameObjects() {
    objects.clear();
}

// Remove all objects that aren't attached to anything
template<typename T>
void DeferredActionList<T>::RemoveAllMarkedGameObjects() {
    for( auto i = objects.begin(); i != objects.end(); ) {      // have to use explicit loop since std::remove_if
        if( i->second->ShouldIgnore() ) {                       // doesn't work on associative containers
            i = objects.erase( i );
        } else {
            ++i;
        }
    }
}

// Apply the function to each of the objects in the set
// ignoring any that have a marked parent object
template<typename T>
void DeferredActionList<T>::ApplyAndRemoveExpiredObjects( std::function< void( T* ) > Fn ) {

    // maps sort their keys, so objects.begin() holds the earliest time
    while( objects.empty() == false && objects.begin()->first < Game::CurrentTime ) {
        if( objects.begin()->second->ShouldIgnore() == false ) {
            Fn( objects.begin()->second );
        }

        objects.erase( objects.begin() );
    }
}

// Update the ValueLogger that keeps tracks of the list size
template<typename T>
void DeferredActionList<T>::UpdateLoggers() {
    sizeLogger.AddNewValue( GetSize() );
}
