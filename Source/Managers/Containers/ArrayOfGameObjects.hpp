/*
 * This header defines a generic
 * container class for holding GameObjects
 * that share an Interface
 *
 * Author:      Stuart Rudderham
 * Created:     September 30, 2012
 * Last Edited: November 23, 2012
 */

#ifndef ARRAYOFGAMEOBJECTS_HPP_INCLUDED
#define ARRAYOFGAMEOBJECTS_HPP_INCLUDED

#include "Interfaces/Logging/LoggableObject.hpp"
#include "DebugTools/ValueLogger.hpp"

#include <vector>
#include <algorithm>
#include <functional>

template<typename T>
class ArrayOfGameObjects : public LoggableObject {
    protected:
        std::vector<T*> objects;                                                    // an array of pointers to GameObjects that have the Interface of type T
        ValueLogger sizeLogger;                                                     // keeps track of the number of objects in the array over time

    public:
        ArrayOfGameObjects( const char* arrayName, sf::Int64 loggerScale );         // ctor. Takes in a name that describes what's in the array
                                                                                    // as well as a number for the scale of the logging graph

        virtual ~ArrayOfGameObjects();                                              // dtor. Makes sure the array is empty first

        unsigned int GetSize();                                                     // get how many GameObjects the array has

        bool InContainer( T* object );                                              // Predicate function to check if a given GameObject is currently in the array

        void AddGameObject( T* object );                                            // Add a GameObject to the array

        /* void RemoveGameObject( T* object ); */                                   // DON'T UNCOMMENT THIS
                                                                                    // We intentionally don't provide functionality to remove
                                                                                    // individual GameObjects because we always want to do a bulk
                                                                                    // removal of all marked GameObjects at the end of the frame

        void RemoveAllGameObjects();                                                // Clear the array

        void RemoveAllMarkedGameObjects();                                          // Remove all marked GameObjects from the array

        void Sort( std::function< bool( T*, T* ) > compare );                       // Sort the array using the given comparision function

        void StableSort( std::function< bool( T*, T* ) > compare );                 // Stable sort the array using the given comparision function

        void ForEach( std::function< void( T* ) > Fn );                             // Apply the function to each of the GameObjects in the array
                                                                                    // ignoring any that are marked for deletion

        void UpdateLoggers() override;                                              // Update the ValueLogger that keeps tracks of the array size
};

#endif // ARRAYOFGAMEOBJECTS_HPP_INCLUDED
