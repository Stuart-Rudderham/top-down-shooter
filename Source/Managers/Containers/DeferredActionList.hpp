/*
 * This header defines a generic
 * container class for holding GameObjects
 * and performing an action on them sometime
 * in the future (e.g. 5 seconds from now)
 *
 * Author:      Stuart Rudderham
 * Created:     November 23, 2012
 * Last Edited: November 23, 2012
 */

#ifndef DEFERREDACTIONLIST_HPP_INCLUDED
#define DEFERREDACTIONLIST_HPP_INCLUDED

#include "Interfaces/Logging/LoggableObject.hpp"
#include "DebugTools/ValueLogger.hpp"
#include <SFML/System/Time.hpp>
#include <map>
#include <functional>

template<typename T>
class DeferredActionList : public LoggableObject {
    protected:
        std::multimap<sf::Time, T*> objects;                                        // The list of objects waiting to have an action performed on them, sorted
                                                                                    // by amount of time until expiration (earliest time is first)

        ValueLogger sizeLogger;                                                     // keeps track of the number of objects in the list over time

    public:
        DeferredActionList( const char* listName, sf::Int64 loggerScale );          // ctor. Takes in a name that describes what's in the list
                                                                                    // as well as a number for the scale of the logging graph

        virtual ~DeferredActionList();                                              // dtor. Makes sure the list is empty first

        unsigned int GetSize();                                                     // get how many GameObjects the list has

        bool InContainer( T* object );                                              // Predicate function to check if a given GameObject is currently in the list

        void AddGameObject( T* object, sf::Time timeUntilAction );                  // Add a GameObject to the list with the amount of time to wait before
                                                                                    // applying the action (e.g. 5 seconds)

        /* void RemoveGameObject( T* object ); */                                   // DON'T UNCOMMENT THIS
                                                                                    // We intentionally don't provide functionality to remove
                                                                                    // individual GameObjects because we always want to do a bulk
                                                                                    // removal of all marked GameObjects at the end of the frame

        void RemoveAllGameObjects();                                                // Clear the list

        void RemoveAllMarkedGameObjects();                                          // Remove all marked GameObjects from the list

        void ApplyAndRemoveExpiredObjects( std::function< void( T* ) > Fn );        // Apply the function any GameObject whos time has expired and
                                                                                    // then remove them from the list

        void UpdateLoggers() override;                                              // Update the ValueLogger that keeps tracks of the list size
};

#endif // DEFERREDACTIONLIST_HPP_INCLUDED
