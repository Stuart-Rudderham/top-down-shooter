/*
 * This file implements a generic
 * container class for holding GameObjects
 * that share an Interface
 *
 * Author:      Stuart Rudderham
 * Created:     September 30, 2012
 * Last Edited: November 23, 2012
 */

// ctor. Takes in a name that describes what's in the array
// as well as a number for the scale of the logging graph
template<typename T>
ArrayOfGameObjects<T>::ArrayOfGameObjects( const char* arrayName, sf::Int64 loggerScale ) : sizeLogger( arrayName, LogType::Manager, loggerScale )
{

}

// dtor. Makes sure the array is empty first
template<typename T>
ArrayOfGameObjects<T>::~ArrayOfGameObjects() {
    if( GetSize() != 0 ) {
        dbCrash( "Tried to destruct a non-empty ArrayOfGameObjects.  Name is ", sizeLogger.GetName() );
    }
}

// get how many objects the array has
 template<typename T>
unsigned int ArrayOfGameObjects<T>::GetSize() {
    return objects.size();
}

// Predicate function to check if a given GameObject is currently in the array
template<typename T>
bool ArrayOfGameObjects<T>::InContainer( T* object ) {
    return std::find( objects.begin(), objects.end(), object ) != objects.end();
}

// Add a GameObject to the back of the array
template<typename T>
void ArrayOfGameObjects<T>::AddGameObject( T* object ) {
    dbAssert( InContainer( object ) == false, "GameObject already in container" );
    objects.push_back( object );
}

// Clear the array
template<typename T>
void ArrayOfGameObjects<T>::RemoveAllGameObjects() {
    objects.clear();
}

// Remove all objects that aren't attached to anything
template<typename T>
void ArrayOfGameObjects<T>::RemoveAllMarkedGameObjects() {
    // Find all the GameObjects that have been marked for deletion and erase them (Remove-Erase Idiom)
    objects.erase( std::remove_if( objects.begin(), objects.end(), [=](T* x) { return x->ShouldIgnore(); } ), objects.end() );
}

// Sort the array using the given comparision function
template<typename T>
void ArrayOfGameObjects<T>::Sort( std::function< bool( T*, T* ) > compare ) {
    std::sort( objects.begin(), objects.end(), compare );
}

// Stable sort the array using the given comparision function
template<typename T>
void ArrayOfGameObjects<T>::StableSort( std::function< bool( T*, T* ) > compare ) {
    std::stable_sort( objects.begin(), objects.end(), compare );
}

// Apply the function to each of the objects in the set
// ignoring any that have a marked parent object
template<typename T>
void ArrayOfGameObjects<T>::ForEach( std::function< void( T* ) > Fn ) {
    for( auto i = objects.begin(); i != objects.end(); ++i ) {
        if( (**i).ShouldIgnore() == false ) {
            Fn( *i );
        }
    }
}

// Update the ValueLogger that keeps tracks of the array size
template<typename T>
void ArrayOfGameObjects<T>::UpdateLoggers() {
    sizeLogger.AddNewValue( GetSize() );
}
