/*
 * This header defines a container class
 * for holding CollidableObjects. It
 * provides some specialized collision
 * detection and resolution functions
 *
 * Author:      Stuart Rudderham
 * Created:     October 17, 2012
 * Last Edited: November 23, 2012
 */

#ifndef ARRAYOFCOLLIDABLEOBJECTS_HPP_INCLUDED
#define ARRAYOFCOLLIDABLEOBJECTS_HPP_INCLUDED

#include "Managers/Containers/ArrayOfGameObjects.hpp"
#include <bitset>
#include <vector>

// Forward Declarations
class CollidableObject;

typedef std::pair<CollidableObject*, CollidableObject*> CollidableObjectPair;               // a pair of CollidableObjects that may be colliding
typedef bool CollisionFilter[CollidableObjectType::Max][CollidableObjectType::Max];         // a 2D array of bools that defines what CollidableObjects are allowed to collide
typedef std::bitset<CollidableObjectType::Max> TypeFilter;                                  // a bitset that defines what CollidableObjects are allowed to be added to the list

class ArrayOfCollidableObjects : public ArrayOfGameObjects<CollidableObject> {
    protected:
        TypeFilter typeFilter;                                                              // only add types we care about (e.g. Character but not StaticGeo)
        CollisionFilter& collisionFilter;                                                   // make sure certain types of GameObjects can't collide (e.g. two FieldOfViews)

        std::vector<CollidableObjectPair> possibleCollisions;                               // an intermediate array used to hold pairs of objects that may be colliding

        // Stats
        unsigned int naiveBroadPhaseChecks;                                                 // holds the number of broad phase checks done if using a naive implementation. Used for comparision
        unsigned int broadPhaseCollisionChecks;                                             // holds the number of collision checks done in the broad phase check
        unsigned int narrowPhaseCollisionChecks;                                            // holds the number of collision checks done in the narrow phase check
        unsigned int numCollisions;                                                         // the total number of collisions that had to be resolved

    public:
        ArrayOfCollidableObjects( const char* arrayName,                                    // ctor. Takes in a type and collision filter
                                  sf::Int64 loggerScale,
                                  unsigned long objectTypes,
                                  CollisionFilter& filter );

        bool CheckTypeFilter( CollidableObject* object );                                   // returns true if CollidableObject is a type that we care about (according to the TypeFilter)

        bool CheckCollisionFilter( CollidableObject* A, CollidableObject* B );              // returns true if the two objects are allowed to collide (according to the CollisionFilter)

        bool CheckObjectSpecificFilter( CollidableObject* A, CollidableObject* B );         // returns true if the two objects want to collide with each other (e.g. FOV doesn't want to
                                                                                            // collide with allied Characters)

        void BroadPhaseCheck();                                                             // Scans the array of CollidableObjects and returns a filtered
                                                                                            // list of objects that are possibly colliding

        void NarrowPhaseCheck();                                                            // This takes in the filtered list and does a more expensive test to
                                                                                            // see if they are colliding and, if so, resolve the collision

        void PrintAndClearStats();                                                          // print out recorded stats and reset them to 0
};

#endif // ARRAYOFCOLLIDABLEOBJECTS_HPP_INCLUDED
