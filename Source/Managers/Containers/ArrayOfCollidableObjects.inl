/*
 * This header defines a container class
 * for holding CollidableObjects. It
 * provides some specialized collision
 * detection and resolution functions
 *
 * Author:      Stuart Rudderham
 * Created:     October 17, 2012
 * Last Edited: November 23, 2012
 */

#include "Math/Geometry/CollisionDetection.hpp"
#include <algorithm>

using namespace std;
using namespace sf;

ArrayOfCollidableObjects::ArrayOfCollidableObjects( const char* arrayName, sf::Int64 loggerScale, unsigned long objectTypes, CollisionFilter& filter )
    : ArrayOfGameObjects<CollidableObject>( arrayName, loggerScale ),
      typeFilter( objectTypes ),
      collisionFilter( filter ),
      naiveBroadPhaseChecks( 0 ),
      broadPhaseCollisionChecks( 0 ),
      narrowPhaseCollisionChecks( 0 ),
      numCollisions( 0 )
{

}


// returns true if CollidableObject is a type that we care about
bool ArrayOfCollidableObjects::CheckTypeFilter( CollidableObject* object ) {
    return typeFilter[ object->GetCollidableObjectType() ];
}

// returns true if the two objects are allowed to collide
bool ArrayOfCollidableObjects::CheckCollisionFilter( CollidableObject* A, CollidableObject* B ) {
    return collisionFilter[ A->GetCollidableObjectType() ][ B->GetCollidableObjectType() ];
}

bool ArrayOfCollidableObjects::CheckObjectSpecificFilter( CollidableObject* A, CollidableObject* B ) {
    return A->CanCollideWith( B ) && B->CanCollideWith( A );
}

// Scans the array of CollidableObjects and returns
// a filtered list of objects that are possibly colliding
void ArrayOfCollidableObjects::BroadPhaseCheck() {

    // Reset the list of possible collisions
    possibleCollisions.clear();

    unsigned int N = GetSize();

    // calculate N choose 2, which is how many checks are done
    // if using the naive, brute force way
    naiveBroadPhaseChecks += ( ( N * N ) - N ) / 2;

    // Sort the CollidableObjects by their position on the x-axis
    Sort(
          [](CollidableObject* A, CollidableObject* B) -> bool {
              AABB* ABox = A->GetBoundingBox();
              AABB* BBox = B->GetBoundingBox();

              return ABox->GetCenter().x - ABox->GetHalfWidth() < BBox->GetCenter().x - BBox->GetHalfWidth();
          }
        );

    // Loop through all the CollidableObjects
    for( unsigned int i = 0; i < N; ++i ) {

        // ignore marked objects
        if( objects[i]->ShouldIgnore() ) {
            continue;
        }

        for( unsigned int j = i + 1; j < N; ++j ) {

            // ignore marked objects
            if( objects[j]->ShouldIgnore() ) {
                continue;
            }

            broadPhaseCollisionChecks++;
            
            // If they don't overlap on the x-axis we can stop, since the bounding boxes were ordered by x position
            if( BoundingBoxesOverlapXCoord( objects[i]->GetBoundingBox(), objects[j]->GetBoundingBox() ) == false ) {
                break;
            }

            // If they overlap on the x-axis and y-axis then their bounding boxes are colliding
            // If the collision and object-specific filters pass then
            if( BoundingBoxesOverlapYCoord( objects[i]->GetBoundingBox(), objects[j]->GetBoundingBox() ) &&
                CheckCollisionFilter      ( objects[i]                  , objects[j]                   ) &&
                CheckObjectSpecificFilter ( objects[i]                  , objects[j]                   ) )
            {
                // add them to the list of possible collisions
                possibleCollisions.push_back( std::make_pair(objects[i], objects[j]) );
            }
        }
    }
}


// this goes through the filtered list of objects and does a more
// expensive check to see if they are actually colliding. If so it
// then resolves the collision
void ArrayOfCollidableObjects::NarrowPhaseCheck() {

    // A 2D array of const function pointers that are initialized to the different shape collision functions
    // The array is indexed with the type of each shape, so the correct function is always called
    static bool (* const rigidBodyCollisionFunction[GeometricShape::NumRigidBody][GeometricShape::NumRigidBody])(BaseShape*, BaseShape*, CollisionResponse, Vector2f&, Vector2f&, Vector2f&)
        = { { BoxBoxCollision,     BoxCircleCollision,     BoxPolygonCollision     },
            { CircleBoxCollision,  CircleCircleCollision,  CirclePolygonCollision  },
            { PolygonBoxCollision, PolygonCircleCollision, PolygonPolygonCollision } };

    // An array of const function pointers that are initialized to the different ray casting functions
    // The array is indexed with the type of rigid body for the non-ray shape, so the correct function is always called
    static bool (* const rayCastingCollisionFunction[GeometricShape::NumRigidBody])(BaseShape*, BaseShape*, Point2f&, Vector2f&)
        = { CastRayAtAABB, CastRayAtCircle, CastRayAtConvexPolygon };

    // Take the possible candidates and do a more complex check to see
    // if they are actually colliding, and if so resolve the collision
    for( auto i = possibleCollisions.begin(); i != possibleCollisions.end(); ++i ) {
        narrowPhaseCollisionChecks++;

        CollidableObject* A = i->first;
        CollidableObject* B = i->second;

        // If either object is marked (probably because of the
        // result of an earlier collision) then do nothing
        if( A->ShouldIgnore() || B->ShouldIgnore() ) {
            continue;
        }

        // If both objects have rigid bodies then do the regular collision detection
        if( A->HasRigidBody() && B->HasRigidBody() ) {

            // Default collision response is to move both shapes
            CollisionResponse collisionResponse = CollisionResponse::MoveBoth;

            // The CollisionFilter should have made sure they both aren't immovable
            dbAssert( ( A->IsImmovable() && B->IsImmovable() ) == false, "Two immovable objects are colliding with each other. Shouldn't have gotten here, check the relavent collision filter" );

            if( A->IsPhantom() || B->IsPhantom() ) {
                collisionResponse = CollisionResponse::MoveNeither;
            } else if( A->IsMovedBy( B ) ) {
                collisionResponse = CollisionResponse::MoveFirst;
            } else if( B->IsMovedBy( A ) ) {
                collisionResponse = CollisionResponse::MoveSecond;
            }

            // Make sure the two obejcts have the correct type of rigid body
            dbAssert( (A->GetCollisionOutline()->IsAABB()) ||
                      (A->GetCollisionOutline()->IsCircle()) ||
                      (A->GetCollisionOutline()->IsConvexPolygon()),
                      "Tried to use rigid body collision function array with invalid shape. Shape type is ", A->GetCollisionOutline()->GetGeometricShape() );

            dbAssert( (B->GetCollisionOutline()->IsAABB()) ||
                      (B->GetCollisionOutline()->IsCircle()) ||
                      (B->GetCollisionOutline()->IsConvexPolygon()),
                      "Tried to use rigid body collision function array with invalid shape. Shape type is ", B->GetCollisionOutline()->GetGeometricShape() );

            Vector2f mtv1;
            Vector2f mtv2;
            Vector2f normal;

            // If the shapes are colliding then resolve the collision
            if( rigidBodyCollisionFunction[ A->GetCollisionOutline()->GetGeometricShape() ][ B->GetCollisionOutline()->GetGeometricShape() ](A->GetCollisionOutline(),
                                                                                                                                             B->GetCollisionOutline(),
                                                                                                                                             collisionResponse,
                                                                                                                                             mtv1,
                                                                                                                                             mtv2,
                                                                                                                                             normal) )
            {
                // Call the specialized collision response code for each object
                A->CollisionResponse( B, mtv1, normal );
                B->CollisionResponse( A, mtv2, normal );

                // Keep track of stats
                numCollisions++;
            }

        // Otherwise do a ray cast
        } else {

            // Make sure the ray is the first object
            if( A->HasRigidBody() ) {
                swap( A, B );
            }

            dbAssert( A->GetCollisionOutline()->IsLineSegment(), "Shape A isn't a LineSegment. Type is ", A->GetCollisionOutline()->GetGeometricShape() );

            dbAssert( (B->GetCollisionOutline()->IsAABB()) ||
                      (B->GetCollisionOutline()->IsCircle()) ||
                      (B->GetCollisionOutline()->IsConvexPolygon()),
                      "Tried to use ray casting collision function array with invalid shape. Shape type is ", B->GetCollisionOutline()->GetGeometricShape() );

            Point2f intersectionPoint;
            Vector2f normal;

            // If the shapes are colliding then resolve the collision
            if( rayCastingCollisionFunction[B->GetCollisionOutline()->GetGeometricShape()]( A->GetCollisionOutline(), B->GetCollisionOutline(), intersectionPoint, normal ) ) {

                A->CollisionResponse( B, intersectionPoint, normal );
                B->CollisionResponse( A, intersectionPoint, normal );

                numCollisions++;
            }
        }
    }
}

void ArrayOfCollidableObjects::PrintAndClearStats() {
    dbPrintToScreen( "", sizeLogger.GetName().c_str()                                                          );
    dbPrintToScreen( "\tNum Objects: ", GetSize()                                                                       );
    dbPrintToScreen( "\tBroad Sweep: ", broadPhaseCollisionChecks                                                       );
    dbPrintToScreen( "\t\tNaive Sweep: ", naiveBroadPhaseChecks                                                         );
    dbPrintToScreen( "\t\tImprovement:  ", static_cast<float>( naiveBroadPhaseChecks ) / broadPhaseCollisionChecks      );
    dbPrintToScreen( "\tNarrow Sweep: ", narrowPhaseCollisionChecks                                                     );
    dbPrintToScreen( "\t\tCollisions: ", numCollisions                                                                  );
    dbPrintToScreen( "\t\tBounding Volume Accuracy: ", static_cast<float>( numCollisions ) / narrowPhaseCollisionChecks );
    dbPrintToScreen( ""  );

    // Clear all the stats
    naiveBroadPhaseChecks = 0;
    broadPhaseCollisionChecks = 0;
    narrowPhaseCollisionChecks = 0;
    numCollisions = 0;
}
