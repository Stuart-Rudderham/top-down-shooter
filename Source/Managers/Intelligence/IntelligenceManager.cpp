/*
 * This file implements the manager that
 * handles updating the logic (both AI
 * and user) of objects
 *
 * Author:      Stuart Rudderham
 * Created:     September 1, 2012
 * Last Edited: November 1, 2012
 */

#include "Managers/Intelligence/IntelligenceManager.hpp"
#include "Interfaces/Intelligence/IntelligentObject.hpp"
#include "GameObjects/Characters/Controllers/AI/AIController.hpp"
#include "Managers/Containers/ArrayOfGameObjects.inl"

IntelligenceManager::IntelligenceManager() : intelligentObjects( "IntelligenceManager", 5000 )
{

}

// Add a new IntelligentObject to the Manager
void IntelligenceManager::AddGameObject( IntelligentObject* object ) {
    intelligentObjects.AddGameObject( object );
}

// Return true if the given IntelligentObject is in the Manager
bool IntelligenceManager::InManager( IntelligentObject* object ) {
    return intelligentObjects.InContainer( object );
}

// Update the internal logic of all IntelligentObjects
void IntelligenceManager::UpdateLogic() {
    intelligentObjects.ForEach( [](IntelligentObject* Obj){ Obj->UpdateLogic(); } );
}

// Do any end-of-frame cleanup
void IntelligenceManager::Cleanup() {
    intelligentObjects.RemoveAllMarkedGameObjects();

    // Move and rotate the FOV to the parent object
    intelligentObjects.ForEach( [](IntelligentObject* Obj){
        if( Obj->IsBot() ) {
            Obj->GetControllerAs<AIController>()->SnapFOVToViewer();
        }
    } );
}

// Remove all GameObjects from the Manager
void IntelligenceManager::RemoveAllGameObjects() {
    intelligentObjects.RemoveAllGameObjects();
}
