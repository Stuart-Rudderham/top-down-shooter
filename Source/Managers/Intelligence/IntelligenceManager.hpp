/*
 * This header defines the manager that
 * handles updating the logic (both AI
 * and user) of objects
 *
 * Author:      Stuart Rudderham
 * Created:     September 1, 2012
 * Last Edited: November 1, 2012
 */

#ifndef INTELLIGENCEMANAGER_HPP_INCLUDED
#define INTELLIGENCEMANAGER_HPP_INCLUDED

#include "Singleton.hpp"
#include "Managers/Containers/ArrayOfGameObjects.hpp"

class IntelligentObject;

class IntelligenceManager : public Singleton<IntelligenceManager> {
    private:
        ArrayOfGameObjects<IntelligentObject> intelligentObjects;   // The list of IntelligentObjects

    public:
        IntelligenceManager();

        void AddGameObject( IntelligentObject* object );            // Add a new IntelligentObject to the Manager
        bool InManager    ( IntelligentObject* object );            // Return true if the given IntelligentObject is in the Manager. It
                                                                    // is only used for debugging/sanity-check purposes so it doesn't
                                                                    // matter that it takes O(n) time

        void UpdateLogic();                                         // Update the internal logic of all IntelligentObjects

        void Cleanup();                                             // Do any end-of-frame cleanup (such as removing marked GameObjects)
        void RemoveAllGameObjects();                                // Remove all GameObjects from the Manager
};

#endif // INTELLIGENCEMANAGER_HPP_INCLUDED
