/*
 * This file implements the manager that
 * handles all the loggers in the game
 *
 * Author:      Stuart Rudderham
 * Created:     September 2, 2012
 * Last Edited: November 22, 2012
 */

#include "Managers/Logging/LoggingManager.hpp"
#include "Interfaces/Logging/LoggableObject.hpp"

using namespace sf;
using namespace std;

LoggingManager::LoggingManager()
{
    graphFilter[LogType::Timer]     = true;
    graphFilter[LogType::Manager]   = true;
    graphFilter[LogType::Factory]   = true;
    graphFilter[LogType::Pool]      = true;
}

void LoggingManager::AddLoggableObject( LoggableObject* object ) {
#if ENABLE_LOGGING
    loggableObjects.push_back( object );
#endif
}

void LoggingManager::AddValueLogger( ValueLogger* logger ) {
#if ENABLE_LOGGING
    valueLoggers.push_back( logger );

    // Sorting everytime something is added is expensive, but ok in this case
    // since all ValueLoggers should be added at the beginning of the program
    // and not every frame. We have a major sort on logger type and a minor sort
    // on logger name
    stable_sort( valueLoggers.begin(), valueLoggers.end(), [](ValueLogger* A, ValueLogger* B) -> bool { return A->GetName()    < B->GetName();    } );
    stable_sort( valueLoggers.begin(), valueLoggers.end(), [](ValueLogger* A, ValueLogger* B) -> bool { return A->GetLogType() < B->GetLogType(); } );
#endif
}

void LoggingManager::UpdateLoggableObjects() {
#if ENABLE_LOGGING
    for( auto i = loggableObjects.begin(); i != loggableObjects.end(); ++i ) {
        (**i).UpdateLoggers();
    }
#endif
}

// draw the graphs of all the logged values
void LoggingManager::DrawGraphs( sf::RenderTarget& Target, Point2f topLeft ) {
#if ENABLE_LOGGING
    float left = topLeft.x;
    float top = topLeft.y;

    // Draw as many graphs vertically as possible, then move over horizontally and continue drawing
    for( auto i = valueLoggers.begin(); i != valueLoggers.end(); ++i ) {

        // Don't draw the graph if the appropriate option isn't set
        if( graphFilter[ (**i).GetLogType() ] == true ) {
            (**i).DrawGraph( Target, Point2f( left, top ), DebugOptions::GraphSize );

            top += DebugOptions::GraphSize.y;

            if( top > Target.getSize().y - DebugOptions::GraphSize.y ) {
                top = topLeft.y;
                left += DebugOptions::GraphSize.x;
            }
        }
    }
#endif
}
