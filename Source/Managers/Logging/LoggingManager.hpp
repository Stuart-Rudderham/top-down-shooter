/*
 * This header defines the manager that
 * handles all the loggers in the game
 *
 * Author:      Stuart Rudderham
 * Created:     September 2, 2012
 * Last Edited: November 22, 2012
 */

#ifndef LOGGINGMANAGER_HPP_INCLUDED
#define LOGGINGMANAGER_HPP_INCLUDED

#include "Singleton.hpp"
#include "DebugTools/ValueLogger.hpp"
#include <bitset>

// Forward Declarations
class LoggableObject;

typedef std::bitset<LogType::Max> GraphFilter;

class LoggingManager : public Singleton<LoggingManager> {
    private:
        std::vector<LoggableObject*> loggableObjects;
        std::vector<ValueLogger*> valueLoggers;
        GraphFilter graphFilter;                                                // let you not draw certain graphs (e.g. Factory size)

    public:
        LoggingManager();

        // There is no mechanism for removing objects from the
        // Manager, so don't delete LoggableObjects or ValueLoggers
        // or there will be dangling pointers
        void AddLoggableObject( LoggableObject* object );
        void AddValueLogger( ValueLogger* logger );

        void UpdateLoggableObjects();
        void DrawGraphs( sf::RenderTarget& Target, Point2f topLeft );           // draw the graphs of all the logged values
};

#endif // LOGGINGMANAGER_HPP_INCLUDED
