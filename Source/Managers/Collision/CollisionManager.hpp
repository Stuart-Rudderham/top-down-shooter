/*
 * This header defines the manager that
 * handles detecting and resolving
 * collisions between objects in the game
 *
 * Author:      Stuart Rudderham
 * Created:     February 25, 2012
 * Last Edited: December 25, 2012
 */

#ifndef COLLISIONMANAGER_HPP_INCLUDED
#define COLLISIONMANAGER_HPP_INCLUDED

#include "Singleton.hpp"
#include "Managers/Containers/ArrayOfCollidableObjects.hpp"
#include <array>

class CollidableObject;

class CollisionManager : public Singleton<CollisionManager> {
    private:
        ArrayOfCollidableObjects physicalObjects;                   // this is an array of all the Characters, StaticGeo, and Projectiles
                                                                    // that we care about detecting and resolving the collision of

        ArrayOfCollidableObjects fieldOfViewObjects;                // this is an array of all the Characters, and FieldOfViews so that we
                                                                    // can check what Characters are in a given FieldOfView for the AI

        ArrayOfCollidableObjects lineOfSightObjects;                // this is an array of all the StaticGeo, LineOfSights, and possibly Characters so that we
                                                                    // can check if a character has an unobstructed line of sight to another character

        ArrayOfCollidableObjects miniMapObjects;                    // this is an array of all the Characters and the single MiniMap collision circle so 
                                                                    // that we can check what Characters are near the MainCharacter

        std::array<ArrayOfCollidableObjects*, 4> arrayOfArrays;     // An array of all the different collision lists the Manager keeps track of.
                                                                    // Not strictly necessary, but it makes any code that has to do something
                                                                    // to each of the lists a lot neater

    public:
        CollisionManager();

        void AddGameObject( CollidableObject* object );             // Add a new CollidableObject to the Manager
        bool InManager    ( CollidableObject* object );             // Return true if the given CollidableObject is in the Manager. It
                                                                    // is only used for debugging/sanity-check purposes so it doesn't
                                                                    // matter that it takes O(n) time

        void ScanLineOfSights();                                    // detects and resolves collisions between LineOfSights, StaticGeo, and possibly Characters
        void ScanFieldOfViews();                                    // detects and resolves collisions between FieldOfViews and Characters
        void DetectAndResolveCollisions();                          // detects and resolves all collisions between all physical objects (i.e. Characters, StaticGeo, and Projectiles)
        void UpdateMiniMap();                                       // detects and resolves all collisions between Characters and the MiniMap collision circle
        
        void DrawCollisionOutlines( sf::RenderTarget& Target );     // Draw all the collision outlines. Used for debugging

        void PrintAndClearStats();                                  // Printing and clearing the stats for all the arrays

        void Cleanup();                                             // Do any end-of-frame cleanup (such as removing marked GameObjects)
        void RemoveAllGameObjects();                                // Remove all GameObjects from the Manager
};

#endif // COLLISIONMANAGER_HPP_INCLUDED
