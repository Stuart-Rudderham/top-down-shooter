/*
 * This file implements the manager that
 * handles detecting and resolving
 * collisions between objects in the game
 *
 * Author:      Stuart Rudderham
 * Created:     February 25, 2012
 * Last Edited: December 25, 2012
 */

#include "Managers/Collision/CollisionManager.hpp"
#include "Interfaces/Collision/CollidableObject.hpp"

#include "Managers/Containers/ArrayOfGameObjects.inl"
#include "Managers/Containers/ArrayOfCollidableObjects.inl"

using namespace std;
using namespace sf;

// Collision filter for resolving physical collisions
// Only care about collisions between Characters, StaticGeo, and Projectiles
static bool physicalObjectsCollisionFilter[CollidableObjectType::Max][CollidableObjectType::Max] 
    /*  | Character | StaticGeo | Projectile | FieldOfView | LineOfSight | MiniMap | */
    = { { true      , true      , true       , false       , false       , false   },         // Character
        { true      , false     , true       , false       , false       , false   },         // StaticGeo
        { true      , true      , false      , false       , false       , false   },         // Projectile
        { false     , false     , false      , false       , false       , false   },         // FieldOfView
        { false     , false     , false      , false       , false       , false   },         // LineOfSight
        { false     , false     , false      , false       , false       , false   } };       // MiniMap
        
// Collision filter for resolving non-physical collisions
// For example, doing line-of-sight checks between Characters 
// while updating the AI or seeing what Characters are close
// to the main character when updating the mini-map
static bool logicObjectsCollisionFilter[CollidableObjectType::Max][CollidableObjectType::Max]
    /*  | Character | StaticGeo | Projectile | FieldOfView | LineOfSight | MiniMap | */
    = { { false     , false     , false      , true        , true        , true    },         // Character
        { false     , false     , false      , false       , true        , false   },         // StaticGeo
        { false     , false     , false      , false       , false       , false   },         // Projectile
        { true      , false     , false      , false       , false       , false   },         // FieldOfView
        { true      , true      , false      , false       , false       , false   },         // LineOfSight
        { true      , false     , false      , false       , false       , false   } };       // MiniMap
        
CollisionManager::CollisionManager()
    : physicalObjects   ( "Physics Collision Objects", 5000, 1ul << CollidableObjectType::Character   | 1ul << CollidableObjectType::StaticGeo    | 1ul << CollidableObjectType::Projectile  , physicalObjectsCollisionFilter ),
      fieldOfViewObjects( "FOV Collision Objects"    , 5000, 1ul << CollidableObjectType::Character   | 1ul << CollidableObjectType::FieldOfView                                             , logicObjectsCollisionFilter    ),
      lineOfSightObjects( "LOS Collision Objects"    , 5000, 1ul << CollidableObjectType::LineOfSight | 1ul << CollidableObjectType::StaticGeo  /*| 1ul << CollidableObjectType::Character */, logicObjectsCollisionFilter    ),
      miniMapObjects    ( "MiniMap Collision Objects", 5000, 1ul << CollidableObjectType::Character   | 1ul << CollidableObjectType::MiniMap                                                 , logicObjectsCollisionFilter    )
{
    arrayOfArrays[0] = &physicalObjects;
    arrayOfArrays[1] = &fieldOfViewObjects;
    arrayOfArrays[2] = &lineOfSightObjects;
    arrayOfArrays[3] = &miniMapObjects;
}

// Add a new CollidableObject to the Manager
void CollisionManager::AddGameObject( CollidableObject* object ) {
    for( auto& array: arrayOfArrays ) {
        if( array->CheckTypeFilter( object ) ) {
            array->AddGameObject( object );
        }    
    }
}

// Return true if the given CollidableObject is in the Manager
bool CollisionManager::InManager( CollidableObject* object ) {
    for( auto& array: arrayOfArrays ) {
        if( array->InContainer( object ) ) {
            return true;
        }    
    }

    return false;
}

// detects and resolves all collisions between all physical objects (i.e. Characters, StaticGeo, and Projectiles)
void CollisionManager::DetectAndResolveCollisions() {

    physicalObjects.BroadPhaseCheck();
    physicalObjects.NarrowPhaseCheck();
}

// detects and resolves collisions between FieldOfViews and Characters
void CollisionManager::ScanFieldOfViews() {

    fieldOfViewObjects.BroadPhaseCheck();
    fieldOfViewObjects.NarrowPhaseCheck();
}

// detects and resolves collisions between LineOfSights, StaticGeo, and possibly Characters
void CollisionManager::ScanLineOfSights() {

    lineOfSightObjects.BroadPhaseCheck();
    lineOfSightObjects.NarrowPhaseCheck();
}

// detects and resolves all collisions between Characters and the MiniMap collision circle
void CollisionManager::UpdateMiniMap() {

    miniMapObjects.BroadPhaseCheck();
    miniMapObjects.NarrowPhaseCheck();
}

// Printing and clearing the stats for all the arrays
void CollisionManager::PrintAndClearStats() {
    for( auto& array: arrayOfArrays ) {
        array->PrintAndClearStats();
    }
}

// Draw all the collision outlines. Used for debugging
void CollisionManager::DrawCollisionOutlines( sf::RenderTarget& Target ) {
    for( auto& array: arrayOfArrays ) {
        array->ForEach( [&](CollidableObject* Obj){ Obj->DrawCollisionOutline( Target, Drawing::CollisionOutlineColor, Drawing::BoundingBoxColor ); } );
    }
}

// Do any end-of-frame cleanup (such as removing marked GameObjects)
void CollisionManager::Cleanup() {
    for( auto& array: arrayOfArrays ) {
        array->RemoveAllMarkedGameObjects();
    }
}

// Remove all GameObjects from the Manager
void CollisionManager::RemoveAllGameObjects() {
    for( auto& array: arrayOfArrays ) {
        array->RemoveAllGameObjects();
    }
}
