/*
 * This header defines a filter for
 * deciding which types of objects can
 * collide
 *
 * Author:      Stuart Rudderham
 * Created:     December 27, 2012
 * Last Edited: December 27, 2012
 */

#ifndef COLLISIONFILTER_HPP_INCLUDED
#define COLLISIONFILTER_HPP_INCLUDED

#include <initializer_list>
#include <utility>

class CollidableObject;
typedef std::pair<CollidableObjectType::Enum, CollidableObjectType::Enum> TypePair;

class CollisionFilter {
    private:
        bool filter[CollidableObjectType::Max][CollidableObjectType::Max];      // The 2D array that is used to check what
                                                                                // types are allowed to collide

    public:
        CollisionFilter( std::initializer_list<TypePair> types ) {
            // By default nothing can collide
            for( unsigned int i = 0; i < CollidableObjectType::Max; ++i ) {
                for( unsigned int j = 0; j < CollidableObjectType::Max; ++j ) {
                    filter[i][j] = false;
                }
            }

            // Selectively enable the types that can collide
            for( auto pair = types.begin(); pair != types.end(); ++pair ) {
                filter[pair->first][pair->second] = true;
                filter[pair->second][pair->first] = true;
            }
        }
                                                                                
        inline bool Check( CollidableObject* A, CollidableObject* B ) {
            filter[ A->GetCollidableObjectType() ][ B->GetCollidableObjectType() ]
        }
};

#endif // COLLISIONFILTER_HPP_INCLUDED
