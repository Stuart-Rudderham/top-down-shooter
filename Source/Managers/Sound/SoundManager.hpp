/*
 * This header defines the manager that
 * handles the playing of sounds and music
 *
 * Author:      Stuart Rudderham
 * Created:     December 11, 2012
 * Last Edited: December 11, 2012
 */

#ifndef SOUNDMANAGER_HPP_INCLUDED
#define SOUNDMANAGER_HPP_INCLUDED

#include "Singleton.hpp"

#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

#include <array>
#include <bitset>

class SoundManager : public Singleton<SoundManager> {
    private:
        sf::Music backgroundMusic;                                      // The currently playing background music

        std::array<sf::Sound, 32> soundFX;                              // The list of currently playing sound effects. OpenAL has a hard limit, so we
                                                                        // want to make sure we don't have too many sounds playing at once

        unsigned int index;                                             // The next index that will be used to play a sound

    public:
        SoundManager();
        ~SoundManager();

        void PlaySoundFX( const ExternalFile& file );                   // Play a new sound effect
        void PlayBackgroundMusic( const ExternalFile& file );           // Start playing the given music

        void PauseEverything();
        void UnpauseEverything();

        void StopEverything();
};

#endif // SOUNDMANAGER_HPP_INCLUDED
