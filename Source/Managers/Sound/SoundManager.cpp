/*
 * This file implements the manager that
 * handles the playing of sounds and music
 *
 * Author:      Stuart Rudderham
 * Created:     December 11, 2012
 * Last Edited: December 11, 2012
 */


#include "Managers/Sound/SoundManager.hpp"
#include "MediaCache/MediaCache.hpp"

SoundManager::SoundManager() : index( 0 )
{

}

SoundManager::~SoundManager() {
    StopEverything();
}

void SoundManager::PlaySoundFX( const ExternalFile& file ) {

    // Hopefully we don't interrupt an already playing sound effect
    soundFX[index].setBuffer( SoundCache::Get().Load( file ) );
    soundFX[index].play();

    index = ( index + 1 ) % soundFX.size();
}

void SoundManager::PlayBackgroundMusic( const ExternalFile& file ) {

    backgroundMusic.stop();

    // We don't want to use the MediaCache here, as we cannot
    // have that many OpenAL sources so we don't want to cache
    // unused ones
    bool result = backgroundMusic.openFromFile( file );

    dbAssert( result == true, "Unable to load background music. Given file was: ", file );
    (void)result;

    backgroundMusic.play();
}

void SoundManager::PauseEverything() {
    backgroundMusic.pause();

    for( unsigned int i = 0; i < soundFX.size(); ++i ) {
        soundFX[i].pause();
    }
}

void SoundManager::UnpauseEverything() {
    backgroundMusic.play();

    for( unsigned int i = 0; i < soundFX.size(); ++i ) {
        soundFX[i].play();
    }
}

void SoundManager::StopEverything() {
    backgroundMusic.stop();

    for( unsigned int i = 0; i < soundFX.size(); ++i ) {
        soundFX[i].stop();
    }
}
