/*
 * This header defines the manager that
 * handles monitoring input from the user
 *
 * Author:      Stuart Rudderham
 * Created:     November 3, 2012
 * Last Edited: November 3, 2012
 */

#ifndef INPUTMANAGER_HPP_INCLUDED
#define INPUTMANAGER_HPP_INCLUDED

#include "Singleton.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <vector>

// Forward Declarations
class KeyboardState;
class MouseState;

class InputManager : public Singleton<InputManager> {
    private:
        std::vector<KeyboardState*>     keysMonitored;                          // the list of unique keyboard keys the Manager is listening to and updating
        std::vector<MouseState*>        buttonsMonitored;                       // the list of unique mouse buttons the Manager is listening to and updating

        bool UniqueInputs();                                                    // sanity check function return true if all monitored inputs are unique

    public:
        InputManager();                                                         // ctor. Makes sure the Allocator for KeyboardState and MouseState is created
        ~InputManager();                                                        // dtor. Deletes all the monitored inputs

        const KeyboardState* ListenForKey   ( sf::Keyboard::Key key    );       // add a new keyboard key to listen for
        const MouseState*    ListenForButton( sf::Mouse::Button button );       // add a new mouse button to listen for

        void UpdateInputs( sf::RenderWindow& GameWindow );                      // update all monitored keys and buttons
};

#endif // INPUTMANAGER_HPP_INCLUDED
