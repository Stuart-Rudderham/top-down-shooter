/*
 * This file implements the manager that
 * handles monitoring input from the user
 *
 * Author:      Stuart Rudderham
 * Created:     November 3, 2012
 * Last Edited: January 30, 2013
 */

#include "Managers/Input/InputManager.hpp"
#include "UpgradesToSFML/Input.hpp"

// ctor
InputManager::InputManager() {

    // Since we access the Allocator for KeyboardState and MouseState in the
    // destructor of the InputManager we want to make sure we access it in the
    // constructor as well, so that the Allocators don't get accessed after they
    // are destructed. It's gross, but we gotta do it

	// Need these because apparently Visual Studio doesn't handle rvalue-references to
	// enums very well. Was getting weird template errors
	sf::Keyboard::Key  k =  sf::Keyboard::Key::A;
	sf::Mouse::Button  b =  sf::Mouse::Button::Left;

	KeyboardState::Destroy( KeyboardState::Create( k ) );
    MouseState::Destroy( MouseState::Create( b ) );
}

// dtor. Deletes all the monitored inputs
InputManager::~InputManager() {
    for( KeyboardState* key : keysMonitored ) {
        KeyboardState::Destroy( key );
    }

    for( MouseState* button : buttonsMonitored ) {
        MouseState::Destroy( button );
    }
}

// Update all the monitored inputs
void InputManager::UpdateInputs( sf::RenderWindow& GameWindow ) {

    // Update the location of the mouse in game coords
    MouseState::UpdateMouseCoords( GameWindow );

    // Update all the inputs we're listening for
    for( KeyboardState* key : keysMonitored ) {
        key->Update();
    }

    for( MouseState* button : buttonsMonitored ) {
        button->Update();
    }
}

const KeyboardState* InputManager::ListenForKey( sf::Keyboard::Key key ) {

    // Add a new keyboard key to listen for
    KeyboardState* listener = KeyboardState::Create( key );
    keysMonitored.push_back( listener );

    // Make sure it's unique
    dbAssert( UniqueInputs(), "Duplicate key ", key );

    // Return a const reference so the caller can
    // monitor the state of the key but not update it
    return listener;
}

const MouseState* InputManager::ListenForButton( sf::Mouse::Button button ) {

    // Add a new keyboard key to listen for
    MouseState* listener = MouseState::Create( button );
    buttonsMonitored.push_back( listener );

    // Make sure it's unique
    dbAssert( UniqueInputs(), "Duplicate button ", button );

    // Return a const reference so the caller can
    // monitor the state of the key but not update it
    return listener;
}

bool InputManager::UniqueInputs() {

    // Make sure each keyboard key monitored is unique
    for( auto i = keysMonitored.begin(); i != keysMonitored.end(); ++i ) {
        for( auto j = i + 1; j != keysMonitored.end(); ++j ) {
            if( (**i) == (**j) ) {
                return false;
            }
        }
    }

    // Make sure each mouse button monitored is unique
    for( auto i = buttonsMonitored.begin(); i != buttonsMonitored.end(); ++i ) {
        for( auto j = i + 1; j != buttonsMonitored.end(); ++j ) {
            if( (**i) == (**j) ) {
                return false;
            }
        }
    }

    return true;
}
