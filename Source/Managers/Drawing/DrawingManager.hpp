/*
 * This header defines the manager that
 * handles the drawing of objects
 *
 * Author:      Stuart Rudderham
 * Created:     August 2, 2012
 * Last Edited: October 31, 2012
 */

#ifndef DRAWINGMANAGER_HPP_INCLUDED
#define DRAWINGMANAGER_HPP_INCLUDED

#include "Singleton.hpp"
#include "Managers/Containers/ArrayOfGameObjects.hpp"

class DrawableObject;

class DrawingManager : public Singleton<DrawingManager> {
    private:
        ArrayOfGameObjects<DrawableObject> drawableObjects;     // The list of DrawableObjects

    public:
        DrawingManager();

        void AddGameObject( DrawableObject* object );           // Add a new DrawableObject to the Manager
        bool InManager    ( DrawableObject* object );           // Return true if the given DrawableObject is in the Manager. It
                                                                // is only used for debugging/sanity-check purposes so it doesn't
                                                                // matter that it takes O(n) time

        void Draw( sf::RenderTarget& Target );                  // Draw everything to the given target

        void Cleanup();                                         // Do any end-of-frame cleanup (such as removing marked GameObjects)
        void RemoveAllGameObjects();                            // Remove all GameObjects from the Manager
};

#endif // DRAWINGMANAGER_HPP_INCLUDED
