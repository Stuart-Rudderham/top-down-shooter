/*
 * This file implements the manager that
 * handles the drawing of objects
 *
 * Author:      Stuart Rudderham
 * Created:     August 2, 2012
 * Last Edited: November 1, 2012
 */

#include "Managers/Drawing/DrawingManager.hpp"
#include "Interfaces/Drawing/DrawableObject.hpp"
#include "Managers/Containers/ArrayOfGameObjects.inl"

DrawingManager::DrawingManager() : drawableObjects( "DrawingManager", 5000 )
{

}

// Add a new DrawableObject to the Manager
void DrawingManager::AddGameObject( DrawableObject* object ) {
    drawableObjects.AddGameObject( object );
}

// Return true if the given DrawableObject is in the Manager
bool DrawingManager::InManager( DrawableObject* object ) {
    return drawableObjects.InContainer( object );
}

// Draw everything to the given target
void DrawingManager::Draw( sf::RenderTarget& Target ) {

    // Sort all the objects by draw level (e.g. background, midground, etc...)
    drawableObjects.StableSort( [](DrawableObject* A, DrawableObject* B) -> bool { return A->GetDrawLevel() < B->GetDrawLevel(); } );

    // Draw everything
    drawableObjects.ForEach( [&](DrawableObject* Obj){ Obj->Draw( Target ); } );
}

// Do any end-of-frame cleanup
void DrawingManager::Cleanup() {
    drawableObjects.RemoveAllMarkedGameObjects();
}

// Remove all GameObjects from the Manager
void DrawingManager::RemoveAllGameObjects() {
    drawableObjects.RemoveAllGameObjects();
}
