/*
 * This header defines the manager that
 * handles deleting objects in the future
 * (e.g. delete this object in 5 seconds)
 *
 * Author:      Stuart Rudderham
 * Created:     November 12, 2012
 * Last Edited: November 24, 2012
 */

#ifndef DELETIONMANAGER_HPP_INCLUDED
#define DELETIONMANAGER_HPP_INCLUDED

#include "Singleton.hpp"
#include "Managers/Containers/DeferredActionList.hpp"

class GameObject;

class DeletionManager : public Singleton<DeletionManager> {
    private:
        DeferredActionList<GameObject> gameObjects;                             // The list of objects to be deleted

    public:
        DeletionManager();

        void AddDeletionEntry( GameObject* object, sf::Time deletionTime );     // Add a new GameObject to the Manager to be deleted after the given time has expired
        bool InManager       ( GameObject* object                        );     // Return true if the given GameObject is in the Manager. It
                                                                                // is only used for debugging/sanity-check purposes so it doesn't
                                                                                // matter that it takes O(n) time

        void DeleteObjects();                                                   // Respawn any objects whos time is up

        void Cleanup();                                                         // Do any end-of-frame cleanup (such as removing marked GameObjects)
        void RemoveAllGameObjects();                                            // Remove all GameObjects from the Manager
};

#endif // DELETIONMANAGER_HPP_INCLUDED
