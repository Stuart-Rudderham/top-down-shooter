/*
 * This file implements the manager that
 * handles deleting objects in the future
 * (e.g. delete this object in 5 seconds)
 *
 * Author:      Stuart Rudderham
 * Created:     November 12, 2012
 * Last Edited: November 24, 2012
 */

#include "Managers/Deletion/DeletionManager.hpp"
#include "Interfaces/GameObject.hpp"
#include "Managers/Containers/DeferredActionList.inl"

DeletionManager::DeletionManager() : gameObjects( "DeletionManager", 5000 )
{

}

// Add a new GameObject to the Manager
void DeletionManager::AddDeletionEntry( GameObject* object, sf::Time deletionTime  ) {
    gameObjects.AddGameObject( object, deletionTime );
}

// Return true if the given GameObject is in the Manager
bool DeletionManager::InManager( GameObject* object ) {
    return gameObjects.InContainer( object );
}

// Delete any objects whos time is up
void DeletionManager::DeleteObjects() {
    gameObjects.ApplyAndRemoveExpiredObjects( [&](GameObject* Obj){ Obj->MarkForDeletion(); } );
}

// Remove all GameObjects from the Manager
void DeletionManager::RemoveAllGameObjects() {
    gameObjects.RemoveAllGameObjects();
}

// Do any end-of-frame cleanup
void DeletionManager::Cleanup() {
    gameObjects.RemoveAllMarkedGameObjects();
}
