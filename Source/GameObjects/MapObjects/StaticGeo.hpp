/*
 * This header defines a class for
 * static geo in the game
 *
 * Author:      Stuart Rudderham
 * Created:     March 17, 2012
 * Last Edited: January 30, 2013
 */

#ifndef STATICGEO_HPP_INCLUDED
#define STATICGEO_HPP_INCLUDED

#include "Interfaces/Collision/CollidableObject.hpp"
#include "Interfaces/Drawing/DrawableShape.hpp"

class StaticGeo : public CollidableObject, public DrawableShape {
    public:
        StaticGeo( BaseShape* physicsOutline, sf::Color shapeColor );       // ctor. Create a StaticGeo with the given shape and color

        USE_WRAPPED_MEM_CALLS(StaticGeo);                                   // Wrapper function around "new" and "delete" for easy grepping
        IS_TRACKED_OBJECT(StaticGeo);                                       // Provide DestroyAll() and DestroyAllMarked() for garbage collecting GameObjects
        SET_ALLOCATOR_NAME(StaticGeo);                                      // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(50);

        // CollidableObject Interface
        void CollisionResponse( CollidableObject* otherObject, sf::Vector2f mtv, sf::Vector2f normal ) override;

        // DrawableShape Interface
        void Draw( sf::RenderTarget& Target ) override;

    private:
        NO_COPY_OR_ASSIGN(StaticGeo);                                       // No copy/assign ctor

        TRACKED_NEW_AND_DELETE(StaticGeo);                                  // Overload "new" and "delete" to use ArenaAllocator
};

#endif // STATICGEO_HPP_INCLUDED
