/*
 * This header defines a class for
 * a location on the map for objects
 * to respawn at
 *
 * Author:      Stuart Rudderham
 * Created:     September 18, 2012
 * Last Edited: January 30, 2013
 */

#ifndef SPAWNPOINT_HPP_INCLUDED
#define SPAWNPOINT_HPP_INCLUDED

#include "Interfaces/GameObject.hpp"

class SpawnPoint : public GameObject {
    public:
        SpawnPoint( Point2f pos );                                      // ctor. Create a SpawnPoint at the given location and adds it to the RespawnManager
        ~SpawnPoint();                                                  // dtor. Makes sure SpawnPoint is removed from RespawnManager

        USE_WRAPPED_MEM_CALLS(SpawnPoint);                              // Wrapper function around "new" and "delete" for easy grepping
        IS_TRACKED_OBJECT(SpawnPoint);                                  // Provide DestroyAll() and DestroyAllMarked() for garbage collecting GameObjects
        SET_ALLOCATOR_NAME(SpawnPoint);                                 // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(20);

        inline Point2f GetPosition() const {
            return position;
        }

    private:
        Point2f position;                                               // the location of the spawn point

        NO_COPY_OR_ASSIGN(SpawnPoint);                                  // No copy/assign ctor

        TRACKED_NEW_AND_DELETE(SpawnPoint);                             // Overload "new" and "delete" to use ArenaAllocator
};

#endif // SPAWNPOINT_HPP_INCLUDED
