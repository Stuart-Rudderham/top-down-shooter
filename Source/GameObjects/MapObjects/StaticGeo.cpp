/*
 * This file implements a class for
 * static geo in the game
 *
 * Author:      Stuart Rudderham
 * Created:     March 17, 2012
 * Last Edited: November 3, 2012
 */

#include "GameObjects/MapObjects/StaticGeo.hpp"

// constructor
StaticGeo::StaticGeo( BaseShape* physicsOutline, sf::Color shapeColor )
    : CollidableObject( CollidableObjectType::StaticGeo, physicsOutline, Physics::Priority::Immovable ),
      DrawableShape( Drawing::Level::Foreground )
{
    SetDrawingOutline( GetCollisionOutline(), shapeColor );
}

// CollidableObject Interface
void StaticGeo::CollisionResponse( CollidableObject* otherObject, sf::Vector2f mtv, sf::Vector2f normal ) {
    // Do nothing, walls don't do anything when you collide with them
    (void)otherObject;
    (void)mtv;
    (void)normal;
}

// DrawableShape Interface
void StaticGeo::Draw( sf::RenderTarget& Target ) {
    Target.draw( *GetDrawingOutline() );
}
