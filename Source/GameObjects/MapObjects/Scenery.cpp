/*
 * This file implements a class for
 * scenery placed on the map
 *
 * Author:      Stuart Rudderham
 * Created:     August 30, 2012
 * Last Edited: November 3, 2012
 */

#include "GameObjects/MapObjects/Scenery.hpp"

// constructors
Scenery::Scenery( const sf::Texture& pic, DrawLevel dLevel ) : DrawableSprite( dLevel )
{
    SetPicture( pic );

    // Center the scenery so that the center of
    // the picture is draw at it's position point
    GetDrawingSprite()->setOrigin( pic.getSize().x / 2.0f, pic.getSize().y / 2.0f );
}

// DrawableSprite Interface
void Scenery::Draw( sf::RenderTarget& Target ) {
    Target.draw( *GetDrawingSprite() );
}
