/*
 * This header defines a class for
 * scenery placed on the map
 *
 * Author:      Stuart Rudderham
 * Created:     August 30, 2012
 * Last Edited: January 30, 2013
 */

#ifndef SCENERY_HPP_INCLUDED
#define SCENERY_HPP_INCLUDED

#include "Interfaces/Drawing/DrawableSprite.hpp"

class Scenery : public DrawableSprite {
    public:
        Scenery( const sf::Texture& pic, DrawLevel dLevel );            // ctor. Create a Scenery with the given picture and drawing level

        USE_WRAPPED_MEM_CALLS(Scenery);                                 // Wrapper function around "new" and "delete" for easy grepping
        IS_TRACKED_OBJECT(Scenery);                                     // Provide DestroyAll() and DestroyAllMarked() for garbage collecting GameObjects
        SET_ALLOCATOR_NAME(Scenery);                                    // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(2000);

        // DrawableSprite Interface
        void Draw( sf::RenderTarget& Target ) override;

    private:
        NO_COPY_OR_ASSIGN(Scenery);                                     // No copy/assign ctor

        TRACKED_NEW_AND_DELETE(Scenery);                                // Overload "new" and "delete" to use ArenaAllocator
};

#endif // SCENERY_HPP_INCLUDED
