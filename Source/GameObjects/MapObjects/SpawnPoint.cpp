/*
 * This file implements a class for
 * a location on the map for objects
 * to respawn at
 *
 * Author:      Stuart Rudderham
 * Created:     November 11, 2012
 * Last Edited: November 11, 2012
 */

#include "GameObjects/MapObjects/SpawnPoint.hpp"
#include "Managers/Respawning/RespawnManager.hpp"

SpawnPoint::SpawnPoint( Point2f pos ) : position( pos )
{
    // Add the SpawnPoint to the RespawnManager
    RespawnManager::Get().AddGameObject( this );
}

SpawnPoint::~SpawnPoint() {

    // Make sure the object has been removed from the RespawnManager
    dbAssert( RespawnManager::Get().InManager( this ) == false, "Object not removed from RespawnManager before deletion" );
}
