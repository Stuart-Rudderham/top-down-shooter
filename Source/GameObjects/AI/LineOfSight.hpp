/*
 * This header defines the class used
 * for the line of sight between two
 * characters
 *
 * Author:      Stuart Rudderham
 * Created:     April 3, 2012
 * Last Edited: January 30, 2013
 */

#ifndef LINEOFSIGHT_HPP_INCLUDED
#define LINEOFSIGHT_HPP_INCLUDED

#include "Interfaces/Collision/CollidableObject.hpp"

// Forward Declarations
class Character;

class LineOfSight : public CollidableObject {
    public:
        LineOfSight( Character* targetCharacter, Character* viewerCharacter );      // ctor. Create a LineOfSight between the two given characters

        USE_WRAPPED_MEM_CALLS(LineOfSight);                                         // Wrapper function around "new" and "delete" for easy grepping
        IS_TRACKED_OBJECT(LineOfSight);                                             // Provide DestroyAll() and DestroyAllMarked() for garbage collecting GameObjects
        SET_ALLOCATOR_NAME(LineOfSight);                                            // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(10000);

        // CollidableObject Interface
        void CollisionResponse( CollidableObject* otherObject, Point2f collisionPoint, sf::Vector2f normal );

        // Returns true if there is nothing blocking the line of sight between the two Characters
        inline bool IsObstructed() const {
            return viewObstructed;
        }

        // Returns the target character in the LineOfSight
        inline Character* GetTarget() const {
            return target;
        }

    private:
        Character* target;                                                          // the Character the line of sight is looking at
        Character* viewer;                                                          // the Character that is doing the looking

        bool viewObstructed;                                                        // this is true if there are obstacles between the viewer and the target
        float distanceToTarget;                                                     // the distance from the viewer to the target

        NO_COPY_OR_ASSIGN(LineOfSight);                                             // No copy/assign ctor

        TRACKED_NEW_AND_DELETE(LineOfSight);                                        // Overload "new" and "delete" to use ArenaAllocator
};

#endif // LINEOFSIGHT_HPP_INCLUDED
