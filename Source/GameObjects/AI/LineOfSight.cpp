/*
 * This file implements the class used
 * for the line of sight between two
 * characters
 *
 * Author:      Stuart Rudderham
 * Created:     April 3, 2012
 * Last Edited: January 10, 2013
 */

#include "GameObjects/AI/LineOfSight.hpp"
#include "GameObjects/Characters/Character.hpp"
#include "Math/Geometry/LineSegment.hpp"
using namespace sf;

// constructor
LineOfSight::LineOfSight( Character* targetCharacter, Character* viewerCharacter )
    : CollidableObject( CollidableObjectType::LineOfSight, LineSegment::Create( viewerCharacter->GetPosition(), targetCharacter->GetPosition() ), Physics::Priority::Phantom ),
      target( targetCharacter ),
      viewer( viewerCharacter ),
      viewObstructed( false ),
      distanceToTarget( GetMagnitudeSquared( GetDirectionVector( viewer->GetPosition(), target->GetPosition() ) ) )
{
}

// CollidableObject Interface
void LineOfSight::CollisionResponse( CollidableObject* otherObject, Point2f collisionPoint, Vector2f normal ) {

    // Make sure that we're colliding with StaticGeo
    dbAssert( otherObject->IsStaticGeo(), "Collided with something that isn't StaticGeo" );

    // Make sure we haven't done any unnecessary checks
    dbAssert( viewObstructed == false, "LineOfSight is already obstructed" );

    // Get the distance to the object we're colliding with
    float distanceToOtherObject = GetMagnitudeSquared( GetDirectionVector( GetCollisionOutline()->GetAs<LineSegment>()->GetStartPoint(), collisionPoint ) );

    // If out target is father away than this object then that means that
    // this object is blocking the line of sight to the target
    if( distanceToTarget > distanceToOtherObject ) {

        // So mark the line of sight as not valid
        viewObstructed = true;

        // And don't do any more collision checks
        DisableCollisionChecking();
    }

    (void)normal;
    (void)otherObject;
}
