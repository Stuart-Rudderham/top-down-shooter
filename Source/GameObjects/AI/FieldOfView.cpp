/*
 * This file implements the class used
 * for the field of vision that AI
 * characters can see
 *
 * Author:      Stuart Rudderham
 * Created:     March 24, 2012
 * Last Edited: January 12, 2013
 */

#include "GameObjects/AI/FieldOfView.hpp"
#include "GameObjects/Characters/Character.hpp"
#include "GameObjects/AI/LineOfSight.hpp"

using namespace sf;

// Constructor
FieldOfView::FieldOfView( Character* character )
    : RotatableObject( CollidableObjectType::FieldOfView, character->GetConstData().LocalFieldOfViewOutline, character->GetPosition(), Physics::Priority::Phantom ),
      viewer( character ),
      target( nullptr )
{
    SnapToViewer();
}

// dtor
FieldOfView::~FieldOfView() {
    ClearLOS();
}


// If we don't have a target then look for one
// If we do have a target then confirm that they're still in the FOV
void FieldOfView::UpdateTarget() {

    // if we're currently targeting someone
    if( HaveTarget() ) {

        bool targetStillInFOV = false;

        // Loop over all our line-of-sight checks and see if we have a valid check to our target
        for( auto i = charactersInRange.begin(); i != charactersInRange.end(); ++i ) {
            if( ( (**i).GetTarget()->GetID() == target->GetID() ) && ( (**i).IsObstructed() == false ) ) {
                targetStillInFOV = true;
                break;
            }
        }

        // If we don't have an unobstructed line-of-sight to them then they are no longer our target
        if( targetStillInFOV == false ) {
            target = nullptr;
        }

    // If we currently don't have a target
    } else {

        // Loop through all the characters we can see and look for a new one
        for( auto i = charactersInRange.begin(); i != charactersInRange.end(); ++i ) {

            // If our line of sight is obstructed then we don't actually "see" them, so ignore them
            if( (**i).IsObstructed() == false ) {

                // Pick the first valid target we see
                target = (**i).GetTarget();
                return;
            }
        }
    }

    // If we get here then there were no valid targets
}


// Move and rotate the FOV to the Character that it is attached to
void FieldOfView::SnapToViewer() {

    MoveCollisionOutlineTo( viewer->GetPosition() );            // Update the position of the FOV

    // Update the angle of the FOV
    SetGlobalOutlineRotation( viewer->GetAngle() );
}


// Clear the list of characters the view can see
void FieldOfView::ClearLOS() {

    // Mark each of the LineOfSights for deletion, they will be cleaned up whenever
    for( auto i = charactersInRange.begin(); i != charactersInRange.end(); ++i ) {
        (**i).MarkForDeletion();
    }

    charactersInRange.clear();
}

Point2f FieldOfView::GetTargetPosition() const {
    dbAssert( HaveTarget(), "No target to get position of" );
    return target->GetPosition();
}

// CollidableObject Interface
void FieldOfView::CollisionResponse( CollidableObject* otherObject, Vector2f mtv, Vector2f normal ) {

    // Make sure collision filter is working
    // This makes sure that we're colliding with a Character
    dbAssert( CanCollideWith( otherObject ), "Collided with object we shouldn't have" );

    Character* characterInFOV = otherObject->GetAs<Character>();

    // If we're allied with the other character then do nothing
    if( Character::ShouldAttack( characterInFOV, viewer ) == false ) {
        return;
    }

    // Make sure we don't add ourselves to the list
    // This check if necessary if for some reason members of the same faction
    // can attack each other
    if( otherObject->GetID() == viewer->GetID() ) {
        return;
    }

    // If we don't have a target then do a line-of-sight test to the other character
    // If we *do* have a target then only do a line-of-sight test if the character we see is our target
    // this cuts down on the number of unnecessary tests we do if we already have a target
    if( ( HaveTarget() == false ) || ( otherObject->GetID() == target->GetID() ) ) {
        charactersInRange.push_back( LineOfSight::Create( characterInFOV, viewer ) );
    }

    (void)mtv;
    (void)normal;
}

bool FieldOfView::CanCollideWith( CollidableObject* otherObject ) {

    // Make sure the thing we're colliding with is a Character
    // Since this is the third collision filter checked (the other two are in CollisionManager)
    // everything other than Characters should have been filtered out already
    dbAssert( otherObject->IsCharacter(), "Collided with something that isn't a character" );

    Character* characterInFOV = otherObject->GetAs<Character>();

    // It only cares about Characters that are not allied
    // with the Character the FieldOfView is attached to
    return Character::ShouldAttack( viewer, characterInFOV ) && ( HaveTarget() == false || otherObject == target );
}
