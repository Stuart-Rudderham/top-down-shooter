/*
 * This header defines the class used
 * for the field of vision that AI
 * characters can see
 *
 * Author:      Stuart Rudderham
 * Created:     March 24, 2012
 * Last Edited: January 30, 2013
 */

#ifndef FIELDOFVIEW_HPP_INCLUDED
#define FIELDOFVIEW_HPP_INCLUDED

#include "Interfaces/Collision/RotatableObject.hpp"

// Forward Declarations
class Character;
class LineOfSight;

class FieldOfView : public RotatableObject {
    public:
        FieldOfView( Character* character );                            // ctor. Create a FieldOfView attached to the given Character

        ~FieldOfView();                                                 // dtor. Marks all the line of sights as invalid, so they
                                                                        // will be cleaned up at some point

        USE_WRAPPED_MEM_CALLS(FieldOfView);                             // Wrapper function around "new" and "delete" for easy grepping
        IS_TRACKED_OBJECT(FieldOfView);                                 // Provide DestroyAll() and DestroyAllMarked() for garbage collecting GameObjects
        SET_ALLOCATOR_NAME(FieldOfView);                                // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(2000);

        // CollidableObject Interface
        void CollisionResponse( CollidableObject* otherObject,
                                sf::Vector2f mtv,
                                sf::Vector2f normal ) override;

        bool CanCollideWith( CollidableObject* otherObject ) override;

        inline bool HaveTarget() const {                                // Returns true if the viewer currently has a target
            return target != nullptr;
        }

        Point2f GetTargetPosition() const;                              // Return the position of the target. If don't have a target an assert will fire

        void UpdateTarget();                                            // If we don't have a target then look for one
                                                                        // If we do have a target then confirm that they're still in the FOV

        void SnapToViewer();                                            // Move and rotate the FOV to the Character that it is attached to

        void ClearLOS();                                                // Remove all the list of sight checkers

    private:
        Character* viewer;                                              // the character the FOV is attached to
        Character* target;                                              // a target in the viewer's FOV. If no target then is NULL

        std::vector<LineOfSight*> charactersInRange;                    // holds the line of sight checker for each other character in range

        NO_COPY_OR_ASSIGN(FieldOfView);                                 // No copy/assign ctor

        TRACKED_NEW_AND_DELETE(FieldOfView);                            // Overload "new" and "delete" to use ArenaAllocator
};

#endif // FIELDOFVIEW_HPP_INCLUDED
