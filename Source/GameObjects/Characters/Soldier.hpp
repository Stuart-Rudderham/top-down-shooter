/*
 * This header defines the soldier
 * character
 *
 * Author:      Stuart Rudderham
 * Created:     March 16, 2012
 * Last Edited: January 30, 2013
 */

#ifndef SOLDIER_HPP_INCLUDED
#define SOLDIER_HPP_INCLUDED

#include "GameObjects/Characters/Character.hpp"

// Forward Declarations
class BaseGun;
enum class GunAction : unsigned char;

// This enum lists the different controls for the Soldier
namespace SoldierControls {
    enum Enum : unsigned char {
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Shoot,
        Reload,
        Switch,
        //Pickup,
        //Sprint,
        Max
    };
};

// The Soldier's shared const data
struct SoldierConstData : public CharacterConstData {

    SoldierConstData()
    {
        // Set movement data
        MoveSpeed = 250.0f;
        MaxSpeed = 250.0f;
        DecelerationSpeed = 0.9f;
        RotationSpeed = 100.0f;

        // Setup health
        MaxHealth = 300;

        // Pictures
        AlivePicture = SoldierAliveImage;
        DeadPicture = SoldierDeadImage;

        // Set physics outline in local coords
        LocalCharacterOutline = ConvexPolygon::Create( 4 );

        LocalCharacterOutline->Set( 0, Point2f(0, 10), false, false );
        LocalCharacterOutline->Set( 1, Point2f(5, 0), false, false );
        LocalCharacterOutline->Set( 2, Point2f(27, 10), false, false );
        LocalCharacterOutline->Set( 3, Point2f(5, 19), true, true );

        LocalCharacterOutline->SetCenter( Point2f(7, 10) );


        // Set FOV outline in local coords
        LocalFieldOfViewOutline = ConvexPolygon::Create( 3 );

        const float fovAngle = DegreeToRadian( 10 );
        const float fovLength = 1000.0f;

        // Calculate the points
        Point2f p1 = Point2f(0, 0);
        Point2f p2 = Point2f( fovLength, -fovLength * tan( fovAngle / 2.0f ) );
        Point2f p3 = Point2f( fovLength,  fovLength * tan( fovAngle / 2.0f ) );

        // Set the points
        LocalFieldOfViewOutline->Set( 0, p1, false, false );
        LocalFieldOfViewOutline->Set( 1, p2, false, false );
        LocalFieldOfViewOutline->Set( 2, p3, true,  true  );

        // Override the calculated center to make it one of the points of the triangle
        LocalFieldOfViewOutline->SetCenter( p1 );
    }
};


class Soldier : public Character {
    public:
        Soldier( Point2f pos, Faction::Enum f = Faction::Survivor );                                // ctor. Create a Soldier bot at the given position
        Soldier( ControlScheme& controls , Point2f pos, Faction::Enum f = Faction::Survivor );      // ctor. Create a human controlled Soldier

        ~Soldier();                                                                                 // dtor. Destroys all the guns associated with the Soldier

        USE_WRAPPED_MEM_CALLS(Soldier);                                                             // Wrapper function around "new" and "delete" for easy grepping
        IS_TRACKED_OBJECT(Soldier);                                                                 // Provide DestroyAll() and DestroyAllMarked() for garbage collecting GameObjects
        SET_ALLOCATOR_NAME(Soldier);                                                                // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(1000);

        void MakeHuman( ControlScheme& controls ) override;                                         // make the Soldier human controlled with the given control scheme
        void MakeBot()                            override;                                         // make the Soldier AI controlled

        void UpdateGun( GunAction desiredAction );                                                  // update the internal state of the current gun using the given
                                                                                                    // desired action

        BaseGun* GetCurrentGun() {																	// Getter for the current gun
            return guns[currentGun];
        }

    private:
        static const SoldierConstData SoldierSpecificConstData;                                     // only have one instance of the const data

        unsigned int currentGun;                                                                    // the index of the current gun the Soldier is using
        std::vector<BaseGun*> guns;                                                                 // the list of all the guns in the Soldier's inventory

        NO_COPY_OR_ASSIGN(Soldier);                                                                 // No copy/assign ctor

        TRACKED_NEW_AND_DELETE(Soldier);                                                            // Overload "new" and "delete" to use ArenaAllocator
};

#endif // SOLDIER_HPP_INCLUDED
