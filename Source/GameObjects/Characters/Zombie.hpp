/*
 * This header defines the zombie
 * character
 *
 * Author:      Stuart Rudderham
 * Created:     March 25, 2012
 * Last Edited: January 30, 2013
 */

#ifndef ZOMBIE_HPP_INCLUDED
#define ZOMBIE_HPP_INCLUDED

#include "GameObjects/Characters/Character.hpp"

// This enum lists the different controls for the Zombie
namespace ZombieControls {
    enum Enum : unsigned char {
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Max
    };
};

// The Zombie's shared const data
struct ZombieConstData : public CharacterConstData {
    ZombieConstData() {

        // Set movement data
        MoveSpeed = 150.0f;
        MaxSpeed = 150.0f;
        DecelerationSpeed = 0.9f;
        RotationSpeed = 100.0f;

        // Setup health
        MaxHealth = 300;

        // Pictures
        AlivePicture = ZombieAliveImage;
        DeadPicture = ZombieDeadImage;

        // Set physics outline in local coords
        LocalCharacterOutline = ConvexPolygon::Create( 5 );

        LocalCharacterOutline->Set( 0, Point2f(0, 9),   false, false );
        LocalCharacterOutline->Set( 1, Point2f(5, 0),   false, false );
        LocalCharacterOutline->Set( 2, Point2f(21, 4),  false, false );
        LocalCharacterOutline->Set( 3, Point2f(21, 14), false, false );
        LocalCharacterOutline->Set( 4, Point2f(5, 17),  true,  true );

        LocalCharacterOutline->SetCenter( Point2f(5, 9) );


        // Set FOV outline in local coords
        LocalFieldOfViewOutline = ConvexPolygon::Create( 4 );

        const float fovAngle = DegreeToRadian( 60 );
        const float fovLength = 100.0f;

        // Calculate the points
        Point2f p1 = Point2f(-15, -15);
        Point2f p2 = Point2f( fovLength, -fovLength * tan( fovAngle / 2.0f ) );
        Point2f p3 = Point2f( fovLength,  fovLength * tan( fovAngle / 2.0f ) );
        Point2f p4 = Point2f(-15, 15);

        // Set the points
        LocalFieldOfViewOutline->Set( 0, p1, false, false );
        LocalFieldOfViewOutline->Set( 1, p2, false, false );
        LocalFieldOfViewOutline->Set( 2, p3, false,  false  );
        LocalFieldOfViewOutline->Set( 3, p4, true,  true  );

        // Override the calculated center to make it sightly inside the FOV
        // so that the Zombie can see slightly behind it
        LocalFieldOfViewOutline->SetCenter( Point2f(0, 0) );
    }
};


class Zombie : public Character {
    public:
        Zombie( Point2f pos, Faction::Enum f = Faction::Zombie );                               // ctor. Create a Zombie bot at the given position
        Zombie( ControlScheme& controls, Point2f pos, Faction::Enum f = Faction::Zombie );      // ctor. Create a human controlled Zombie

        USE_WRAPPED_MEM_CALLS(Zombie);                                                          // Wrapper function around "new" and "delete" for easy grepping
        IS_TRACKED_OBJECT(Zombie);                                                              // Provide DestroyAll() and DestroyAllMarked() for garbage collecting GameObjects
        SET_ALLOCATOR_NAME(Zombie);                                                             // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(1000);

        void MakeHuman( ControlScheme& controls ) override;                                     // make the Zombie human controlled with the given control scheme
        void MakeBot()                            override;                                     // make the Zombie AI controlled

    private:
        static const ZombieConstData ZombieSpecificConstData;                                   // only have one instance of the const data

        NO_COPY_OR_ASSIGN(Zombie);                                                              // No copy/assign ctor

        TRACKED_NEW_AND_DELETE(Zombie);                                                         // Overload "new" and "delete" to use ArenaAllocator
};

#endif // ZOMBIE_HPP_INCLUDED
