/*
 * This file implements the generic
 * character class that is used for
 * every player and NPC in the game
 *
 * Author:      Stuart Rudderham
 * Created:     March 16, 2012
 * Last Edited: January 12, 2013
 */

#include "GameObjects/Characters/Character.hpp"
#include "GameObjects/Characters/Controllers/AI/AIController.hpp"
#include "GameObjects/MapObjects/Scenery.hpp"

// the player that the camera and audio is relative to
Character* Character::MainCharacter = nullptr;

// Defines which factions attack each other                              | SURVIVOR  | ZOMBIE  |
const bool Character::FactionDisposition[Faction::Max][Faction::Max] = { { false,      true,   },          // SURVIVOR
                                                                         { true,       false   } };        // ZOMBIE

// Constructor
Character::Character( const CharacterConstData& characterData, Point2f pos, Faction::Enum f )
    : MoveableObject( pos ),
      RotatableObject( CollidableObjectType::Character, characterData.LocalCharacterOutline, pos, Physics::Priority::Character ),
      DrawableSprite( Drawing::Level::Foreground ),
      IntelligentObject(),
      MortalObject( characterData.MaxHealth ),
      ConstData( characterData ),
      faction( f )
{
    // Setup the Character's alive picture
    SetPicture( TextureCache::Get().Load( ConstData.AlivePicture ) );

    // Center the picture
    GetDrawingSprite()->setOrigin( GetLocalCollisionOutline()->GetCenter() );
}

// MoveableObject Interface
void Character::MoveBy( sf::Vector2f displacement ) {

    // update your position
    position += displacement;

    // Move the global outline and the bounding box
    MoveCollisionOutlineBy( displacement );
}

void Character::FrameUpdate( float deltaT ) {

    // THIS WILL ALSO DAMPEN THE ROTATION, DO WE WANT ANOTHER FUNCTION??????
    ApplyDampening( ConstData.DecelerationSpeed );          // apply friction
    UpdateVelocity( deltaT );                               // update velocity based on acceleration
    CapVelocity( ConstData.MaxSpeed );                      // make sure Character isn't going faster than max speed

    UpdateAngle( deltaT );                                  // update the angle based on rotation speed
    SetGlobalOutlineRotation( GetAngle() );                 // rotate the global outline to the Character's angle
}

void Character::TimeSliceUpdate( float deltaT ) {
    UpdatePosition( deltaT );                               // update position based on velocity
}

// CollidableObject Interface
void Character::CollisionResponse( CollidableObject* otherObject, sf::Vector2f mtv, sf::Vector2f normal ) {

    // Resolve the collision
    MoveBy( mtv );

    // If controlled by AI have it react to the collision
    if( IsBot() ) {
        GetControllerAs<AIController>()->ReactToCollision( otherObject );
    }

    // To avoid warnings
    (void)mtv;
    (void)normal;
}

// DrawableSprite Interface
void Character::Draw( sf::RenderTarget& Target ) {

    // Update the picture's position and rotation
    GetDrawingSprite()->setRotation( GetAngle() );
    GetDrawingSprite()->setPosition( GetPosition() );

    // character gets more red the more damaged they are
    unsigned int healthPercent = Lerp( 0u, GetHealthPercent(), 255u );
    GetDrawingSprite()->setColor( sf::Color(255u, healthPercent, healthPercent ) );

    // Draw the picture
    if( Drawing::DrawCharacterPicture ) {
        Target.draw( *GetDrawingSprite() );
    }
}

// MortalObject Interface
void Character::DeathReaction() {

    // Remove the Character from all Managers
    DisableCollisionChecking();
    DisableDrawing();
    DisableController();
    DisableLocomotion();

    // and add an entry to the RespawnManager
    RespawnIn( Game::CharacterRespawnTime );

    // Create a dead body picture
    Scenery* deadBody = Scenery::Create( TextureCache::Get().Load( ConstData.DeadPicture ), Drawing::Level::Midground );
    deadBody->GetDrawingSprite()->setOrigin( GetLocalCollisionOutline()->GetCenter() );
    deadBody->GetDrawingSprite()->setPosition( GetPosition() );
    deadBody->GetDrawingSprite()->setRotation( GetAngle() );

    // and delete it in a while
    deadBody->DeleteIn( Game::DeadBodyDespawnTime );
}

void Character::Respawn(/* Point2f spawnPoint */) {

    // Re-add the Character to all relevant Managers
    EnableCollisionChecking();
    EnableDrawing();
    EnableController();
    EnableLocomotion();

    // Restore their health to max
    RestoreToMaxHealth();

    // Move to the spawn point
    //MoveTo( spawnPoint );
}
