/*
 * This file implements the soldier
 * character
 *
 * Author:      Stuart Rudderham
 * Created:     March 16, 2012
 * Last Edited: January 8, 2013
 */


#include "GameObjects/Characters/Soldier.hpp"
#include "MediaCache/MediaCache.hpp"
#include "UpgradesToSFML/Input.hpp"

#include "GameObjects/Characters/Weapons/Guns/Specifics/Pistol.hpp"
#include "GameObjects/Characters/Weapons/Guns/Specifics/SuperGun.hpp"

#include "GameObjects/Characters/Controllers/Human/HumanController.hpp"
#include "GameObjects/Characters/Controllers/AI/AIController.hpp"

Soldier::Soldier( Point2f pos, Faction::Enum f ) : Character( Soldier::SoldierSpecificConstData, pos, f )
{
    // Set up the Soldier's guns
//    guns.push_back( new Pistol( *this ) );
//    guns.push_back( new Pistol( this ) );
    //guns.push_back( new Pistol( this ) );

    guns.push_back( SuperGun::Create( this ) );
    guns.push_back( Pistol::Create( this ) );
//    guns.push_back( new BaseGun( *this, "Gun2" ) );

	currentGun = 0;

	// Setup AI
    MakeBot();
}

Soldier::Soldier( ControlScheme& controls, Point2f pos, Faction::Enum f ) : Soldier( pos, f )
{
    // Remove the AI and add a user controller
	MakeHuman( controls );
}

Soldier::~Soldier() {

    // Free the memory associated with the Soldier's guns
    for( auto i = guns.begin(); i != guns.end(); ++i ) {
        BaseGun::Destroy( *i );
    }
}

void Soldier::UpdateGun( GunAction desiredAction ) {

    if( desiredAction == GunAction::Switch ) {
        if( GetCurrentGun()->CanSwitch() && guns.size() > 1 ) {
            currentGun = (currentGun + 1) % guns.size();
            GetCurrentGun()->Equip();
        }
    }

    GetCurrentGun()->Update( desiredAction );
}

// USER CONTROLLER
template<>
void HumanController::SpecializedUpdateLogic<Soldier>() {

    Soldier* parent = GetParentAs<Soldier>();

    // Update the direction of the Soldier based on the position of the mouse cursor
    sf::Vector2f direction = GetDirectionVector( parent->GetPosition(), MouseState::GetPosition() );
    parent->SetAngle( ConvetToAngle( direction ) );

    float movementSpeed = parent->GetConstData().MoveSpeed * parent->GetCurrentGun()->GetMovementSpeedMultiplier();

    // Update the movement of the Soldier
    if( userInput[SoldierControls::MoveUp]->IsDown() ) {
        parent->ApplyImpulse( MathConstants::yAxis * -movementSpeed );

    } else if( userInput[SoldierControls::MoveDown]->IsDown() ) {
        parent->ApplyImpulse( MathConstants::yAxis * movementSpeed );

    } else {
        //velocity.y = 0;
    }

    if( userInput[SoldierControls::MoveLeft]->IsDown() ) {
        parent->ApplyImpulse( MathConstants::xAxis * -movementSpeed );

    } else if( userInput[SoldierControls::MoveRight]->IsDown() ) {
        parent->ApplyImpulse( MathConstants::xAxis * movementSpeed );

    } else {
        //velocity.x = 0;
    }


    GunAction desiredAction = GunAction::None;

    if( userInput[SoldierControls::Shoot]->IsJustPressed() ) {
        desiredAction = GunAction::PullTrigger;

    } else if( userInput[SoldierControls::Shoot]->IsDown() ) {
        desiredAction = GunAction::TriggerHeldDown;
    }

    if( userInput[SoldierControls::Switch]->IsJustPressed() ) {
        desiredAction = GunAction::Switch;
    }

    if( userInput[SoldierControls::Reload]->IsJustPressed() ) {
        desiredAction = GunAction::Reload;
    }

    parent->UpdateGun( desiredAction );
}

// AI CONTROLLER
template<>
void AIController::SpecializedHaveTargetAction<Soldier>() {

    // Make sure we actually have a target
    dbAssert( FOV->HaveTarget() == true, "Don't have a target" );

    Soldier* parent = GetParentAs<Soldier>();

    // Just look at them
    sf::Vector2f direction = GetDirectionVector( parent->GetPosition(), FOV->GetTargetPosition() );
    parent->SetAngle( ConvetToAngle( direction ) );
}

template<>
void AIController::SpecializedNoTargetAction<Soldier>() {

    // Make sure we actually don't have a target
    dbAssert( FOV->HaveTarget() == false, "Have a target" );

    Soldier* parent = GetParentAs<Soldier>();

    // Keep searching
    parent->SetRotationSpeed( parent->GetConstData().RotationSpeed );
}



void Soldier::MakeHuman( ControlScheme& controls ) {
    dbAssert( controls.NumInputs() == SoldierControls::Max, "Number of inputs given was ", controls.NumInputs() );
    SetController( SpecializedHumanController<Soldier>::Create( this, controls ) );
}

void Soldier::MakeBot() {
    SetController( SpecializedAIController<Soldier>::Create( this ) );
}
