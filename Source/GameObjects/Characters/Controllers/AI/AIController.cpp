/*
 * This file implements the controller
 * class that uses AI and vision to
 * control Characters
 *
 * Author:      Stuart Rudderham
 * Created:     September 28, 2012
 * Last Edited: January 12, 2013
 */

#include "GameObjects/Characters/Controllers/AI/AIController.hpp"
#include "GameObjects/Characters/Character.hpp"

// ctor. Takes in a reference to the object the controller is controlling
// and created the field of view
AIController::AIController( Character* parent ) : CharacterController( parent, ControllerType::Bot ),
                                                  FOV( nullptr )
{
    // Create the FieldOfView
    FOV = FieldOfView::Create( GetParentAs<Character>() );

    // If the parent Character is currently not in the IntelligenceManager
    // (e.g. they are respawning) then we don't want to check for things colliding
    // with the FieldOfView
    if( parent->IntelligentObject::ShouldIgnore() == true && parent->ShouldDelete() == false ) {
        FOV->DisableCollisionChecking();
    }
}

// dtor. Frees the field of view
AIController::~AIController() {
    FOV->MarkForDeletion();
}

// Move and rotate the FieldOfView so that it matches its parent Character
void AIController::SnapFOVToViewer() {
    FOV->SnapToViewer();
}

void AIController::TurnOnVision() {
    FOV->EnableCollisionChecking();
}

void AIController::TurnOffVision() {
    FOV->DisableCollisionChecking();
}
