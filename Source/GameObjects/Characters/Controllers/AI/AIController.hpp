/*
 * This header defines the controller
 * class that uses AI and vision to
 * control Characters
 *
 * Author:      Stuart Rudderham
 * Created:     September 28, 2012
 * Last Edited: January 30, 2013
 */

#ifndef AICONTROLLER_HPP_INCLUDED
#define AICONTROLLER_HPP_INCLUDED

#include "GameObjects/Characters/Controllers/CharacterController.hpp"
#include "GameObjects/AI/FieldOfView.hpp"

// Forward Declarations
class Character;
class CollidableObject;

class AIController : public CharacterController {
    public:
        AIController( Character* parent );                                          // ctor. Takes in a reference to the object the controller is controlling
                                                                                    // and creates the field of view

        virtual ~AIController();                                                    // destructor. Frees the field of view

        SET_ALLOCATOR_NAME(AIController);                                           // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(2500);

        virtual void UpdateLogic() = 0;                                             // SpecializedAIController will make sure the correct function is called
        virtual void ReactToCollision( CollidableObject* otherObject ) = 0;         // SpecializedAIController will make sure the correct function is called
        virtual void ReactToDamage() = 0;                                           // SpecializedAIController will make sure the correct function is called

        void SnapFOVToViewer();                                                     // Move and rotate the FieldOfView so that it matches its parent Character

        void TurnOnVision();                                                        // Enables collision checking for the FOV, which lets the parent Character
                                                                                    // see objects infront of it

        void TurnOffVision();                                                       // Disables collision checking for the FOV, which makes the parent Character
                                                                                    // blind to any Characters infront of it
    protected:
        FieldOfView* FOV;                                                           // a pointer to the parent object's field of view

        // These templated functions allow us to easily write
        // specialized action/reaction functions for any class.
        // They also allow us to share code, for example
        // SpecializedHaveTargetAction<Soldier> can call
        // SpecializedHaveTargetAction<Character>
        template<typename T>                                                        // what the parent object should do if they currently have a target
        void SpecializedHaveTargetAction();

        template<typename T>                                                        // what the parent object should do if they don't have a target
        void SpecializedNoTargetAction();

        template<typename T>                                                        // what the parent object should do if it detects a collision
        void SpecializedCollisionReaction( CollidableObject* otherObject ) {        // default reaction is to do nothing
            (void)otherObject;
        }

        template<typename T>                                                        // what the parent object should do if it takes damage
        void SpecializedDamageReaction() {                                          // default reaction is to do nothing

        }

        inline bool HaveFOV() const {                                               // predicate function to check if the FieldOfView has been created yet
            return FOV != nullptr;
        }

    private:
        NO_COPY_OR_ASSIGN(AIController);                                            // No copy/assign ctor
};



// A specialized controller for each class
// Its ensures that the specialized functions
// defined in the base class are called with the correct type
template<typename T>
class SpecializedAIController : public AIController {
    public:

        // Ctor. Just forwards the arguments to base class
        // Could replace this with an inheriting construct when
        // compilers support it
        // using AIController::AIController
        SpecializedAIController( Character* parent )
            : AIController( parent )
        { }

        // Wrapper function around "new" and "delete" for easy grepping
        // Don't need to define the ArenaAllocator name and size, as that
        // is done in the parent class
        USE_WRAPPED_MEM_CALLS(SpecializedAIController);

        // Update the logic of the object based on what is in
        // their field of view
        void UpdateLogic() {

            // Make sure our current target is still valid
            // and if not try and find a new one
            FOV->UpdateTarget();

            // based on whether or not we have a target, control the object
            if( FOV->HaveTarget() ) {
                SpecializedHaveTargetAction<T>();
            } else {
                SpecializedNoTargetAction<T>();
            }

            // Clean up any created LineOfSight tests
            // The IntelligenceManager will take care of
            // moving the FOV to the parent object after
            // all positions/rotations have been updated
            FOV->ClearLOS();
        }

        // Update the logic if there is a collision
        void ReactToCollision( CollidableObject* otherObject ) {
            SpecializedCollisionReaction<T>( otherObject );
        }

        // Update the logic if the parent object is damaged
        void ReactToDamage() {
            SpecializedDamageReaction<T>();
        }

    private:
        TEMPLATE_NO_COPY_OR_ASSIGN(SpecializedAIController);                            // No copy/assign ctor

        UNTRACKED_NEW_AND_DELETE(AIController);                                         // Overload "new" and "delete" to use ArenaAllocator
                                                                                        // Since the class is the same size regardless of what type T
                                                                                        // is they can all share the HumanController allocator
};

#endif // AICONTROLLER_HPP_INCLUDED
