/*
 * This header defines the generic
 * controller class that is used
 * to control Characters
 *
 * Author:      Stuart Rudderham
 * Created:     August 12, 2012
 * Last Edited: January 8, 2013
 */

#ifndef CHARACTERCONTROLLER_HPP_INCLUDED
#define CHARACTERCONTROLLER_HPP_INCLUDED

#include "DebugTools/Debug.hpp"
#include "Memory/MemoryAllocationMacros.hpp"

// Forward Declarations
class Character;

// This enum defines the two different types
// of controllers we can have, AI controlled or
// user controlled
enum class ControllerType : unsigned char {
    Human,
    Bot,
};

class CharacterController {
    public:
        CharacterController( Character* parent, ControllerType type )               // ctor. Takes in a reference to the object we're controlling
            : objectBeingControlled( parent ),                                      // and the type of the controller
              controllerType( type )
        {
            dbAssert( objectBeingControlled != nullptr,                             // make sure the parent is valid
                      "Given a NULL Character to control" );
        }

        virtual ~CharacterController() {}                                           // dtor. Doesn't need to do anything, is virtual in case of children

        USE_WRAPPED_MEM_CALLS(CharacterController);                                 // Wrapper function around "new" and "delete" for easy grepping

        inline ControllerType GetControllerType() const {                           // Getter for the controller type
            return controllerType;
        }

        template<typename T>                                                        // Convience function to get a reference to the controlled Character
        inline T* GetParentAs() {                                                   // casted to the desired type
            return checked_cast<T*>( objectBeingControlled );
        }

        virtual void UpdateLogic() = 0;                                             // update the internal logic of the controlled Character

    private:
        NO_COPY_OR_ASSIGN(CharacterController);                                     // No copy/assign ctor

        Character* objectBeingControlled;                                           // a reference to the GameObject we control
        ControllerType controllerType;                                              // the actual type of controller this is
};

#endif // CHARACTERCONTROLLER_HPP_INCLUDED
