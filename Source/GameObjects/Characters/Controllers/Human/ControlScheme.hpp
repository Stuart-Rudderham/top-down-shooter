/*
 * This header defines a list of
 * user inputs that can be queried
 * about their state
 *
 * Author:      Stuart Rudderham
 * Created:     November 3, 2012
 * Last Edited: November 3, 2012
 */

#ifndef CONTROLSCHEME_HPP_INCLUDED
#define CONTROLSCHEME_HPP_INCLUDED

#include "DebugTools/Debug.hpp"

#include <vector>
#include <initializer_list>

class InputState;

class ControlScheme {
    private:
        std::vector<const InputState*> controls;                                        // the list of inputs for the ControlScheme

    public:
        ControlScheme( std::initializer_list<const InputState*> inputs ) {              // ctor. Takes in an initializer list of arbitrary
            controls.insert( controls.end(), inputs.begin(), inputs.end() );            // length and adds the contents to the vector
        }

        unsigned int NumInputs() const {                                                // gets how many inputs the ControlScheme has
            return controls.size();
        }

        const InputState* operator[]( unsigned int i ) {                                // Access the internal array
            dbAssert( i < controls.size(), "Out of bounds access. Index is ", i );
            return controls[i];
        }
};

#endif // CONTROLSCHEME_HPP_INCLUDED
