/*
 * This header defines the controller
 * class that humans can use to
 * control Characters
 *
 * Author:      Stuart Rudderham
 * Created:     September 1, 2012
 * Last Edited: January 30, 2013
 */

#ifndef HUMANCONTROLLER_HPP_INCLUDED
#define HUMANCONTROLLER_HPP_INCLUDED

#include "GameObjects/Characters/Controllers/CharacterController.hpp"
#include "GameObjects/Characters/Controllers/Human/ControlScheme.hpp"

class HumanController : public CharacterController {
    public:
        HumanController( Character* parent,                     // constructor. Takes in a list of input keys
                         ControlScheme& inputList );

        virtual ~HumanController() {};                          // destructor. Does nothing, virtual in case of children

        SET_ALLOCATOR_NAME(HumanController);                    // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(1);

        virtual void UpdateLogic() = 0;                         // control the object. Is defined in the child classes

    protected:
        ControlScheme& userInput;                               // the inputs the controller uses

        // This templated function allow us to easily write
        // a specialized input reaction function for any class.
        // It also allow us to share code, for example
        // SpecializedUpdateLogic<Soldier> can
        // call SpecializedUpdateLogic<Character>
        template<typename T>
        void SpecializedUpdateLogic();

    private:
        NO_COPY_OR_ASSIGN(HumanController);                     // No copy/assign ctor
};



// A specialized controller for each class
// Its ensures that the specialized functions
// defined in the base class are called with the correct type
template<typename T>
class SpecializedHumanController : public HumanController {
    public:

        // Ctor. Just forwards the arguments to base class
        // Could replace this with an inheriting construct when
        // compilers support it
        // using HumanController::HumanController
        SpecializedHumanController( Character* parent, ControlScheme& inputList )
            : HumanController( parent, inputList )
        { }

        USE_WRAPPED_MEM_CALLS(SpecializedHumanController);                              // Wrapper function around "new" and "delete" for easy grepping
                                                                                        // Don't need to define the ArenaAllocator name and size, as that
                                                                                        // is done in the parent class

        void UpdateLogic() {                                                            // Control the character
            SpecializedUpdateLogic<T>();
        }

    private:
        TEMPLATE_NO_COPY_OR_ASSIGN(SpecializedHumanController);                         // No copy/assign ctor

        UNTRACKED_NEW_AND_DELETE(HumanController);                                      // Overload "new" and "delete" to use ArenaAllocator
                                                                                        // Since the class is the same size regardless of what type T
                                                                                        // is they can all share the HumanController allocator
};

#endif // HUMANCONTROLLER_HPP_INCLUDED
