/*
 * This file implements the controller
 * class that humans can use to
 * control Characters
 *
 * Author:      Stuart Rudderham
 * Created:     September 28, 2012
 * Last Edited: December 12, 2012
 */

#include "GameObjects/Characters/Controllers/Human/HumanController.hpp"

// ctor
HumanController::HumanController( Character* parent, ControlScheme& inputList ) : CharacterController( parent, ControllerType::Human ),
                                                                                  userInput( inputList )
{

}
