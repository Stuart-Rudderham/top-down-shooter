/*
 * This header defines the SuperGun weapon
 *
 * Author:      Stuart Rudderham
 * Created:     January 12, 2013
 * Last Edited: January 30, 2013
 */

#ifndef SUEPRGUN_HPP_INCLUDED
#define SUEPRGUN_HPP_INCLUDED

#include "Singleton.hpp"

#include "GameObjects/Characters/Weapons/Guns/Interfaces/Reloading/MagazineFed.hpp"
#include "GameObjects/Characters/Weapons/Guns/Interfaces/Shooting/FiresSlowBullets.hpp"

class SuperGun : public MagazineFed, public FiresSlowBullets {
    public:
        struct ConstData : public Singleton<ConstData>
        {
            BaseGun::ConstData              BaseGunConstData;
            MagazineFed::ConstData          MagazineFedConstData;
            FiresSlowBullets::ConstData     FiresSlowBulletsConstData;

            ConstData();
        };

        USE_WRAPPED_MEM_CALLS(SuperGun);                                            // Wrapper function around "new" and "delete" for easy grepping
        SET_ALLOCATOR_NAME(SuperGun);                                               // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(1000);

        SuperGun( Character* c );                                                   // ctor

    private:
        NO_COPY_OR_ASSIGN(SuperGun);                                                // No copy/assign ctor

        UNTRACKED_NEW_AND_DELETE(SuperGun);                                         // Overload "new" and "delete" to use ArenaAllocator
};

#endif // PISTOL_HPP_INCLUDED
