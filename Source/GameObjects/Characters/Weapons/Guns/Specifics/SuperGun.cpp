/*
 * This file implements the SuperGun weapon
 *
 * Author:      Stuart Rudderham
 * Created:     January 12, 2013
 * Last Edited: January 12, 2013
 */

#include "GameObjects/Characters/Weapons/Guns/Specifics/SuperGun.hpp"
#include "MediaCache/MediaCache.hpp"

// Initialize all the constant data for the pistol
SuperGun::ConstData::ConstData()
    : BaseGunConstData
      (
            "SuperGun",                     /* Name                        */
            3,                              /* NumberOfBurstsPerShot       */
            10,                             /* NumberOfProjectilesPerBurst */
            sf::milliseconds(100),          /* DelayTimeBetweenBursts      */
            false,                          /* IsAutomatic                 */
            sf::milliseconds(400),          /* ShotTime                    */
            sf::milliseconds(1500),         /* ReloadTime                  */
            sf::milliseconds(100),          /* EquipTime                   */
            PistolEquipSound,               /* EquipSound                  */
            SuperGunImage,                  /* Picture                     */
            5,                              /* InherentAccuracy            */
            2,                              /* RecoilIncreasePerBurst      */
            20,                             /* MaxRecoil                   */
            sf::milliseconds(1000),         /* TimeToLoseMaxRecoil         */
            1.0f,                           /* MovementSpeedMultiplier     */
            1000.0f,                        /* Damage                      */
            ProjectileResponse::Delete,     /* HitCharacterResponse        */
            ProjectileResponse::Bounce      /* HitStaticGeoResponse        */
      ),
      MagazineFedConstData
      (
            10,                             /* MaxNumberOfClips            */
            3,                              /* StartingNumberOfClips       */
            999,                            /* NumberOfBurstsPerClip       */
            PistolReloadSound,              /* ReloadSound                 */
            BaseGunConstData                /* Needed for sanity checks    */
      ),
      FiresSlowBulletsConstData
      (
            700,                            /* BulletSpeed                 */
            5,                              /* BulletSize                  */
            sf::Color::Cyan,                /* BulletColor                 */
            SuperGunShotSound,              /* ShotSound                   */
            BaseGunConstData                /* Needed for sanity checks    */
      )
{

}

// ctor
SuperGun::SuperGun( Character* character )
    : BaseGun         ( character, SuperGun::ConstData::Get().BaseGunConstData          ),
      MagazineFed     (            SuperGun::ConstData::Get().MagazineFedConstData      ),
      FiresSlowBullets(            SuperGun::ConstData::Get().FiresSlowBulletsConstData )
{

}
