/*
 * This file implements the Pistol weapon
 *
 * Author:      Stuart Rudderham
 * Created:     December 10, 2012
 * Last Edited: January 7, 2013
 */

#include "GameObjects/Characters/Weapons/Guns/Specifics/Pistol.hpp"
#include "MediaCache/MediaCache.hpp"

// Initialize all the constant data for the pistol
Pistol::ConstData::ConstData()
    : BaseGunConstData
      (
            "Pistol",                       /* Name                        */
            1,                              /* NumberOfBurstsPerShot       */
            1,                              /* NumberOfProjectilesPerBurst */
            sf::milliseconds(100),          /* DelayTimeBetweenBursts      */
            false,                          /* IsAutomatic                 */
            sf::milliseconds(250),          /* ShotTime                    */
            sf::milliseconds(1500),         /* ReloadTime                  */
            sf::milliseconds(100),          /* EquipTime                   */
            PistolEquipSound,               /* EquipSound                  */
            PistolImage,                    /* Picture                     */
            3,                              /* InherentAccuracy            */
            2,                              /* RecoilIncreasePerBurst      */
            20,                             /* MaxRecoil                   */
            sf::milliseconds(3000),         /* TimeToLoseMaxRecoil         */
            1.0f,                           /* MovementSpeedMultiplier     */
            10.0f,                          /* Damage                      */
            ProjectileResponse::Delete,     /* HitCharacterResponse        */
            ProjectileResponse::Delete      /* HitStaticGeoResponse        */
      ),
      MagazineFedConstData
      (
            10,                             /* MaxNumberOfClips            */
            3,                              /* StartingNumberOfClips       */
            12,                             /* NumberOfBurstsPerClip       */
            PistolReloadSound,              /* ReloadSound                 */
            BaseGunConstData                /* Needed for sanity checks    */
      ),
      FiresSlowBulletsConstData
      (
            700,                            /* BulletSpeed                 */
            2,                              /* BulletSize                  */
            sf::Color::Black,               /* BulletColor                 */
            PistolShotSound,                /* ShotSound                   */
            BaseGunConstData                /* Needed for sanity checks    */
      )
{

}

// ctor
Pistol::Pistol( Character* character )
    : BaseGun         ( character, Pistol::ConstData::Get().BaseGunConstData          ),
      MagazineFed     (            Pistol::ConstData::Get().MagazineFedConstData      ),
      FiresSlowBullets(            Pistol::ConstData::Get().FiresSlowBulletsConstData )
{

}
