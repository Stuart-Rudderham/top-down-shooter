/*
 * This header defines the Pistol weapon
 *
 * Author:      Stuart Rudderham
 * Created:     December 10, 2012
 * Last Edited: January 30, 2013
 */

#ifndef PISTOL_HPP_INCLUDED
#define PISTOL_HPP_INCLUDED

#include "Singleton.hpp"

#include "GameObjects/Characters/Weapons/Guns/Interfaces/Reloading/MagazineFed.hpp"
#include "GameObjects/Characters/Weapons/Guns/Interfaces/Shooting/FiresSlowBullets.hpp"

class Pistol : public MagazineFed, public FiresSlowBullets {
    public:
        struct ConstData : public Singleton<ConstData>
        {
            BaseGun::ConstData              BaseGunConstData;
            MagazineFed::ConstData          MagazineFedConstData;
            FiresSlowBullets::ConstData     FiresSlowBulletsConstData;

            ConstData();
        };

        USE_WRAPPED_MEM_CALLS(Pistol);                                              // Wrapper function around "new" and "delete" for easy grepping
        SET_ALLOCATOR_NAME(Pistol);                                                 // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(1000);

        Pistol( Character* c );                                                     // ctor

    private:
        NO_COPY_OR_ASSIGN(Pistol);                                                  // No copy/assign ctor

        UNTRACKED_NEW_AND_DELETE(Pistol);                                           // Overload "new" and "delete" to use ArenaAllocator
};

#endif // PISTOL_HPP_INCLUDED
