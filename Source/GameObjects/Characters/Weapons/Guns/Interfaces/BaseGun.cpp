/*
 * This file implements the gun class
 *
 * Author:      Stuart Rudderham
 * Created:     March 16, 2012
 * Last Edited: January 7, 2013
 */

#include "GameObjects/Characters/Weapons/Guns/Interfaces/BaseGun.hpp"
#include "Managers/Sound/SoundManager.hpp"
#include "MediaCache/MediaCache.hpp"
#include "Math/MathFunctions.hpp"

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

BaseGun::BaseGun( Character* character, const BaseGun::ConstData& data  )
    : BaseGunConstData( &data ),
      holder( character ),
      currentState( GunState::Idle ),
      timeInCurrentState( sf::Time::Zero ),
      burstsToBeFired( 0 ),
      currentRecoil( 0.0f )
{

}

// Equip the gun
void BaseGun::Equip() {
    SwitchToState( GunState::Equipping );

    // Play equipping sound
    SoundManager::Get().PlaySoundFX( BaseGunConstData->EquipSound );
}

const sf::Texture& BaseGun::GetHUDPicture() const {
    return TextureCache::Get().Load( BaseGunConstData->Picture );
}

void BaseGun::AdjustRecoil( float delta ) {
    currentRecoil += delta;
    Clamp( 0.0f, currentRecoil, BaseGunConstData->MaxRecoil );
}

void BaseGun::DrawCrosshair( sf::RenderTarget& Target, Point2f position, float radius ) const {

    // Draw the big circle
    {
        sf::CircleShape crossHair(radius, 60);      // want a higher res circle because the cross hair can get pretty big

        crossHair.setPosition( position );
        crossHair.setRadius( radius );
        crossHair.setOrigin( radius, radius );

        crossHair.setFillColor( sf::Color::Transparent );
        crossHair.setOutlineThickness( -2.0f );
        crossHair.setOutlineColor( sf::Color::Black );

        Target.draw( crossHair );
    }

    // Draw the crossing lines
    {
        sf::RectangleShape line;

        line.setFillColor( sf::Color::Black );
        line.setPosition( position );

        line.setSize( sf::Vector2f(2, radius * 2) );            // vertical line
        line.setOrigin( line.getSize() / 2.0f );
        Target.draw( line );

        line.setSize( sf::Vector2f(radius * 2, 2) );            // horizontal line
        line.setOrigin( line.getSize() / 2.0f );
        Target.draw( line );
    }

    // Draw the center dot
    {
        sf::CircleShape centerDot(6, 6);       // don't need a super high resolution circle
        centerDot.setFillColor( sf::Color::Red );

        centerDot.setPosition( position );
        centerDot.setRadius( 2 );
        centerDot.setOrigin( 2, 2 );

        Target.draw( centerDot );
    }
}
