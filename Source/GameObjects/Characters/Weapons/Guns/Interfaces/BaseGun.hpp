/*
 * This header defines the abstract
 * base gun class
 *
 * Author:      Stuart Rudderham
 * Created:     March 16, 2012
 * Last Edited: January 7, 2013
 */

#ifndef BASEGUN_HPP_INCLUDED
#define BASEGUN_HPP_INCLUDED

#include "DebugTools/Debug.hpp"
#include "Memory/MemoryAllocationMacros.hpp"

// Forward Declarations
class Character;

// This enum defines the different way that the projectiles
// the gun shoots can react when colliding with another object
enum class ProjectileResponse : unsigned char
{
    Delete,                         // The projectile is deleted
    Penetrate,                      // The projectile pierces through the object and can collide with other objects
    Bounce,                         // The projectile bounces off the object and can collide with other objects
    //Stick                           // The projectile sticks to the object and cannot collide with other objects
};

#if 0
// This enum defines the two different types of damage a gun
// can inflict.
enum class DamageType : unsigned char {
    Instant;        // How much damage it does in a instantous, one-hit value
    DPS;            // How much damage it does over 1 second of time. It is used if the gun fires some sort
                    // of persistant projectile (such as the flames from the flamethrower or the laser wall)
                    // that can damage the same thing over multiple frames
};
#endif

union DamageType {
    float Instant;
    float DPS;

    DamageType( float damage = 0 ) : Instant( damage ) {}
};

// This enum defines all the different states that the gun can be in
enum class GunState : unsigned char {
    Idle,                           // not doing anything
    Shooting,                       // in the process of shooting bullets
    Reloading,                      // in the process of reloading
    Equipping                       // in the process of being equipped (e.g. just got picked up, or switched to)
};

// This enum defines all the different actions the holder can perform on the gun
enum class GunAction : unsigned char {
    PullTrigger,                    // the holder pulled the trigger (and the trigger wasn't previously held down)
    TriggerHeldDown,                // the holder is holding down the trigger
    Reload,                         // the holder is trying to reload
    Switch,                         // the holder is trying to equip the gun
    None                            // the holder isn't trying to do anything
};


class BaseGun {
    public:
        struct ConstData
        {
            const char* Name;                                   // the name of the gun

            const unsigned int NumberOfBurstsPerShot;		    // how many sets of bullets are created every time you pull the trigger (i.e. the Battle Rifle in halo has 3 bursts per shot with one bullet per burst)
            const unsigned int NumberOfProjectilesPerBurst;	    // how many projectiles are created with each burst (i.e. the Shotgun in halo has 1 bursts per shot with 12 bullets per burst)
            const sf::Time DelayTimeBetweenBursts;		        // how many milliseconds of delay happen between successive bursts (time is given in milliseconds but converted to number of frames)

            const bool IsAutomatic;                             // true if the gun is automatic

            const sf::Time ShotTime;						    // how long it takes for the gun to be ready to fire again after shooting it
            const sf::Time ReloadTime;                          // how long it takes for the gun to be ready to fire again after reloading it
            const sf::Time EquipTime;						    // how long it takes for the gun to be ready to fire after equipping it

            const ExternalFile EquipSound;                      // the sound the gun makes when you equip it

            const ExternalFile Picture;                         // A picture of the gun to be displayed in the HUD and as an ItemOnGround

            const float InherentAccuracy;                       // The accuracy of the gun with no recoil. Expressed in degrees (e.g. 5 -> shot could be up to 5 degrees + or - from center)

            const float RecoilIncreasePerBurst;                 // How much recoil you get for each burst fired, in degrees
            const float MaxRecoil;                              // The most recoil the gun can have
            const sf::Time TimeToLoseMaxRecoil;                 // The amount of time it takes to go from max recoil to 0 recoil

            const float MovementSpeedMultiplier;                // how the gun affects the holders base movement speed (0.5 -> move at half speed)

            const DamageType Damage;                            // how much damage the projectiles the the gun shoots do. Can either be instant damage or DPS

            const ProjectileResponse HitCharacterResponse;      // how the projectiles the gun shoots will react when they collide with a Character
            const ProjectileResponse HitStaticGeoResponse;      // how the projectiles the gun shoots will react when they collide with StaticGeo

            // ctor
            ConstData(
                        const char* name,
                        unsigned int number_of_bursts_per_shot,
                        unsigned int number_of_projectiles_per_burst,
                        sf::Time delay_time_between_bursts,
                        bool is_automatic,
                        sf::Time shot_time,
                        sf::Time reload_time,
                        sf::Time equip_time,
                        ExternalFile equip_sound,
                        ExternalFile picture,
                        float inherent_accuracy,
                        float recoil_increase_per_burst,
                        float max_recoil,
                        sf::Time time_to_lose_max_recoil,
                        float movement_speed_multiplier,
                        DamageType damage,
                        ProjectileResponse hit_character_response,
                        ProjectileResponse hit_static_geo_response
                     )
                : Name                          ( name                            ),
                  NumberOfBurstsPerShot         ( number_of_bursts_per_shot       ),
                  NumberOfProjectilesPerBurst   ( number_of_projectiles_per_burst ),
                  DelayTimeBetweenBursts        ( delay_time_between_bursts       ),
                  IsAutomatic                   ( is_automatic                    ),
                  ShotTime                      ( shot_time                       ),
                  ReloadTime                    ( reload_time                     ),
                  EquipTime                     ( equip_time                      ),
                  EquipSound                    ( equip_sound                     ),
                  Picture                       ( picture                         ),
                  InherentAccuracy              ( inherent_accuracy               ),
                  RecoilIncreasePerBurst        ( recoil_increase_per_burst       ),
                  MaxRecoil                     ( max_recoil                      ),
                  TimeToLoseMaxRecoil           ( time_to_lose_max_recoil         ),
                  MovementSpeedMultiplier       ( movement_speed_multiplier       ),
                  Damage                        ( damage                          ),
                  HitCharacterResponse          ( hit_character_response          ),
                  HitStaticGeoResponse          ( hit_static_geo_response         )
            {
                dbAssert( NumberOfBurstsPerShot > 0u                                           , "ConstData is wrong for ", Name );     // Have fire at least one burst
                dbAssert( NumberOfProjectilesPerBurst > 0u                                     , "ConstData is wrong for ", Name );     // Have to fire at least projectile per burst
                dbAssert( DelayTimeBetweenBursts * (sf::Int64)NumberOfBurstsPerShot <= ShotTime, "ConstData is wrong for ", Name );     // Won't have time to fire all bursts otherwise. There can still be issues because we only fire one burst per frame (e.g. NumberOfBurstsPerShot = 4, DelayTimeBetweenBursts = 0 will still take 4 frames which could be 50ms )
                dbAssert( InherentAccuracy >= 0.0f                                             , "ConstData is wrong for ", Name );     // Cannot have negative accuracy
                dbAssert( RecoilIncreasePerBurst >= 0.0f                                       , "ConstData is wrong for ", Name );     // Cannot gain negative recoil
                dbAssert( MaxRecoil >= 0.0f                                                    , "ConstData is wrong for ", Name );     // Cannot have negative max recoil
                dbAssert( MaxRecoil > 0.0f || RecoilIncreasePerBurst == 0.0f                   , "ConstData is wrong for ", Name );     // No point in gaining recoil if max recoil is 0
                dbAssert( RecoilIncreasePerBurst > 0.0f || MaxRecoil == 0.0f                   , "ConstData is wrong for ", Name );     // No point in having max recoil if cannot gain any recoil in the first place
                dbAssert( RecoilIncreasePerBurst <= MaxRecoil                                  , "ConstData is wrong for ", Name );     // Can't gain more than the max recoil per shot
                dbAssert( TimeToLoseMaxRecoil > sf::Time::Zero                                 , "ConstData is wrong for ", Name );     // Cannot have Zero time or else we have a divide by 0
                dbAssert( MovementSpeedMultiplier >= 0.0f                                      , "ConstData is wrong for ", Name );     // Cannot have negative speed multiplier (however if we want a gun to reverse controls we could set this to -1.0f)
                dbAssert( Damage.Instant > 0.0f                                                , "ConstData is wrong for ", Name );     // Gun should do damage
            }
        };

        BaseGun( Character* character, const BaseGun::ConstData& data );    // ctor. Needs a reference to the Character holding the gun
                                                                            // and a reference to the appropriate ConstData

        BaseGun() { dbCrash( "Called default ctor" ); }                     // Default ctor is needed to make the compiler happy
                                                                            // It should never actually get called, as the most derived
                                                                            // class (e.g. Pistol, Shotgun, etc...) virtually inherits
                                                                            // this class and explicitly call the above ctor with the
                                                                            // appropriate parameters

        virtual ~BaseGun() {}                                               // dtor. Does nothing, virtual for child classes

        USE_WRAPPED_MEM_CALLS(BaseGun);                                     // Wrapper function around "new" and "delete" for easy grepping

        virtual void Update( GunAction holderAction ) = 0;                  // Update the gun's state, based on (optional) input from the holder
                                                                            // Different types of guns will implement different state machines

        inline bool CanSwitch() const {                                     // The holder can only switch or drop the gun if it's currently idle
            return currentState == GunState::Idle;
        }

        void Equip();                                                       // Equip the gun (either from picking it up off the ground or
                                                                            // switching from another weapon)

        inline Character* GetHolder() {                                     // Return a reference to the Character holding the gun
            return holder;
        }

        const sf::Texture& GetHUDPicture() const;                           // Getter for the picture of the gun that is displayed in the HUD

        float GetCurrentAccuracy() const {                                  // Function to easily get the current accuracy of the gun
            return BaseGunConstData->InherentAccuracy + currentRecoil;
        }

        float GetMovementSpeedMultiplier() const {                          // Getter for the effect the gun has on the holder's movement speed
            return BaseGunConstData->MovementSpeedMultiplier;
        }

        const BaseGun::ConstData* GetBaseGunConstData() const {             // Getter for the ConstData. Used by SlowBullet
            return BaseGunConstData;
        }

        virtual void DrawCrosshair( sf::RenderTarget& Target,               // Draw the gun's crosshair at the given position and with the given size
                                    Point2f position,
                                    float radius ) const;

        virtual void DrawAmmoCount( sf::RenderTarget& Target ) const {      // Draw the gun's current ammo situation (e.g. how much ammo in current clip
            (void)Target;                                                   // and how many clips left, or how long until overheating, etc...)
        }                                                                   // Right now does nothing, will implement when adding UI classes back
                                                                            // so we can easily draw text

    protected:
        const BaseGun::ConstData* BaseGunConstData;                         // A reference to the shared const data

        Character* holder;                                                  // the character that is holding the Gun

        GunState currentState;                                              // the current state that the gun in is
        sf::Time timeInCurrentState;                                        // how long the gun has been in it's current state

        unsigned int burstsToBeFired;                                       // the number of bursts that are queued up to be fired

        float currentRecoil;                                                // how much recoil the gun currently has

        void SwitchToState( GunState newState ) {                           // Switch the gun to a new state, resetting the timer
            dbAssert( newState != currentState, "Didn't switch states" );
            currentState = newState;
            timeInCurrentState = sf::Time::Zero;
        }

        void AdjustRecoil( float delta );                                   // Add or remove recoil

        virtual void ShootBurst() = 0;                                      // Fire a burst of projectiles
};

#endif // BASEGUN_HPP_INCLUDED
