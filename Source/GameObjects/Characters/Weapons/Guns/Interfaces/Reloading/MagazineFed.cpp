/*
 * This file implements the abstract
 * base class for magazine-fed guns
 * such as a pistol or machine gun
 *
 * Author:      Stuart Rudderham
 * Created:     April 7, 2012
 * Last Edited: January 7, 2013
 */

#include "GameObjects/Characters/Weapons/Guns/Interfaces/Reloading/MagazineFed.hpp"
#include "Managers/Sound/SoundManager.hpp"
#include "Math/MathFunctions.hpp"

MagazineFed::MagazineFed( const MagazineFed::ConstData& data )
    : MagazineFedConstData( &data ),
      currentNumberOfClips( data.StartingNumberOfClips ),
      numberOfBurstsInCurrentClip( data.NumberOfBurstsPerClip )
{

}

bool MagazineFed::HaveEnoughAmmoToShoot() const {
    return numberOfBurstsInCurrentClip >= BaseGunConstData->NumberOfBurstsPerShot;
}

bool MagazineFed::HaveClipsLeft() const {
    return currentNumberOfClips > 0;
}

bool MagazineFed::OutOfAmmo() const {
    return numberOfBurstsInCurrentClip == 0;
}

bool MagazineFed::NeedToReload() const {
    return numberOfBurstsInCurrentClip != MagazineFedConstData->NumberOfBurstsPerClip;
}

bool MagazineFed::CanShootBurst() const {
    sf::Int64 burstNum = BaseGunConstData->NumberOfBurstsPerShot - burstsToBeFired;
    return timeInCurrentState >= burstNum * BaseGunConstData->DelayTimeBetweenBursts;
}

void MagazineFed::TryToReload() {

    // If we don't need to reload then do nothing
    if( NeedToReload() == false ) {
        return;

    // Otherwise if we have clips left then reload
    } else if( HaveClipsLeft() ) {
        currentNumberOfClips--;

        // We don't want to update this here, we want to wait
        // until we're actually "finished" reloading
        /* numberOfBurstsInCurrentClip = MagazineFedConstData->NumberOfBurstsPerClip; */

        // Play reloading sound
        SoundManager::Get().PlaySoundFX( MagazineFedConstData->ReloadSound );

        // Make the crosshair go big
        AdjustRecoil( BaseGunConstData->MaxRecoil / 2 );

        SwitchToState( GunState::Reloading );

    // If we have no clips left then can't do anything
    } else {
        // Play empty "click" sound
    }
}

void MagazineFed::Update( GunAction holderAction ) {
    switch( currentState ) {

        // If we're not doing anything
        case GunState::Idle:

            // If the user wants to shoot the gun
            if( holderAction == GunAction::PullTrigger || ( holderAction == GunAction::TriggerHeldDown && BaseGunConstData->IsAutomatic == true ) ) {

                // If they have enough ammo then shoot, otherwise try to reload
                if( HaveEnoughAmmoToShoot() ) {
                    SwitchToState( GunState::Shooting );
                    burstsToBeFired = BaseGunConstData->NumberOfBurstsPerShot;
                } else {
                    TryToReload();
                }

            // If the user wants to reload then try to oblige them
            } else if( holderAction == GunAction::Reload ) {
                TryToReload();

            // If the user doesn't want to do anything then reduce recoil
            } else {
                AdjustRecoil( -Game::DeltaT.asSeconds() / BaseGunConstData->TimeToLoseMaxRecoil.asSeconds() * BaseGunConstData->MaxRecoil );
            }
            break;

        // If we're in the process of shooting
        case GunState::Shooting:

            // If we still have bursts to fire and enough time has passed
            // since our last burst (if there was one) then shoot
            if( burstsToBeFired > 0 && CanShootBurst() ) {

                // Delegate to a sister class that has defined this function.
                // See -> http://www.parashift.com/c++-faq/mi-delegate-to-sister.html
                ShootBurst();

                // Update bookkeeping
                burstsToBeFired--;
                numberOfBurstsInCurrentClip--;
            }

            // If the appropriate amount of time has passed
            if( timeInCurrentState >= BaseGunConstData->ShotTime ) {
                dbAssert( burstsToBeFired == 0, "Didn't get to fire all bursts" );

                // If that was our last shot then automatically
                //  reload if we can
                if( OutOfAmmo() ) {
                    TryToReload();

                // Otherwise we're done
                } else {
                    SwitchToState( GunState::Idle );
                }
            }
            break;

        // If we're in the process of reloading then just wait
        // until the appropriate amount of time has passed
        case GunState::Reloading:
            if( timeInCurrentState >= BaseGunConstData->ReloadTime ) {

                // We set this here because we wan't want the HUD
                // to update until we have "finished" reloading
                numberOfBurstsInCurrentClip = MagazineFedConstData->NumberOfBurstsPerClip;
                SwitchToState( GunState::Idle );
            }
            break;

        // If we're in the process of equipping then just wait
        // until the appropriate amount of time has passed
        case GunState::Equipping:
            if( timeInCurrentState >= BaseGunConstData->EquipTime ) {
                SwitchToState( GunState::Idle );
            }
            break;

        default:
            // Make warnings happy
            break;
    }

    dbPrintToScreen("Gun name: ", BaseGunConstData->Name );
    dbPrintToScreen("Gun state: ", (int)currentState );
    dbPrintToScreen("Time in state: ", timeInCurrentState.asSeconds() );
    dbPrintToScreen("Num Clips left: ", currentNumberOfClips );
    dbPrintToScreen("Current Ammo: ", numberOfBurstsInCurrentClip );

    timeInCurrentState += Game::DeltaT;
}
