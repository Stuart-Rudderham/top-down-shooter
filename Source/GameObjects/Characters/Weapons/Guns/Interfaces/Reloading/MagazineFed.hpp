/*
 * This header defines the abstract
 * base class for magazine-fed guns
 * such as a pistol or machine gun
 *
 * Author:      Stuart Rudderham
 * Created:     April 7, 2012
 * Last Edited: January 7, 2013
 */

#ifndef MAGAZINEFED_HPP_INCLUDED
#define MAGAZINEFED_HPP_INCLUDED

#include "GameObjects/Characters/Weapons/Guns/Interfaces/BaseGun.hpp"

class MagazineFed : virtual public BaseGun {
    public:
        struct ConstData
        {
            const unsigned int MaxNumberOfClips;			    // the total number of clips the character can carry
            const unsigned int StartingNumberOfClips;			// the default starting number of clips the gun has when you first pick it up
            const unsigned int NumberOfBurstsPerClip;			// how many bursts are in one clip
            const ExternalFile ReloadSound;                     // the sound the gun makes when it's reloaded

            ConstData(
                       unsigned int max_number_of_clips,
                       unsigned int starting_number_of_clips,
                       unsigned int number_of_bursts_per_clip,
                       ExternalFile reload_sound,
                       BaseGun::ConstData data
                     )
                : MaxNumberOfClips          ( max_number_of_clips       ),
                  StartingNumberOfClips     ( starting_number_of_clips  ),
                  NumberOfBurstsPerClip     ( number_of_bursts_per_clip ),
                  ReloadSound               ( reload_sound              )
            {
                // Sanity checks
                dbWarn  ( MaxNumberOfClips      > 0u                             , "ConstData may be wrong for ", data.Name );    // Should be able to hold at least one clip
                dbWarn  ( StartingNumberOfClips > 0u                             , "ConstData may be wrong for ", data.Name );    // Should start with at least one clip
                dbAssert( MaxNumberOfClips >= StartingNumberOfClips              , "ConstData is wrong for "    , data.Name );    // Cannot start with more clips than the max
                dbAssert( NumberOfBurstsPerClip > 0u                             , "ConstData is wrong for "    , data.Name );    // Each clip must have at least one shot
                dbWarn  ( NumberOfBurstsPerClip % data.NumberOfBurstsPerShot == 0, "ConstData may be wrong for ", data.Name );    // If NumberOfBurstsPerClip isn't a multiple of NumberOfBurstsPerShot then there will always be wasted ammo
                (void)data;
            }
        };

        void Update( GunAction holderAction ) override;             // Update the internal state of the gun based on user action

    protected:
        MagazineFed( const MagazineFed::ConstData& data );          // Ctor. Takes in a reference to the appropriate ConstData

    private:
        const MagazineFed::ConstData* MagazineFedConstData;         // A reference to the shared const data

        unsigned int currentNumberOfClips;				            // The current number of clips the character has
        unsigned int numberOfBurstsInCurrentClip;			        // How many bursts are in the clip that is currently loaded

        bool HaveEnoughAmmoToShoot() const;                         // Query function to check how if we have enough ammo left to shoot

        bool OutOfAmmo() const;                                     // Query function to check how if our current clip is empty

        bool HaveClipsLeft() const;                                 // Query function to check if we have any clips left to use to reload

        bool CanShootBurst() const;                                 // Query function to check if we can shoot a burst right now
                                                                    // (i.e. enough time has passed since our last burst)

        bool NeedToReload() const;                                  // Query function to check if we have a full clip

        void TryToReload();                                         // Reload the gun if we have ammo left
};

#endif // MAGAZINEFED_HPP_INCLUDED
