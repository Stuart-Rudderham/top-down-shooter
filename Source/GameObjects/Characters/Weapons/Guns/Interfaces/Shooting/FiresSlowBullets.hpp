/*
 * This header defines an interface
 * for a gun that fires non-hitscan
 * bullets
 *
 * Author:      Stuart Rudderham
 * Created:     December 10, 2012
 * Last Edited: January 7, 2013
 */

#ifndef FIRESSLOWBULLETS_HPP_INCLUDED
#define FIRESSLOWBULLETS_HPP_INCLUDED

#include "GameObjects/Characters/Weapons/Guns/Interfaces/BaseGun.hpp"

class FiresSlowBullets : virtual public BaseGun {
    public:
        struct ConstData
        {
            const float BulletSpeed;                                                    // how fast the fired bullets travel
            const float BulletSize;                                                     // how big the fired bullets are
            const sf::Color BulletColor;                                                // what color the fired bullets are
            const ExternalFile ShotSound;                                               // the sound the gun makes when a burst is fired

            ConstData(
                       float bullet_speed,
                       float bullet_size,
                       sf::Color bullet_color,
                       ExternalFile shot_sound,
                       BaseGun::ConstData data
                     )
                : BulletSpeed   ( bullet_speed ),
                  BulletSize    ( bullet_size  ),
                  BulletColor   ( bullet_color ),
                  ShotSound     ( shot_sound   )
            {
                dbAssert( BulletSpeed > 0.0f , "ConstData is wrong for ", data.Name );  // bullet cannot have negative speed
                dbAssert( BulletSize > 0.0f  , "ConstData is wrong for ", data.Name );  // bullet cannot have negative size
                (void)data;
            }
        };

        const FiresSlowBullets::ConstData* GetFiresSlowBulletsConstData() const {       // Getter for the ConstData. Used by SlowBullet
            return FiresSlowBulletsConstData;
        }

    protected:
        FiresSlowBullets( const FiresSlowBullets::ConstData& data );                    // Ctor. Takes in a reference to the appropriate ConstData

    private:
        const FiresSlowBullets::ConstData* FiresSlowBulletsConstData;                   // A reference to the shared const data

        void FireProjectile();                                                          // Fire one projectile
        void ShootBurst() override;                                                     // Fire a burst of projectiles
};

#endif // FIRESSLOWBULLETS_HPP_INCLUDED
