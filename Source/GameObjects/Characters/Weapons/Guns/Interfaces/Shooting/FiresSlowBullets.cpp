/*
 * This file implements an interface
 * for a gun that fires non-hitscan
 * bullets
 *
 * Author:      Stuart Rudderham
 * Created:     December 10, 2012
 * Last Edited: January 12, 2013
 */

#include "GameObjects/Characters/Weapons/Guns/Interfaces/Shooting/FiresSlowBullets.hpp"
#include "GameObjects/Projectiles/SlowBullet.hpp"
#include "GameObjects/Characters/Character.hpp"
#include "Managers/Sound/SoundManager.hpp"

FiresSlowBullets::FiresSlowBullets( const FiresSlowBullets::ConstData& data )
    : FiresSlowBulletsConstData( &data )
{

}

void FiresSlowBullets::FireProjectile() {

    // Calculate the direction the SlowBullet will travel in
    float accuracy = GetCurrentAccuracy();
    float direction = GetHolder()->GetAngle() + RandFloat( -accuracy, accuracy );   // get the direction for the bullet the travel

    // Should each holder have a "BulletSpawnPoint" variable in their
    // const data so that it is customized for each class??
    Point2f creationPoint = GetHolder()->GetPosition() + GetDirectionVector( GetHolder()->GetAngle(), 30 );

    // Create the bullet
    SlowBullet::Create( this, creationPoint, direction );
}

void FiresSlowBullets::ShootBurst() {

    // Play shooting sound
    SoundManager::Get().PlaySoundFX( FiresSlowBulletsConstData->ShotSound );

    for( unsigned int i = 0; i < BaseGunConstData->NumberOfProjectilesPerBurst; ++i ) {
        FireProjectile();
    }

    AdjustRecoil( BaseGunConstData->RecoilIncreasePerBurst );
}
