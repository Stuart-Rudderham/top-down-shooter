/*
 * This header defines the generic
 * character class that is used for
 * every player and NPC in the game
 *
 * Author:      Stuart Rudderham
 * Created:     March 16, 2012
 * Last Edited: January 10, 2013
 */

#ifndef CHARACTER_HPP_INCLUDED
#define CHARACTER_HPP_INCLUDED

#include "Interfaces/Movement/MoveableObject.hpp"
#include "Interfaces/Collision/RotatableObject.hpp"
#include "Interfaces/Drawing/DrawableSprite.hpp"
#include "Interfaces/Intelligence/IntelligentObject.hpp"
#include "Interfaces/Health/MortalObject.hpp"

#include "Math/Geometry/ConvexPolygon.hpp"

#include "MediaCache/MediaCache.hpp"

#include <SFML/Window/Keyboard.hpp>
#include <vector>

// Forward Declarations
class ControlScheme;

// This enum defines the different factions that characters
// can belong to. What faction a Character is in determines
// what other Characters it will attack on sight
namespace Faction {
    enum Enum : unsigned char {
        Survivor,
        Zombie,
        Max
    };
};

// This class defines data that can be shared between all
// instances of a class (e.g. movement speed, local outline, etc...)
// Each child class will have its own version of this (e.g. SoldierConstData)
// that will have values unique to the child class
struct CharacterConstData {

    // Movement data
    float MoveSpeed;
    float MaxSpeed;
    float DecelerationSpeed;
    float RotationSpeed;

    // Health data
    float MaxHealth;

    // Pictures
    ExternalFile AlivePicture;
    ExternalFile DeadPicture;

    // Collision outlines (in local coords)
    ConvexPolygon* LocalCharacterOutline;
    ConvexPolygon* LocalFieldOfViewOutline;

    CharacterConstData() : MoveSpeed( 0 ),
                           MaxSpeed( 0 ),
                           DecelerationSpeed( 0 ),
                           RotationSpeed( 0 ),
                           MaxHealth( 0 ),
                           AlivePicture( nullptr ),
                           DeadPicture( nullptr ),
                           LocalCharacterOutline( nullptr ),
                           LocalFieldOfViewOutline( nullptr )
    {

    }

    ~CharacterConstData() {
        BaseShape::Destroy( LocalCharacterOutline );
        BaseShape::Destroy( LocalFieldOfViewOutline );
    }
};

class Character : public MoveableObject, public RotatableObject, public DrawableSprite, public IntelligentObject, public MortalObject {
    protected:
        const CharacterConstData& ConstData;                                            // a reference to the const data that is shared by all instances of a class

        Faction::Enum faction;                                                          // what faction this Character belongs to

        // MortalObject Interface
        void DeathReaction() override;

    public:
        Character( const CharacterConstData& characterData,                             // ctor. Created a Character at the given position with the given faction and
                   Point2f pos,                                                         // const data
                   Faction::Enum f );

        virtual ~Character() {};                                                        // dtor. Doesn't need to do anything, just declaring it so
                                                                                        // it will be virtual

        // MoveableObject Interface
        virtual void MoveBy( sf::Vector2f displacement ) override;
        void FrameUpdate( float deltaT )                 override;
        void TimeSliceUpdate( float deltaT )             override;

        // CollidableObject Interface
        void CollisionResponse( CollidableObject* otherObject, sf::Vector2f mtv, sf::Vector2f normal ) override;

        // DrawableSprite Interface
        void Draw( sf::RenderTarget& Target ) override;

        // MortalObject Interface
        void Respawn(/* Point2f spawnPoint */) override;

        // Faction stuff
        inline Faction::Enum GetFaction() const {                                       // gettter for the Faction
            return faction;
        }

        inline void SetFaction( Faction::Enum f ) {                                     // setter for the Faction
            faction = f;
        }

        inline const CharacterConstData& GetConstData() const {                         // getter for the ConstData
            return ConstData;
        }

        // Controller stuff
        virtual void MakeHuman( ControlScheme& controls ) = 0;                          // make the Character human controlled with the given control scheme
        virtual void MakeBot()                            = 0;                          // make the Character AI controlled

        static Character* MainCharacter;                                                // the player that the camera and audio are relative to
        const static bool FactionDisposition[Faction::Max][Faction::Max];               // this array defines which factions will attack each other

        static inline bool ShouldAttack( const Character* A, const Character* B ) {     // helper function for deciding if characters should fight each other
            return FactionDisposition[A->GetFaction()][B->GetFaction()] == true;
        }
};

#endif // CHARACTER_HPP_INCLUDED
