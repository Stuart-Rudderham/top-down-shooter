/*
 * This file implements the zombie
 * character
 *
 * Author:      Stuart Rudderham
 * Created:     March 25, 2012
 * Last Edited: January 8, 2013
 */


#include "GameObjects/Characters/Zombie.hpp"
#include "MediaCache/MediaCache.hpp"
#include "UpgradesToSFML/Input.hpp"

#include "GameObjects/Characters/Controllers/Human/HumanController.hpp"
#include "GameObjects/Characters/Controllers/AI/AIController.hpp"

Zombie::Zombie( Point2f pos, Faction::Enum f ) : Character( Zombie::ZombieSpecificConstData, pos, f )
{
    // Setup AI
    MakeBot();
}

Zombie::Zombie( ControlScheme& controls, Point2f pos, Faction::Enum f ) : Zombie( pos, f )
{
    // Remove the AI and add a user controller
	MakeHuman( controls );
}

// USER CONTROLLER
template<>
void HumanController::SpecializedUpdateLogic<Zombie>() {

    Zombie* parent = GetParentAs<Zombie>();

    // Update the direction of the Soldier based on the position of the mouse cursor
    sf::Vector2f direction = GetDirectionVector( parent->GetPosition(), MouseState::GetPosition() );
    parent->SetAngle( ConvetToAngle( direction ) );


    // Update the movement of the Soldier
    if( userInput[ZombieControls::MoveUp]->IsDown() ) {
        parent->ApplyImpulse( MathConstants::yAxis * -parent->GetConstData().MoveSpeed );

    } else if( userInput[ZombieControls::MoveDown]->IsDown() ) {
        parent->ApplyImpulse( MathConstants::yAxis * parent->GetConstData().MoveSpeed );

    } else {
        //velocity.y = 0;
    }

    if( userInput[ZombieControls::MoveLeft]->IsDown() ) {
        parent->ApplyImpulse( MathConstants::xAxis * -parent->GetConstData().MoveSpeed );

    } else if( userInput[ZombieControls::MoveRight]->IsDown() ) {
        parent->ApplyImpulse( MathConstants::xAxis * parent->GetConstData().MoveSpeed );

    } else {
        //velocity.x = 0;
    }
}

// AI CONTROLLER
template<>
void AIController::SpecializedHaveTargetAction<Zombie>() {

    // Make sure we actually have a target
    dbAssert( FOV->HaveTarget() == true, "Don't have a target" );

    Zombie* parent = GetParentAs<Zombie>();

    // Look and walk towards them
    sf::Vector2f direction = GetDirectionVector( parent->GetPosition(), FOV->GetTargetPosition() );
    parent->SetAngle( ConvetToAngle( direction ) );

    // Speed boost when near target
    if( GetMagnitudeSquared(direction) < 150 * 150 ) {
        direction = GetUnitVector( direction );
        parent->ApplyImpulse( direction * parent->GetConstData().MoveSpeed);
    } else {
        direction = GetUnitVector( direction );
        parent->ApplyImpulse( direction * parent->GetConstData().MoveSpeed);
    }
}

template<>
void AIController::SpecializedNoTargetAction<Zombie>() {

    // Make sure we actually don't have a target
    dbAssert( FOV->HaveTarget() == false, "Have a target" );

    Zombie* parent = GetParentAs<Zombie>();

    // Keep searching
    parent->SetRotationSpeed( parent->GetConstData().RotationSpeed );
}

template<>
void AIController::SpecializedCollisionReaction<Zombie>( CollidableObject* otherObject ) {

    // If we collided with a Character
    if( otherObject->IsCharacter() ) {
        Character* character = otherObject->GetAs<Character>();

        // If we collided with an enemy character
        if( Character::ShouldAttack( GetParentAs<Zombie>(), character ) ) {

            // TODO FIXME so it only happens when we're facing them
            character->ModifyHealth( -1 );
        }
    }
}



void Zombie::MakeHuman( ControlScheme& controls ) {
    dbAssert( controls.NumInputs() == ZombieControls::Max, "Number of inputs given was ", controls.NumInputs() );
    SetController( SpecializedHumanController<Zombie>::Create( this, controls ) );
}

void Zombie::MakeBot() {
    SetController( SpecializedAIController<Zombie>::Create( this ) );
}
