/*
 * This header defines the HUD class
 * for drawing game UI onto the screen
 *
 * Author:      Stuart Rudderham
 * Created:     December 12, 2012
 * Last Edited: December 25, 2012
 */

#ifndef HUD_HPP_INCLUDED
#define HUD_HPP_INCLUDED

#include "GameObjects/Characters/HUD/MiniMap.hpp"
#include "Singleton.hpp"
#include <SFML/Graphics/RenderTarget.hpp>

class HUD : public Singleton<HUD> {
    private:
        // We don't need a reference to the Character that the HUD is for
        // because we use the static variable Character::MainCharacter
        
        MiniMap miniMap;

    public:
        HUD();

        void UpdateMiniMap();                       // Update the MiniMap
            
        template<typename T>                        // Draw the HUD for the current main Character
        void Draw( sf::RenderTarget& Target );      // The templated argument defines the type of HUD
                                                    // to draw
};

#endif // HUD_HPP_INCLUDED
