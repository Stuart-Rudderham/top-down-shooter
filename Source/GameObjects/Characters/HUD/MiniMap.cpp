/*
 * This header implements the class used
 * for the minimap section of the HUD
 *
 * Author:      Stuart Rudderham
 * Created:     December 24, 2012
 * Last Edited: January 12, 2013
 */

#include "GameObjects/Characters/HUD/MiniMap.hpp"
#include "GameObjects/Characters/Character.hpp"
#include "Math/Geometry/Circle.hpp"
#include <SFML/Graphics/CircleShape.hpp>

// Constructor
MiniMap::MiniMap( float r )
    : CollidableObject( CollidableObjectType::MiniMap, Circle::Create( MathConstants::Origin, r ), Physics::Priority::Phantom ),
      range( r )
{

}

void MiniMap::UpdateLocation() {
    dbAssert( Character::MainCharacter != nullptr, "No current main Character" );
    MoveCollisionOutlineTo( Character::MainCharacter->GetPosition() );
}

void MiniMap::ClearCharactersInRange() {
    charactersInRange.clear();
}

void MiniMap::Draw( sf::RenderTarget& Target, Point2f pos, float size ) const {

    dbAssert( Character::MainCharacter != nullptr, "No current main Character" );

    // Draw the outline
    {
        sf::CircleShape outline(size);

        outline.setPosition( pos );
        outline.setRadius( size );
        outline.setOrigin( size, size );

        outline.setFillColor( HUDOptions::MiniMap::Color );
        outline.setOutlineThickness( -HUDOptions::MiniMap::BezelThickness );
        outline.setOutlineColor( sf::Color::Black );

        Target.draw( outline );
    }

    // Draw all the near by characters
    for( auto i = charactersInRange.begin(); i != charactersInRange.end(); ++i ) {

        // Get the distance from the nearby Character to the main Character
        sf::Vector2f displacement = GetDirectionVector( Character::MainCharacter->GetPosition(), (**i).GetPosition() );
        float distance = GetMagnitude( displacement );

        // Because we do an actual collision check (rather than a distance check)
        // it is possible that the Character collided with the MiniMap collision
        // outline but their center point is actually (slightly) outside the range
        // of the MiniMap. To make the math/drawing nice we just clamp any values
        // to the designated range
        Clamp( 0.0f, distance, range );

        // Convert the distance in game units into distance in screen units
        float scaledDistance = Lerp( 0.0f, distance / range, size );
        sf::Vector2f scaledDisplacement = sf::Vector2f(0, 0);

        // If the scaledDistance is 0 then we have a divide by 0 error when
        // calculating the unit vector
        if( scaledDistance != 0.0f ) {
            scaledDisplacement = GetUnitVector(displacement) * scaledDistance;
        }

        // Draw the dot representing the Character
        sf::CircleShape character( HUDOptions::MiniMap::CharacterDotSize, 6 );
        character.setOrigin( HUDOptions::MiniMap::CharacterDotSize, HUDOptions::MiniMap::CharacterDotSize );
        character.setPosition( pos + scaledDisplacement );

        if( (**i).GetID() == Character::MainCharacter->GetID() ) {
            character.setFillColor( HUDOptions::MiniMap::SelfColor );
        } else if( Character::ShouldAttack( *i, Character::MainCharacter ) == false ) {
            character.setFillColor( HUDOptions::MiniMap::AllyColor );
        } else {
            character.setFillColor( HUDOptions::MiniMap::EnemyColor );
        }

        character.setOutlineThickness( 0.0f );

        Target.draw( character );
    }
}

// CollidableObject Interface
void MiniMap::CollisionResponse( CollidableObject* otherObject, sf::Vector2f mtv, sf::Vector2f normal ) {

    // Make sure the thing we're colliding with is a Character
    dbAssert( otherObject->IsCharacter(), "Collided with something that isn't a Character" );

    // Add the character to the list
    charactersInRange.push_back( otherObject->GetAs<Character>() );

    (void)mtv;
    (void)normal;
}
