/*
 * This file implements the HUD class
 * for drawing game UI onto the screen
 *
 * Author:      Stuart Rudderham
 * Created:     December 12, 2012
 * Last Edited: December 25, 2012
 */

#include "GameObjects/Characters/HUD/HUD.hpp"
#include "GameObjects/Characters/Soldier.hpp"
#include "GameObjects/Characters/Weapons/Guns/Interfaces/BaseGun.hpp"
#include "DebugTools/Debug.hpp"
#include "UpgradesToSFML/Input.hpp"
#include "Managers/Collision/CollisionManager.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

HUD::HUD() : miniMap( Game::MiniMapRange )
{

}

void HUD::UpdateMiniMap() {
    miniMap.ClearCharactersInRange();
    miniMap.UpdateLocation();
    CollisionManager::Get().UpdateMiniMap();
}

template<>
void HUD::Draw<Soldier>( sf::RenderTarget& Target ) {

    // We assume that the current MainCharacter is a Soldier
    dbAssert( Character::MainCharacter != nullptr, "No current main Character" );
    Soldier* soldier = checked_cast<Soldier*>( Character::MainCharacter );

    const BaseGun* currentGun = soldier->GetCurrentGun();

    // Draw HUD elements that are relative to the camera (e.g. crosshair)
    {
        // Draw crosshair
        float accuracy = DegreeToRadian( currentGun->GetCurrentAccuracy() );
        float distance = GetMagnitude( GetDirectionVector( soldier->GetPosition(), MouseState::GetPosition() ) );
        float crosshairRadius = 4 + distance * tan(accuracy);

        currentGun->DrawCrosshair( Target, MouseState::GetPosition(), crosshairRadius );
    }

    // Cache the current view
    sf::View gameView = Target.getView();
    Target.setView( Target.getDefaultView() );

    // Draw HUD elements that are relative to the screen (e.g. mini-map)
    {
        // Draw the picture of the current weapon
        {
            // Draw the outline
            sf::RectangleShape outline;

            outline.setSize( HUDOptions::GunPicture::Size );
            outline.setPosition( HUDOptions::GunPicture::Position );
            //outline.setOrigin( HUDOptions::GunPicture::Size / -2.0f );


            outline.setFillColor( HUDOptions::GunPicture::BackgroundColor );
            outline.setOutlineColor( sf::Color::Black );
            outline.setOutlineThickness( -2.0f );

            Target.draw( outline );

            // Draw the picture
            sf::Sprite gunPicture( currentGun->GetHUDPicture() );       // The map lookup to find the texture each frame is a little worrysome, but
                                                                        // it's probably cheaper than implementing a listener infrastructure or
                                                                        // something similar (since we never know when the soldier is going to
                                                                        // switch to a new gun)
            sf::FloatRect pictureDimensions = gunPicture.getLocalBounds();

            float xScaleFactor = HUDOptions::GunPicture::Size.x / pictureDimensions.width;
            float yScaleFactor = HUDOptions::GunPicture::Size.y / pictureDimensions.height;

            // We want to scale the by the smaller amount, so neither
            // dimension is over the limit
            float scaleFactor = std::min( xScaleFactor, yScaleFactor );

            // Scale by the same amount in each direction, so there
            // is no distorion
            gunPicture.setScale( scaleFactor, scaleFactor );

            gunPicture.setPosition( HUDOptions::GunPicture::Position + HUDOptions::GunPicture::Size / 2.0f );
            gunPicture.setOrigin( pictureDimensions.width / 2.0f, pictureDimensions.height / 2.0f );

            Target.draw( gunPicture );

        }

        // Draw the current ammo count
        currentGun->DrawAmmoCount( Target );    // right now this does nothing, will change once we add
                                                // the UI classes back in so we can easily draw text

        // Draw the mini-map
        miniMap.Draw( Target, HUDOptions::MiniMap::Position, HUDOptions::MiniMap::Size );
    }

    // Revert back to the game view
    Target.setView( gameView );
}
