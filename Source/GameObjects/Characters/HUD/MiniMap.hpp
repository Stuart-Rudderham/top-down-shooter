/*
 * This header defines the class used
 * for the minimap section of the HUD
 *
 * Author:      Stuart Rudderham
 * Created:     December 24, 2012
 * Last Edited: December 24, 2012
 */

#ifndef MINIMAP_HPP_INCLUDED
#define MINIMAP_HPP_INCLUDED

#include "Interfaces/Collision/CollidableObject.hpp"
#include "Singleton.hpp"
#include <vector>

// Forward Declarations
class Character;

class MiniMap : public Singleton<MiniMap>, public CollidableObject {
    private:
        float range;                                            // how far out the MiniMap checks for Characters
            
        std::vector<Character*> charactersInRange;              // holds all the Characters that are in range of the MiniMap
       
    public:
        MiniMap( float r );                                     // ctor. Takes in the range

        void UpdateLocation();                                  // Snap the collision outline to the current main Character
        
        void ClearCharactersInRange();                          // Clear the list of Characters that are within range of the
                                                                // MiniMap
        
        void Draw( sf::RenderTarget& Target, Point2f pos, float size ) const;

        // CollidableObject Interface
        void CollisionResponse( CollidableObject* otherObject, sf::Vector2f mtv, sf::Vector2f normal ) override;

    // Friend classes for Factory creation
    friend class GameObjectFactory;
    template <typename T, unsigned int N> friend class GenericFactory;
};

#endif // MINIMAP_HPP_INCLUDED
