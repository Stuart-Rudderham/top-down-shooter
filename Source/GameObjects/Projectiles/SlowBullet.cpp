/*
 * This file implements the slow-moving
 * (i.e. non-hitscan) bullet class
 *
 * Author:      Stuart Rudderham
 * Created:     April 8, 2012
 * Last Edited: January 10, 2013
 */

#include "GameObjects/Projectiles/SlowBullet.hpp"
#include "GameObjects/Characters/Character.hpp"
#include "Math/Geometry/Circle.hpp"

SlowBullet::SlowBullet( FiresSlowBullets* gun, Point2f pos, float dir )
    : MoveableObject( pos ),
      CollidableObject( CollidableObjectType::Projectile, Circle::Create( pos, gun->GetFiresSlowBulletsConstData()->BulletSize ), Physics::Priority::SlowBullet ),
      DrawableShape( Drawing::Level::Foreground ),
      shooter( gun->GetHolder() ),
      constData( *gun->GetBaseGunConstData() ),
      distanceTravelled( 0.0f )
{
    // MoveableObject Interface
    SetVelocity( GetDirectionVector( dir, gun->GetFiresSlowBulletsConstData()->BulletSpeed ) );

    // DrawableShape Interface
    SetDrawingOutline( GetCollisionOutline(), gun->GetFiresSlowBulletsConstData()->BulletColor );
}

// Damage the given character based on the SlowBullet's response
// to collision with a Character
void SlowBullet::ApplyDamage( Character* victim ) {
    switch( constData.HitCharacterResponse ) {
        case ProjectileResponse::Delete:
        case ProjectileResponse::Bounce:
            victim->ModifyHealth( -constData.Damage.Instant );
            break;
        case ProjectileResponse::Penetrate:
            victim->ModifyHealth( -constData.Damage.DPS * Game::DeltaT.asSeconds() );
            break;
    };
}

// Change position, velocity, and possibly delete the bullet in response to collision
void SlowBullet::ResolveCollision( ProjectileResponse response, sf::Vector2f mtv, sf::Vector2f normal ) {
    switch( response ) {
        case ProjectileResponse::Delete:
            MoveBy( mtv );
            MarkForDeletion();
            break;
        case ProjectileResponse::Bounce:
            MoveBy( mtv );
            SetVelocity( ReflectVector( GetVelocity(), normal ) );
            break;
        case ProjectileResponse::Penetrate:
            // Do nothing
            break;
    };
}

// MoveableObject Interface
void SlowBullet::MoveBy( sf::Vector2f displacement ) {
    // update your position and move the collision outline
    position += displacement;
    MoveCollisionOutlineBy( displacement );

    // keep track of total distance travelled
    distanceTravelled += (displacement.x * displacement.x) + (displacement.y * displacement.y);
}

void SlowBullet::FrameUpdate( float deltaT ) {
    (void)deltaT;
}

void SlowBullet::TimeSliceUpdate( float deltaT ) {

    // Move the SlowBullet
    UpdatePosition( deltaT );
}

// CollidableObject Interface
void SlowBullet::CollisionResponse( CollidableObject* otherObject, sf::Vector2f mtv, sf::Vector2f normal ) {

    // Should only collide with StaticGeo or Character
    dbAssert( otherObject->IsStaticGeo() || otherObject->IsCharacter(),
              "Collision filter isn't working, collided with object of type ",
              otherObject->GetCollidableObjectType() );

    // If hit StaticGeo then just resolve the collsiion
    if( otherObject->IsStaticGeo() ) {
        ResolveCollision( constData.HitStaticGeoResponse, mtv, normal );

    // If hit a Character then damage them
    } else if( otherObject->IsCharacter() ) {

        Character* character = otherObject->GetAs<Character>();

        ApplyDamage( character );
        ResolveCollision( constData.HitCharacterResponse, mtv, normal );
    }

    // To avoid warnings
    (void)mtv;
    (void)normal;
}

// DrawableShape Interface
void SlowBullet::Draw( sf::RenderTarget& Target ) {

    // Move the drawing outline to the current position and draw it
    GetDrawingOutline()->setPosition( GetPosition() );
    Target.draw( *GetDrawingOutline() );
}
