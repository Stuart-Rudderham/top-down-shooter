/*
 * This header defines the slow-moving
 * (i.e. non-hitscan) bullet class
 *
 * Author:      Stuart Rudderham
 * Created:     April 8, 2012
 * Last Edited: January 30, 2013
 */

#ifndef SLOWBULLET_HPP_INCLUDED
#define SLOWBULLET_HPP_INCLUDED

#include "Interfaces/Collision/CollidableObject.hpp"
#include "Interfaces/Movement/MoveableObject.hpp"
#include "Interfaces/Drawing/DrawableShape.hpp"
#include "GameObjects/Characters/Weapons/Guns/Interfaces/Shooting/FiresSlowBullets.hpp"

// Forward Declarations
class Character;

class SlowBullet : public MoveableObject, public CollidableObject, public DrawableShape {
    public:
        SlowBullet( FiresSlowBullets* gun, Point2f pos, float dir );    		// ctor. Create a SlowBullet at the given position
                                                                                // travelling in the given velocity, with the given size

        USE_WRAPPED_MEM_CALLS(SlowBullet);                                      // Wrapper function around "new" and "delete" for easy grepping
        IS_TRACKED_OBJECT(SlowBullet);                                          // Provide DestroyAll() and DestroyAllMarked() for garbage collecting GameObjects
        SET_ALLOCATOR_NAME(SlowBullet);                                         // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(1000);

        // MoveableObject Interface
        void MoveBy( sf::Vector2f displacement ) override;
        void FrameUpdate( float deltaT )         override;
        void TimeSliceUpdate( float deltaT )     override;

        // CollidableObject Interface
        void CollisionResponse( CollidableObject* otherObject, sf::Vector2f mtv, sf::Vector2f normal ) override;

        // DrawableShape Interface
        void Draw( sf::RenderTarget& Target ) override;

    private:
        Character* shooter;                                                     // A pointer to the Character that fired the bullet
        const BaseGun::ConstData& constData;

        float distanceTravelled;                                                // How far the bullet has travelled

        NO_COPY_OR_ASSIGN(SlowBullet);                                          // No copy/assign ctor

        TRACKED_NEW_AND_DELETE(SlowBullet);                                     // Overload "new" and "delete" to use ArenaAllocator

        void ApplyDamage( Character* victim );									// Damage the given character based on the SlowBullet's response
																				// to collision with a Character

        void ResolveCollision( ProjectileResponse response,						// Change position, velocity, and possibly delete the bullet in response to collision
                               sf::Vector2f mtv,
                               sf::Vector2f normal );
};

#endif // SLOWBULLET_HPP_INCLUDED
