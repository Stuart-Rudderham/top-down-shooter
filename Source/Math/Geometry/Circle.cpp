/*
 * This file implements a 2D circle
 *
 * Author:      Stuart Rudderham
 * Created:     January 29, 2012
 * Last Edited: November 4, 2012
 */

#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

using namespace std;
using namespace sf;

// default constructor. Creates an Circle located at (0, 0) with a radius of 0
Circle::Circle() : BaseShape( Point2f(0, 0), GeometricShape::Circle ),
                   radius( 0 )
{
    SanityCheck();
}

// constructor. Needs the center of the circle and the radius
Circle::Circle( Point2f c, float r ) : BaseShape( c, GeometricShape::Circle ),
                                       radius( r )
{
    SanityCheck();
}


// Sets the provided AABB to be the bounding box that encloses the shape
void Circle::SetBoundingBox( AABB* boundingBox ) const {

    boundingBox->MoveTo( center );
    boundingBox->SetHalfWidth( radius );
    boundingBox->SetHalfHeight( radius );
}


// Draws the circle onto the target
void Circle::Draw( RenderTarget& Target, Color fill, Color outline, float outlineThickness ) const {
    dbAssert( outlineThickness >= 0.0f, "Circle::Draw() - outlineThickness is negative. Given value was ", outlineThickness );

    CircleShape circle;

    // Set the location and size
    circle.setOrigin( radius, radius );
    circle.setPosition( center );
    circle.setRadius( radius );

    // Set the colors
    circle.setFillColor( fill );
    circle.setOutlineColor( outline );
    circle.setOutlineThickness( outlineThickness );

    // Draw the Circle
    Target.draw( circle );
}



// A sanity check function to confirm that the Circle is set up properly
void Circle::SanityCheck() {
    dbAssert( radius >= 0, "Radius is negative. Value is ", radius );
    dbAssert( IsPointInside( center ) == true, "Center is not inside Circle" );
    dbAssert( IsCircle(), "Type is not set to GeometricShape::Circle. Given type was ", geometricShape );
}
