/*
 * This header defines the generic
 * base shape class that all shapes
 * inherit from
 *
 * Author:      Stuart Rudderham
 * Created:     February 2, 2012
 * Last Edited: January 10, 2013
 */

#ifndef BASESHAPE_HPP_INCLUDED
#define BASESHAPE_HPP_INCLUDED

#include "Math/MathFunctions.hpp"
#include "Memory/MemoryAllocationMacros.hpp"
#include "DebugTools/Debug.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Color.hpp>

#include <utility>

// Forward Declarations
class AABB;
class Circle;
class ConvexPolygon;
class LineSegment;

// This enum defines the different type of shapes the BaseShape is parent to
namespace GeometricShape {
    enum Enum : signed char {
        Invalid = -1,
        AABB,
        Circle,
        ConvexPolygon,
        NumRigidBody,
        LineSegment
    };
};

class BaseShape {
    public:
        BaseShape( Point2f c, GeometricShape::Enum t );                         // constructor. Sets the location of the center point and what type of shape it actually is
        virtual ~BaseShape() {};                                                // dtor. Doesn't need to do anything, just declaring it so it will be virtual

        USE_WRAPPED_MEM_CALLS(BaseShape);                                       // Wrapper function around "new" and "delete" for easy grepping

        // Getter functions
        template<typename T>                                                    // Convenience function to cast the BaseShape to a child class
        inline T* GetAs() { return checked_cast<T*>( this ); }                  // If you cast it to the wrong type an assert will fire

        inline Point2f              GetCenter()           const { return center;         }
        inline GeometricShape::Enum GetGeometricShape()   const { return geometricShape; }

        inline bool IsAABB()          const { return geometricShape == GeometricShape::AABB;          }
        inline bool IsCircle()        const { return geometricShape == GeometricShape::Circle;        }
        inline bool IsConvexPolygon() const { return geometricShape == GeometricShape::ConvexPolygon; }
        inline bool IsLineSegment()   const { return geometricShape == GeometricShape::LineSegment;   }

        // Bounding Volumes
        virtual void SetBoundingBox( AABB* boundingBox ) const = 0;             // Sets the provided AABB to be the bounding box that encloses the shape


        // Movement functions
        virtual void MoveBy( sf::Vector2f displacement ) {                      // shifts the BaseShape by the displacement vector
            center += displacement;
        }

        inline void MoveTo( Point2f newPosition ) {                             // moves the BaseShape so that its center point is at
            MoveBy( GetDirectionVector( center, newPosition ) );                // the given position
        }


        // Drawing functions
        virtual void Draw( sf::RenderTarget& Target,                            // draws the shape to the target (Window, texture, etc...)
                           sf::Color fill = sf::Color::White,                   // If no arguments are passed a white shape with a thin
                           sf::Color outline = sf::Color::Black,                // black outline is drawn
                           float outlineThickness = 1.0 ) const = 0;

        void DrawBoundingBox( sf::RenderTarget& Target,                         // draws the axis-aligned bounding box that encloses the shape
                              sf::Color fill = sf::Color::Transparent,          // If no arguments are passed it draws a transparent box with a
                              sf::Color outline = sf::Color::Black,             // thin black outline
                              float outlineThickness = 1.0 ) const;

        void DrawBoundingCircle( sf::RenderTarget& Target,                      // draws a circle that is located at the center point of the shape
                                 sf::Color fill = sf::Color::Transparent,       // and encloses the shape completely. If no arguments are passed it
                                 sf::Color outline = sf::Color::Black,          // draws a transparent circle with a thin black outline
                                 float outlineThickness = 1.0 ) const;

        void DrawCenterDot( sf::RenderTarget& Target,                           // draws a dot located at the center point of the shape
                            sf::Color color = sf::Color::Black,                 // If no arguments are passed it draws a black dot with a radius of 5
                            float dotRadius = 2.0f ) const;


        // Linear algebra
        virtual Pair Project( sf::Vector2f axis ) const = 0;                    // projects the shape onto the provided axis, which is a unit vector
                                                                                // the minimum value of the projection is stored in Pair.x, while the
                                                                                // maximum value of the projection is stored in Pair.y

        virtual bool IsPointInside( Point2f p ) const = 0;                      // returns true if the point is inside the shape

        virtual Point2f GetClosestPointOnOutline( Point2f p ) const = 0;        // Given a shape and a point P, return the point on the outline of the shape that
                                                                                // is closest to the point P

        virtual Point2f GetClosestPointOnShape( Point2f p ) const ;             // Given a shape and a point P, return the point on the shape (possibly inside the shape)
                                                                                // that is closest to the point P
                                                                                // It is implemented in BaseShape but marked as virtual because AABB has an optimized version
                                                                                // that we want to use if possible
    protected:
        Point2f center;                                                         // the center point of the shape

        GeometricShape::Enum geometricShape;                                    // What type of shape the BaseShape actually is
                                                                                // Used for collision detection

        virtual void SanityCheck() = 0;                                         // runs a bunch of checks to confirm that the shape is formatted properly
                                                                                // If it's not a dbAssert will catch and exit the program
};

#endif // BASESHAPE_HPP_INCLUDED
