#if 0

/*
 * This file implements a 2D Convex Polygon. The points
 * are defined in a clockwise ordering
 *
 * Author:      Stuart Rudderham
 * Created:     January 31, 2012
 * Last Edited: March 16, 2012
 */

#include "Math/Geometry/FastConvexPoly.hpp"
#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"

#include "UpgradesToSFML/Drawing.hpp"
#include <mmintrin.h>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

using namespace std;
using namespace sf;

// Constructor. Can only pass the number of sides, need to initialize the points separately
FastConvexPoly::FastConvexPoly( unsigned int n ) : BaseShape( Point2f(0, 0), SHAPE_TYPE_CONVEX_POLYGON ),
                                                   numPoints( n )
{
    // Make sure have valid number of points
    dbAssert( numPoints >= 3, "Too few points. Number of points is ", numPoints );
    dbAssert( numPoints <= maxPoints, "Too many points. Number of points in ", numPoints );

    // Initialize all the points to (0, 0)
    for( unsigned int i = 0; i < maxPoints; ++i ) {
        xCoords[i] = 0;
        yCoords[i] = 0;
        normals[i] = Vector2f(0, 0);
    }

    //SanityCheck();        // can't do check because the polygon isn't setup yet
}

// Constructor that initializes all the points at once
FastConvexPoly::FastConvexPoly( const std::vector<Point2f>& polygonPoints ) : BaseShape( Point2f(0, 0), SHAPE_TYPE_CONVEX_POLYGON ),
                                                                              numPoints( polygonPoints.size() )
{
    // Make sure have valid number of points
    dbAssert( numPoints >= 3, "Too few points. Number of points is ", numPoints );
    dbAssert( numPoints <= maxPoints, "Too many points. Number of points in ", numPoints );

    unsigned int i;
    for( i = 0; i < numPoints; ++i ) {
        xCoords[i] = polygonPoints[i].x;
        yCoords[i] = polygonPoints[i].y;
    }

    // Initialize all the points to (0, 0)
    for( ; i < maxPoints; ++i ) {
        xCoords[i] = xCoords[numPoints - 1];
        yCoords[i] = yCoords[numPoints - 1];
    }

    // Calculate the location of the center point
    center = CalculateCenter();

    // Calculate the normals
    CalculateSideNormals();

    SanityCheck();
}


// Alternate constructor, makes the polygon a regular n-gon
FastConvexPoly::FastConvexPoly( unsigned int n, Point2f c, float radius ) : BaseShape( Point2f(0, 0), SHAPE_TYPE_CONVEX_POLYGON ),
                                                                            numPoints( n )
{
    // Make sure have valid number of points
    dbAssert( numPoints >= 3, "Too few points. Number of points is ", numPoints );
    dbAssert( numPoints <= maxPoints, "Too many points. Number of points in ", numPoints );

    MakeRegularNGon( c, radius );

    SanityCheck();
}


// copy constructor
FastConvexPoly::FastConvexPoly( const FastConvexPoly& original ) : BaseShape( original.center, SHAPE_TYPE_CONVEX_POLYGON ),
                                                                  numPoints( original.numPoints )
{
    // Make sure have valid number of points
    dbAssert( numPoints >= 3, "Too few points. Number of points is ", numPoints );
    dbAssert( numPoints <= maxPoints, "Too many points. Number of points in ", numPoints );

    // Copy over the info
    for( unsigned int i = 0; i < maxPoints; ++i ) {
        xCoords[i] = original.xCoords[i];
        yCoords[i] = original.yCoords[i];
        normals[i] = original.normals[i];
    }

    // Make sure everything makes sense
    SanityCheck();
}


// Assignment operator
FastConvexPoly& FastConvexPoly::operator=( const FastConvexPoly& other ) {

    dbAssert( numPoints == other.numPoints, "Cannot assign to a FastConvexPoly with different number of points" );

    // Copy it over
    for( unsigned int i = 0; i < maxPoints; ++i ) {
        xCoords[i] = other.xCoords[i];
        yCoords[i] = other.yCoords[i];
        normals[i] = other.normals[i];
    }

    center = other.center;

    // Confirm everything makes sense
    SanityCheck();

    return *this;
}


// Makes the polygon a regular n-gon that is is circumscribed by a circle centered at the given point with given radius
void FastConvexPoly::MakeRegularNGon( Point2f c, float radius ) {

    float angleStep = DegreeToRadian( 360.0f / (float)numPoints );

    float currentAngle = (float)-MathConstants::PI_2;               // start at -90 degrees (i.e. pointing at the top of the screen)

    // Make sure the bottom of the shape is a flat edge rather than a point
    // So if there's an even number of sides we have to rotate the shape slightly
    if( numPoints % 2 == 0 ) {
        currentAngle -= angleStep / 2.0f;
    }

    unsigned int i;
    for( i = 0; i < numPoints; ++i ) {
        xCoords[i] = c.x + cos(currentAngle) * radius;
        yCoords[i] = c.y + sin(currentAngle) * radius;

        currentAngle += angleStep;
    }

    // Initialize all the points to (0, 0)
    for( ; i < maxPoints; ++i ) {
        xCoords[i] = xCoords[numPoints - 1];
        yCoords[i] = yCoords[numPoints - 1];
    }

    center = c;

    CalculateSideNormals();
}

// rotate the FastConvexPoly around the center point by the given number of degrees
void FastConvexPoly::Rotate( float angle ) {

    // Create the rotation matrix
    Transform pointRotation;
    pointRotation.rotate( angle, center );

    Transform normalRotation;
    normalRotation.rotate( angle );

    // Rotate the entire shape relative to its center, and rotate the normals relative to (0, 0)
     unsigned int i;
    for( i = 0; i < numPoints; ++i ) {
        Point2f transformedPoint = pointRotation.transformPoint( xCoords[i], yCoords[i] );

        xCoords[i] = transformedPoint.x;
        yCoords[i] = transformedPoint.y;

        normals[i] = normalRotation.transformPoint( normals[i] );
    }

        // Initialize all the points to (0, 0)
    for( ; i < maxPoints; ++i ) {
        xCoords[i] = xCoords[numPoints - 1];
        yCoords[i] = yCoords[numPoints - 1];
    }

    // If the FastConvexPoly is rotated often (i.e. every frame) then the floating point
    // errors will accumulate and eventually the unit normals will no longer have a
    // magnitude of 1. So when this happens we just re-normalize them
    for( unsigned int i = 0; i < numPoints; ++i ) {
        if( IsAlmostEqual( GetMagnitudeSquared( normals[i] ), 1.0f, MathConstants::EpsilonAroundOne ) == false ) {
            normals[i] = GetUnitVector( normals[i] );
            dbPrintToConsole( "Had to re-normalize a normal on FastConvexPoly stored at: ", this );
        }
    }

    // And make sure the results make sense
    SanityCheck();
}

// Functions to calculate properties of the polygon
// Algorithm borrowed from here -> http://paulbourke.net/geometry/polyarea/
Point2f FastConvexPoly::CalculateCenter() {

    float area = 0.0f;
    sf::Vector2f c;

    for( unsigned int i = 0; i < numPoints; ++i ) {
        int j = (i + 1) % numPoints;

        float temp = CrossProduct2D( Point2f( xCoords[i], xCoords[j] ), Point2f( xCoords[j], yCoords[j] ) );

        area += temp;
        c.x += (xCoords[i] + xCoords[j]) * temp;
        c.y += (yCoords[i] + yCoords[j]) * temp;
    }


    area /= 2.0f;
    c.x /= (6 * area);
    c.y /= (6 * area);

    return c;
}

// Function to calculate the normal to each side of the polygon
void FastConvexPoly::CalculateSideNormals() {
    for( unsigned int i = 0; i < numPoints; ++i ) {
        int j = (i + 1) % numPoints;
        normals[i] = GetUnitClockwiseNormal( GetDirectionVector( Point2f( xCoords[i], yCoords[i] ), Point2f( xCoords[j], yCoords[j] ) ) );
    }
}

// Shift the entire polygon by the displacement vector
void FastConvexPoly::MoveBy( Vector2f displacement ) {
    for( unsigned int i = 0; i < maxPoints; ++i ) {
        xCoords[i] += displacement.x;
        yCoords[i] += displacement.y;
    }

    center += displacement;
}

// Set the location of the point at the specified index in the array
void FastConvexPoly::Set( unsigned int i, Point2f p, bool recalculateNormals, bool recalculateCenter ) {
    dbAssert( i < numPoints, "FastConvexPoly::Set() - array index is too high, out of bounds. Given value was ", i);

    xCoords[i] = p.x;
    yCoords[i] = p.y;

    // If things need to be recalculated
    if( recalculateCenter || recalculateNormals ) {

        // Recalculate them
        if( recalculateCenter ) {
            center = CalculateCenter();
        }

        if( recalculateNormals ) {
            CalculateSideNormals();
        }

        // And make sure the results make sense
        SanityCheck();
    }
}

// Returns the AABB that surrounds the FastConvexPoly
void FastConvexPoly::SetBoundingBox( AABB* boundingBox ) const {
    float minX = xCoords[0];
    float maxX = xCoords[0];

    float minY = yCoords[0];
    float maxY = yCoords[0];

    for( unsigned int i = 1; i < numPoints; ++i ) {
        minX = min( minX, xCoords[i] );
        maxX = max( maxX, xCoords[i] );

        minY = min( minY, yCoords[i] );
        maxY = max( maxY, yCoords[i] );
    }

    boundingBox->SetCorners( Point2f(minX, minY), Point2f(maxX, maxY) );
}

// projects the polygon onto the provided axis, which is a unit vector.
Pair FastConvexPoly::Project( Vector2f axis ) const {

    // The axis must be a unit vector, otherwise the formula is wrong
    dbAssert( IsAlmostEqual( GetMagnitudeSquared(axis), 1.0f, MathConstants::EpsilonAroundOne ), "ConvexPolygon::Project() - the axis was not a unit vector. Given axis was ", axis );

    __m128* pX = (__m128*) xCoords;
    __m128* pY = (__m128*) yCoords;
    __m128 aX  = _mm_set_ps1( axis.x );
    __m128 aY  = _mm_set_ps1( axis.y );

    __m128 dotProds[2];

    dotProds[0] = _mm_add_ps( _mm_mul_ps( *pX      , aX ), _mm_mul_ps( *pY      , aY ) );
    dotProds[1] = _mm_add_ps( _mm_mul_ps( *(pX + 1), aX ), _mm_mul_ps( *(pY + 1), aY ) );

    union u
    {
        __m128 m;
        float f[4];
    } x;

    x.m = _mm_min_ps( dotProds[0], dotProds[1] );
    float m_fMin = min(x.f[0], min(x.f[1], min(x.f[2], x.f[3])));

    x.m = _mm_max_ps( dotProds[0], dotProds[1] );
    float m_fMax = max(x.f[0], max(x.f[1], max(x.f[2], x.f[3])));

    return Pair( m_fMin, m_fMax );
}


// return the point on the polygon that is closest to the point P
Point2f FastConvexPoly::GetClosestPointOnOutline( Point2f p ) const {

   return Point2f(0, 0);
}

// Get the vertex of the polygon that is closest to the point P
Point2f FastConvexPoly::GetClosestVertex( Point2f p ) const {

    // For each vertex get the distance to the point, and choose the minimum of all those
    Point2f closestPoint = Point2f( xCoords[0], yCoords[0] );
    float minDistance = GetMagnitudeSquared( GetDirectionVector(p, closestPoint) );

    for( unsigned int i = 1; i < numPoints; ++i ) {

        Point2f testPoint = Point2f( xCoords[i], yCoords[i] );
        float testDistance = GetMagnitudeSquared( GetDirectionVector(p, testPoint) );

        if( testDistance < minDistance ) {
            minDistance = testDistance;
            closestPoint = testPoint;
        }
    }

    return closestPoint;
}

// returns true if the point is inside the polygon
bool FastConvexPoly::IsPointInside( Point2f p ) const {
    return false;
}

// Draw the polygon onto the target
void FastConvexPoly::Draw( RenderTarget& Target, Color fill, Color outline, float outlineThickness ) const {
    dbAssert( outlineThickness >= 0.0f, "FastConvexPoly::Draw() - outlineThickness is negative" );

    ConvexShape polygon( numPoints );

    // Set the location and size
    for( unsigned int i = 0; i < numPoints; ++i ) {
        polygon.setPoint(i, Point2f( xCoords[i], yCoords[i] ) );
    }

    // Set the colors
    polygon.setFillColor( fill );
    polygon.setOutlineColor( outline );
    polygon.setOutlineThickness( outlineThickness );

    // Draw the FastConvexPoly and center point dot
    Target.draw( polygon );

/*
    // Draw the normals
    for( unsigned int i = 0; i < numPoints; ++i ) {
        unsigned int j = (i + 1) % numPoints;
        DrawVector( Target, (points[i] + points[j]) / 2.0f, normals[i] * 25.0f );
    }
*/
}

Point2f FastConvexPoly::GetPoint( unsigned int i ) const {
    return Point2f( xCoords[i], yCoords[i] );
}

// A sanity check function to confirm that the polygon is set up properly
void FastConvexPoly::SanityCheck() {

}

#endif
