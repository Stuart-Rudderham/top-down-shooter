/*
 * This header defines a 2D line segment
 *
 * Author:      Stuart Rudderham
 * Created:     March 30, 2012
 * Last Edited: January 30, 2013
 */

#ifndef LINESEGMENT_HPP_INCLUDED
#define LINESEGMENT_HPP_INCLUDED

#include "Math/MathFunctions.hpp"
#include "Math/Geometry/BaseShape.hpp"

class LineSegment : public BaseShape {


    public:
        LineSegment( Point2f start, Point2f end );                              // ctor. Created a LineSegment with the given start and end points

        LineSegment( Point2f start, sf::Vector2f direction, float magnitude );  // ctor. Created a LineSegment with the given start point, direction, and magnitude

        USE_WRAPPED_MEM_CALLS(LineSegment);                                     // Wrapper function around "new" and "delete" for easy grepping
        SET_ALLOCATOR_NAME(LineSegment);                                        // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(10000);

        // Getter functions
        inline Point2f GetStartPoint() const    { return startPoint;                               }
        inline Point2f GetEndPoint()   const    { return endPoint;                                 }
        inline Point2f GetDirection()  const    { return GetDirectionVector(startPoint, endPoint); }

        // Setter functions
        inline void SetStartPoint( Point2f newStart ) { startPoint = newStart; }
        inline void SetEndPoint  ( Point2f newEnd   ) { endPoint = newEnd;     }

        void Set( Point2f start, Point2f end );                                 // sets the start and end points of the LineSegment
        void Set( Point2f start, sf::Vector2f direction, float magnitude );     // sets the start point, direction and magnitude


        // Bounding Volumes
        void SetBoundingBox( AABB* boundingBox ) const;                         // Sets the provided AABB to be the bounding box that encloses the shape


        // Movement functions
        inline void MoveBy( sf::Vector2f displacement ) {                       // shifts the LineSegment by the displacement vector.
            startPoint += displacement;
            endPoint += displacement;
        }


        // Drawing function
        void Draw( sf::RenderTarget& Target,                                    // draws the AABB to the target (Window, texture, etc...)
                   sf::Color fill = sf::Color::White,                           // If no arguments are passed a white rectangle with a thin
                   sf::Color outline = sf::Color::Black,                        // black outline is drawn
                   float outlineThickness = 1.0 ) const;


        // Linear algebra
        Pair Project( sf::Vector2f axis ) const;                                // projects the LineSegment onto the provided axis, which is a unit vector
                                                                                // the minimum value of the projection is stored in Pair.x, while the
                                                                                // maximum value of the projection is stored in Pair.y

        inline Point2f GetClosestPointOnOutline( Point2f p ) const {            // return the point on the outline of the LineSegment that is closest to the point P
            return ClosestPointOnLine( p, startPoint, endPoint );
        }

        inline Point2f GetClosestPointOnShape( Point2f p ) const {              // return the point (possibly inside the shape) that is closest to the point P
            return GetClosestPointOnOutline( p );
        }

        inline bool IsPointInside( Point2f p ) const {                          // always return false, since a line has no area so nothing can be "inside" it
            (void)p;
            return false;
        };

    private:
        Point2f startPoint;                                                     // the starting point of the line segment
        Point2f endPoint;                                                       // the ending point of the line segment

        inline Point2f CalculateCenter( Point2f p1, Point2f p2 ) const {        // calculate the location of the center point given the start and end
            return ( p1 + p2 ) / 2.0f;                                          // points on the line
        }

        void SanityCheck();                                                     // runs a bunch of checks to confirm that the LineSegment is formatted properly
                                                                                // If it's not a dbAssert will catch and exit the program

        UNTRACKED_NEW_AND_DELETE(LineSegment);                                  // Overload "new" and "delete" to use ArenaAllocator

        NO_COPY_OR_ASSIGN(LineSegment);                                         // No copy/assign ctor

    // Friend functions for collision detection/resolution
    friend bool LineSegmentLineSegmentIntersection( LineSegment* line, Point2f p1, Point2f p2, Point2f& intersectionPoint );
    friend bool CastRayAtAABB          ( BaseShape* lineSegment, BaseShape* shape, Point2f& intersectionPoint, sf::Vector2f& normal );
    friend bool CastRayAtCircle        ( BaseShape* lineSegment, BaseShape* shape, Point2f& intersectionPoint, sf::Vector2f& normal );
    friend bool CastRayAtConvexPolygon ( BaseShape* lineSegment, BaseShape* shape, Point2f& intersectionPoint, sf::Vector2f& normal );
};

#endif // AABB_HPP_INCLUDED
