/*
 * This header defines a 2D Convex Polygon. The points
 * are defined in a clockwise ordering
 *
 * Author:      Stuart Rudderham
 * Created:     January 31, 2012
 * Last Edited: January 30, 2013
 */

#ifndef CONVEXPOLYGON_HPP_INCLUDED
#define CONVEXPOLYGON_HPP_INCLUDED

#include "Math/Geometry/BaseShape.hpp"

enum class CollisionResponse : unsigned char;

class ConvexPolygon : public BaseShape {
    public:
        ConvexPolygon( unsigned int n );                                        // ctor. Can only pass the number of sides, need to initialize the points separately

        ConvexPolygon( const std::vector<Point2f>& polygonPoints );             // ctor that initializes all the points at once

        ConvexPolygon( unsigned int n, Point2f c, float radius );               // ctor that created a regular n-gon ConvexPolygon
                                                                                // has lots of sin() and cos() calls

        ConvexPolygon( const ConvexPolygon& original );                         // copy ctor

        ConvexPolygon& operator=( const ConvexPolygon& other );                 // assignment operator

        USE_WRAPPED_MEM_CALLS(ConvexPolygon);                                   // Wrapper function around "new" and "delete" for easy grepping
        SET_ALLOCATOR_NAME(ConvexPolygon);                                      // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(2500);

        // Getter functions
        Point2f GetPoint( unsigned int i ) const;                               // get the point at index i in the array
        inline unsigned int GetNumPoints() const { return numPoints; }          // return the number of points in the polygon


        // Setter functions
        inline void SetCenter( Point2f newCenter ) { center = newCenter; }      // lets you set the center of the ConvexPolygon.
                                                                                // Useful for rotating around an arbitrary point

        void Set( unsigned int i, Point2f p,                                    // changes the location of the point at the specified index in the array. The
                  bool recalculateNormals = true,                               // centroid and normals are recalculated (which is expensive) if the booleans
                  bool recalculateCenter = true );                              // are set to true


        // Bounding Volumes
        void SetBoundingBox( AABB* boundingBox ) const;                         // Sets the provided AABB to be the bounding box that encloses the shape


        // Movement functions
        void MoveBy( sf::Vector2f displacement );                               // shifts the polygon by the displacement vector. Changes the location of
                                                                                // all the points and the centroid

        void Rotate( float angle );                                             // set the rotation angle of ConvexPolygon around the center point to the given number of degrees



        // Drawing function
        void Draw( sf::RenderTarget& Target,                                    // draws the polygon to the target (Window, texture, etc...)
                   sf::Color fill = sf::Color::White,                           // If no arguments are passed a white shape with a thin
                   sf::Color outline = sf::Color::Black,                        // black outline is drawn
                   float outlineThickness = 1.0 ) const;


        // Linear algebra
        Pair Project( sf::Vector2f axis ) const;                                // projects the polygon onto the provided axis, which is a unit vector.
                                                                                // the minimum value of the projection is stored in Pair.x, while the
                                                                                // maximum value of the projection is stored in Pair.y

        Point2f GetClosestPointOnOutline( Point2f p ) const;                    // return the point on the outline of the polygon that is closest to the point P
        Point2f GetClosestVertex( Point2f p ) const;                            // return the vertex of the polygon that is closest to the point P
        bool IsPointInside( Point2f p ) const;                                  // returns true if the point is inside the polygon

    private:
        const static unsigned int maxPoints = 5;

        Point2f points[maxPoints];                                              // an array of the points of the polygon, defined in a clockwise ordering
        sf::Vector2f normals[maxPoints];                                        // an array that holds the clockwise unit normals to each side

        const unsigned int numPoints;                                           // the number of points the polygon has

        Point2f CalculateCenter();                                              // calculate the center of the polygon
        void CalculateSideNormals();                                            // calculate and populate the array of normals

        bool IsClockwiseOrdering();                                             // returns true of the polygon points have a clockwise ordering
        bool IsConvex();                                                        // returns true if the polygon is convex

        void SanityCheck();                                                     // runs a bunch of checks to confirm that the polygon is formatted properly
                                                                                // If it's not a dbAssert will catch and exit the program

        void MakeRegularNGon( Point2f c, float radius );                        // sets all the points so that the polygon is a regular n-gon that
                                                                                // is circumscribed by a circle centered at the given point with the given radius

        UNTRACKED_NEW_AND_DELETE(ConvexPolygon);                                // Overload "new" and "delete" to use ArenaAllocator

    // Friend functions for collision detection/resolution
    friend bool PolygonPolygonCollision ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
    friend bool BoxPolygonCollision     ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
    friend bool PolygonBoxCollision     ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
    friend bool CirclePolygonCollision  ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
    friend bool PolygonCircleCollision  ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );

    friend bool CastRayAtConvexPolygon  ( BaseShape* line, BaseShape* shape, Point2f& collisionPoint, sf::Vector2f& normal );
};

#endif // CONVEXPOLYGON_HPP_INCLUDED
