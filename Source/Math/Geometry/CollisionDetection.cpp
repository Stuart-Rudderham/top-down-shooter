/*
 * This header implements functions to deal
 * with collision dection and response between
 * two 2D shapes
 *
 * Author:      Stuart Rudderham
 * Created:     February 12, 2012
 * Last Edited: July 11, 2012
 */

#include "Math/Geometry/CollisionDetection.hpp"

#include "Math/Geometry/LineSegment.hpp"
#include "Math/Geometry/ConvexPolygon.hpp"

#include <typeinfo>

using namespace std;
using namespace sf;

// Used to set the MTVs for two shapes colliding
inline void SetMTV( Vector2f& mtvA, Vector2f& mtvB, float mtvMagnitude, Vector2f normal, CollisionResponse response ) {
    switch( response ) {
        case CollisionResponse::MoveFirst:
            mtvA = normal * -mtvMagnitude;
            mtvB = MathConstants::ZeroVector;
            break;

        case CollisionResponse::MoveSecond:
            mtvA = MathConstants::ZeroVector;
            mtvB = normal * mtvMagnitude;
            break;

        case CollisionResponse::MoveBoth:
            mtvMagnitude /= 2.0f;

            mtvA = normal * -mtvMagnitude;
            mtvB = normal * mtvMagnitude;
            break;

        case CollisionResponse::MoveNeither:
            mtvA = MathConstants::ZeroVector;
            mtvB = MathConstants::ZeroVector;
            break;
    }
}


// Collision detection and reaction between two AABBs
bool BoxBoxCollision( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, Vector2f& mtvA, Vector2f& mtvB, Vector2f& normal ) {

    // Convert the BaseShape pointers to AABB pointers
    AABB* aabb1 = shape1->GetAs<AABB>();
    AABB* aabb2 = shape2->GetAs<AABB>();

    // If the AABBs are still colliding
    if( BoundingBoxesColliding(aabb1, aabb2) == true ) {

        // calculate how much they are intersecting over each axis, relative to A
        float xIntersection = (aabb1->center.x - aabb1->halfWidth) - (aabb2->center.x + aabb2->halfWidth);
        float otherPossibleXIntersection = (aabb1->center.x + aabb1->halfWidth) - (aabb2->center.x - aabb2->halfWidth);

        if( abs(xIntersection) > abs(otherPossibleXIntersection) ) {
            xIntersection = otherPossibleXIntersection;
        }

        float yIntersection = (aabb1->center.y - aabb1->halfHeight) - (aabb2->center.y + aabb2->halfHeight);
        float otherPossibleYIntersection = (aabb1->center.y + aabb1->halfHeight) - (aabb2->center.y - aabb2->halfHeight);

        if( abs(yIntersection) > abs(otherPossibleYIntersection) ) {
            yIntersection = otherPossibleYIntersection;
        }

        float mtvMagnitude;

        // Take the minimum intersection distance, this is what we return as the MTV
        if( abs(xIntersection) < abs(yIntersection) ) {
            mtvMagnitude = xIntersection;
            normal = MathConstants::xAxis;
        } else {
            mtvMagnitude = yIntersection;
            normal = MathConstants::yAxis;
        }

        SetMTV( mtvA, mtvB, mtvMagnitude, normal, response );

        return true;
    } else {
        return false;
    }
}

// Collision detection and reaction between two Circles
bool CircleCircleCollision( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, Vector2f& mtvA, Vector2f& mtvB, Vector2f& normal ) {

    // Convert the BaseShape pointers to Circle pointers
    Circle* circle1 = shape1->GetAs<Circle>();
    Circle* circle2 = shape2->GetAs<Circle>();

    Vector2f displacement = GetDirectionVector( circle1->center, circle2->center );

    // Check to see if the circles are actually colliding
    if( GetMagnitudeSquared( displacement ) <= (circle1->radius + circle2->radius) * (circle1->radius + circle2->radius) ) {

        // If they are then get the magnitude of the MTV
        float displacementMagnitude = GetMagnitude( displacement );

        float mtvMagnitude = circle1->radius + circle2->radius - displacementMagnitude;

        // And get the direction of the MTV (which also happens to be the surface normal)
        // Calculate the unit normal vector ourself rather than use the GetUnitVector()
        // function so we can reuse the square root we already calculated above
        normal = displacement / displacementMagnitude;

        SetMTV( mtvA, mtvB, mtvMagnitude, normal, response );

        return true;
    } else {
        return false;
    }
}

// Collision detection and reaction between two ConvexPolygons
// Help from here -> http://www.gamedev.net/topic/545071-yet-another-separating-axis-theorem-question/
bool PolygonPolygonCollision( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, Vector2f& mtvA, Vector2f& mtvB, Vector2f& normal ) {

    // Convert the BaseShape pointers to ConvexPolygon pointers
    ConvexPolygon* polygon1 = shape1->GetAs<ConvexPolygon>();
    ConvexPolygon* polygon2 = shape2->GetAs<ConvexPolygon>();


    float minOverlap = FLT_MAX;
    float currentOverlap;

    Vector2f overlapAxis;

    Pair projectA;
    Pair projectB;

    // For each normal for shape A
    for( unsigned int i = 0; i < polygon1->numPoints; ++i ) {

        // Project both shapes onto it
        projectA = polygon1->Project( polygon1->normals[i] );
        projectB = polygon2->Project( polygon1->normals[i] );

        // if they don't overlap then they aren't colliding
        if( (projectA.x > projectB.y) || (projectA.y < projectB.x) ) {
            return false;
        }

        // If they do overlap then get the amount they overlap by
        currentOverlap = min( projectA.y - projectB.x, projectB.y - projectA.x );

        // Remember the minimum we've seen so far
        if( currentOverlap < minOverlap ) {
            minOverlap = currentOverlap;
            overlapAxis = polygon1->normals[i];
        }
    }

    // For each normal for shape B
    for( unsigned int i = 0; i < polygon2->numPoints; ++i ) {

        // Project both shapes onto it
        projectA = polygon1->Project( polygon2->normals[i] );
        projectB = polygon2->Project( polygon2->normals[i] );

        // if they don't overlap then they aren't colliding
        if( (projectA.x > projectB.y) || (projectA.y < projectB.x) ) {
            return false;
        }

        // If they do overlap then get the amount they overlap by
        currentOverlap = min( projectB.y - projectA.x, projectA.y - projectB.x );

        // Remember the minimum we've seen so far
        if( currentOverlap < minOverlap ) {
            minOverlap = currentOverlap;
            overlapAxis = polygon2->normals[i];
        }
    }

    // This is necessary because SAT only tells you the normal on which the shapes are overlapping
    // Shapes with parallel sides will have two normals that face in opposite directions and SAT is not
    // guaranteed to return the normal that corresponds to the side that the collision is happening on
    // So we check and make sure that we're actually moving the shape in the correct direction
    if( DotProduct( GetDirectionVector(polygon1->center, polygon2->center), overlapAxis ) < 0.0f ) {
        minOverlap *= -1.0f;
    }

    // If we get here then the polygons are colliding, so calculate the MTVs
    normal = overlapAxis;
    SetMTV( mtvA, mtvB, minOverlap, normal, response );

    return true;
}



// Collision detection and reaction between an AABB and a Circle
bool BoxCircleCollision( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal ) {

    // Convert the BaseShape pointers to the appropriate derived pointers
    AABB*   aabb   = shape1->GetAs<AABB>();
    Circle* circle = shape2->GetAs<Circle>();

    // The closest point on the AABB to the Circle
    Point2f closestPointToCircle = aabb->GetClosestPointOnShape( circle->center );

    // Once we have to closest point we can calculate the distance from the AABB to the Circle
    Vector2f displacement = GetDirectionVector( closestPointToCircle, circle->center );

    // If they're actually colliding
    if( GetMagnitudeSquared(displacement) <= circle->radius * circle->radius ) {

        float mtvMagnitude;

        // If the center of the Circle is inside the AABB
        if( closestPointToCircle == circle->center ) {

            // get the side of the AABB that is closest to the center of the Circle
            float xIntersection = (aabb->center.x - aabb->halfWidth) - (circle->center.x + circle->radius);
            float otherPossibleXIntersection = (aabb->center.x + aabb->halfWidth) - (circle->center.x - circle->radius);

            if( abs(xIntersection) > abs(otherPossibleXIntersection) ) {
                xIntersection = otherPossibleXIntersection;
            }

            float yIntersection = (aabb->center.y - aabb->halfHeight) - (circle->center.y + circle->radius);
            float otherPossibleYIntersection = (aabb->center.y + aabb->halfHeight) - (circle->center.y - circle->radius);

            if( abs(yIntersection) > abs(otherPossibleYIntersection) ) {
                yIntersection = otherPossibleYIntersection;
            }

            // Take the minimum intersection distance, this is what we return as the MTV
            if( abs(xIntersection) < abs(yIntersection) ) {
                mtvMagnitude = xIntersection;
                normal = MathConstants::xAxis;
            } else {
                mtvMagnitude = yIntersection;
                normal = MathConstants::yAxis;
            }

            displacement = GetDirectionVector( aabb->center, circle->center );
        } else {
            // Get the length of the displacement
            float displacementMagnitude = GetMagnitude( displacement );

            // and from that we calculate the MTV and normal
            mtvMagnitude = circle->radius - displacementMagnitude;
            normal = displacement / displacementMagnitude;
        }

        SetMTV( mtvA, mtvB, mtvMagnitude, normal, response );

        return true;
    } else {
        return false;
    }
}

// Collision detection and reaction between an AABB and a Circle
// Reverse of the above function, but with the shapes and resulting MTVs flipped
bool CircleBoxCollision( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, Vector2f& mtvA, Vector2f& mtvB, Vector2f& normal ) {
    bool result = BoxCircleCollision( shape2, shape1, response, mtvA, mtvB, normal );

    // Have to flip the direction of the vectors so that collisions are resolved properly
    mtvA *= -1.0f;
    mtvB *= -1.0f;

    return result;
}



// Collision detection and reaction between an AABB and a ConvexPolygon
bool BoxPolygonCollision( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, Vector2f& mtvA, Vector2f& mtvB, Vector2f& normal ) {

    // Convert the BaseShape pointers to the appropriate derived pointers
    AABB*          aabb    = shape1->GetAs<AABB>();
    ConvexPolygon* polygon = shape2->GetAs<ConvexPolygon>();

    float minOverlap = MathConstants::BigFloat;
    float currentOverlap;

    Vector2f overlapAxis;

    Pair projectA;
    Pair projectB;

    // For each normal for shape B
    for( unsigned int i = 0; i < polygon->numPoints; ++i ) {

        // Project both shapes onto it
        projectA = aabb->Project( polygon->normals[i] );
        projectB = polygon->Project( polygon->normals[i] );

        // if they don't overlap then they aren't colliding
        if( (projectA.x > projectB.y) || (projectA.y < projectB.x) ) {
            return false;
        }

        // If they do overlap then get the amount they overlap by
        currentOverlap = min( projectA.y - projectB.x, projectB.y - projectA.x );

        // Remember the minimum we've seen so far
        if( currentOverlap < minOverlap ) {
            minOverlap = currentOverlap;
            overlapAxis = polygon->normals[i];
        }
    }


    // Then we have to also test the x-axis

    // Project both shapes onto it
    projectA = aabb->Project( MathConstants::xAxis );
    projectB = polygon->Project( MathConstants::xAxis );

    // if they don't overlap then they aren't colliding
    if( (projectA.x > projectB.y) || (projectA.y < projectB.x) ) {
        return false;
    }

    // If they do overlap then get the amount they overlap by
    currentOverlap = min( projectB.y - projectA.x, projectA.y - projectB.x );

    // Remember the minimum we've seen so far
    if( currentOverlap < minOverlap ) {
        minOverlap = currentOverlap;
        overlapAxis = MathConstants::xAxis;
    }

    // and the y-axis
    // Project both shapes onto it
    projectA = aabb->Project( MathConstants::yAxis );
    projectB = polygon->Project( MathConstants::yAxis );

    // if they don't overlap then they aren't colliding
    if( (projectA.x > projectB.y) || (projectA.y < projectB.x) ) {
        return false;
    }

    // If they do overlap then get the amount they overlap by
    currentOverlap = min( projectB.y - projectA.x, projectA.y - projectB.x );

    // Remember the minimum we've seen so far
    if( currentOverlap < minOverlap ) {
        minOverlap = currentOverlap;
        overlapAxis = MathConstants::yAxis;
    }

    // This is necessary because SAT only tells you the normal on which the shapes are overlapping
    // Shapes with parallel sides will have two normals that face in opposite directions and SAT is not
    // guaranteed to return the normal that corresponds to the side that the collision is happening on
    // So we check and make sure that we're actually moving the shape in the correct direction
    if( DotProduct( GetDirectionVector(aabb->center, polygon->center), overlapAxis ) < 0.0f ) {
        minOverlap *= -1.0f;
    }

    // If we get here then the polygons are colliding, so calculate the MTVs
    normal = overlapAxis;
    SetMTV( mtvA, mtvB, minOverlap, normal, response );

    return true;
}

// Collision detection and reaction between a ConvexPolygon and an AABB
// Reverse of the above function, but with the shapes and resulting MTVs flipped
bool PolygonBoxCollision( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, Vector2f& mtvA, Vector2f& mtvB, Vector2f& normal ) {
    bool result = BoxPolygonCollision( shape2, shape1, response, mtvA, mtvB , normal );

    // Have to flip the direction of the vectors so that collisions are resolved properly
    mtvA *= -1.0f;
    mtvB *= -1.0f;

    return result;
}



// Collision detection and reaction between an AABB and a ConvexPolygon
bool CirclePolygonCollision( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, Vector2f& mtvA, Vector2f& mtvB, Vector2f& normal ) {

    // Convert the BaseShape pointers to the appropriate derived pointers
    Circle*        circle  = shape1->GetAs<Circle>();
    ConvexPolygon* polygon = shape2->GetAs<ConvexPolygon>();

    float minOverlap = MathConstants::BigFloat;
    float currentOverlap;

    Vector2f overlapAxis;

    Pair projectA;
    Pair projectB;

    // For each normal for shape B
    for( unsigned int i = 0; i < polygon->numPoints; ++i ) {

        // Project both shapes onto it
        projectA = circle->Project( polygon->normals[i] );
        projectB = polygon->Project( polygon->normals[i] );

        // if they don't overlap then they aren't colliding
        if( (projectA.x > projectB.y) || (projectA.y < projectB.x) ) {
            return false;
        }

        // If they do overlap then get the amount they overlap by
        currentOverlap = min( projectA.y - projectB.x, projectB.y - projectA.x );

        // Remember the minimum we've seen so far
        if( currentOverlap < minOverlap ) {
            minOverlap = currentOverlap;
            overlapAxis = polygon->normals[i];
        }
    }

    // Then we have to also test one other axis: the axis between the center of the circle
    // and the point on the polygon that is closest to the circle
    Point2f closestPointToCircle = polygon->GetClosestVertex(circle->center);

    Vector2f lastAxis = GetUnitVector( GetDirectionVector( circle->center, closestPointToCircle ) );

    // Project both shapes onto it
    projectA = circle->Project( lastAxis );
    projectB = polygon->Project( lastAxis );

    // if they don't overlap then they aren't colliding
    if( (projectA.x > projectB.y) || (projectA.y < projectB.x) ) {
        return false;
    }

    // If they do overlap then get the amount they overlap by
    currentOverlap = min( projectB.y - projectA.x, projectA.y - projectB.x );

    // Remember the minimum we've seen so far
    if( currentOverlap < minOverlap ) {
        minOverlap = currentOverlap;
        overlapAxis = lastAxis;
    }

    // This is necessary because SAT only tells you the normal on which the shapes are overlapping
    // Shapes with parallel sides will have two normals that face in opposite directions and SAT is not
    // guaranteed to return the normal that corresponds to the side that the collision is happening on
    // So we check and make sure that we're actually moving the shape in the correct direction
    if( DotProduct( GetDirectionVector(circle->center, polygon->center), overlapAxis ) < 0.0f ) {
        minOverlap *= -1.0f;
    }

    // If we get here then the polygons are colliding, so calculate the MTVs
    normal = overlapAxis;
    SetMTV( mtvA, mtvB, minOverlap, normal, response );

    return true;
}

// Collision detection and reaction between a ConvexPolygon and an AABB
// Reverse of the above function, but with the shapes and resulting MTVs flipped
bool PolygonCircleCollision( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, Vector2f& mtvA, Vector2f& mtvB, Vector2f& normal ) {
    bool result = CirclePolygonCollision( shape2, shape1, response, mtvA, mtvB, normal );

    // Have to flip the direction of the vectors so that collisions are resolved properly
    mtvA *= -1.0f;
    mtvB *= -1.0f;

    return result;
}


// This function takes in a LineSegment and two points that describe another line segment. If the two segments intersect
// the function returns true and the intersection point in put into the collisionPoint parameter that is passed by reference
// Idea from here -> http://paulbourke.net/geometry/lineline2d/
// Also good explanation here -> http://thirdpartyninjas.com/blog/2008/10/07/line-segment-intersection/
bool LineSegmentLineSegmentIntersection( LineSegment* line, Point2f p1, Point2f p2, Point2f& intersectionPoint ) {

    // Calculate the denominator for the fraction
    float denom = (p2.y - p1.y) * (line->endPoint.x - line->startPoint.x) - (p2.x - p1.x) * (line->endPoint.y - line->startPoint.y);

    // If it's 0 then the lines are parallel so no intersection (we ignore the case where the lines are coincident)
    if( IsAlmostEqual( denom, 0.0f, MathConstants::EpsilonAroundZero ) ) {
        return false;

    // Otherwise calculate the intersection point between the two lines
    } else {

        // Calculate the values that gives the intersection point when the line is of the form P = p1 + (u * p2)
        float u = ( (p2.x - p1.x) * (line->startPoint.y - p1.y) - (p2.y - p1.y) * (line->startPoint.x - p1.x) ) / denom;

        // If not in the range [0, 1] then the intersection point is not
        // on the first line segment, so we can quit early
        if( (u < 0) || (u > 1) ) {
            return false;
        }

        float t = ( (line->endPoint.x - line->startPoint.x) * (line->startPoint.y - p1.y) - (line->endPoint.y - line->startPoint.y) * (line->startPoint.x - p1.x) ) / denom;

        // If not in the range [0, 1] then the intersection point is not
        // on the second line segment, so we can quit
        if( (t < 0) || (t > 1) ) {
            return false;
        }

        // Get the intersection point. We know it's on both line segments since we checked
        // to make sure that t and u are in the range [0, 1]
        intersectionPoint = line->startPoint + (line->endPoint - line->startPoint) * u;
        return true;
    }
}


// Cast a ray against an axis-aligned bounding aabb
bool CastRayAtAABB ( BaseShape* lineSegment, BaseShape* shape, Point2f& intersectionPoint, Vector2f& normal ) {

    // Convert the BaseShape pointers to the appropriate derived pointers
    LineSegment* line = lineSegment->GetAs<LineSegment>();
    AABB*        aabb = shape->GetAs<AABB>();

    // Get the 4 corners of the AABB
    Point2f corners[4] = { aabb->GetTopLeftCorner(),
                           aabb->GetTopRightCorner(),
                           aabb->GetBottomRightCorner(),
                           aabb->GetBottomLeftCorner() };

    // Check for collision between the LineSegment and the 4 sides of the AABB
    float minDistance = MathConstants::BigFloat;
    bool hitAABB = false;

    for( unsigned int i = 0u; i < 4u; i++ ) {
        unsigned int j = (i + 1) % 4u;

        Point2f currIntersectionPoint;

        // Check to see if the line segment intersects with the side
        if( LineSegmentLineSegmentIntersection( line, corners[i], corners[j], currIntersectionPoint ) ) {
            hitAABB = true;

            // If so get the distance from the start of the LineSegment to the intersection point
            float currDistance = GetMagnitudeSquared( GetDirectionVector( line->GetStartPoint(), currIntersectionPoint ) );

            // If this is the closest intersection point so far
            if( currDistance < minDistance ) {

                // Store the intersection point and the distance
                minDistance = currDistance;
                intersectionPoint = currIntersectionPoint;

                // Calculate the normal of the side. Since we know the sides are axis-aligned we can figure out the normal
                // just based on the index of the points that make up the side
                normal = Vector2f( (i % 2 == 0) ? 0.0f : ( (i == 1) ? 1.0f : -1.0f ),
                                   (i % 2 == 1) ? 0.0f : ( (i == 0) ? -1.0f : 1.0f ) );
            }
        }
    }

    return hitAABB;
}

// Case a ray against a circle
// Idea from here -> http://paulbourke.net/geometry/sphereline/
bool CastRayAtCircle( BaseShape* lineSegment, BaseShape* shape,  Point2f& intersectionPoint, Vector2f& normal ) {

    // Convert the BaseShape pointers to the appropriate derived pointers
    LineSegment* line = lineSegment->GetAs<LineSegment>();
    Circle*      circle = shape->GetAs<Circle>();

    // Get the closest point on the line segment to the circle
    Point2f closestPoint = ClosestPointOnLine( circle->center, line->startPoint, line->endPoint );

    // If the closest point is farther away than the radius of the circle then we know that they aren't colliding
    if( GetMagnitudeSquared( GetDirectionVector( closestPoint, circle->center) ) > (circle->radius * circle->radius) ) {
        return false;
    }

    // Otherwise the intersection points can be found by solving a quadratic equation
    float a = GetMagnitudeSquared( GetDirectionVector( line->startPoint, line->endPoint ) );
    float b = 2 * ( (line->endPoint.x - line->startPoint.x) * (line->startPoint.x - circle->center.x) + (line->endPoint.y - line->startPoint.y) * (line->startPoint.y - circle->center.y) );
    float c = GetMagnitudeSquared( circle->center ) + GetMagnitudeSquared( line->startPoint ) - 2 * ( DotProduct( circle->center, line->startPoint ) ) - (circle->radius * circle->radius);

    float discriminant = (b * b) - (4 * a * c);

    // The line intersects the circle at two points (entering and leaving)
    if( discriminant > 0 ) {

        float sqrtOfDiscriminant = sqrt(discriminant);

        float u1 = ( -b + sqrtOfDiscriminant ) / (2 * a);
        float u2 = ( -b - sqrtOfDiscriminant ) / (2 * a);

        Point2f possibleIntersectionPoint1 = MathConstants::BigVector;
        Point2f possibleIntersectionPoint2 = MathConstants::BigVector;

        // Get the first intersection point, assuming it's on the line segment
        // If it's not then the intersection point will be (+Infinity, +Infinity)
        if( (u1 >= 0) && (u1 <= 1) ) {
            possibleIntersectionPoint1 = line->startPoint + (line->endPoint - line->startPoint) * u1;
        }

        // Get the second intersection point, assuming it's on the line segment
        // If it's not then the intersection point will be (+Infinity, +Infinity)
        if( (u2 >= 0) && (u2 <= 1) ) {
            possibleIntersectionPoint2 = line->startPoint + (line->endPoint - line->startPoint) * u2;
        }

        // Get the closer of the two possible intersection points
        if( GetMagnitudeSquared( GetDirectionVector( line->startPoint, possibleIntersectionPoint1) ) < GetMagnitudeSquared( GetDirectionVector( line->startPoint, possibleIntersectionPoint2) ) ) {
            intersectionPoint = possibleIntersectionPoint1;
        } else {
            intersectionPoint = possibleIntersectionPoint2;
        }

        // If there is no intersection
        if( intersectionPoint == MathConstants::BigVector ) {
            return false;

        // Otherwise get the normal and return true
        } else {
            normal = GetUnitClockwiseNormal( GetDirectionVector(circle->center, intersectionPoint) );
            return true;
        }

    // The line intersects the circle at only one point (i.e. the line is tangent to the circle)
    } else if( IsAlmostEqual( discriminant, 0.0f, MathConstants::EpsilonAroundZero ) ) {

        float u = -b / (2 * a);

        dbAssert( (u >= 0) && (u <= 1), "u was out of range. Value was ", u );

        intersectionPoint = line->startPoint + (line->endPoint - line->startPoint) * u;
        normal = GetUnitClockwiseNormal( circle->center - intersectionPoint );
        return true;

    // The line doesn't intersect the circle. We shouldn't get here
    } else {
        dbPrintToConsole( "lineSegment doesn't intersect circle and it should" );
        return false;
    }
    /*
    Vector2f collidePoint = closestPointOnShape( C.center, A );
    Vector2f displacement = collidePoint - C.center;

    if( getMagnitudeSquared( collidePoint - C.center ) < C.radius * C.radius ) {
        float a = getMagnitudeSquared( A.endPoint - A.startPoint );
        float b = 2 * ( (A.endPoint.x - A.startPoint.x) * (A.startPoint.x - C.center.x) + (A.endPoint.y - A.startPoint.y) * (A.startPoint.y - C.center.y) );
        float c = getMagnitudeSquared( C.center ) + getMagnitudeSquared( A.startPoint ) - 2 * (C.center.x * A.startPoint.x + C.center.y * A.startPoint.y) - C.radius * C.radius;

        float discriminant = b * b - 4 * a * c;
        float u;

        if( discriminant > 0 ) {
            u = ( b + sqrt(discriminant) ) / (-2 * a);
        } else if( discriminant == 0 ) {
            u = -b / (2 * a);
        } else {
            exit(20);
        }

        collisionPoint = A.startPoint + (A.endPoint - A.startPoint) * u;
        normal = getUnitNormal( C.center - collisionPoint );
        return true;
    } else {
        return false;
    }
    */
}


bool CastRayAtConvexPolygon ( BaseShape* lineSegment, BaseShape* shape, Point2f& intersectionPoint, Vector2f& normal ) {

    // Convert the BaseShape pointers to the appropriate derived pointers
    LineSegment*   line = lineSegment->GetAs<LineSegment>();
    ConvexPolygon* polygon = shape->GetAs<ConvexPolygon>();

    // Check for collision between the LineSegment and the sides of the ConvexPolygon
    float minDistance = MathConstants::BigFloat;
    bool hitPolygon = false;

    for( unsigned int i = 0; i < polygon->numPoints; i++ ) {
        unsigned int j = (i + 1) % polygon->numPoints;

        Point2f currIntersectionPoint;

        // Check to see if the line segment intersects with the side
        if( LineSegmentLineSegmentIntersection( line, polygon->points[i], polygon->points[j], currIntersectionPoint ) ) {
            hitPolygon = true;

            // If so get the distance from the start of the LineSegment to the intersection point
            float currDistance = GetMagnitudeSquared( GetDirectionVector( line->GetStartPoint(), currIntersectionPoint ) );

            // If this is the closest intersection point so far
            if( currDistance < minDistance ) {

                // Store the intersection point and the distance
                minDistance = currDistance;
                intersectionPoint = currIntersectionPoint;

                // Calculate the normal of the side. Since we know the sides are axis-aligned we can figure out the normal
                // just based on the index of the points that make up the side
                normal = polygon->normals[i];
            }
        }
    }

    return hitPolygon;
}
