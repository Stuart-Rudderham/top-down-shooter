/*
 * This file implements the non-virtual
 * functionsof the base shape class
 *
 * Author:      Stuart Rudderham
 * Created:     February 3, 2012
 * Last Edited: January 10, 2013
 */

#include "Math/Geometry/BaseShape.hpp"
#include "Math/Geometry/AABB.hpp"

#include "UpgradesToSFML/Drawing.hpp"

using namespace sf;
using namespace std;

// Constructor
BaseShape::BaseShape( Point2f c, GeometricShape::Enum t ) : center( c ),
                                                            geometricShape( t )
{

}


// Return the point on the shape (possibly inside the shape) that is closest to the point P
Point2f BaseShape::GetClosestPointOnShape( Point2f p ) const {

    // If the point is inside the shape then just return the point
    if( IsPointInside( p ) ) {
        return p;
    }

    // Otherwise return the point on the shape's outline that is closest to P
    return GetClosestPointOnOutline( p );
}



// draws the axis-aligned bounding box that encloses the shape
void BaseShape::DrawBoundingBox( RenderTarget& Target, Color fill, Color outline, float outlineThickness ) const {
    AABB boundingBox;
    SetBoundingBox( &boundingBox );

    boundingBox.Draw( Target, fill, outline, outlineThickness );
}

// draws a dot located at the center point of the shape
void BaseShape::DrawCenterDot( RenderTarget& Target, Color color, float dotRadius ) const {
    DrawDot( Target, center, dotRadius, color );
}
