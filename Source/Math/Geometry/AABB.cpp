/*
 * This file implements a 2D Axis-Aligned Bounding Box
 *
 * Author:      Stuart Rudderham
 * Created:     January 29, 2012
 * Last Edited: November 4, 2012
 */

#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

#include <limits>
#include <cmath>

using namespace std;
using namespace sf;

// default constructor. Creates an AABB located at (0, 0) with a width and height of 0
AABB::AABB() : BaseShape( Point2f(0, 0), GeometricShape::AABB ),
               halfWidth( 0 ),
               halfHeight( 0 )
{
    SanityCheck();
}


// Constructor for two points
AABB::AABB( Point2f p1, Point2f p2 ) : BaseShape( CalculateCenter(p1, p2), GeometricShape::AABB ),
                                       halfWidth( abs(p1.x - p2.x) / 2.0f ),
                                       halfHeight( abs(p1.y - p2.y) / 2.0f )
{
    SanityCheck();
}

// Constructor for a point, width, and height
AABB::AABB( Point2f topLeft, float w, float h ) : BaseShape( topLeft + (Vector2f(w, h) / 2.0f), GeometricShape::AABB ),
                                                  halfWidth( w / 2.0f ),
                                                  halfHeight( h / 2.0f )
{
    SanityCheck();
}



// Reposition the corners of the AABB, then recalculate the properties
void AABB::SetCorners( Point2f p1, Point2f p2 ) {

    center = CalculateCenter( p1, p2 );                                 // calculate the center point

    sf::Vector2f distanceBetweenPoints = p1 - p2;                       // Subtract the two points to get the distance between them

    halfWidth = abs( distanceBetweenPoints.x / 2.0f );                  // divide by 2 to get the half-width
    halfHeight = abs( distanceBetweenPoints.y / 2.0f );                 // divide by 2 to get the half-height

    SanityCheck();

}

/*
// Reposition the top-left corner as well as the width and height, then recalculate the properties
void AABB::Set( Point2f topLeft, float w, float h ) {

    halfWidth = w / 2.0f;                                               // set the width
    halfHeight = h / 2.0f;                                              // set the height

    center = Point2f( topLeft.x + halfWidth, topLeft.y + halfHeight );  // set the center point

    SanityCheck();
}
*/


// Sets the provided AABB to be the bounding box that encloses the shape
void AABB::SetBoundingBox( AABB* boundingBox ) const {
    *boundingBox = *this;
}


// projects the AABB onto the provided axis
// concept from here -> http://en.wikipedia.org/wiki/Scalar_projection
Pair AABB::Project( Vector2f axis ) const {

    // The axis must be a unit vector, otherwise the formula is wrong
    dbAssert( IsAlmostEqual( GetMagnitudeSquared(axis), 1.0f, MathConstants::EpsilonAroundOne ), "The axis was not a unit vector. Given axis was ", axis );

    float dotProd;
    float maxValue;
    float minValue;

    // Get the 4 corners of the AABB
    Point2f corners[4] = { GetTopLeftCorner(),
                           GetTopRightCorner(),
                           GetBottomRightCorner(),
                           GetBottomLeftCorner() };


    // Project the top-left corner
    dotProd = DotProduct( corners[0], axis );
    maxValue = dotProd;
    minValue = dotProd;

    // Project the rest of the corners
    for( unsigned int i = 1; i < 4; ++i ) {

        dotProd = DotProduct( corners[i], axis );

        if( dotProd < minValue ) {
            minValue = dotProd;
        } else if( dotProd > maxValue ) {
            maxValue = dotProd;
        }
    }

    return Pair( minValue, maxValue );                              // Return the min/max value
}

// return the point (possibly inside the AABB) that is closest to the point P
// Taken from the book: 3D math primer for graphics and game development, Section 13.4
// http://books.google.ca/books?id=wCfWkc_E3GkC&pg=PA280&lpg=PA280&dq=closest+point+on+a+circle+to+a+point&source=bl#v=onepage&q=closest%20point%20on%20a%20circle%20to%20a%20point&f=false
Point2f AABB::GetClosestPointOnShape( Point2f p ) const {
    Point2f topLeft = GetTopLeftCorner();
    Point2f bottomRight = GetBottomRightCorner();

    if( p.x < topLeft.x ) {
        p.x = topLeft.x;
    } else if( p.x > bottomRight.x ) {
        p.x = bottomRight.x;
    }

    if( p.y < topLeft.y ) {
        p.y = topLeft.y;
    } else if( p.y > bottomRight.y ) {
        p.y = bottomRight.y;
    }

    return p;
}

// return the point on the outline of the AABB that is closest to the point P
Point2f AABB::GetClosestPointOnOutline( Point2f p ) const {
    float xDist;
    float yDist;

    // Get the closest point on the shape
    p = GetClosestPointOnShape( p );

    // Then push it to the nearest side of the AABB
    if( p.x > center.x ) {
        xDist = center.x + halfWidth - p.x;
    } else {
        xDist = center.x - halfWidth - p.x;
    }

    if( p.y > center.y ) {
        yDist = center.y + halfHeight - p.y;
    } else {
        yDist = center.y - halfHeight - p.y;
    }

    if( abs(xDist) < abs(yDist) ) {
        p.x += xDist;
    } else {
        p.y += yDist;
    }

    return p;
}


// Draw the AABB onto the target
void AABB::Draw( RenderTarget& Target, Color fill, Color outline, float outlineThickness ) const {
    dbAssert( outlineThickness >= 0.0f, "OutlineThickness is negative. Given value was ", outlineThickness );

    RectangleShape rectangle;

    // Set the location and size
    rectangle.setPosition( GetTopLeftCorner() );
    rectangle.setSize( Vector2f(halfWidth * 2.0f, halfHeight * 2.0f) );

    // Set the colors
    rectangle.setFillColor( fill );
    rectangle.setOutlineColor( outline );
    rectangle.setOutlineThickness( outlineThickness );

    // Draw the AABB
    Target.draw( rectangle );
}



// A sanity check function to confirm that the AABB is set up properly
void AABB::SanityCheck() {
    Point2f topLeft = GetTopLeftCorner();
    Point2f bottomRight = GetBottomRightCorner();

    dbAssert( halfWidth >= 0.f,  "Width is negative. Value is ", halfWidth );
    dbAssert( halfHeight >= 0.f, "Height is negative. Value is ", halfHeight );

    dbAssert( (center.x >= topLeft.x) && (center.x <= bottomRight.x) && (center.y >= topLeft.y) && (center.y <= bottomRight.y), "Not calculating the corners of the AABB properly" );

    dbAssert( IsPointInside( center ) == true, "Center is not inside AABB" );

    dbAssert( IsAABB(), "Type is not set to GeometricShape::AABB. Given type was ", geometricShape );

    (void)topLeft;
    (void)bottomRight;
}
