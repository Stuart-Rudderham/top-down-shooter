/*
 * This header defines functions to deal
 * with collision dection and response between
 * two 2D shapes
 *
 * Author:      Stuart Rudderham
 * Created:     February 12, 2012
 * Last Edited: February 12, 2012
 */

#ifndef COLLISIONDETECTION_HPP_INCLUDED
#define COLLISIONDETECTION_HPP_INCLUDED

#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"

#include <SFML/System/Vector2.hpp>

// This enum defines the different ways that objects
// can react to collisions
enum class CollisionResponse : unsigned char {
    MoveFirst,
    MoveSecond,
    MoveBoth,
    MoveNeither
};


// Forward Declarations
class BaseShape;
class AABB;
class Circle;
class LineSegment;

// Used to set the MTVs for two shapes colliding
inline void SetMTV( sf::Vector2f& mtvA, sf::Vector2f& mtvB, float mtvMagnitude, sf::Vector2f normal, CollisionResponse response );

// Used for detecting if bounding volumes are colliding
// Doesn't give any collision resolution information

// returns true if the AABBs overlap on the x-axis
inline bool BoundingBoxesOverlapXCoord( const AABB* A, const AABB* B ) { return (A->center.x + A->halfWidth >= B->center.x - B->halfWidth) && (A->center.x - A->halfWidth <= B->center.x + B->halfWidth); }

// returns true if the AABBs overlap on the y-axis
inline bool BoundingBoxesOverlapYCoord( const AABB* A, const AABB* B ) { return (A->center.y + A->halfHeight >= B->center.y - B->halfHeight) && (A->center.y - A->halfHeight <= B->center.y + B->halfHeight); }

// collision between two AABBs
inline bool BoundingBoxesColliding( const AABB* A, const AABB* B ) { return BoundingBoxesOverlapXCoord( A, B ) && BoundingBoxesOverlapYCoord( A, B ); }


// Used for detecting and resolving collisions between
// various shapes. If the two shapes are not colliding the
// function returns false.
// If the shapes are colliding then:
//    - the function returns true
//    - mtvA and mtvB are set to the Minimum Translation Vector (i.e. the vector of
//      smallest magnitude that separates the two objects). Depending on what
//      CollisionResponse is passed these could have different values
//    - Normal is set the the normal of the sides that are colliding.

bool BoxBoxCollision          ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
bool CircleCircleCollision    ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
bool PolygonPolygonCollision  ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );

bool BoxCircleCollision       ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
bool CircleBoxCollision       ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );

bool BoxPolygonCollision      ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
bool PolygonBoxCollision      ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );

bool CirclePolygonCollision   ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
bool PolygonCircleCollision   ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );


// Used for casting a ray into other shapes
// If the ray does not collide with the shape the function
// returns false.
// If the ray does collide then:
//    - the function returns true
//    - collisionPoint is set the the point of intersection between
//    - the ray and the shape
//    - Normal is set the the normal of the side of the shape that
//    - the ray collides with
bool CastRayAtAABB            ( BaseShape* lineSegment, BaseShape* shape, Point2f& intersectionPoint, sf::Vector2f& normal );
bool CastRayAtCircle          ( BaseShape* lineSegment, BaseShape* shape, Point2f& intersectionPoint, sf::Vector2f& normal );
bool CastRayAtConvexPolygon   ( BaseShape* lineSegment, BaseShape* shape, Point2f& intersectionPoint, sf::Vector2f& normal );



bool LineSegmentLineSegmentIntersection( LineSegment* line, Point2f p1, Point2f p2, Point2f& intersectionPoint );


#endif // COLLISIONDETECTION_HPP_INCLUDED
