/*
 * This header defines a 2D Axis-Aligned Bounding Box
 *
 * Author:      Stuart Rudderham
 * Created:     January 29, 2012
 * Last Edited: January 30, 2013
 */

#ifndef AABB_HPP_INCLUDED
#define AABB_HPP_INCLUDED

#include "Math/Geometry/BaseShape.hpp"

enum class CollisionResponse : unsigned char;

class AABB : public BaseShape {
    public:
        AABB();                                                                 // default ctor. Creates an AABB located at (0, 0) with a width and height of 0

        AABB( Point2f p1, Point2f p2 );                                         // ctor. Creates a rectangle such that the two points become the top-left
                                                                                // and bottom-right corners of the box. The order of the points doesn't matter,

        AABB( Point2f topLeft, float w, float h );                              // Alternate ctor. Created a rectangle with the given top-left corner, width and height

        USE_WRAPPED_MEM_CALLS(AABB);                                            // Wrapper function around "new" and "delete" for easy grepping
        SET_ALLOCATOR_NAME(AABB);                                               // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(15000);

        // Getter functions
        inline Point2f GetTopLeftCorner()     const { return Point2f( center.x - halfWidth, center.y - halfHeight ); }
        inline Point2f GetTopRightCorner()    const { return Point2f( center.x + halfWidth, center.y - halfHeight ); }
        inline Point2f GetBottomLeftCorner()  const { return Point2f( center.x - halfWidth, center.y + halfHeight ); }
        inline Point2f GetBottomRightCorner() const { return Point2f( center.x + halfWidth, center.y + halfHeight ); }

        inline float GetWidth()      const { return halfWidth * 2.0f;  }
        inline float GetHeight()     const { return halfHeight * 2.0f; }
        inline float GetHalfWidth()  const { return halfWidth;         }
        inline float GetHalfHeight() const { return halfHeight;        }


        // Setter functions
        void SetCorners( Point2f p1, Point2f p2 );                              // Reposition the corners of the AABB, then recalculate the properties
        //void Set( Point2f topLeft, float w, float h );                          // Reposition the top-left corner as well as the width and height, then recalculate the properties

        inline void SetHalfWidth( float w ) { halfWidth = w; }
        inline void SetHalfHeight( float h ) { halfHeight = h; }

        // Bounding Volumes
        void SetBoundingBox( AABB* boundingBox ) const;                         // Sets the provided AABB to be the bounding box that encloses the shape


         // Drawing function
        void Draw( sf::RenderTarget& Target,                                    // draws the AABB to the target (Window, texture, etc...)
                   sf::Color fill = sf::Color::White,                           // If no arguments are passed a white rectangle with a thin
                   sf::Color outline = sf::Color::Black,                        // black outline is drawn
                   float outlineThickness = 1.0 ) const;


        // Linear algebra
        Pair Project( sf::Vector2f axis ) const;                                // projects the AABB onto the provided axis, which is a unit vector
                                                                                // the minimum value of the projection is stored in Pair.x, while the
                                                                                // maximum value of the projection is stored in Pair.y

        Point2f GetClosestPointOnOutline( Point2f p ) const;                    // return the point on the outline of the AABB that is closest to the point P
        Point2f GetClosestPointOnShape( Point2f p ) const;                      // return the point (possibly inside the AABB) that is closest to the point P

        inline bool IsPointInside( Point2f p ) const {                          // returns true if the point is inside the AABB
            return ( p.x >= center.x - halfWidth  )  &&
                   ( p.x <= center.x + halfWidth  )  &&
                   ( p.y >= center.y - halfHeight )  &&
                   ( p.y <= center.y + halfHeight );
        };

    private:
        float halfWidth;                                                        // the half-width of the AABB (i.e. the distance from the center to the left/right side
        float halfHeight;                                                       // the half-height of the AABB (i.e. the distance from the center to the top/bottom side

        inline Point2f CalculateCenter( Point2f p1, Point2f p2 ) const {        // calculate the location of the center point given two points that are
            return ( p1 + p2 ) / 2.0f;                                          // opposite corners of the box (e.g. the top-left and bottom-right)
        }

        void SanityCheck();                                                     // runs a bunch of checks to confirm that the AABB is formatted properly
                                                                                // If it's not a dbAssert will catch and exit the program

        UNTRACKED_NEW_AND_DELETE(AABB);                                         // Overload "new" and "delete" to use ArenaAllocator

        NO_COPY(AABB);                                                          // No copy ctor

    // Friend functions for collision detection/resolution
    friend bool BoundingBoxesColliding( const AABB* A, const AABB* B );
    friend bool BoundingBoxesOverlapXCoord( const AABB* A, const AABB* B );
    friend bool BoundingBoxesOverlapYCoord( const AABB* A, const AABB* B );
    friend bool BoxBoxCollision     ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
    friend bool BoxCircleCollision  ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
    friend bool BoxPolygonCollision ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
    friend bool PolygonBoxCollision ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
};

#endif // AABB_HPP_INCLUDED
