/*
 * This header defines a 2D circle
 *
 * Author:      Stuart Rudderham
 * Created:     January 29, 2012
 * Last Edited: January 30, 2013
 */

#ifndef CIRCLE_HPP_INCLUDED
#define CIRCLE_HPP_INCLUDED

#include "Math/MathFunctions.hpp"
#include "Math/Geometry/BaseShape.hpp"

enum class CollisionResponse : unsigned char;

class Circle : public BaseShape {
    public:
        Circle();                                                               // default ctor. Creates a Circle located at (0, 0) with a radius of 0

        Circle( Point2f c, float r);                                            // ctor. Creates a Circle located at the given position and with the given radius

        USE_WRAPPED_MEM_CALLS(Circle);                                          // Wrapper function around "new" and "delete" for easy grepping
        SET_ALLOCATOR_NAME(Circle);                                             // Set the attributes for the ArenaAllocator
        SET_ALLOCATOR_SIZE(500);

        // Getter functions
        inline float GetRadius() const { return radius; }


        // Setter functions
        inline void SetRadius( float r ) { radius = r; }


        // Bounding Volumes
        void SetBoundingBox( AABB* boundingBox ) const;                         // Sets the provided AABB to be the bounding box that encloses the shape


        // Drawing function
        void Draw( sf::RenderTarget& Target,                                    // draws the Circle to the target (Window, texture, etc...) with the
                   sf::Color fill = sf::Color::White,                           // passed in colors. If nothing is passed in it draws a white circle
                   sf::Color outline = sf::Color::Black,                        // with a thin black outline
                   float outlineThickness = 1.0 ) const;


        // Linear algebra
        inline Pair Project( sf::Vector2f axis ) const {                        // projects the Circle onto the provided axis, which is a unit vector
            // The axis must be a unit vector, otherwise the formula is wrong
            dbAssert( IsAlmostEqual( GetMagnitudeSquared(axis), 1.0f, MathConstants::EpsilonAroundOne ),
                      "Circle::Project() - the axis was not a unit vector. Given axis was ",
                      axis );

            float dotProd = DotProduct( center, axis );
            return Pair( dotProd - radius, dotProd + radius );
        }

        inline Point2f GetClosestPointOnOutline( Point2f p ) const {            // return the point on the outline of the Circle that is closest to the point P
            return center + GetUnitVector( GetDirectionVector(center, p) ) * radius;
        }

        inline bool IsPointInside( Point2f p ) const {                          // returns true if the point is inside the Circle
            return GetMagnitudeSquared( GetDirectionVector(center, p) ) <= radius * radius;
        }

    private:
        float radius;                                                           // the radius of the circle

        void SanityCheck();                                                     // runs a bunch of checks to confirm that the circle is formatted properly
                                                                                // If it's not a dbAssert will catch and exit the program

        UNTRACKED_NEW_AND_DELETE(Circle);                                       // Overload "new" and "delete" to use ArenaAllocator

        NO_COPY_OR_ASSIGN(Circle);                                              // No copy/assign ctor

    // Friend functions for collision detection/resolution
    friend bool CircleCircleCollision    ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
    friend bool BoxCircleCollision       ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
    friend bool CirclePolygonCollision   ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );
    friend bool PolygonCircleCollision   ( BaseShape* shape1, BaseShape* shape2, CollisionResponse response, sf::Vector2f& mtvA, sf::Vector2f& mtvB, sf::Vector2f& normal );

    friend bool CastRayAtCircle( BaseShape* lineSegment, BaseShape* shape,  Point2f& intersectionPoint, sf::Vector2f& normal );
};

#endif // CIRCLE_HPP_INCLUDED
