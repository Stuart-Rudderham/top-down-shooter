/*
 * This header defines a 2D line segment
 *
 * Author:      Stuart Rudderham
 * Created:     March 30, 2012
 * Last Edited: July 11, 2012
 */

#include "Math/Geometry/LineSegment.hpp"
#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"
using namespace std;
using namespace sf;

LineSegment::LineSegment( Point2f start, Point2f end ) : BaseShape( CalculateCenter(start, end), GeometricShape::LineSegment )
{
    Set( start, end );
}

LineSegment::LineSegment( Point2f start, Vector2f direction, float magnitude ) : BaseShape( CalculateCenter(start, start + (direction * magnitude)), GeometricShape::LineSegment )
{
    Set( start, direction, magnitude );
}


// sets the location of the LineSegment
void LineSegment::Set( Point2f start, Point2f end ) {
    startPoint = start;
    endPoint = end;

    center = CalculateCenter( startPoint, endPoint );
}

void LineSegment::Set( Point2f start, Vector2f direction, float magnitude ) {
    startPoint = start;
    endPoint = start + ( direction * magnitude );

    center = CalculateCenter( startPoint, endPoint );
}


// Drawing function
void LineSegment::Draw( RenderTarget& Target, Color fill, Color outline, float outlineThickness ) const {
    Vertex line[2];
	line[0] = Vertex( startPoint, fill );
	line[1] = Vertex( endPoint, fill );          // create the line

    Target.draw(line, 2, Lines);                                                        // draw the line

    (void)outline;
    (void)outlineThickness;
}

// Returns the AABB that encloses the shape
void LineSegment::SetBoundingBox( AABB* boundingBox ) const {
    boundingBox->SetCorners( startPoint, endPoint );
}


// projects the LineSegment onto the provided axis
Pair LineSegment::Project( Vector2f axis ) const {

    // The axis must be a unit vector, otherwise the formula is wrong
    dbAssert( IsAlmostEqual( GetMagnitude(axis), 1.0f, MathConstants::EpsilonAroundOne ), "LineSegment::Project() - the axis was not a unit vector. Given axis was ", axis );

    float projectStartPoint = DotProduct( startPoint, axis );
    float projectEndPoint = DotProduct( endPoint, axis );

    if( projectStartPoint < projectEndPoint ) {
        return Pair( projectStartPoint, projectEndPoint );
    } else {
        return Pair( projectEndPoint, projectStartPoint );
    }
}



void LineSegment::SanityCheck() {
    // Nothing to put here ???
}
