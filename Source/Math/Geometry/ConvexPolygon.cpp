/*
 * This file implements a 2D Convex Polygon. The points
 * are defined in a clockwise ordering
 *
 * Author:      Stuart Rudderham
 * Created:     January 31, 2012
 * Last Edited: March 16, 2012
 */

#include "Math/Geometry/ConvexPolygon.hpp"
#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"

#include "UpgradesToSFML/Drawing.hpp"

#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

using namespace std;
using namespace sf;

// Constructor. Can only pass the number of sides, need to initialize the points separately
ConvexPolygon::ConvexPolygon( unsigned int n ) : BaseShape( Point2f(0, 0), GeometricShape::ConvexPolygon ),
                                                 numPoints( n )
{
    // Make sure have valid number of points
    dbAssert( numPoints >= 3, "Too few points. Number of points is ", numPoints );
    dbAssert( numPoints <= maxPoints, "Too many points. Number of points in ", numPoints );

    // Initialize all the points to (0, 0)
    for( unsigned int i = 0; i < numPoints; ++i ) {
        points[i] = Point2f(0, 0);
        normals[i] = Vector2f(0, 0);
    }

    //SanityCheck();        // can't do check because the polygon isn't setup yet
}

// Constructor that initializes all the points at once
ConvexPolygon::ConvexPolygon( const std::vector<Point2f>& polygonPoints ) : BaseShape( Point2f(0, 0), GeometricShape::ConvexPolygon ),
                                                                            numPoints( polygonPoints.size() )
{
    // Make sure have valid number of points
    dbAssert( numPoints >= 3, "Too few points. Number of points is ", numPoints );
    dbAssert( numPoints <= maxPoints, "Too many points. Number of points in ", numPoints );

    // Initialize all the points
    unsigned int i = 0;
    for( auto p = begin(polygonPoints); p != end(polygonPoints); ++p, ++i ) {
        points[i] = *p;
    }

    // Calculate the location of the center point
    center = CalculateCenter();

    // Calculate the normals
    CalculateSideNormals();

    SanityCheck();
}


// Alternate constructor, makes the polygon a regular n-gon
ConvexPolygon::ConvexPolygon( unsigned int n, Point2f c, float radius ) : BaseShape( Point2f(0, 0), GeometricShape::ConvexPolygon ),
                                                                          numPoints( n )
{
    // Make sure have valid number of points
    dbAssert( numPoints >= 3, "Too few points. Number of points is ", numPoints );
    dbAssert( numPoints <= maxPoints, "Too many points. Number of points in ", numPoints );

    MakeRegularNGon( c, radius );

    SanityCheck();
}


// copy constructor
ConvexPolygon::ConvexPolygon( const ConvexPolygon& original ) : BaseShape( original.center, GeometricShape::ConvexPolygon ),
                                                                numPoints( original.numPoints )
{
    // Make sure have valid number of points
    dbAssert( numPoints >= 3, "Too few points. Number of points is ", numPoints );
    dbAssert( numPoints <= maxPoints, "Too many points. Number of points in ", numPoints );

    // Copy over the info
    for( unsigned int i = 0; i < numPoints; ++i ) {
        points[i] = original.points[i];
        normals[i] = original.normals[i];
    }

    // Make sure everything makes sense
    SanityCheck();
}


// Assignment operator
ConvexPolygon& ConvexPolygon::operator=( const ConvexPolygon& other ) {

    dbAssert( numPoints == other.numPoints, "Cannot assign to a ConvexPolygon with different number of points" );

    // Copy it over
    for( unsigned int i = 0; i < numPoints; ++i ) {
        points[i] = other.points[i];
        normals[i] = other.normals[i];
    }

    center = other.center;

    // Confirm everything makes sense
    SanityCheck();

    return *this;
}


// Makes the polygon a regular n-gon that is is circumscribed by a circle centered at the given point with given radius
void ConvexPolygon::MakeRegularNGon( Point2f c, float radius ) {

    float angleStep = DegreeToRadian( 360.0f / (float)numPoints );

    float currentAngle = (float)-MathConstants::PI_2;               // start at -90 degrees (i.e. pointing at the top of the screen)

    // Make sure the bottom of the shape is a flat edge rather than a point
    // So if there's an even number of sides we have to rotate the shape slightly
    if( numPoints % 2 == 0 ) {
        currentAngle -= angleStep / 2.0f;
    }

    for( unsigned int i = 0; i < numPoints; ++i ) {
        points[i] = c + Point2f( cos(currentAngle) * radius, sin(currentAngle) * radius );
        currentAngle += angleStep;
    }

    center = c;

    CalculateSideNormals();
}

// rotate the ConvexPolygon around the center point by the given number of degrees
void ConvexPolygon::Rotate( float angle ) {

    // Create the rotation matrix
    Transform pointRotation;
    pointRotation.rotate( angle, center );

    Transform normalRotation;
    normalRotation.rotate( angle );

    // Rotate the entire shape relative to its center, and rotate the normals relative to (0, 0)
    for( unsigned int i = 0; i < numPoints; ++i ) {
        points[i] = pointRotation.transformPoint( points[i] );
        normals[i] = normalRotation.transformPoint( normals[i] );
    }

    // If the ConvexPolygon is rotated often (i.e. every frame) then the floating point
    // errors will accumulate and eventually the unit normals will no longer have a
    // magnitude of 1. So when this happens we just re-normalize them
    for( unsigned int i = 0; i < numPoints; ++i ) {
        if( IsAlmostEqual( GetMagnitudeSquared( normals[i] ), 1.0f, MathConstants::EpsilonAroundOne ) == false ) {
            normals[i] = GetUnitVector( normals[i] );
            dbPrintToConsole( "Had to re-normalize a normal on ConvexPolygon stored at: ", this );
        }
    }

    // And make sure the results make sense
    SanityCheck();
}

// Functions to calculate properties of the polygon
// Algorithm borrowed from here -> http://paulbourke.net/geometry/polyarea/
Point2f ConvexPolygon::CalculateCenter() {

    float area = 0.0f;
    sf::Vector2f c;

    for( unsigned int i = 0; i < numPoints; ++i ) {
        int j = (i + 1) % numPoints;

        float temp = CrossProduct2D( points[i], points[j] );

        area += temp;
        c.x += (points[i].x + points[j].x) * temp;
        c.y += (points[i].y + points[j].y) * temp;
    }


    area /= 2.0f;
    c.x /= (6 * area);
    c.y /= (6 * area);

    return c;
}

// Function to calculate the normal to each side of the polygon
void ConvexPolygon::CalculateSideNormals() {
    for( unsigned int i = 0; i < numPoints; ++i ) {
        int j = (i + 1) % numPoints;
        normals[i] = GetUnitClockwiseNormal( GetDirectionVector(points[i], points[j]) );
    }
}


// Algorithm borrowed from here -> http://paulbourke.net/geometry/clockwise/index.html
bool ConvexPolygon::IsClockwiseOrdering() {
    for( unsigned int i = 0; i < numPoints; ++i ) {
        int j = (i + 1) % numPoints;
        int k = (i + 2) % numPoints;

        float cross = CrossProduct2D( GetDirectionVector(points[i], points[j]),
                                      GetDirectionVector(points[j], points[k]) );

        if( cross < 0 ) {
            return false;
        }
    }

    return true;
}

// Algorithm borrowed from here -> http://paulbourke.net/geometry/clockwise/index.html
bool ConvexPolygon::IsConvex() {

    // This is OK because we know that there are at least 3 points
    int correctSign = Sign( CrossProduct2D( GetDirectionVector(points[0], points[1]),
                                            GetDirectionVector(points[1], points[2]) ));

    for( unsigned int i = 1; i < numPoints; ++i ) {
        int j = (i + 1) % numPoints;
        int k = (i + 2) % numPoints;

        int testSign = Sign( CrossProduct2D( GetDirectionVector(points[i], points[j]),
                                             GetDirectionVector(points[j], points[k]) ));

        if( testSign != correctSign ) {
            return false;
        }
    }

    return true;
}

// Shift the entire polygon by the displacement vector
void ConvexPolygon::MoveBy( Vector2f displacement ) {
    for( unsigned int i = 0; i < numPoints; ++i ) {
        points[i] += displacement;
    }

    center += displacement;
}

// Set the location of the point at the specified index in the array
void ConvexPolygon::Set( unsigned int i, Point2f p, bool recalculateNormals, bool recalculateCenter ) {
    dbAssert( i < numPoints, "ConvexPolygon::Set() - array index is too high, out of bounds. Given value was ", i);

    points[i] = p;

    // If things need to be recalculated
    if( recalculateCenter || recalculateNormals ) {

        // Recalculate them
        if( recalculateCenter ) {
            center = CalculateCenter();
        }

        if( recalculateNormals ) {
            CalculateSideNormals();
        }

        // And make sure the results make sense
        SanityCheck();
    }
}

// Getter functions
Point2f ConvexPolygon::GetPoint( unsigned int i ) const {
    dbAssert( i < numPoints, "ConvexPolygon::GetPoint() - array index is too high, out of bounds. Given value was ", i );

    return points[i];
}


// Returns the AABB that surrounds the ConvexPolygon
void ConvexPolygon::SetBoundingBox( AABB* boundingBox ) const {
    float minX = points[0].x;
    float maxX = points[0].x;

    float minY = points[0].y;
    float maxY = points[0].y;

    for( unsigned int i = 1; i < numPoints; ++i ) {
        minX = min( minX, points[i].x );
        maxX = max( maxX, points[i].x );

        minY = min( minY, points[i].y );
        maxY = max( maxY, points[i].y );
    }

    boundingBox->SetCorners( Point2f(minX, minY), Point2f(maxX, maxY) );
}

// projects the polygon onto the provided axis, which is a unit vector.
Pair ConvexPolygon::Project( Vector2f axis ) const {

    // The axis must be a unit vector, otherwise the formula is wrong
    dbAssert( IsAlmostEqual( GetMagnitudeSquared(axis), 1.0f, MathConstants::EpsilonAroundOne ), "ConvexPolygon::Project() - the axis was not a unit vector. Given axis was ", axis );

    float dotProd = DotProduct( points[0], axis );

    float maxValue = dotProd;
    float minValue = dotProd;

    // For each corner of the box
    for( unsigned int i = 1; i < numPoints; ++i ) {
        dotProd = DotProduct( points[i], axis );                    // get the scalar projection of the point onto the axis

        // keep track of min and max values
        if( dotProd < minValue ) {
            minValue = dotProd;
        } else if( dotProd > maxValue ) {
            maxValue = dotProd;
        }
    }

    return Pair( minValue, maxValue );                              // Return the min/max value
}


// return the point on the polygon that is closest to the point P
Point2f ConvexPolygon::GetClosestPointOnOutline( Point2f p ) const {

    // For each side get the closest point, and choose the minimum of all those
    Point2f closestPoint = ClosestPointOnLine( p, points[0], points[1] );
    float minDistance = GetMagnitudeSquared( GetDirectionVector(p, closestPoint) );

    for( unsigned int i = 1; i < numPoints; ++i ) {
        unsigned int j = (i + 1) % numPoints;

        Point2f testPoint = ClosestPointOnLine( p, points[i], points[j] );
        float testDistance = GetMagnitudeSquared( GetDirectionVector(p, testPoint) );

        if( testDistance < minDistance ) {
            minDistance = testDistance;
            closestPoint = testPoint;
        }
    }

    return closestPoint;
}

// Get the vertex of the polygon that is closest to the point P
Point2f ConvexPolygon::GetClosestVertex( Point2f p ) const {

    // For each vertex get the distance to the point, and choose the minimum of all those
    Point2f closestPoint = points[0];
    float minDistance = GetMagnitudeSquared( GetDirectionVector(p, closestPoint) );

    for( unsigned int i = 1; i < numPoints; ++i ) {

        Point2f testPoint = points[i];
        float testDistance = GetMagnitudeSquared( GetDirectionVector(p, testPoint) );

        if( testDistance < minDistance ) {
            minDistance = testDistance;
            closestPoint = testPoint;
        }
    }

    return closestPoint;
}

// returns true if the point is inside the polygon
bool ConvexPolygon::IsPointInside( Point2f p ) const {

    // For each edge of the polygon do a halfspace test to see what
    // side of the edge the point is on. Since the polygon is convex
    // the results should be the same for each side. If we find one that's
    // different then the point isn't inside the shape
    bool firstSide = HalfSpaceTest( p, points[0], points[1] );

    for( unsigned int i = 1; i < numPoints; ++i ) {
        unsigned int j = (i + 1) % numPoints;

        if( HalfSpaceTest( p, points[i], points[j] ) != firstSide ) {
            return false;
        }
    }

    return true;
}

// Draw the polygon onto the target
void ConvexPolygon::Draw( RenderTarget& Target, Color fill, Color outline, float outlineThickness ) const {
    dbAssert( outlineThickness >= 0.0f, "ConvexPolygon::Draw() - outlineThickness is negative" );

    ConvexShape polygon( numPoints );

    // Set the location and size
    for( unsigned int i = 0; i < numPoints; ++i ) {
        polygon.setPoint(i, points[i] );
    }

    // Set the colors
    polygon.setFillColor( fill );
    polygon.setOutlineColor( outline );
    polygon.setOutlineThickness( outlineThickness );

    // Draw the ConvexPolygon and center point dot
    Target.draw( polygon );

/*
    // Draw the normals
    for( unsigned int i = 0; i < numPoints; ++i ) {
        unsigned int j = (i + 1) % numPoints;
        DrawVector( Target, (points[i] + points[j]) / 2.0f, normals[i] * 25.0f );
    }
*/
}

// A sanity check function to confirm that the polygon is set up properly
void ConvexPolygon::SanityCheck() {

    dbAssert( numPoints >= 3, "Too few points. Number of points is ", numPoints );
    dbAssert( numPoints <= maxPoints, "Too many points. Number of points in ", numPoints );

    dbAssert( IsConvex() == true, "Polygon is not convex" );
    dbAssert( IsClockwiseOrdering() == true, "Polygon does not have a clockwise ordering" );

    dbAssert( IsConvexPolygon(), "Type is not set to SHAPE_TYPE_CONVEX_POLYGON. Given type was ", geometricShape );

    for( unsigned int i = 0; i < numPoints; ++i ) {
        dbAssert( IsAlmostEqual( GetMagnitude( normals[i] ), 1.0f, MathConstants::EpsilonAroundOne ), "Normal had magnitude greater than 1. Given normal was ", normals[i] );
    }
}
