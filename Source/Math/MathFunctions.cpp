/*
 * This file implements various math functions
 * used throughout the program
 *
 * Author:      Stuart Rudderham
 * Created:     February 1, 2012
 * Last Edited: February 3, 2012
 */

#include "Math/MathFunctions.hpp"

using namespace std;
using namespace sf;

/*******************************************************
 *            Linear algebra functions                 *
 *******************************************************/

// return the vector rotated clockwise by R radians. The original vector isn't modified
// algorithm from here -> http://en.wikipedia.org/wiki/Rotation_(mathematics)
Vector2f RotateClockwise( const Vector2f v, const float r ) {
    const float sinAngle = sin( r );
    const float cosAngle = cos( r );

    return Vector2f( v.x * cosAngle - v.y * sinAngle, v.x * sinAngle + v.y * cosAngle );
}

// return the vector rotated counter clockwise by R radians. The original vector isn't modified
Vector2f RotateCounterClockwise( const Vector2f v, const float r ) {
    return RotateClockwise( v, -r );
}

// returns the point on the line AB that is closest to point P
// algorithm is from here -> http://paulbourke.net/geometry/pointline/
Point2f ClosestPointOnLine( const Point2f p, const Point2f a, const Point2f b ) {

    // If the points are coincident then there is no line, so just return the point
    if( a == b ) {
        return a;
    }

    Vector2f AB = GetDirectionVector(a, b);                     // get the vector that makes up the line
    Vector2f AP = GetDirectionVector(a, p);                     // get the vector that starts at A and goes to P

    float u = DotProduct(AB, AP) / GetMagnitudeSquared(AB);     // solve for the coefficient "u"

    if( u < 0 ) {
        return a;
    } else if( u > 1 ) {
        return b;
    } else {
        return a + (u * AB);
    }
}
