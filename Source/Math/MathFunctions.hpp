/*
 * This header defines various math functions
 * used throughout the program
 *
 * Author:      Stuart Rudderham
 * Created:     January 31, 2012
 * Last Edited: February 3, 2012
 */

#ifndef MATHFUNCTIONS_HPP_INCLUDED
#define MATHFUNCTIONS_HPP_INCLUDED

#include "DebugTools/Debug.hpp"

#include <SFML/System/Vector2.hpp>

#include <cfloat>
#include <utility>
#include <cmath>
#include <cstdlib>
#include <algorithm>

/*******************************************************
 *               Floating Point Equality               *
 *******************************************************/
// This allows you to define an absolute difference between the numbers as a cutoff
// Remember, floating points numbers have a precision that depends on their magnitude
// so an epsilon of 0.0001 may work when the floats are small but will break if the
// floats become large. This is good if one of the floats has a know value (e.g. comparing to 0)
inline bool IsAlmostEqual( const float a, const float b, const float epsilon ) {
    return abs(a - b) <= epsilon;
}


/*******************************************************
 *               Random number generators              *
 *******************************************************/
// return a floating point number in the range [0, 1]
// code is copied from here -> http://stackoverflow.com/questions/686353/c-random-float
inline float RandUniformFloat() {
    return (float)rand() / (float)RAND_MAX;
}

// return a floating point number in the range [min, max]
// code is copied from here -> http://www.cplusplus.com/forum/general/14192/
inline float RandFloat( const float min, const float max ) {
    return min + RandUniformFloat() * (max - min);
}

// return an integer in the range [min, max]
inline int RandInt( const int min, const int max ) {
    return min + rand() % (max - min + 1);
}


/*******************************************************
 *                   Misc Functions                    *
 *******************************************************/
// Sign Function. Takes in a number and returns
//    -1 if it is negative
//     0 if it is zero
//     1 if it is positive
template<typename T>
inline int Sign( T number ) {
    if( number > 0 ) {
        return 1;
    } else if ( number < 0 ) {
        return -1;
    } else {
        return 0;
    }
}

// Clamps the given value between the two extremes
template<typename T>
inline void Clamp( T minValue, T& value, T maxValue ) {
    value = std::min( std::max( minValue, value ), maxValue );
}

// Do linear interpolation between the two extremes
template<typename T>
inline T Lerp( const T min, const float current, const T max ) {
    dbAssert( current >= 0.0f && current <= 1.0f, "Invalid value for LERP" );
    return static_cast<T>( min + ( (max - min) * current ) );
}


/*******************************************************
 *                Trig functions                       *
 *******************************************************/
// convert degrees to radians
inline float DegreeToRadian( const float angle ) {
    return angle * ( MathConstants::PI / 180.0f );
}

// convert radians to degrees
inline float RadianToDegree( const float angle ) {
    return angle * ( 180.0f / MathConstants::PI );
}

/*******************************************************
 *            Linear algebra functions                 *
 *******************************************************/
// return the dot product of the two vectors
inline float DotProduct( const sf::Vector2f a, const sf::Vector2f b ) {
    return (a.x * b.x) + (a.y * b.y);
}

// return the Z component of the cross product of the two vectors
// We are not actually computing a cross product (as we only deal with 2D vectors)
// but since the Z component of the cross product only depends on the X and Y
// components we can calculate it anyways. This has uses when dealing with convex
// polygons, e.g. computing the location of the centroid and confirming convexity
inline float CrossProduct2D( const sf::Vector2f a, const sf::Vector2f b ) {
    return (a.x * b.y) - (a.y * b.x);
}

// returns the direction vector that starts at A and ends at B
inline sf::Vector2f GetDirectionVector( const Point2f a, const Point2f b ) {
    return b - a;
}

// returns a direction vector with the given angle relative to the x-axis, and the given magnitude
inline sf::Vector2f GetDirectionVector( const float angle, const float magnitude ) {
    return sf::Vector2f( cos(DegreeToRadian(angle)) * magnitude, sin(DegreeToRadian(angle)) * magnitude );
}

// return the magnitude of the vector squared (avoid the square root)
inline float GetMagnitudeSquared( const sf::Vector2f v ) {
    return DotProduct( v, v );
}

// return the magnitude of the vector
inline float GetMagnitude( const sf::Vector2f v ) {
    return sqrt( GetMagnitudeSquared(v) );
}

// return the unit vector. The original vector isn't modified
inline sf::Vector2f GetUnitVector( const sf::Vector2f v ) {
    return v / GetMagnitude(v);
}

// return the normal to the passed in vector resulting from a 90 degree clockwise rotation
inline sf::Vector2f GetClockwiseNormal( const sf::Vector2f v ) {
    return sf::Vector2f( v.y, -v.x );
}

// return the unit vector of the clockwise normal to the passed in vector
inline sf::Vector2f GetUnitClockwiseNormal( const sf::Vector2f v ) {
    return GetUnitVector( GetClockwiseNormal(v) );
}

// return the normal to the passed in vector resulting from a 90 degree counterclockwise rotation
inline sf::Vector2f GetCounterClockwiseNormal( const sf::Vector2f v ) {
    return sf::Vector2f( -v.y, v.x );
}

// return the unit vector of the counterclockwise normal to the passed in vector
inline sf::Vector2f GetUnitCounterClockwiseNormal( const sf::Vector2f v ) {
    return GetUnitVector( GetCounterClockwiseNormal(v) );
}

// return the vector reflected off a surface with the passed in normal
// Algorithm borrowed from here -> http://paulbourke.net/geometry/reflected/
inline sf::Vector2f ReflectVector( const sf::Vector2f v, const sf::Vector2f normal ) {

    // Not entirely sure why this formula works. Magic?
    return v - ( 2.0f * normal * DotProduct(v, normal) );
}

// returns true if the point P is in front (i.e. the direction that the clockwise normal points) of the axis AB
// idea and code are borrowed from here -> http://www.miguelcasillas.com/?p=43
inline bool HalfSpaceTest( const Point2f p, const Point2f a, const Point2f b ) {

    // Get a vector from the point in space to a point on the axis
    // and do a dot product with the axis to get the angle between them.
    // answer <= 0 implies that the angle is <= 180, so it's on the "front" side
    return DotProduct( GetDirectionVector(p, a), GetClockwiseNormal(GetDirectionVector(a, b)) ) <= 0;
}

// Take in a direction vector and return the angle (in degrees) between
// the vector and the axis
inline float ConvetToAngle( sf::Vector2f v ) {
    return RadianToDegree( atan2( v.y, v.x ) );
}

sf::Vector2f RotateClockwise( const sf::Vector2f v, const float r );                    // return the vector rotated clockwise by R radians. The original vector isn't modified
sf::Vector2f RotateCounterClockwise( const sf::Vector2f v, const float r );             // return the vector rotated counter clockwise by R radians. The original vector isn't modified

Point2f ClosestPointOnLine( const Point2f p, const Point2f a, const Point2f b );        // returns the point on the line AB that is closest to point P

#endif // MATHFUNCTIONS_HPP_INCLUDED
