/*
 * This header defines a base class
 * that forces any class that inherits
 * from it to be a singleton
 *
 * Author:      Stuart Rudderham
 * Created:     August 28, 2012
 * Last Edited: March 30, 2013
 */

#ifndef SINGLETON_HPP_INCLUDED
#define SINGLETON_HPP_INCLUDED

#include "DebugTools/Debug.hpp"

// Flag to control the printing of creation/destruction messages
#ifdef SINGLETON_PRINT_MESSAGE
    #error "SINGLETON_PRINT_MESSAGE already defined"
#endif

#define SINGLETON_PRINT_MESSAGE 1

#if SINGLETON_PRINT_MESSAGE
    #include <typeinfo>
#endif

template<typename T>
class Singleton {
    private:
        static bool Exists;                                     // True if the Singleton has been created

        Singleton( const Singleton& );                          // define these but don't implement them
        Singleton& operator =( const Singleton& );              // will cause a linker error if they are
                                                                // somehow called, even though they are
                                                                // marked "private"

    // ctor and dtor are marked "protected" so that you can't instantiate an
    // object of type Singleton, you need to have a child class inherit from it
    protected:
        Singleton() {
            if( Exists == true ) {
                dbCrash( "Cannot create more than one instance of Singleton."
                         "Always use the Get() function to access the Singleton, "
                         "never create an instance manually." );
            }

            Exists = true;

            #if SINGLETON_PRINT_MESSAGE
                dbPrintToConsole( "Created Singleton of type ", typeid(T).name() );
            #endif
        }

        virtual ~Singleton() {
            dbAssert( Exists == true, "Cannot delete, Singleton doesn't 'Exist'. Shouldn't be possible." );
            Exists = false;

            #if SINGLETON_PRINT_MESSAGE
                dbPrintToConsole( "Deleted Singleton of type ", typeid(T).name() );
            #endif
        }

    public:
        // Getter for the Singleton.
        // The C++ FAQ describes some potential problems with the initialization of static variables
        //      http://www.parashift.com/c++-faq-lite/static-init-order.html
        //      http://www.parashift.com/c++-faq-lite/static-init-order-on-first-use.html
        //      http://www.parashift.com/c++-faq-lite/construct-on-first-use-v2.html
        //
        // This implementation avoids the static *initialization* problem because the singleton instance
        // is lazily created the first time that Get() is called.
        //
        // It can suffer from the static *de-initialization* problem, however. If another static object
        // *doesn't* access the Singleton in its constructor but *does* access the Singleton in its
        // destructor then there will be a problem, because the Singleton will be used after it has been
        // destructed. In Debug mode there is an assert that will catch this.
        // This problem could have been avoided by using a static pointer rather than a static
        // object for the singleton instance variable, but that approach means that the destructor for
        // the Singleton never gets called.
        //
        // There are also thread-safety issues. In C++11 it is guaranteed that the singleton instance is
        // only initialized once, but C++03 makes no such guarantees.
        //
        // There is an implicit check (added by the compiler) at the beginning of this function that checks
        // every time if the static variable has been created. This is usually not a big deal, but is something
        // to keep in mind if calling Get() in a tight loop.
        //
        // Similar explanation here -> http://stackoverflow.com/questions/335369/finding-c-static-initialization-order-problems/335746#335746
        static inline T& Get() {
            static T Instance;
            dbAssert( Exists == true, "Cannot get, Singleton has already been destructed" );
            return Instance;
        }
};

// Having the variable in the header file is fine because of the
// way that templates work, there will only be one copy per type T
template<typename T>
bool Singleton<T>::Exists = false;

// Don't need this anymore
#undef SINGLETON_PRINT_MESSAGE

#endif // SINGLETON_HPP_INCLUDED
