#if 0

/*
 * This file implements the unit test function
 * for GUI
 *
 * Author:      Stuart Rudderham
 * Created:     February 5, 2012
 * Last Edited: February 7, 2012
 */

#include "Testing/UnitTests.hpp"

#include "GUI/Label.hpp"
#include "GUI/Picture.hpp"
#include "GUI/CheckBox.hpp"
#include "GUI/PictureButton.hpp"
#include "GUI/TextButton.hpp"
#include "MediaCache/MediaCache.hpp"

#include "UpgradesToSFML/Input.hpp"
#include "UpgradesToSFML/Printing.hpp"
#include "UpgradesToSFML/Drawing.hpp"

#include "Profile/TimeProfiler.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <cstdlib>
#include <vector>

using namespace std;
using namespace sf;

static bool shouldExitGUI = false;

vector<Label*> labels;
vector<CheckBox*> checkBoxes;
vector<Picture*> pictures;
vector<ActionButton*> actionButtons;


void TestFunction1() {
    cout << "Worked" << endl;
}

void TestFunction2() {
    shouldExitGUI = true;
}

void Blah() {
    vector<CheckBox*>::iterator c;
    vector<Label*>::iterator l;
    vector<Picture*>::iterator p;
    vector<ActionButton*>::iterator a;

    for( p = pictures.begin(); p != pictures.end(); ++p ) {
        (**p).ChangeAlignment( LEFT_ALIGN, TOP_ALIGN );
    }

    for( l = labels.begin(); l != labels.end(); ++l ) {
        (**l).ChangeAlignment( LEFT_ALIGN, TOP_ALIGN );
    }

    for( c = checkBoxes.begin(); c != checkBoxes.end(); ++c ) {
        (**c).ChangeAlignment( LEFT_ALIGN, TOP_ALIGN );
    }

    for( a = actionButtons.begin(); a != actionButtons.end(); ++a ) {
        (**a).ChangeAlignment( LEFT_ALIGN, TOP_ALIGN );
    }
}

void TestGUI( RenderWindow& Window ) {

    // Used to handle window events (e.g. clicking the close button)
    Event Event;


    labels.push_back( new Label( LabelInfo("Test Label 1", FontCache::Get(TestFont), 40, Color::Black), BaseElementInfo( "Top Left Label",      Point2f( 0,                        0                         ), LEFT_ALIGN,   TOP_ALIGN    ) ) );
    labels.push_back( new Label( LabelInfo("Test Label 2", FontCache::Get(TestFont), 40, Color::Black), BaseElementInfo( "Top Right Label",     Point2f( Window.GetWidth(),        0                         ), RIGHT_ALIGN,  TOP_ALIGN    ) ) );
    labels.push_back( new Label( LabelInfo("Test Label 3", FontCache::Get(TestFont), 40, Color::Black), BaseElementInfo( "Bottom Left Label",   Point2f( 0,                        Window.GetHeight()        ), LEFT_ALIGN,   BOTTOM_ALIGN ) ) );
    labels.push_back( new Label( LabelInfo("Test Label 4", FontCache::Get(TestFont), 40, Color::Black), BaseElementInfo( "Bottom Right Label",  Point2f( Window.GetWidth(),        Window.GetHeight()        ), RIGHT_ALIGN,  BOTTOM_ALIGN ) ) );

    labels.push_back( new Label( LabelInfo("Test Label 5", FontCache::Get(TestFont), 40, Color::Green), BaseElementInfo( "Center Top Label",    Point2f( Window.GetWidth() / 2.0f, 0                         ), CENTER_ALIGN, TOP_ALIGN    ) ) );
    labels.push_back( new Label( LabelInfo("Test Label 6", FontCache::Get(TestFont), 40, Color::Green), BaseElementInfo( "Center Bottom Label", Point2f( Window.GetWidth() / 2.0f, Window.GetHeight()        ), CENTER_ALIGN, BOTTOM_ALIGN ) ) );
    labels.push_back( new Label( LabelInfo("Test Label 7", FontCache::Get(TestFont), 40, Color::Green), BaseElementInfo( "Center Left Label",   Point2f( 0,                        Window.GetHeight() / 2.0f ), LEFT_ALIGN,   CENTER_ALIGN ) ) );
    labels.push_back( new Label( LabelInfo("Test Label 8", FontCache::Get(TestFont), 40, Color::Green), BaseElementInfo( "Center Right Label",  Point2f( Window.GetWidth(),        Window.GetHeight() / 2.0f ), RIGHT_ALIGN,  CENTER_ALIGN ) ) );

    labels.push_back( new Label( LabelInfo("Test Label 9", FontCache::Get(TestFont), 40, Color::Blue ), BaseElementInfo( "Center Label",        Point2f( Window.GetWidth() / 2.0f, Window.GetHeight() / 2.0f ), CENTER_ALIGN, CENTER_ALIGN ) ) );

    bool bool1 = false;
    checkBoxes.push_back( new CheckBox( CheckBoxInfo( bool1 ),
                                        PictureInfo( ImageCache::Get(CheckBoxImage), 2 ),
                                        ClickableElementInfo( 100, 100, Color::White, Color::Black, 5.0f ),
                                        BaseElementInfo( "Checkbox 1", Point2f(100, 100) ) ) );

    pictures.push_back( new Picture( PictureInfo( ImageCache::Get(TestPNGImage)                 ), BaseElementInfo( "Both Faces", Point2f( Window.GetWidth() / 2.0f,       Window.GetHeight() / 2.0f )                             ) ) );
    pictures.push_back( new Picture( PictureInfo( ImageCache::Get(TestPNGImage), 2, 0, 200, 200 ), BaseElementInfo( "Happy Face", Point2f( Window.GetWidth() / 2.0f - 200, Window.GetHeight() / 2.0f ), CENTER_ALIGN, TOP_ALIGN    ) ) );
    pictures.push_back( new Picture( PictureInfo( ImageCache::Get(TestPNGImage), 2, 1, 200, 200 ), BaseElementInfo( "Sad Face",   Point2f( Window.GetWidth() / 2.0f + 200, Window.GetHeight() / 2.0f ), CENTER_ALIGN, BOTTOM_ALIGN ) ) );

    actionButtons.push_back( new PictureButton( ActionButtonInfo( &TestFunction1 ),
                                                PictureInfo( ImageCache::Get(LeftArrowImage) ),
                                                ClickableElementInfo( 100, 100 ),
                                                BaseElementInfo( "Move Left Button", Point2f(250, 100) ) ) );

    actionButtons.push_back( new TextButton( ActionButtonInfo( &Blah ),
                                             LabelInfo( "Exit", FontCache::Get(TestFont), 40, Color::Blue ),
                                             ClickableElementInfo( 100, 100, Color::White, Color::Black, 1.0f ),
                                             BaseElementInfo( "Move Right Button", Point2f(400, 100) ) ) );

    vector<CheckBox*>::iterator c;
    vector<Label*>::iterator l;
    vector<Picture*>::iterator p;
    vector<ActionButton*>::iterator a;



    MouseState leftClick( Mouse::Left );
    MouseState rightClick( Mouse::Right );

    KeyboardState exit( Keyboard::Escape );

    while( !shouldExitGUI ) {
        // Clear out the event queue
        while( Window.PollEvent(Event) ) {

            // If the user tries to close the window
            if( Event.Type == Event::Closed) {

                // Exit the program
                shouldExitGUI = true;
            }
        }

        // Update the input
        leftClick.Update();                         // mouse left click
        rightClick.Update();                        // mouse right click
        MouseState::UpdateMouseCoords( Window );    // mouse coords

        exit.Update();                  // escape key

        if( exit.IsJustPressed() ) {
            shouldExitGUI = true;
        }



        // Lets you click and drag a shape
        if( leftClick.IsDown() ) {
            if( leftClick.IsJustPressed() ) {

            }
        }

        for( c = checkBoxes.begin(); c != checkBoxes.end(); ++c ) {
            (**c).Update( leftClick );
        }

        for( a = actionButtons.begin(); a != actionButtons.end(); ++a ) {
            (**a).Update( leftClick );
        }


        if( bool1 == true ) {
            labels.at(8)->SetText( "true" );
        } else {
            labels.at(8)->SetText( "false false false" );
        }

        for( p = pictures.begin(); p != pictures.end(); ++p ) {
            (**p).Draw( Window );
        }

        for( l = labels.begin(); l != labels.end(); ++l ) {
            (**l).Draw( Window );
        }

        for( c = checkBoxes.begin(); c != checkBoxes.end(); ++c ) {
            (**c).Draw( Window );
        }

        for( a = actionButtons.begin(); a != actionButtons.end(); ++a ) {
            (**a).Draw( Window );
        }

        // Update the screen
        Window.Display();

        Window.Clear( Color::Red );
    }
}

#endif
