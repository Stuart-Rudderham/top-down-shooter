#if 0

/*
 * This file implements the unit test function
 * for the particle engine
 *
 * Author:      Stuart Rudderham
 * Created:     February 5, 2012
 * Last Edited: February 7, 2012
 */

#include "Testing/UnitTests.hpp"

#include "ParticleEffect/ParticleEngine.hpp"

#include "UpgradesToSFML/Input.hpp"
#include "UpgradesToSFML/Printing.hpp"
#include "UpgradesToSFML/Drawing.hpp"

#include "Profile/TimeProfiler.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <cstdlib>
#include <vector>

using namespace std;
using namespace sf;


void TestParticles( sf::RenderWindow& Window ) {

    // Different options you can set to test different things
    bool drawDebugText = true;

    // Used to handle window events (e.g. clicking the close button)
    Event Event;

    bool shouldExit = false;

    MouseState leftClick( Mouse::Left );
    MouseState rightClick( Mouse::Right );

    vector< pair<KeyboardState, bool*> > toggleOptions;
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Space), &drawDebugText) );               // toggle drawing the debug text on the screen

    KeyboardState exit( Keyboard::Escape );

    const int particleTime = 10000;
    const int numParticles = 10000000;
    const int numToCreatePerClick = 100;

    const int graphWidth = 200;
    const int graphHeight = 100;

    float angleVariance = 10.0f;
    float angleOffset = 45.0f;
    float angleStepSize = 1.0f;

    ParticleEngine particles(numParticles);



    // Count frames per second
    TimeProfiler fps(30, graphWidth);

    while( !shouldExit ) {
        // Clear out the event queue
        while( Window.PollEvent(Event) ) {

            // If the user tries to close the window
            if( Event.Type == Event::Closed) {

                // Exit the program
                shouldExit = true;
            }
        }

        // Iterators for traversing the key listener and the projection axes
        vector< pair<KeyboardState, bool*> >::iterator k;

        // Update the input
        leftClick.Update();                         // mouse left click
        rightClick.Update();                        // mouse right click
        MouseState::UpdateMouseCoords( Window );    // mouse coords

        exit.Update();                  // escape key

        if( exit.IsJustPressed() ) {
            shouldExit = true;
        }

        // Update keys and toggle options if necessary
        for( k = toggleOptions.begin(); k != toggleOptions.end(); ++k ) {
            k->first.Update();

            if( k->first.IsJustPressed() ) {
                *(k->second) = !( *(k->second) );
            }
        }

        // Lets you click and drag a shape
        if( leftClick.IsDown() ) {
            //if( leftClick.IsJustPressed() ) {
                for( int i = 0; i < numToCreatePerClick; ++i ) {
                    //particles.CreateParticle( MouseState::GetPosition(), GetUnitVector( Vector2f(RandFloat(-10, 10), RandFloat(-10, 10))), Color( RandInt(0, 255), RandInt(0, 255), RandInt(0, 255) ), particleTime );
                    particles.CreateParticle( MouseState::GetPosition(), RandFloat(0   + angleOffset + angleVariance, 0   + angleOffset - angleVariance), RandFloat(50.0f, 500.0f), Color( RandInt(0, 255), RandInt(0, 255), RandInt(0, 255) ), particleTime );
                    particles.CreateParticle( MouseState::GetPosition(), RandFloat(90  + angleOffset + angleVariance, 90  + angleOffset - angleVariance), RandFloat(50.0f, 500.0f), Color( RandInt(0, 255), RandInt(0, 255), RandInt(0, 255) ), particleTime );
                    particles.CreateParticle( MouseState::GetPosition(), RandFloat(180 + angleOffset + angleVariance, 180 + angleOffset - angleVariance), RandFloat(50.0f, 500.0f), Color( RandInt(0, 255), RandInt(0, 255), RandInt(0, 255) ), particleTime );
                    particles.CreateParticle( MouseState::GetPosition(), RandFloat(270 + angleOffset + angleVariance, 270 + angleOffset - angleVariance), RandFloat(50.0f, 500.0f), Color( RandInt(0, 255), RandInt(0, 255), RandInt(0, 255) ), particleTime );
                }
            //}

            angleOffset += angleStepSize;

            while( angleOffset > 360 ) {
                angleOffset -= 360;
            }
        }

        particles.Update( 16, 16.0f / 1000.0f );         // time step is 16 milliseconds (approx 60 FPS)
        particles.Draw( Window );

        dbPrintToScreen( "Press Esc to exit" );
        dbPrintToScreen( "Press the space bar to turn debug text ", drawDebugText ? "off" : "on");


        if( drawDebugText == true ) {
            fps.PrintStatsToScreen();
            fps.GraphStatsOnScreen( Window, Point2f(Window.GetWidth() - graphWidth, Window.GetHeight() - graphHeight), graphWidth, graphHeight, 100 );

            dbDrawDebugText( Window );
        }

        // Update the screen
        Window.Display();

        if( drawDebugText == false ) {
            dbDrawDebugText( Window );
        }

        Window.Clear( Color::White );

        fps.Stop();
        fps.Start();

        //fps.PrintStatsToLog();
    }
}

#endif
