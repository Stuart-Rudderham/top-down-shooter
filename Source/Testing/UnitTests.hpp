/*
 * This header defines all the unit tests
 * for functions and classes in the game
 *
 * Author:      Stuart Rudderham
 * Created:     February 5, 2012
 * Last Edited: February 5, 2012
 */

#include <SFML/Graphics/RenderWindow.hpp>

#ifndef UNITTESTS_HPP_INCLUDED
#define UNITTESTS_HPP_INCLUDED

// Unit test for the shape geometry and collision detection/resolution
void TestShapes( sf::RenderWindow& Window, unsigned int numShapes );

// Unit test for the particle engine
void TestParticles( sf::RenderWindow& Window );

// Unit test for the caching system
void TestMediaCaching( sf::RenderWindow& Window );

// Unit test for the GUI system
void TestGUI( sf::RenderWindow& Window );

// Unit test for the Entity system
void TestEntities( sf::RenderWindow& GameWindow, sf::RenderWindow& DebugWindow );

// Unit test for the RayCasting system
void TestRayCasting( sf::RenderWindow& Window );

#endif // UNITTESTS_HPP_INCLUDED
