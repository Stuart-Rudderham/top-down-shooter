#if 0

/*
 * This file implements the unit test function
 * for shape geometry
 *
 * Author:      Stuart Rudderham
 * Created:     February 5, 2012
 * Last Edited: February 7, 2012
 */

#include "Testing/UnitTests.hpp"

#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"
#include "Math/Geometry/ConvexPolygon.hpp"
#include "Math/Geometry/CollisionDetection.hpp"

#include "UpgradesToSFML/Input.hpp"
#include "UpgradesToSFML/Printing.hpp"
#include "UpgradesToSFML/Drawing.hpp"

#include "Profile/TimeProfiler.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <cstdlib>
#include <vector>

using namespace std;
using namespace sf;

// Basic struct to represent a world object
// Has an outline, color, speed, and acceleration
struct BasicEntity {
    BaseShape* outline;
    AABB boundingBox;
    Circle boundingCircle;
    Color color;
    Vector2f speed;
    Vector2f acceleration;

    BasicEntity() {}
};

// Specific collision functions for each type of shape
void DetectAndResolveCollisions( BasicEntity* A, BasicEntity* B ) {

    Vector2f mtv1;
    Vector2f mtv2;
    Vector2f normal;

    // A 2D array of function pointers that are initialized to the different shape collision functions
    // The array is indexed with the type of each shape, so the correct function is always called
    static bool (*collisionFunction[NUM_RIGID_BODY][NUM_RIGID_BODY])(BaseShape*, BaseShape*, CollisionResponse, Vector2f&, Vector2f&, Vector2f&) = { {BoxBoxCollision,     BoxCircleCollision,     BoxPolygonCollision},
                                                                                                                                                     {CircleBoxCollision,  CircleCircleCollision,  CirclePolygonCollision},
                                                                                                                                                     {PolygonBoxCollision, PolygonCircleCollision, PolygonPolygonCollision} };

    // If the shapes are colliding
    if( collisionFunction[A->outline->GetType()][B->outline->GetType()](A->outline, B->outline, MOVE_BOTH, mtv1, mtv2, normal) ) {

        // Resolve the collision
        A->outline->MoveBy( mtv1 );
        A->boundingBox.MoveBy( mtv1 );
        A->boundingCircle.MoveBy( mtv1 );
        A->color = Color::Red;

        B->outline->MoveBy( mtv2 );
        B->boundingBox.MoveBy( mtv2 );
        B->boundingCircle.MoveBy( mtv2 );
        B->color = Color::Red;

        A->speed = ReflectVector( A->speed, normal );
        B->speed = ReflectVector( B->speed, normal );
    }
}

// Brute force
unsigned int BruteForce( vector<BasicEntity*>& objects, unsigned int numObjects ) {

    unsigned int numCollisionChecks = 0;

    for( unsigned int i = 0; i < numObjects; ++i ) {
        for( unsigned int j = i + 1; j < numObjects; ++j ) {

            if( BoundingBoxesColliding(objects[i]->boundingBox, objects[j]->boundingBox) ) {
            //if( BoundingCirclesColliding(objects[i].boundingCircle, objects[j].boundingCircle) ) {
                DetectAndResolveCollisions( objects[i], objects[j] );
            }

            numCollisionChecks++;
        }
    }

    return numCollisionChecks;
}


// Comparision function for Entities
bool EntityAABBCompare( BasicEntity* A, BasicEntity* B ) {
    AABB& ABox = A->boundingBox;
    AABB& BBox = B->boundingBox;

    return ABox.GetCenter().x - ABox.GetHalfWidth() <= BBox.GetCenter().x - BBox.GetHalfWidth();
}

// Sort and sweep
unsigned int SortAndSweep( vector<BasicEntity*>& objects, unsigned int numObjects ) {

    unsigned int numCollisionChecks = 0;

    // Holds the pairs of objects that have colliding bounding volumes
    vector< pair<BasicEntity*, BasicEntity*> > narrowSweep;

    // Sort by x-coord first
    sort( objects.begin(), objects.end(), EntityAABBCompare );

    for( unsigned int i = 0; i < numObjects; ++i ) {
        for( unsigned int j = i + 1; j < numObjects; ++j ) {

            numCollisionChecks++;

            // If they don't overlap on the x-axis we can stop, since the bounding boxes were ordered by x position
            if( BoundingBoxesOverlapXCoord(objects[i]->boundingBox, objects[j]->boundingBox) == false ) {
                break;
            }

            // If they overlap on the x-axis and y-axis then their bounding boxes are colliding
            if( BoundingBoxesOverlapYCoord(objects[i]->boundingBox, objects[j]->boundingBox) == true ) {

                narrowSweep.push_back( make_pair(objects[i], objects[j]) );
                objects[i]->color = Color::Yellow;
                objects[j]->color = Color::Yellow;
            }
        }
    }

    unsigned int narrowNum = narrowSweep.size();
    for( unsigned int i = 0; i < narrowNum; ++i ) {
        DetectAndResolveCollisions( narrowSweep[i].first, narrowSweep[i].second );
    }

    return numCollisionChecks;
}


void TestShapes( RenderWindow& Window, unsigned int numShapes ) {

    // Different options you can set to test different things
    bool drawShapes = true;
    bool drawBoundingVolumes = false;
    bool drawCenterDot = false;
    bool checkMouseInsideShape = false;
    bool projectShapes = false;
    bool moveShapes = true;
    bool drawClosestPoint = false;
    bool checkCollision = true;
    bool useOptimizedCollision = true;
    bool drawDebugText = true;
    bool changeColors = false;

    const int graphWidth = 400;
    const int graphHeight = 200;

    // Used to count how many times we check if two shapes are colliding
    unsigned int numCollisionChecks = 0;

    bool createBox = true;
    bool createCircle = true;
    bool createPoly = true;
    bool bigShapes = true;
    if( numShapes > 150 ) {
        bigShapes = false;
    }

    // Holds all the shapes, their movement direction, and color
    vector<BasicEntity*> objects;
    unsigned int numObjects;

    // Projection axis
    vector< pair<Point2f, Vector2f> > axis;
    axis.push_back( make_pair( Point2f(10, 0), GetUnitVector(Vector2f(0, 1)) ) );                                                   // vertical axis
    axis.push_back( make_pair( Point2f(0, 10), GetUnitVector(Vector2f(-1, 0)) ) );                                                  // horizontal axis
    axis.push_back( make_pair( Point2f(Window.GetWidth() - 10, Window.GetHeight() - 10), GetUnitVector(Vector2f(-2, -2)) ) );       // diagonal
    axis.push_back( make_pair( Point2f(Window.GetWidth() - 10, 10), GetUnitVector(Vector2f(-1, 1)) ) );                             // another diagonal

    const float BIG_FLOAT = 10000.0f;

    // Generate all the different kinds of shapes
    while( objects.size() < numShapes ) {

        if( createBox ) {
            // AABB
            unsigned int boxWidth = RandInt( 5, bigShapes ? 100 : 10 );
            unsigned int boxHeight = RandInt( 5, bigShapes ? 100 : 10 );
            float boxX = RandFloat( 0, Window.GetWidth() - boxWidth );
            float boxY = RandFloat( 0, Window.GetHeight() - boxHeight );
            BasicEntity* box = new BasicEntity;
            box->outline = new AABB( Point2f( boxX, boxY ), boxWidth, boxHeight );
            box->boundingBox = box->outline->GetBoundingBox();
            box->boundingCircle = box->outline->GetBoundingCircle();
            box->speed = RandFloat(1, 5) * GetUnitVector( Vector2f( RandFloat(-1, 1), RandFloat(-1, 1) ) );
            box->acceleration = Vector2f(0, 0);
            box->color = Color::Green;
            objects.push_back( box );
        }

        if( createCircle ) {
            // Circle
            unsigned int circleRadius = RandInt( 5, bigShapes ? 50 : 10 );
            float circleX = RandFloat( circleRadius, Window.GetWidth() - circleRadius );
            float circleY = RandFloat( circleRadius, Window.GetHeight() - circleRadius );
            BasicEntity* circle = new BasicEntity;
            circle->outline = new Circle( Point2f( circleX, circleY ), circleRadius );
            circle->boundingBox = circle->outline->GetBoundingBox();
            circle->boundingCircle = circle->outline->GetBoundingCircle();
            circle->speed = RandFloat(1, 5) * GetUnitVector( Vector2f( RandFloat(-1, 1), RandFloat(-1, 1) ) );
            circle->acceleration = Vector2f(0, 0);
            circle->color = Color::Green;
            objects.push_back( circle );
        }

        if( createPoly ) {
            // Convex polygons
            unsigned int polygonRadius = RandInt( 5, bigShapes ? 100 : 10 );
            float polygonX = RandFloat( polygonRadius, Window.GetWidth() - polygonRadius );
            float polygonY = RandFloat( polygonRadius, Window.GetHeight() - polygonRadius );
            BasicEntity* polygon = new BasicEntity;
            polygon->outline = new ConvexPolygon( RandInt(3, 8), Point2f(polygonX, polygonY), polygonRadius );
            polygon->boundingBox = polygon->outline->GetBoundingBox();
            polygon->boundingCircle = polygon->outline->GetBoundingCircle();
            polygon->speed = RandFloat(1, 5) * GetUnitVector( Vector2f( RandFloat(-1, 1), RandFloat(-1, 1) ) );
            polygon->acceleration = Vector2f(0, 0);
            polygon->color = Color::Green;
            objects.push_back( polygon );
        }
    }

    numObjects = objects.size();

    // A point to the shape that the user is currently controlling
    BasicEntity* userControlledShape = nullptr;
    Vector2f shapeOffsetFromMouse(0, 0);

    // Used to handle window events (e.g. clicking the close button)
    Event Event;

    bool shouldExit = false;

    MouseState leftClick( Mouse::Left );
    MouseState rightClick( Mouse::Right );

    vector< pair<KeyboardState, bool*> > toggleOptions;
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num0), &drawShapes) );                   // toggle drawing the shapes
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num1), &drawBoundingVolumes) );          // toggle drawing bounding volumes
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num2), &drawCenterDot) );                // toggle drawing the center dot
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num3), &checkMouseInsideShape) );        // toggle checking if the mouse is inside the shape
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num4), &projectShapes) );                // toggle projecting shapes onto the axis
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num5), &moveShapes) );                   // toggle moving the shapes
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num6), &drawClosestPoint) );             // toggle drawing the closest point to the mouse
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num7), &checkCollision) );               // toggle checking for collisions
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num8), &useOptimizedCollision) );        // toggle using brute force checking for collisions
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num9), &changeColors) );                 // toggle color changes for colliding shapes
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Space), &drawDebugText) );               // toggle drawing the debug text on the screen




    KeyboardState exit( Keyboard::Escape );

    // Count frames per second
    TimeProfiler fps(30, 150);

    while( !shouldExit ) {
        // Clear out the event queue
        while( Window.PollEvent(Event) ) {

            // If the user tries to close the window
            if( Event.Type == Event::Closed) {

                // Exit the program
                shouldExit = true;
            }
        }

        // Iterators for traversing the key listener and the projection axes
        vector< pair<KeyboardState, bool*> >::iterator k;
        vector< pair<Point2f, Vector2f> >::iterator a;

        // Update the input
        leftClick.Update();                         // mouse left click
        rightClick.Update();                        // mouse right click
        MouseState::UpdateMouseCoords( Window );    // mouse coords

        exit.Update();                  // escape key

        if( exit.IsJustPressed() ) {
            shouldExit = true;
        }

        // Update keys and toggle options if necessary
        for( k = toggleOptions.begin(); k != toggleOptions.end(); ++k ) {
            k->first.Update();

            if( k->first.IsJustPressed() ) {
                *(k->second) = !( *(k->second) );
            }
        }

        // Lets you click and drag a shape
        if( leftClick.IsDown() ) {
            if( leftClick.IsJustPressed() ) {
                // Check to see if they clicked on a shape
                // Check in reverse so the shape that is drawn on top is chosen first
                for( int i = numObjects - 1; i >= 0; i-- ) {
                    if( objects[i]->outline->IsPointInside( MouseState::GetPosition() ) ) {

                        // If so then control it
                        userControlledShape = objects[i];
                        shapeOffsetFromMouse = MouseState::GetPosition() - userControlledShape->outline->GetCenter();
                        break;
                    }
                }
            }

            // Control the shape if the user is clicking on it
            if( userControlledShape != nullptr ) {
                Vector2f displacement = MouseState::GetPosition() - userControlledShape->outline->GetCenter();

                userControlledShape->outline->MoveBy( displacement - shapeOffsetFromMouse );
                userControlledShape->boundingBox.MoveBy( displacement - shapeOffsetFromMouse );
                userControlledShape->boundingCircle.MoveBy( displacement - shapeOffsetFromMouse );
            }
        } else {
            userControlledShape = nullptr;
        }


        // Draw the projection axis
        if( projectShapes ) {
            for( a = axis.begin(); a != axis.end(); ++a ) {
                Point2f startingPoint = a->first;
                Vector2f direction = a->second;

                DrawVector( Window, startingPoint, direction * BIG_FLOAT, Color::Black, false);
                DrawVector( Window, startingPoint, direction * -BIG_FLOAT, Color::Black, false);
            }
        }

        // Loop through the shapes, doing things if necesary
        for( unsigned int i = 0; i < numObjects; ++i ) {

            // Reset the color
            objects[i]->color = Color::Green;

            // Move the shapes, if necessary
            if( moveShapes ) {
                float bezel = 500;
                float width = Window.GetWidth();
                float height = Window.GetHeight();

                objects[i]->outline->MoveBy( objects[i]->speed );
                objects[i]->boundingBox.MoveBy( objects[i]->speed );
                objects[i]->boundingCircle.MoveBy( objects[i]->speed );

                if( objects[i]->outline->GetType() == SHAPE_TYPE_CONVEX_POLYGON ) {
                    static_cast<ConvexPolygon*>(objects[i]->outline)->Rotate( 1 );
                    objects[i]->boundingBox = objects[i]->outline->GetBoundingBox();
                }

                Vector2f center = objects[i]->outline->GetCenter();

                if( center.x < -bezel ) {
                    objects[i]->outline->MoveBy( Vector2f(width + bezel + bezel, 0) );
                    objects[i]->boundingBox.MoveBy( Vector2f(width + bezel + bezel, 0) );
                    objects[i]->boundingCircle.MoveBy( Vector2f(width + bezel + bezel, 0) );

                } else if( center.x > width + bezel ) {
                    objects[i]->outline->MoveBy( Vector2f( -(width + bezel + bezel), 0) );
                    objects[i]->boundingBox.MoveBy( Vector2f( -(width + bezel + bezel), 0) );
                    objects[i]->boundingCircle.MoveBy( Vector2f( -(width + bezel + bezel), 0) );
                }

                if( center.y < -bezel ) {
                    objects[i]->outline->MoveBy( Vector2f( 0, height + bezel + bezel ) );
                    objects[i]->boundingBox.MoveBy( Vector2f( 0, height + bezel + bezel ) );
                    objects[i]->boundingCircle.MoveBy( Vector2f( 0, height + bezel + bezel ) );

                } else if( center.y > height + bezel ) {
                    objects[i]->outline->MoveBy( Vector2f( 0, -(height + bezel + bezel) ) );
                    objects[i]->boundingBox.MoveBy( Vector2f( 0, -(height + bezel + bezel) ) );
                    objects[i]->boundingCircle.MoveBy( Vector2f( 0, -(height + bezel + bezel) ) );

                }
            }

            // Check if the mouse is inside the shape, if necessary
            if( checkMouseInsideShape ) {
                if( objects[i]->outline->IsPointInside( MouseState::GetPosition() ) ) {
                    objects[i]->color = Color::Yellow;
                }
            }
        }

        // Detect and resolve any collisions, if necessary
        if( checkCollision ) {
            if( useOptimizedCollision == true ) {
                numCollisionChecks = SortAndSweep( objects, numObjects );
            } else {
                numCollisionChecks = BruteForce( objects, numObjects );
            }
        }

        // Draw the shapes
        for( unsigned int i = 0; i < numObjects; ++i ) {

            if( drawShapes ) {
                objects[i]->outline->Draw( Window, changeColors ? objects[i]->color : Color::Green );
            }

            //objects[i]->boundingBox.Draw( Window, Color::Blue, Color::Black );

            if( drawBoundingVolumes ) {
                objects[i]->boundingBox.Draw( Window, Color::Transparent, Color::Black );
                objects[i]->boundingCircle.Draw( Window, Color::Transparent, Color::Black );
            }

            if( drawCenterDot ){
                objects[i]->outline->DrawCenterDot( Window );
            }

            if( drawClosestPoint ) {
                DrawDot( Window, objects[i]->outline->GetClosestPointOnShape( MouseState::GetPosition() ), 5.0f, Color::Red );
                DrawDot( Window, objects[i]->outline->GetClosestPointOnOutline( MouseState::GetPosition() ), 5.0f, Color::Black );
            }

            // Project the shape onto the axis
            if( projectShapes ) {
                for( a = axis.begin(); a != axis.end(); ++a ) {
                    Pair projection = objects[i]->outline->Project( a->second );

                    Vector2f normal = GetUnitClockwiseNormal( a->second );

                    // Thanks Saad
                    float pointProjection = DotProduct( a->first, normal );

                    Point2f p1 = (normal * pointProjection) + (a->second * projection.x);
                    Point2f p2 = (normal * pointProjection) + (a->second * projection.y);

                    DrawDot( Window, p1, 5.0f );
                    DrawDot( Window, p2, 5.0f );

                    DrawVector( Window, p1, normal * BIG_FLOAT );
                    DrawVector( Window, p1, normal * -BIG_FLOAT );

                    DrawVector( Window, p2, normal * BIG_FLOAT );
                    DrawVector( Window, p2, normal * -BIG_FLOAT );
                }
            }
        }

        dbPrintToScreen( "Press Esc to exit" );
        dbPrintToScreen( "Press the space bar to turn debug text ", drawDebugText ? "off" : "on");
        dbPrintToScreen( "Press the 1 key to turn bounding volumes ", drawBoundingVolumes ? "off" : "on");
        dbPrintToScreen( "Press the 2 key to turn the center dot ", drawCenterDot ? "off" : "on");
        dbPrintToScreen( "Press the 3 key to turn mouse highlighting ", checkMouseInsideShape ? "off" : "on");
        dbPrintToScreen( "Press the 4 key to turn projections ", projectShapes ? "off" : "on");
        dbPrintToScreen( "Press the 5 key to turn movement ", moveShapes ? "off" : "on");
        dbPrintToScreen( "Press the 6 key to turn closest point ", drawClosestPoint ? "off" : "on");
        dbPrintToScreen( "Press the 7 key to turn collision checking ", checkCollision ? "off" : "on");
        dbPrintToScreen( "Press the 8 key to turn collision optimization ", useOptimizedCollision ? "off" : "on");
        dbPrintToScreen( "Press the 9 key to turn color changes ", changeColors ? "off" : "on");
        dbPrintToScreen( "Press the 9 key to turn drawing shapes ", drawShapes ? "off" : "on");

        dbPrintToScreen( "Number of shapes: ", numObjects );

        if( checkCollision == true ) {
            dbPrintToScreen( "Number of checks: ", numCollisionChecks );
        }




        if( drawDebugText == true ) {
            fps.PrintStatsToScreen();
            fps.GraphStatsOnScreen( Window, Point2f(Window.GetWidth() - graphWidth, Window.GetHeight() - graphHeight), graphWidth, graphHeight, 100 );

            dbDrawDebugText( Window );
        }

        // Update the screen
        Window.Display();

        if( drawDebugText == false ) {
            dbDrawDebugText( Window );
        }

        Window.Clear( Color::White );

        fps.Stop();
        fps.Start();

        //fps.PrintStatsToLog();
    }
}

#endif
