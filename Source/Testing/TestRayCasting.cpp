#if 0

/*
 * This file implements the unit test function
 * for the RayCasting system
 *
 * Author:      Stuart Rudderham
 * Created:     March 30, 2012
 * Last Edited: March 30, 2012
 */

#include "Testing/UnitTests.hpp"

#include "Interfaces/GameObject.hpp"
#include "GameObjects/Characters/Soldier.hpp"
#include "GameObjects/Characters/Zombie.hpp"
#include "GameObjects/MapObjects/StaticGeo.hpp"

#include "Managers/Collision/CollisionManager.hpp"
#include "Map/Map.hpp"

#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"
#include "Math/Geometry/ConvexPolygon.hpp"
#include "Math/Geometry/LineSegment.hpp"
#include "Math/Geometry/CollisionDetection.hpp"

#include "UpgradesToSFML/Input.hpp"
#include "UpgradesToSFML/Printing.hpp"
#include "UpgradesToSFML/Drawing.hpp"

#include "Profile/TimeProfiler.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <cstdlib>
#include <vector>
#include <set>
using namespace std;
using namespace sf;


void TestRayCasting( sf::RenderWindow& Window ) {

    // Different options you can set to test different things
    bool drawDebugText = true;
    bool drawShapes = true;

    // Used to handle window events (e.g. clicking the close button)
    Event Event;

    const int graphWidth = 400;
    const int graphHeight = 200;

    bool shouldExit = false;

    MouseState leftClick( Mouse::Left );
    MouseState rightClick( Mouse::Right );

    vector< pair<KeyboardState, bool*> > toggleOptions;
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Space), &drawDebugText) );               // toggle drawing the debug text on the screen
    toggleOptions.push_back( make_pair(KeyboardState(Keyboard::Num0), &drawShapes) );                   // toggle drawing the shapes

    KeyboardState exit( Keyboard::Escape );

    // Count frames per second
    TimeProfiler fps(30, graphWidth);

//    Map testMap( "Test Map" );

    LineSegment test( Point2f(0, 0), Point2f(Window.GetWidth(), Window.GetHeight()) );
    LineSegment test1( Point2f(200, 100), Vector2f(100, 100), 1.0f );

    const unsigned int numShapes = 5;
    vector<BaseShape*> shapes;

    for( unsigned int i = 0; i < numShapes; ++i ) {

        unsigned int boxWidth = RandInt( 5, 100 );
        unsigned int boxHeight = RandInt( 5, 100 );
        float boxX = RandFloat( 0, Window.GetWidth() - boxWidth );
        float boxY = RandFloat( 0, Window.GetHeight() - boxHeight );
        shapes.push_back( new AABB( Point2f(boxX, boxY), boxWidth, boxHeight ) );

        unsigned int polygonRadius = RandInt( 5, 100 );
        float polygonX = RandFloat( polygonRadius, Window.GetWidth() - polygonRadius );
        float polygonY = RandFloat( polygonRadius, Window.GetHeight() - polygonRadius );
        shapes.push_back( new ConvexPolygon( RandInt(3, 8), Point2f(polygonX, polygonY), polygonRadius ) );

        unsigned int circleRadius = RandInt( 5, 100 );
        float circleX = RandFloat( circleRadius, Window.GetWidth() - circleRadius );
        float circleY = RandFloat( circleRadius, Window.GetHeight() - circleRadius );
        shapes.push_back( new Circle( Point2f(circleX, circleY), circleRadius ) );
    }

    while( !shouldExit ) {
        // Clear out the event queue
        while( Window.PollEvent(Event) ) {

            // If the user tries to close the window
            if( Event.Type == Event::Closed) {

                // Exit the program
                shouldExit = true;
            }
        }

        // Iterators for traversing the key listener and the projection axes
        vector< pair<KeyboardState, bool*> >::iterator k;

        // Update the input
        leftClick.Update();                         // mouse left click
        rightClick.Update();                        // mouse right click
        MouseState::UpdateMouseCoords( Window );    // mouse coords

        exit.Update();                  // escape key

        if( exit.IsJustPressed() ) {
            shouldExit = true;
        }

        // Update keys and toggle options if necessary
        for( k = toggleOptions.begin(); k != toggleOptions.end(); ++k ) {
            k->first.Update();

            if( k->first.IsJustPressed() ) {
                *(k->second) = !( *(k->second) );
            }
        }

        // Lets you click and drag a shape
        if( leftClick.IsDown() ) {
            //if( leftClick.IsJustPressed() ) {
                test.SetStartPoint( MouseState::GetPosition() );
            //}
        }

        if( rightClick.IsDown() ) {
            //if( rightClick.IsJustPressed() ) {
                test.SetEndPoint( MouseState::GetPosition() );
            //}
        }

        if( drawShapes ) {
            //testMap.Draw( Window );
            test.Draw( Window, Color::Black );


             for( unsigned int i = 0; i < numShapes; ++i ) {
                shapes[i]->Draw( Window );
            }
        }


        Point2f intersectionPoint;
        Vector2f normal;

        for( unsigned int i = 0; i < numShapes; ++i ) {
            if( ( shapes[i]->GetType() == SHAPE_TYPE_AABB           && CastRayAtAABB         ( &test, shapes[i], intersectionPoint, normal ) ) ||
                ( shapes[i]->GetType() == SHAPE_TYPE_CIRCLE         && CastRayAtCircle       ( &test, shapes[i], intersectionPoint, normal ) ) ||
                ( shapes[i]->GetType() == SHAPE_TYPE_CONVEX_POLYGON && CastRayAtConvexPolygon( &test, shapes[i], intersectionPoint, normal ) ) ) {

                if( drawShapes ) {
                    DrawDot( Window, intersectionPoint, 5, Color::Black );
                    DrawDot( Window, normal, 5, Color::Black );
                    DrawVector( Window, intersectionPoint, normal * 50.0f );
                }
            }
        }
/*

*/
        if( drawDebugText == true ) {
            dbPrintToScreen( "" );
            fps.PrintStatsToScreen();
            fps.GraphStatsOnScreen( Window, Point2f(Window.GetWidth() - graphWidth, Window.GetHeight() - graphHeight), graphWidth, graphHeight, 100 );

            dbDrawDebugText( Window );
        }

        // Update the screen
        Window.Display();

        if( drawDebugText == false ) {
            dbDrawDebugText( Window );
        }

        Window.Clear( Color::White );

        fps.Stop();
        fps.Start();

        dbPrintToScreen( "Press Esc to exit" );
        dbPrintToScreen( "Press the space bar to turn debug text ", drawDebugText ? "off" : "on");
        dbPrintToScreen( "" );

        //fps.PrintStatsToLog();
    }
}

#endif
