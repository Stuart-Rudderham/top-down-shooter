/*
 * This file implements the unit test function
 * for the Entity system
 *
 * Author:      Stuart Rudderham
 * Created:     February 5, 2012
 * Last Edited: January 12, 2013
 */

#include "Testing/UnitTests.hpp"

#include "Interfaces/GameObject.hpp"
#include "GameObjects/Characters/Soldier.hpp"
#include "GameObjects/Characters/Zombie.hpp"
#include "GameObjects/MapObjects/StaticGeo.hpp"
#include "GameObjects/MapObjects/SpawnPoint.hpp"
#include "GameObjects/MapObjects/Scenery.hpp"
#include "GameObjects/Projectiles/SlowBullet.hpp"
#include "GameObjects/AI/FieldOfView.hpp"
#include "GameObjects/AI/LineOfSight.hpp"

#include "Managers/Collision/CollisionManager.hpp"
#include "Managers/Drawing/DrawingManager.hpp"
#include "Managers/Movement/MovementManager.hpp"
#include "Managers/Intelligence/IntelligenceManager.hpp"
#include "Managers/Respawning/RespawnManager.hpp"
#include "Managers/Deletion/DeletionManager.hpp"
#include "Managers/Logging/LoggingManager.hpp"
#include "Managers/Input/InputManager.hpp"
#include "Managers/Sound/SoundManager.hpp"

#include "GameObjects/Characters/Controllers/Human/ControlScheme.hpp"

#include "Map/Map.hpp"

#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"
#include "Math/Geometry/ConvexPolygon.hpp"
#include "Math/Geometry/CollisionDetection.hpp"

#include "UpgradesToSFML/Input.hpp"
#include "UpgradesToSFML/Printing.hpp"
#include "UpgradesToSFML/Drawing.hpp"

#include "Profile/TimeProfiler.hpp"

#include "GameObjects/Characters/HUD/HUD.hpp"
#include "GameObjects/Characters/HUD/MiniMap.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <cstdlib>
#include <vector>
#include <set>
using namespace std;
using namespace sf;


void TestEntities( RenderWindow& GameWindow, sf::RenderWindow& DebugWindow ) {

    // Used to handle window events (e.g. clicking the close button)
    Event event;

    // Control variables for game state
    bool paused = false;
    bool shouldExit = false;

    // Setup the input
    const KeyboardState* toggleControllerType   = InputManager::Get().ListenForKey( Keyboard::Num1 );
    const KeyboardState* toggleCollisions       = InputManager::Get().ListenForKey( Keyboard::Num2 );
    const KeyboardState* toggleMovement         = InputManager::Get().ListenForKey( Keyboard::Num3 );
    const KeyboardState* toggleDrawing          = InputManager::Get().ListenForKey( Keyboard::Num4 );
    const KeyboardState* toggleIntelligence     = InputManager::Get().ListenForKey( Keyboard::Num5 );
    const KeyboardState* manualDelay            = InputManager::Get().ListenForKey( Keyboard::Space );

    bool isBot          = false;
    bool canCollide     = true;
    bool canMove        = true;
    bool shouldDraw     = true;
    bool canThink       = true;

    ControlScheme player1{ InputManager::Get().ListenForKey   ( Keyboard::W ),
                           InputManager::Get().ListenForKey   ( Keyboard::S ),
                           InputManager::Get().ListenForKey   ( Keyboard::A ),
                           InputManager::Get().ListenForKey   ( Keyboard::D ),
                           InputManager::Get().ListenForButton( Mouse::Left ),
                           InputManager::Get().ListenForKey   ( Keyboard::R ),
                           InputManager::Get().ListenForKey   ( Keyboard::E ) };

    // Create the map
    Map testMap;
    testMap.Load( TestMap );

    // Create the characters
    Character::MainCharacter = Soldier::Create( Point2f( 100, 100 ) );
    Character::MainCharacter->MakeHuman( player1 );
    Character::MainCharacter->MoveTo( Point2f(800, -100) );
    HUD hud;

    for( unsigned int i = 0; i < Game::NumCharacters; ) {

        // If we enable the stress test then spawn all Characters in the same place
        // otherwise choose a random spot on the map
        Point2f spawnPosition = Physics::EnableStressTest ? Point2f( testMap.GetWidth() - 200                , testMap.GetHeight() - 200                 ) :
                                                            Point2f( RandInt(100,  testMap.GetWidth() - 100 ), RandInt(100,  testMap.GetHeight() - 100 ) );
		if( Game::SpawnSoldiers ) {
		    Soldier* soldier = Soldier::Create( spawnPosition );

		    if( Game::MakeSoldiersZombies ) {
                soldier->SetFaction( Faction::Zombie );
		    }
            i++;
		}

        if( Game::SpawnZombies ) {
            Zombie* zombie = Zombie::Create( spawnPosition );
            (void)zombie;
            i++;
        }
    }

    // Setup the camera
    View cameraView( Point2f(0, 0), Game::Camera::ViewportSize );
    GameWindow.setView( cameraView );

    // Start the background music
    SoundManager::Get().PlayBackgroundMusic( MenuSound );

    // Start the global game timer
    Game::GameTimer.restart();

    // GAME LOOP
    while( !shouldExit ) {

        Profile::GlobalFramerate.Start();

        // Clear out the event queue
        while( GameWindow.pollEvent(event) ) {
            switch( event.type ) {
                case Event::Closed:          shouldExit = true;                                                             break;
                case Event::KeyPressed:      if( event.key.code == Keyboard::Escape ) { shouldExit = true; }                break;
                case Event::MouseWheelMoved: cameraView.zoom( 1.0f + (-event.mouseWheel.delta * Game::Camera::ZoomSpeed) ); break;
                case Event::LostFocus:       paused = true;  SoundManager::Get().PauseEverything();                         break;
                case Event::GainedFocus:     paused = false; SoundManager::Get().UnpauseEverything();                       break;
                default: /* Ignore all other events */                                                                      break;
            }
        }

        // We only really care about Close events for the debugging window
        while( DebugWindow.pollEvent(event) ) {
            switch( event.type ) {
                case Event::Closed:          shouldExit = true;                                                             break;
                case Event::KeyPressed:      if( event.key.code == Keyboard::Escape ) { shouldExit = true; }                break;
                default: /* Ignore all other events */                                                                      break;
            }
        }

        // If the game is paused, wait for a bit and then
        // check if the window has regained focus
        // Kinda gross, but a simple way to not busy-wait
        if( paused ) {
            sf::sleep( sf::milliseconds(10) );
            continue;
        }

        // Update the inputs
        InputManager::Get().UpdateInputs( GameWindow );

        // Debugging actions
        if( Character::MainCharacter != nullptr ) {
            if( toggleControllerType->IsJustPressed() ) {
                if( isBot == true ) {
                    Character::MainCharacter->MakeHuman( player1 );
                } else {
                    Character::MainCharacter->MakeBot();
                }

                isBot = !isBot;
            }

            if( toggleCollisions->IsJustPressed() ) {
                if( canCollide == true ) {
                    Character::MainCharacter->DisableCollisionChecking();
                } else {
                    Character::MainCharacter->EnableCollisionChecking();
                }

                canCollide = !canCollide;
            }

            if( toggleMovement->IsJustPressed() ) {
                if( canMove == true ) {
                    Character::MainCharacter->DisableLocomotion();
                } else {
                    Character::MainCharacter->EnableLocomotion();
                }

                canMove = !canMove;
            }

            if( toggleDrawing->IsJustPressed() ) {
                if( shouldDraw == true ) {
                    Character::MainCharacter->DisableDrawing();
                } else {
                    Character::MainCharacter->EnableDrawing();
                }

                shouldDraw = !shouldDraw;
            }

            if( toggleIntelligence->IsJustPressed() ) {
                if( canThink == true ) {
                    Character::MainCharacter->DisableController();
                } else {
                    Character::MainCharacter->EnableController();
                }

                canThink = !canThink;
            }
        }

        if( manualDelay->IsDown() ) {
            sf::sleep( Game::IntentionalDelay );
        }

        // Do collision checked to determine what other objects each object can see
        Profile::FieldOfViewCollision.Start();
            CollisionManager::Get().ScanFieldOfViews();
        Profile::FieldOfViewCollision.Stop();

        Profile::LineOfSightCollision.Start();
            CollisionManager::Get().ScanLineOfSights();
        Profile::LineOfSightCollision.Stop();

        // Use that information, along with user input, to update the internal logic of each object
        Profile::ControllerUpdate.Start();
            IntelligenceManager::Get().UpdateLogic();
        Profile::ControllerUpdate.Stop();

        Profile::PhysicsUpdate.Start();

            // Update the movement of each object
            MovementManager::Get().FrameUpdate( Physics::DeltaT );

            for( unsigned int i = 0; i < Physics::IterationsPerFrame; ++i ) {
                MovementManager::Get().TimeSliceUpdate( Physics::DeltaT / Physics::IterationsPerFrame );
                CollisionManager::Get().DetectAndResolveCollisions();
            }

        Profile::PhysicsUpdate.Stop();

        // Find all characters on the minimap
        hud.UpdateMiniMap();

        // Move the camera to the main player
        //MainCharacter.SnapCamera( cameraView, GameWindow );

        if( Character::MainCharacter != nullptr ) {
            cameraView.setCenter( Character::MainCharacter->GetPosition() );
            GameWindow.setView( cameraView );
        }

        Profile::Drawing.Start();

            // Clear the screen
            GameWindow.clear( Color::White );
            DebugWindow.clear( Color::White );

            // Draw the game
            DrawingManager::Get().Draw( GameWindow );

            if( Drawing::DrawCollisionOutline ) {
                CollisionManager::Get().DrawCollisionOutlines( GameWindow );
            }

            // Draw the HUD
            hud.Draw<Soldier>( GameWindow );

            // Print collision stats to screen
            CollisionManager::Get().PrintAndClearStats();

            // Draw logging/profiling stats in milliseconds
            dbPrintToScreen( "FOV Collision - ",     Profile::FieldOfViewCollision.GetMostRecentTime() / 1000.0f );
            dbPrintToScreen( "LOS Collision - ",     Profile::LineOfSightCollision.GetMostRecentTime() / 1000.0f );
            dbPrintToScreen( "Controller Update - ", Profile::ControllerUpdate.GetMostRecentTime()     / 1000.0f );
            dbPrintToScreen( "Physics Update - ",    Profile::PhysicsUpdate.GetMostRecentTime()        / 1000.0f );
            dbPrintToScreen( "Drawing Time - ",      Profile::Drawing.GetMostRecentTime()              / 1000.0f );
            dbPrintToScreen( "Display Time - ",      Profile::Displaying.GetMostRecentTime()           / 1000.0f );
            dbPrintToScreen( "Object Cleanup - ",    Profile::Cleanup.GetMostRecentTime()              / 1000.0f );
            Profile::GlobalFramerate.PrintStatsToScreen();

            dbPrintToScreen( "" );
            dbPrintToScreen( "Press Esc to exit" );
            dbPrintToScreen( "" );

            dbDrawDebugText( DebugWindow );

            LoggingManager::Get().DrawGraphs( DebugWindow, Point2f( DebugOptions::TextSpaceWidth, 0 ) );

        Profile::Drawing.Stop();

        // Update the screen
        Profile::Displaying.Start();
            GameWindow.display();
            DebugWindow.display();
        Profile::Displaying.Stop();

        Profile::Cleanup.Start();

            // Update all the logs
            LoggingManager::Get().UpdateLoggableObjects();

            // Mark any objects whos time is up for deletion
            DeletionManager::Get().DeleteObjects();

            // Have to change where the camera is pointing if main character dies
            if( Character::MainCharacter != nullptr && Character::MainCharacter->ShouldDelete() ) {
                Character::MainCharacter = nullptr;
            }

            // Remove all marked objects from the managers
            CollisionManager::Get().Cleanup();
            MovementManager::Get().Cleanup();
            DrawingManager::Get().Cleanup();
            IntelligenceManager::Get().Cleanup();
            DeletionManager::Get().Cleanup();

            // Destroy any marked objects
            //
            // Should destroy marked FieldOfViews first, since when Characters
            // are destroyed they mark their FOV, so if we destroyed Characters
            // first the FieldOfView would be marked and then immediately deleted,
            // not giving the Managers time to remove it.
            //
            // The solution to this problem is to mark all child GameObjects as
            // well when the parent GameObject is marked. Since this problem is
            // only for FieldOfViews right now we'll settle for this quick and
            // dirty hack
            FieldOfView::DestroyAllMarked();
            Soldier::DestroyAllMarked();
            Zombie::DestroyAllMarked();
            StaticGeo::DestroyAllMarked();
            SpawnPoint::DestroyAllMarked();
            Scenery::DestroyAllMarked();
            SlowBullet::DestroyAllMarked();
            LineOfSight::DestroyAllMarked();

            // Respawn any waiting objects
            RespawnManager::Get().RespawnObjects();

            // Record the frame time and update the global time
            Game::DeltaT = Game::GameTimer.getElapsedTime() - Game::CurrentTime;
            Clamp( sf::milliseconds(1), Game::DeltaT, sf::milliseconds(50) );
            Game::CurrentTime += Game::DeltaT;

            // Can either have a fixed or variable timestep
            if( Physics::UseFixedTimeStep ) {
                Physics::DeltaT = Physics::FixedDeltaT;
            } else {
                Physics::DeltaT = Game::DeltaT.asSeconds();

                if( Physics::ClampDeltaT ) {
                    Clamp( Physics::MinDeltaT, Physics::DeltaT, Physics::MaxDeltaT );
                }
            }

        Profile::Cleanup.Stop();

        Profile::GlobalFramerate.Stop();

        dbPrintToScreen( "Game::DeltaT    - ", Game::DeltaT.asMilliseconds() );
        dbPrintToScreen( "Physics::DeltaT - ", Physics::DeltaT );
    }

    // Clear all the managers
    CollisionManager::Get().RemoveAllGameObjects();
    MovementManager::Get().RemoveAllGameObjects();
    DrawingManager::Get().RemoveAllGameObjects();
    IntelligenceManager::Get().RemoveAllGameObjects();
    RespawnManager::Get().RemoveAllGameObjects();
    DeletionManager::Get().RemoveAllGameObjects();

    // Destroy all the GameObjects
    //
    // Unlike above, should destroy Characters before FieldOfViews in this
    // case. Since Character marks the FOV for deletion when destroyed if
    // we deleted FieldOfViews first when the Character tried to mark the FOV
    // it would be touching an already destructed object, which is bad.
    Soldier::DestroyAll();
    Zombie::DestroyAll();
    StaticGeo::DestroyAll();
    SpawnPoint::DestroyAll();
    Scenery::DestroyAll();
    SlowBullet::DestroyAll();
    FieldOfView::DestroyAll();
    LineOfSight::DestroyAll();
}
