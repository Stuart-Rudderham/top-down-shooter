#if 0

/*
 * This file implements the unit test function
 * for media caching
 *
 * Author:      Stuart Rudderham
 * Created:     March 3, 2012
 * Last Edited: March 3, 2012
 */

#include "Testing/UnitTests.hpp"

#include "MediaCache/MediaCache.hpp"

#include "UpgradesToSFML/Input.hpp"
#include "UpgradesToSFML/Printing.hpp"
#include "UpgradesToSFML/Drawing.hpp"

#include "Math/MathFunctions.hpp"
#include "Profile/TimeProfiler.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <cstdlib>
#include <vector>
#include <sstream>

using namespace std;
using namespace sf;

void TestMediaCaching( sf::RenderWindow& Window ) {

    // Used to handle window events (e.g. clicking the close button)
    Event Event;

    bool shouldExit = false;

    MouseState leftClick( Mouse::Left );
    MouseState rightClick( Mouse::Right );

    KeyboardState exit( Keyboard::Escape );

    // Set up objects to draw/play the media
    vector<Sprite*> images;
    vector<Sprite*>::iterator i;

    vector<Sound*> sounds;
    vector<Sound*>::iterator s;

    vector<Text*> texts;
    vector<Text*>::iterator t;

    const int numInstances = 5;
    const float bezel = 200.0f;

    // Images
    for( int n = 0; n < numInstances; ++n ) {
        images.push_back( new Sprite( ImageCache::Get(TestBMPImage) ) );
        images.push_back( new Sprite( ImageCache::Get(TestPNGImage) ) );
    }

    // Sounds
    for( int n = 0; n < numInstances; ++n ) {
        sounds.push_back( new Sound( SoundCache::Get( TestWAVSound ) ) );
    }

    // Fonts
    for( int n = 0; n < numInstances; ++n ) {
        texts.push_back( new Text( "", FontCache::Get( TestFont ), RandInt(10, 50) ) );
    }

    // Randomize everything
    for( i = images.begin(); i != images.end(); ++i ) {
        (**i).SetPosition( RandFloat(bezel, Window.GetWidth() - bezel), RandFloat(bezel, Window.GetHeight() - bezel) );
        ResizeSprite( (**i), RandFloat(25, 200), RandFloat(25, 200) );
        (**i).SetColor( Color( RandInt(0, 255), RandInt(0, 255), RandInt(0, 255) ) );
    }

    // Randomize everything
    for( s = sounds.begin(); s != sounds.end(); ++s ) {

    }

    // Randomize everything
    for( t = texts.begin(); t != texts.end(); ++t ) {
        (**t).SetPosition( RandFloat(bezel, Window.GetWidth() - bezel), RandFloat(bezel, Window.GetHeight() - bezel) );
        (**t).SetColor( Color( RandInt(0, 255), RandInt(0, 255), RandInt(0, 255) ) );

        stringstream tempString;
        for( int n = 0; n < 10; ++n ) {
            tempString << static_cast<char>( RandInt(97, 122) );
        }
        (**t).SetString( tempString.str() );
    }


    // Play the sound
//    testWavFileSound.Play();

    while( !shouldExit ) {
        // Clear out the event queue
        while( Window.PollEvent(Event) ) {

            // If the user tries to close the window
            if( Event.Type == Event::Closed) {

                // Exit the program
                shouldExit = true;
            }
        }

        // Update the input
        leftClick.Update();                         // mouse left click
        rightClick.Update();                        // mouse right click
        MouseState::UpdateMouseCoords( Window );    // mouse coords

        exit.Update();                  // escape key

        if( exit.IsJustPressed() ) {
            shouldExit = true;
        }

        // Lets you click and drag a shape
        if( leftClick.IsDown() ) {
            if( leftClick.IsJustPressed() ) {
//                ImageCache::Remove( TestBMPImage );
//                ImageCache::Remove( TestPNGImage );
            }
        }

        // Draw images
        for( i = images.begin(); i != images.end(); ++i ) {
            Window.Draw( **i );
        }

        // Draw fonts
        for( t = texts.begin(); t != texts.end(); ++t ) {
            Window.Draw( **t );
        }

        // Update the screen
        Window.Display();
        Window.Clear( Color(255, 120, 120) );
    }


    // free everything
    for( i = images.begin(); i != images.end(); ++i ) {
        delete (*i);
    }

    for( s = sounds.begin(); s != sounds.end(); ++s ) {
        delete (*s);
    }

    for( t = texts.begin(); t != texts.end(); ++t ) {
        delete (*t);
    }
}

#endif
