/*
 * This header defines a generic
 * class for a media caching system
 *
 * Author:      Stuart Rudderham
 * Created:     March 3, 2012
 * Last Edited: December 11, 2012
 */

#ifndef MEDIACACHE_HPP_INCLUDED
#define MEDIACACHE_HPP_INCLUDED

#include "Singleton.hpp"
#include "ExternalFiles.hpp"
#include <map>

// Forward Declarations
namespace sf {
    class Texture;
}

template<typename T>
class MediaCache : public Singleton< MediaCache<T> > {
    private:
        std::map< ExternalFile, T* > media;                         // The map that holds the cached media

        const T& Add( const ExternalFile& file );                         // Loads the media and return a reference to it
                                                                    // If loading fails the program aborts

        void PostLoadProcessing( T* item ) {                        // Do something with the just-loaded media. Default is to do nothing, you
            (void)item;                                             // can specialize this if necessary
        }

    public:
        ~MediaCache();                                              // dtor. Clears the cache

        const T& Load( const ExternalFile& file );                         // Return a reference to the media stored in the given file
                                                                    // Will cache loaded items for future use

        void Clear();                                               // Removes all items in the cache and frees the memory associated
                                                                    // with the media
};

// Specialization for sf::Texture to enable/disable anti-aliasing
template<>
inline void MediaCache<sf::Texture>::PostLoadProcessing( sf::Texture* texture ) {
    texture->setSmooth( Drawing::UseImageAntiAliasing );
}

#include "MediaCache/MediaCache.inl"

#endif // MEDIACACHE_HPP_INCLUDED
