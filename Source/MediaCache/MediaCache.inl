/*
 * This file implements a generic
 * class for a media caching system
 *
 * Author:      Stuart Rudderham
 * Created:     March 3, 2012
 * Last Edited: December 11, 2012
 */

// Free all memory when destroyed
template<typename T>
MediaCache<T>::~MediaCache() {
    Clear();
}

// Removes all items in the cache and frees the memory associated with the media
template<typename T>
void MediaCache<T>::Clear() {

    // Loop through all the items and free their memory
    for( auto i = media.begin(); i != media.end(); ++i ) {
        delete i->second;
    }

    // Remove all the files from the map
    media.clear();
}

// Return a reference to the media stored in the given file
// Will cache loaded items for future use
template<typename T>
const T& MediaCache<T>::Load( const ExternalFile& file ) {

    // Find the index in the map where the item is located
    auto i = media.find( file );

    // If the item is in the cache then return it
    if( i != media.end() ) {
        return *(i->second);
    }

    // Otherwise try and add it to the cache and return a reference to it
    return Add( file );
}

// Loads the media and return a reference to it
// If loading fails the program aborts
template<typename T>
const T& MediaCache<T>::Add( const ExternalFile& file )
{
    // Make sure we don't have duplicate files
    dbAssert( media.count( file ) == 0, "This file has already been cached. Given file was: ", file );

    // Create the new item
    T* newItem = new T;

    // Load the media
    bool result = newItem->loadFromFile( file );

    // Make sure we were able to successfully load the media
    dbAssert( result == true, "Unable to load media. Given file was: ", file );
    (void)result;

    // Do any necessary processing on the item
    PostLoadProcessing( newItem );

    // Add the media to the map
    media.insert( std::make_pair( file, newItem ) );

    // and return a reference to it
    return *newItem;
}
