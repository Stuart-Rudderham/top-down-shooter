/*
 * This header defines functions that deal
 * with printing things built into SFML
 *
 * Author:      Stuart Rudderham
 * Created:     February 2, 2012
 * Last Edited: March 4, 2012
 */

#ifndef PRINTING_HPP_INCLUDED
#define PRINTING_HPP_INCLUDED

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <iostream>

// This functions overloads the "<<" operator to allow you to print out SFML 2D vectors of any type
template<typename T>
std::ostream& operator<<( std::ostream& os, const sf::Vector2<T>& v ) {
    os << "(" << v.x << ", " << v.y << ")";
    return os;
}

// This functions overloads the "<<" operator to allow you to print out SFML rectangles of any type
template<typename T>
std::ostream& operator<<( std::ostream& os, const sf::Rect<T>& r ) {
    os << "(" << r.Left << ", " << r.Top << ") with width " << r.Width << " and height " << r.Height;
    return os;
}

#endif // PRINTING_HPP_INCLUDED
