/*
 * This header defines functions that deal
 * with drawing things
 *
 * Author:      Stuart Rudderham
 * Created:     February 4, 2012
 * Last Edited: February 4, 2012
 */

#ifndef DRAWING_HPP_INCLUDED
#define DRAWING_HPP_INCLUDED

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Sprite.hpp>

/*******************************************************
 *                  Drawing Shapes                     *
 *******************************************************/

// This draws a direction vector that is rooted at the point P
void DrawVector( sf::RenderTarget& Target, sf::Vector2f p, sf::Vector2f v, sf::Color color = sf::Color::Black, bool drawArrow = false, float arrowSize = 10.0f, float arrowAngle = 30.0f );

// This draws a dot with the specified radius centered at point P
void DrawDot( sf::RenderTarget& Target, sf::Vector2f p, float radius = 0.0f, sf::Color color = sf::Color::Black );


/*******************************************************
 *                 Resizing Sprites                    *
 *******************************************************/

// This takes in a sprite and sets its scale factor so its new size are the
// values passed in
inline void ResizeSprite( sf::Sprite& sprite, float newWidth, float newHeight ) {
    sprite.setScale( newWidth / sprite.getLocalBounds().width, newHeight / sprite.getLocalBounds().height );
}
#endif // DRAWING_HPP_INCLUDED
