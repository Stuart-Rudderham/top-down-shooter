/*
 * This header defines functions that wrap
 * the "new" and "delete" functions so that
 * it is easy to search for where memory is
 * allocated/deallocated (new and delete are
 * very common words)
 *
 * Author:      Stuart Rudderham
 * Created:     January 10, 2013
 * Last Edited: January 10, 2013
 */

#ifndef WRAPPEDMEMORYCALLS_HPP_INCLUDED
#define WRAPPEDMEMORYCALLS_HPP_INCLUDED

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/Sprite.hpp>

namespace SFML {
    namespace RectangleShape {
        inline sf::RectangleShape* Create() {
            return new sf::RectangleShape();
        }
    };

    namespace CircleShape {
        inline sf::CircleShape* Create() {
            return new sf::CircleShape();
        }
    };

    namespace ConvexShape {
        inline sf::ConvexShape* Create() {
            return new sf::ConvexShape();
        }
    };

    namespace Shape {
        inline void Destroy( sf::Shape* shape ) {
            delete shape;
        }
    };

    namespace Sprite {
        inline sf::Sprite* Create() {
            return new sf::Sprite();
        }

        inline void Destroy( sf::Sprite* sprite ) {
            delete sprite;
        }
    };
};

#endif // WRAPPEDMEMORYCALLS_HPP_INCLUDED
