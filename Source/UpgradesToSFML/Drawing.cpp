/*
 * This file implements functions that deal
 * with drawing things
 *
 * Author:      Stuart Rudderham
 * Created:     February 4, 2012
 * Last Edited: February 4, 2012
 */

#include "UpgradesToSFML/Drawing.hpp"
#include "Math/MathFunctions.hpp"
#include "DebugTools/Debug.hpp"

#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/CircleShape.hpp>

#include <cmath>

using namespace std;
using namespace sf;

// This draws a direction vector that is rooted at the point P
void DrawVector( RenderTarget& Target, Vector2f p, Vector2f v, Color color, bool drawArrow, float arrowSize, float arrowAngle ) {
    Vertex line[2] = { Vertex( p, color ), Vertex( p + v, color ) };        // create the line
    Target.draw(line, 2, Lines);                                            // draw the line

    if( drawArrow ) {
        Vector2f a = GetUnitVector(v) * (-arrowSize);                                                       // flip the direction vector and scale it to the correct size

        Vertex arrow[3];
        arrow[0] = line[1];                                                                                 // arrow starts at the end of the line
        arrow[1] = Vertex( p + v + RotateClockwise( a, DegreeToRadian(arrowAngle) ), color );               // rotate to get one base of the triangle
        arrow[2] = Vertex( p + v + RotateCounterClockwise( a, DegreeToRadian(arrowAngle) ), color );        // rotate the other way to get the other base

        Target.draw(arrow, 3, Triangles );                                                                  // draw the arrow
    }
}


// This draws a dot with the specified radius centered at point P
void DrawDot( RenderTarget& Target, Vector2f p, float radius, Color color ) {

    dbAssert( radius >= 0.0f, "DrawDot() - radius is negative. Value given is ", radius );

    // Don't need a super high resolution circle if the radius is tiny
    CircleShape dot( radius, radius < 10.0f ? 6 : 30 );

    // Set the position and size
    dot.setOrigin( radius, radius );
    dot.setPosition( p );

    // Set the colors
    dot.setFillColor( color );
    dot.setOutlineColor( Color::Transparent );       // no outline
    dot.setOutlineThickness( 0.0f );

    // Draw it
    Target.draw( dot );
}
