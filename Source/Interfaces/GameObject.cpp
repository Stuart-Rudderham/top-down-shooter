/*
 * This file implements the generic
 * GameObject class that is the base class
 * of every object in the game (but not
 * things like UI elements)
 *
 * Author:      Stuart Rudderham
 * Created:     March 16, 2012
 * Last Edited: December 16, 2012
 */

#include "Interfaces/GameObject.hpp"
#include "Managers/Deletion/DeletionManager.hpp"

GameObjectID GameObject::InvalidID = 0;
GameObjectID GameObject::NextID = GameObject::InvalidID + 1;

// Constructor
GameObject::GameObject() : ID( GameObject::NextID )
{
    GameObject::NextID++;               // update the bookkeeping
}

// Delete the GameObject sometime in the future (e.g. 5 seconds from now)
void GameObject::DeleteIn( sf::Time deletionTime ) {
    DeletionManager::Get().AddDeletionEntry( this, deletionTime );
}
