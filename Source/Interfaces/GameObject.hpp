/*
 * This header defines the generic
 * GameObject class that is the base class
 * of every object in the game (but not
 * things like UI elements)
 *
 * Author:      Stuart Rudderham
 * Created:     March 16, 2012
 * Last Edited: December 16, 2012
 */

#ifndef GAMEOBJECT_HPP_INCLUDED
#define GAMEOBJECT_HPP_INCLUDED

#include "DebugTools/Debug.hpp"
#include "Memory/MemoryAllocationMacros.hpp"
#include <bitset>

// This enum defines the different types of
// deletion/removal a GameObject can have
namespace DeletionFlag {
    enum Enum : unsigned char {
        TotalDeletion,              // Delete the GameObject
        Collision,                  // Remove from the CollisionManager
        Drawing,                    // Remove from the DrawingManager
        Movement,                   // Remove from the MovementManager
        Intelligence,               // Remove from the IntelligenceManager
        Max
    };
};

typedef unsigned int GameObjectID;

class GameObject {
    private:
        static GameObjectID NextID;                                                         // holds the next ID number to be assigned
        static GameObjectID InvalidID;                                                      // an ID number that no object will ever have

        const GameObjectID ID;                                                              // the unique ID number for this GameObject

        std::bitset<DeletionFlag::Max> deletionFlags;                                       // holds the different flags for deletion/removal from Managers

    public:
        GameObject();                                                                       // constructor
        /*virtual*/ ~GameObject() {};                                                       // destructor. Right now we never call delete on GameObject*
                                                                                            // pointers, so the destructor doesn't need to be virtual
                                                                                            // and we don't need a vtable for this class

        inline void SetDeletionFlag( DeletionFlag::Enum flag ) {                            // Sets only the given deletion flag
            deletionFlags[ flag ] = true;
        }

        inline void ClearDeletionFlag( DeletionFlag::Enum flag ) {                          // Clear only the given deletion flag
            deletionFlags[ flag ] = false;
        }

        inline bool GetDeletionFlag( DeletionFlag::Enum flag )const {                       // Getter for the given deletion flag
            return deletionFlags[ flag ];
        }

        inline void MarkForDeletion() {                                                     // Mark the GameObject for defered deletion. This sets all the
            deletionFlags.set();                                                            // deletion flags, so Managers will ignore this object from this
        }                                                                                   // point forward and then remove the object at the end of the frame

        inline bool ShouldDelete() const {                                                  // true if the GameObject is going to be deleted "soon"
            return deletionFlags[ DeletionFlag::TotalDeletion ];                            // (probably at the end of the frame). This flag is only set
        }                                                                                   // by calling MarkForDeletion(), so if this is true then you
                                                                                            // know that all the other deletion flags are set as well

        inline bool ShouldIgnore() const {                                                  // If GameObjects are being stored in a Manager then they should be
            return ShouldDelete();                                                          // ignored and removed if marked for deletion
        }

        void DeleteIn( sf::Time deletionTime );                                             // Delete the GameObject sometime in the future (e.g. 5 seconds from now)

        inline GameObjectID GetID() const {                                                 // Getter for the ID
            return ID;
        }

        static GameObjectID GetNumGameObjectsCreated() {                                    // returns the total number of GameObjects created
            return NextID - InvalidID - 1;
        }
};

#endif // GAMEOBJECT_HPP_INCLUDED
