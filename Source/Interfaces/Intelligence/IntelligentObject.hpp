/*
 * This header defines an abstract
 * base class for any object that
 * is controlled by something (could
 * be user-controlled or have AI)
 *
 * Author:      Stuart Rudderham
 * Created:     August 12, 2012
 * Last Edited: December 12, 2012
 */

#ifndef INTELLIGENTOBJECT_HPP_INCLUDED
#define INTELLIGENTOBJECT_HPP_INCLUDED

#include "Interfaces/GameObject.hpp"
#include "GameObjects/Characters/Controllers/CharacterController.hpp"

class IntelligentObject : virtual public GameObject {
    private:
        CharacterController* controller;                                                    // a pointer to the controller object. Right now we can only
                                                                                            // control Characters, because making a generic controller
                                                                                            // class is a hassle due to the fact that we can't down-cast
                                                                                            // from GameObject* without using dynamic_cast (due to multiple
                                                                                            // inheritance)

        inline bool HaveController() const {                                                // Predicate function to test if we have a controller
            return controller != nullptr;
        }

    public:
        IntelligentObject();                                                                // ctor. Sets the controller to NULL

        virtual ~IntelligentObject();                                                       // dtor. Destroys the controller

        void EnableController();                                                            // Add the object to the IntelligenceManager so its controller
                                                                                            // will update it

        void DisableController();                                                           // Mark the object for removal from the IntelligenceManager so its
                                                                                            // controller will no longer update it

        bool ShouldIgnore();                                                                // Predicate function to check if the IntelligenceManager should
                                                                                            // update this object

        inline CharacterController* GetController() {                                       // Getter for the controller
            dbAssert( HaveController(), "Don't have a controller to get" );
            return controller;
        }

        void SetController( CharacterController* objectController );                       // Setter for the controller

        inline void UpdateLogic() {                                                         // Update the controller
            GetController()->UpdateLogic();
        };

        inline bool IsBot()   {                                                             // return true if object is AI controlled
            return GetController()->GetControllerType() == ControllerType::Bot;
        }

        inline bool IsHuman() {                                                             // return true if object is user controlled
            return GetController()->GetControllerType() == ControllerType::Human;
        }

        template<typename T>
        T* GetControllerAs() {
            return checked_cast<T*>( GetController() );
        }
};

#endif // INTELLIGENTOBJECT_HPP_INCLUDED
