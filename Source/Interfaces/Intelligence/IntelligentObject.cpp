/*
 * This file implements an abstract
 * base class for any object that
 * is controlled by something (could
 * be user-controlled or have AI)
 *
 * Author:      Stuart Rudderham
 * Created:     August 12, 2012
 * Last Edited: January 8, 2013
 */

#include "Interfaces/Intelligence/IntelligentObject.hpp"
#include "Managers/Intelligence/IntelligenceManager.hpp"
#include "GameObjects/Characters/Controllers/Human/HumanController.hpp"
#include "GameObjects/Characters/Controllers/AI/AIController.hpp"

// constructor
IntelligentObject::IntelligentObject() : controller( nullptr )
{
    // Add the object to the IntelligenceManager
    EnableController();
}

// destructor. Destroys the controller
IntelligentObject::~IntelligentObject() {
    CharacterController::Destroy( controller );

    // Make sure the object has been removed from the IntelligenceManager
    dbAssert( IntelligenceManager::Get().InManager( this ) == false, "Object not removed from IntelligenceManager before deletion" );
}

void IntelligentObject::SetController( CharacterController* objectController ) {

    // If we already have a controller then delete it first
    if( HaveController() ) {
        CharacterController::Destroy( controller );
    }

    controller = objectController;
}

// Add the object to the IntelligenceManager
void IntelligentObject::EnableController() {

    // If the object has just been created then at this point
    // its controller hasn't been created yet
    if( HaveController() && IsBot() ) {
        GetControllerAs<AIController>()->TurnOnVision();
    }

    ClearDeletionFlag( DeletionFlag::Intelligence );

    // ArrayOfGameObjects<T> will assert that this object hasn't already been added
    IntelligenceManager::Get().AddGameObject( this );
}

// Mark the object for removal from the IntelligenceManager
void IntelligentObject::DisableController() {
    SetDeletionFlag( DeletionFlag::Intelligence );

    // We should always have a controller at this point
    if( IsBot() ) {
        GetControllerAs<AIController>()->TurnOffVision();
    }
}

// return true if the IntelligenceManager *shouldn't* update this object
bool IntelligentObject::ShouldIgnore() {
    return GetDeletionFlag( DeletionFlag::Intelligence );
}
