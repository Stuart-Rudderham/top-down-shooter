/*
 * This header defines an abstract
 * base class for any object that
 * has health, can be damaged, can
 * die, and be respawned
 *
 * Author:      Stuart Rudderham
 * Created:     September 9, 2012
 * Last Edited: November 13, 2012
 */

#ifndef MORTALOBJECT_HPP_INCLUDED
#define MORTALOBJECT_HPP_INCLUDED

#include "Interfaces/GameObject.hpp"

class MortalObject : virtual public GameObject {
    private:
        float health;                                                           // the current health of the object
        float maxHealth;                                                        // the maximum health of the object

        // void DrawHealthBar( sf::RenderTarget)
        virtual void DeathReaction() = 0;                                       // called by ModifyHealth when the objects
                                                                                // health becomes 0
    public:
        MortalObject( float maxH );                                             // ctor. Takes in the max health
        ~MortalObject();                                                        // dtor. Makes sure the object isn't in the RespawnManager

        virtual void ModifyHealth( float delta );                               // used to modify the health of the object
                                                                                // can either hurt or heal. If the object is
                                                                                // already dead then it does nothing

        inline bool IsDead() const {                                            // Returns true if the object is "dead"
            return health <= 0.0f;
        }

        inline float GetHealthPercent() const {                                 // Get the object's health percent in the range [0, 1]
            return health / maxHealth;
        }

        inline void RestoreToMaxHealth() {                                      // Reset the object's current health to their max health
            health = maxHealth;
        }

        void RespawnIn( sf::Time respawnTime );                                 // Respawn the object sometime in the future (e.g. 5 seconds from now)
                                                                                // The object has to be "dead" first

        virtual void Respawn(/* Point2f spawnPoint */) = 0;                     // Respawn the object at the given position

};

#endif // MORTALOBJECT_HPP_INCLUDED
