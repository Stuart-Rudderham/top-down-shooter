/*
 * This file implements an abstract
 * base class for any object that
 * has health, can be damaged, and
 * die
 *
 * Author:      Stuart Rudderham
 * Created:     September 9, 2012
 * Last Edited: November 13, 2012
 */

#include "Interfaces/Health/MortalObject.hpp"
#include "Managers/Respawning/RespawnManager.hpp"
#include "Math/MathFunctions.hpp"
#include <algorithm>

// constructor
MortalObject::MortalObject( float maxH ) : health( maxH ),
                                           maxHealth( maxH )
{

}

// Destructor
MortalObject::~MortalObject() {
    // Make sure the object has been removed from the RespawnManager
    dbAssert( RespawnManager::Get().InManager( this ) == false, "Object not removed from RespawnManager before deletion" );
}

void MortalObject::ModifyHealth( float delta ) {

    // if already dead then nothing to do
    if( IsDead() ){
        return;
    }

    // Change the health
    health += delta;

    // And then cap it in the correct range;
    Clamp( 0.0f, health, maxHealth );

    if( IsDead() ) {
        DeathReaction();
    }
}

void MortalObject::RespawnIn( sf::Time respawnTime ) {
    dbAssert( IsDead(), "Cannot respawn a object that isn't dead" );
    RespawnManager::Get().AddRespawnEntry( this, respawnTime );
}
