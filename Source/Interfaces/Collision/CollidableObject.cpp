/*
 * This file implements an abstract
 * base class for any object that
 * can collide with another object
 *
 * Author:      Stuart Rudderham
 * Created:     March 23, 2012
 * Last Edited: January 10, 2013
 */

#include "Interfaces/Collision/CollidableObject.hpp"
#include "Managers/Collision/CollisionManager.hpp"
#include "Math/Geometry/ConvexPolygon.hpp"

#include <limits>
using namespace std;
using namespace sf;

// Constructor
CollidableObject::CollidableObject( CollidableObjectType::Enum objectType, BaseShape* gOutline, CollisionPriority priority )
    : collidableObjectType( objectType ),
      globalCollisionOutline( gOutline ),
      boundingBox( AABB::Create() ),
      collisionPriority( priority )
{
    // Make sure we were actually passed an outline
    dbAssert( globalCollisionOutline != nullptr, "nullptr pointer passed as global outline" );

    // Update the bounding box so that it encompasses the global outline
    UpdateBoundingBox();

    // Add the object to the CollisionManager
    EnableCollisionChecking();
}

// Destructor
CollidableObject::~CollidableObject() {
    // free any memory associated with the outline
    BaseShape::Destroy( globalCollisionOutline );
    BaseShape::Destroy( boundingBox );

    // Make sure the object has been removed from the CollisionManager
    dbAssert( CollisionManager::Get().InManager( this ) == false, "Object not removed from CollisionManager before deletion" );
}

// Add the object to the CollisionManager
void CollidableObject::EnableCollisionChecking() {
    ClearDeletionFlag( DeletionFlag::Collision );

    // ArrayOfGameObjects<T> will assert that this object hasn't already been added
    CollisionManager::Get().AddGameObject( this );
}

// Mark the object for removal from the CollisionManager
void CollidableObject::DisableCollisionChecking() {
    SetDeletionFlag( DeletionFlag::Collision );
}

// return true if the CollisionManager *shouldn't* check this object for collisions
bool CollidableObject::ShouldIgnore() {
    return GetDeletionFlag( DeletionFlag::Collision );
}

// draws the entity to the target
void CollidableObject::DrawCollisionOutline( RenderTarget& Target, Color outlineColor, Color boxColor ) {
    // Draw the bounding box
    GetBoundingBox()->Draw( Target, boxColor, Color::Transparent, 0.0f );

    // Draw the outline
    GetCollisionOutline()->Draw( Target, outlineColor, Color::Transparent, 0.0f );

    // Draw the center dot of the outline
    GetCollisionOutline()->DrawCenterDot( Target );
}
