/*
 * This header defines an abstract
 * base class for any object that
 * can collide with another object
 * and is able to rotate
 *
 * Author:      Stuart Rudderham
 * Created:     March 23, 2012
 * Last Edited: November 1, 2012
 */

#ifndef ROTATABLEOBJECT_HPP_INCLUDED
#define ROTATABLEOBJECT_HPP_INCLUDED

#include "Interfaces/Collision/CollidableObject.hpp"

class RotatableObject : public CollidableObject {
    private:
        ConvexPolygon* localCollisionOutline;                                               // the collision outline of the shape in local coordinates

    public:
        RotatableObject( CollidableObjectType::Enum objectType,                             // ctor. Takes an outline in local coords as well as a position
                         ConvexPolygon* lOutline,                                           // to move the outline to
                         Point2f pos,
                         CollisionPriority priority );

        virtual ~RotatableObject();                                                         // dtor. Doesn't need to do anything, is virtual in case of children

        // Get/Set for the local outline
        inline ConvexPolygon* GetLocalCollisionOutline() const
        {
            dbAssert( localCollisionOutline != nullptr, "Don't have a local collision outline" );
            return localCollisionOutline;
        }

        void SetGlobalOutlineRotation( float angle );                                       // rotate the global outline to its angle
};

#endif // ROTATABLEOBJECT_HPP_INCLUDED
