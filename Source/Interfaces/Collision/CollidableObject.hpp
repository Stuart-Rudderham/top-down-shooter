/*
 * This header defines an abstract
 * base class for any object that
 * can collide with another object
 *
 * Author:      Stuart Rudderham
 * Created:     March 23, 2012
 * Last Edited: November 11, 2012
 */

#ifndef COLLIDABLEOBJECT_HPP_INCLUDED
#define COLLIDABLEOBJECT_HPP_INCLUDED

#include "Interfaces/GameObject.hpp"
#include "Math/Geometry/BaseShape.hpp"
#include "Math/Geometry/AABB.hpp"

class CollidableObject : virtual public GameObject {
    private:
        CollidableObjectType::Enum collidableObjectType;                                        // the type of object the CollidableObject actually is

        BaseShape* globalCollisionOutline;                                                      // the shape used for collision detection
        AABB* boundingBox;                                                                      // the bounding box of the CollidableObject


        CollisionPriority collisionPriority;                                                    // used to determine which object should move to resolve collisions

    public:
        CollidableObject( CollidableObjectType::Enum objectType,                                // ctor. Takes in a geometric shape (in global coords) to use as
                          BaseShape* gOutline,                                                  // the collision outline
                          CollisionPriority priority );

        virtual ~CollidableObject();                                                            // dtor. Destroys the collision outline and bounding box

        void EnableCollisionChecking();                                                         // Add the object to the CollisionManager so it will collide
                                                                                                // with other objects

        void DisableCollisionChecking();                                                        // Mark the object for removal from the CollisionManager so it won't
                                                                                                // collide with other objects anymore

        bool ShouldIgnore();                                                                    // Predicate function to check if the CollisionManager should
                                                                                                // check this object for collisions

        inline virtual bool CanCollideWith( CollidableObject* otherObject ) {                   // used as a secondary collision filter. If the first filter (which
            (void)otherObject;                                                                  // is located in ArrayOfCollidableObjects) is not selective enough
            return true;                                                                        // a child class can implement a more specialized version
        }

        inline CollidableObjectType::Enum GetCollidableObjectType() const {                     // get what type of object we actually are (e.g. Character, Bullet, etc...)
            return collidableObjectType;
        }

        // These are convenience functions for querying the actual type of the CollidableObject
        inline bool IsCharacter()   const { return collidableObjectType == CollidableObjectType::Character;   }
        inline bool IsStaticGeo()   const { return collidableObjectType == CollidableObjectType::StaticGeo;   }
        inline bool IsProjectile()  const { return collidableObjectType == CollidableObjectType::Projectile;  }
        inline bool IsFieldOfView() const { return collidableObjectType == CollidableObjectType::FieldOfView; }
        inline bool IsLineOfSight() const { return collidableObjectType == CollidableObjectType::LineOfSight; }

        template<typename T>                                                                    // Convenience function to cast the CollidableObject to a child class
        inline T* GetAs() { return checked_cast<T*>( this ); }                                  // If you cast it to the wrong type an assert will fire

        inline BaseShape* GetCollisionOutline() const {                                         // Get the collision outline of the object
            dbAssert( globalCollisionOutline != nullptr, "Don't have a global collision outline" );
            return globalCollisionOutline;
        }

        inline AABB* GetBoundingBox() {                                                         // Get the bounding box of the object
            dbAssert( boundingBox != nullptr, "Don't have a bounding box" );
            return boundingBox;
        }

        inline CollisionPriority GetCollisionPriority() const {                                 // get the collision priority of the object
            return collisionPriority;
        }

        inline bool IsPhantom() const {                                                         // return true if the object is a phantom (e.g. the field of view)
            return collisionPriority == Physics::Priority::Phantom;
        }

        inline bool IsImmovable() const {                                                       // return true if the object can't be moved (e.g. walls)
            return collisionPriority == Physics::Priority::Immovable;
        }

        inline bool IsMovedBy( const CollidableObject* otherObject ) const {                    // returns true if this object can be moved by the provided
            return collisionPriority < otherObject->GetCollisionPriority();
        }

        inline bool HasRigidBody() const {                                                      // returns true if the object's outline is a shape (i.e. not a ray)
            return GetCollisionOutline()->IsLineSegment() == false;
        }

        //Setters
        inline void MoveCollisionOutlineTo( Point2f newPos ) {                                  // Move the object's outline to the new position
            GetCollisionOutline()->MoveTo( newPos );
            GetBoundingBox()->MoveTo( newPos );
        }

        inline void MoveCollisionOutlineBy( sf::Vector2f displacement ) {                       // Shift the object's outline by the displacement vector
            GetCollisionOutline()->MoveBy( displacement );
            GetBoundingBox()->MoveBy( displacement );
        }

        inline void UpdateBoundingBox() {                                                       // Update the bounding box of the CollidableObject
            dbAssert( boundingBox != nullptr, "Don't have a bounding box but tried to update it" );
            GetCollisionOutline()->SetBoundingBox( boundingBox );
        }

        // Draw the outline to the target with the provided colors
        void DrawCollisionOutline( sf::RenderTarget& Target, sf::Color outlineColor, sf::Color boxColor );

        // Used to resolve a collision between two objects
        virtual void CollisionResponse( CollidableObject* otherObject,
                                        sf::Vector2f mtv,
                                        sf::Vector2f normal ) = 0;
};

#endif // COLLIDABLEOBJECT_HPP_INCLUDED
