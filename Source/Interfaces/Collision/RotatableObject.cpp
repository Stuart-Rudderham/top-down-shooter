/*
 * This file implements an abstract
 * base class for any object that
 * can collide with another object
 * and is able to rotate
 *
 * Author:      Stuart Rudderham
 * Created:     March 23, 2012
 * Last Edited: January 10, 2013
 */

#include "Interfaces/Collision/RotatableObject.hpp"
#include "Math/Geometry/ConvexPolygon.hpp"

// Constructor
RotatableObject::RotatableObject( CollidableObjectType::Enum objectType, ConvexPolygon* lOutline, Point2f pos, CollisionPriority priority )
    : CollidableObject( objectType, ConvexPolygon::Create( *lOutline ), priority ),
      localCollisionOutline( lOutline )
{
    // Since our global outline is currently just a cloned copy
    // of the local outline we need to move it to the correct position
    GetCollisionOutline()->MoveTo( pos );
}

// Destructor
RotatableObject::~RotatableObject() {

    // The local outline is freed in the destructor for the static const data
    // for the child class, so we don't need to do anything
    //delete localOutline;

    // THE ONLY REASON THIS IS OK IS BECAUSE RIGHT NOW ONLY Characters and FieldOfViews
    // ARE RotatableObjects AND THEY HAVE A SHARED localOutline,
}

void RotatableObject::SetGlobalOutlineRotation( float angle ) {

    dbAssert( GetCollisionOutline()->IsConvexPolygon(), "Trying to rotate something that isn't a ConvexPolygon" );

    // Remember the current position of the global outline
    Point2f currentPosition = GetCollisionOutline()->GetCenter();

    // Reset the global outline to the local coords
    ConvexPolygon* globalOutline = GetCollisionOutline()->GetAs<ConvexPolygon>();
    *globalOutline = *localCollisionOutline;

    // Rotate and translate it
    globalOutline->Rotate( angle );
    globalOutline->MoveTo( currentPosition );

    // Update the bounding box
    UpdateBoundingBox();
}
