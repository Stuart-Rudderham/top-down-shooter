/*
 * This header defines an abstract
 * base class for any object that
 * is drawn with a sprite/texture
 *
 * Author:      Stuart Rudderham
 * Created:     August 2, 2012
 * Last Edited: November 1, 2012
 */

#ifndef DRAWABLESPRITE_HPP_INCLUDED
#define DRAWABLESPRITE_HPP_INCLUDED

#include "Interfaces/Drawing/DrawableObject.hpp"
#include "UpgradesToSFML/Drawing.hpp"
#include <SFML/Graphics/Sprite.hpp>

class DrawableSprite : public DrawableObject {
    private:
        sf::Sprite* sprite;                                                                 // the sprite that is to be drawn

    public:
        DrawableSprite( DrawLevel drawingLevel );                                           // ctor. Creates the SFML sprite
        virtual ~DrawableSprite();                                                          // dtor. Deletes the SFML sprite

        virtual void Draw( sf::RenderTarget& Target ) = 0;                                  // draws the GameObject to the target

        void SetPicture( const sf::Texture& newPicture );                                   // Setter for the sprite's picture

        inline sf::Sprite* GetDrawingSprite() {                                             // Getter for the sprite
            dbAssert( sprite != nullptr, "sprite was nullptr" );
            return sprite;
        }

        inline void SetSize( sf::Vector2f size ) {                                          // convience function to set the size of
            ResizeSprite( *GetDrawingSprite(), size.x, size.y );                            // the drawn picture
        }
};

#endif // DRAWABLESPRITE_HPP_INCLUDED
