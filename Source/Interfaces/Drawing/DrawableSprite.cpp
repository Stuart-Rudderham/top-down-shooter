/*
 * This file implements an abstract
 * base class for any object that
 * is drawn with a sprite/texture
 *
 * Author:      Stuart Rudderham
 * Created:     August 2, 2012
 * Last Edited: January 11, 2013
 */

#include "Interfaces/Drawing/DrawableSprite.hpp"
#include "UpgradesToSFML/WrappedMemoryCalls.hpp"

// Constructor
DrawableSprite::DrawableSprite( DrawLevel drawingLevel ) : DrawableObject( drawingLevel )
{
    sprite = SFML::Sprite::Create();
}

// Destructor
DrawableSprite::~DrawableSprite() {
    SFML::Sprite::Destroy( sprite );
}

// Set the picture of the sprite
void DrawableSprite::SetPicture( const sf::Texture& newPicture ) {
    GetDrawingSprite()->setTexture( newPicture, true );        // true -> we want to reset the texture rectangle of the sprite
}
