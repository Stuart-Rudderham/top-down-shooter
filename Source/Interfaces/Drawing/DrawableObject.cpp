/*
 * This file implements an abstract
 * base class for any object that
 * can be drawn to the screen
 *
 * Author:      Stuart Rudderham
 * Created:     November 11, 2012
 * Last Edited: November 11, 2012
 */


#include "Interfaces/Drawing/DrawableObject.hpp"
#include "Managers/Drawing/DrawingManager.hpp"

// Constructor
DrawableObject::DrawableObject( DrawLevel level )
    : drawLevel( level )
{
    // Add the object to the DrawingManager
    EnableDrawing();
}

// Destructor
DrawableObject::~DrawableObject() {

    // Make sure the object has been removed from the DrawingManager
    dbAssert( DrawingManager::Get().InManager( this ) == false, "Object not removed from DrawingManager before deletion" );
}

// Add the object to the DrawingManager
void DrawableObject::EnableDrawing() {
    ClearDeletionFlag( DeletionFlag::Drawing );

    // ArrayOfGameObjects<T> will assert that this object hasn't already been added
    DrawingManager::Get().AddGameObject( this );
}

// Mark the object for removal from the DrawingManager
void DrawableObject::DisableDrawing() {
    SetDeletionFlag( DeletionFlag::Drawing );
}

// return true if the DrawingManager *shouldn't* draw this object
bool DrawableObject::ShouldIgnore() {
    return GetDeletionFlag( DeletionFlag::Drawing );
}
