/*
 * This header defines an abstract
 * base class for any object that
 * is drawn with a 2D polygon
 *
 * Author:      Stuart Rudderham
 * Created:     August 2, 2012
 * Last Edited: January 11, 2013
 */

#ifndef DRAWABLESHAPE_HPP_INCLUDED
#define DRAWABLESHAPE_HPP_INCLUDED

#include "Interfaces/Drawing/DrawableObject.hpp"

#include <SFML/Graphics/Shape.hpp>
#include <SFML/Graphics/Color.hpp>

// Forward Declarations
class BaseShape;
class AABB;
class Circle;
class ConvexPolygon;

namespace GeometricShape {
    enum Enum : signed char;
};

class DrawableShape : public DrawableObject {
    private:
        sf::Shape* drawingOutline;                                                          // The 2D shape that is to be drawn

        inline bool HaveDrawingOutline() const {                                            // Predicate function to test if we have a drawing outline
            return drawingOutline != nullptr;
        }

        void SetDrawingOutlineToAABB         ( AABB*          outline );                    // These functions takes in a geometric
        void SetDrawingOutlineToCircle       ( Circle*        outline );                    // shape and created an identical drawable
        void SetDrawingOutlineToConvexPolygon( ConvexPolygon* outline );                    // SFML shape for drawing

    public:
        DrawableShape( DrawLevel drawingLevel );                                            // ctor. Creates the SFML shape
        virtual ~DrawableShape();                                                           // dtor. Deletes the SFML shape

        virtual void Draw( sf::RenderTarget& Target ) = 0;                                  // draws the GameObject to the target

        void SetDrawingOutline( BaseShape* outline, sf::Color color );                      // Single public function to set the drawing outline, will
                                                                                            // choose the correct private function to call

        inline sf::Shape* GetDrawingOutline() {                                             // Getter for the drawing outline
            dbAssert( HaveDrawingOutline(), "shape was nullptr" );
            return drawingOutline;
        }
};

#endif // DRAWABLESHAPE_HPP_INCLUDED
