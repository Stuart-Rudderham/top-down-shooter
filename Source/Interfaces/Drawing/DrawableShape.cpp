/*
 * This file implements an abstract
 * base class for any object that
 * is drawn with a 2D polygon
 *
 * Author:      Stuart Rudderham
 * Created:     August 2, 2012
 * Last Edited: January 11, 2013
 */

#include "Interfaces/Drawing/DrawableShape.hpp"

#include "Math/Geometry/BaseShape.hpp"
#include "Math/Geometry/AABB.hpp"
#include "Math/Geometry/Circle.hpp"
#include "Math/Geometry/ConvexPolygon.hpp"

#include "UpgradesToSFML/WrappedMemoryCalls.hpp"

using namespace sf;


// Constructor
DrawableShape::DrawableShape( DrawLevel drawingLevel ) : DrawableObject( drawingLevel ),
                                                         drawingOutline( nullptr )
{}

// Destructor
DrawableShape::~DrawableShape() {
    SFML::Shape::Destroy( drawingOutline );
}

void DrawableShape::SetDrawingOutline( BaseShape* outline, sf::Color color ) {
    dbAssert( HaveDrawingOutline() == false, "Already have a drawing outline" );

    // Create the drawing outline based on the type
    // of the provided geometric shape
    switch( outline->GetGeometricShape() ) {
        case GeometricShape::AABB:
            SetDrawingOutlineToAABB( outline->GetAs<AABB>() );
            break;

        case GeometricShape::Circle:
            SetDrawingOutlineToCircle( outline->GetAs<Circle>() );
            break;

        case GeometricShape::ConvexPolygon:
            SetDrawingOutlineToConvexPolygon( outline->GetAs<ConvexPolygon>() );
            break;

        default:
            dbCrash( "Invalid geometric shape given for creation. Outline type was ", outline->GetGeometricShape() );
            break;
    }

    GetDrawingOutline()->setFillColor( color );
    GetDrawingOutline()->setOutlineThickness( 0.0f );
    GetDrawingOutline()->setOutlineColor( Color::Transparent );
}

void DrawableShape::SetDrawingOutlineToAABB( AABB* outline ) {
    dbAssert( HaveDrawingOutline() == false, "Already have a drawing outline" );

    RectangleShape* rectangleShape = SFML::RectangleShape::Create();

    // Setup the shape to be drawn
    rectangleShape->setPosition( outline->GetTopLeftCorner() );
    rectangleShape->setSize( Vector2f( outline->GetWidth(), outline->GetHeight() ) );

    drawingOutline = rectangleShape;
}

void DrawableShape::SetDrawingOutlineToCircle( Circle* outline ) {
    dbAssert( HaveDrawingOutline() == false, "Already have a drawing outline" );

    CircleShape* circleShape = SFML::CircleShape::Create();

    // If the radius is tiny can get away with not super high-res circle
    if( outline->GetRadius() < 10.0f ) {
        circleShape->setPointCount( 6 );
    }

    // Setup the shape to be drawn
    circleShape->setPosition( outline->GetCenter() );
    circleShape->setRadius( outline->GetRadius() );
    circleShape->setOrigin( outline->GetRadius(), outline->GetRadius() );

    drawingOutline = circleShape;
}

void DrawableShape::SetDrawingOutlineToConvexPolygon( ConvexPolygon* outline ) {
    dbAssert( HaveDrawingOutline() == false, "Already have a drawing outline" );

    ConvexShape* polygonShape = SFML::ConvexShape::Create();

    polygonShape->setPointCount( outline->GetNumPoints() );

    // Setup the shape to be drawn
    for( unsigned int i = 0; i < outline->GetNumPoints(); ++i ) {
        polygonShape->setPoint( i, outline->GetPoint(i) );
    }

    drawingOutline = polygonShape;
}
