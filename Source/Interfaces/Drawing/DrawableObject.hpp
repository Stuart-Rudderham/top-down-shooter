/*
 * This header defines an abstract
 * base class for any object that
 * can be drawn to the screen
 *
 * Author:      Stuart Rudderham
 * Created:     August 2, 2012
 * Last Edited: November 11, 2012
 */

#ifndef DRAWABLEOBJECT_HPP_INCLUDED
#define DRAWABLEOBJECT_HPP_INCLUDED

#include "Interfaces/GameObject.hpp"

class DrawableObject : virtual public GameObject {
    private:
        DrawLevel drawLevel;                                            // Where the object should be drawn (e.g. background, forground, etc...)

    public:
        DrawableObject( DrawLevel level );                              // ctor. Sets the draw level
        virtual ~DrawableObject();                                      // dtor. Doesn't need to do anything, is virtual in case of children

        void EnableDrawing();                                           // Add the object to the DrawingManager so it will be drawn to the screen

        void DisableDrawing();                                          // Mark the object for removal from the DrawingManager so it won't
                                                                        // be drawn to the screen

        bool ShouldIgnore();                                            // Predicate function to check if the DrawingManager should
                                                                        // draw this object

        virtual void Draw( sf::RenderTarget& Target ) = 0;              // Draws the GameObject to the target

        inline DrawLevel GetDrawLevel() const {                         // Getter for the draw level
            return drawLevel;
        }
};

#endif // DRAWABLEOBJECT_HPP_INCLUDED
