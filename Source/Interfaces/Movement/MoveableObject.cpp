/*
 * This file implements an abstract
 * base class for any object that
 * can move
 *
 * Author:      Stuart Rudderham
 * Created:     March 23, 2012
 * Last Edited: November 11, 2012
 */

#include "Interfaces/Movement/MoveableObject.hpp"
#include "Managers/Movement/MovementManager.hpp"

// constructor
MoveableObject::MoveableObject( Point2f pos, float ang ) : position( pos ),
                                                           velocity( MathConstants::ZeroVector ),
                                                           acceleration( MathConstants::ZeroVector ),
                                                           angle( ang ),
                                                           rotationSpeed( 0.0f )
{
    // Add the object to the MovementManager
    EnableLocomotion();
}

// destructor. Doesn't need to do anything, is virtual in case of children
MoveableObject::~MoveableObject() {

    // Make sure the object has been removed from the MovementManager
    dbAssert( MovementManager::Get().InManager( this ) == false, "Object not removed from MovementManager before deletion" );
}

// Add the object to the MovementManager
void MoveableObject::EnableLocomotion() {
    ClearDeletionFlag( DeletionFlag::Movement );

    // ArrayOfGameObjects<T> will assert that this object hasn't already been added
    MovementManager::Get().AddGameObject( this );
}

// Mark the object for removal from the MovementManager
void MoveableObject::DisableLocomotion() {
    SetDeletionFlag( DeletionFlag::Movement );
}

// return true if the MovementManager *shouldn't* move this object
bool MoveableObject::ShouldIgnore() {
    return GetDeletionFlag( DeletionFlag::Movement );
}
