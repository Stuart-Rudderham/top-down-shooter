/*
 * This header defines an abstract
 * base class for any object that
 * can move
 *
 * Author:      Stuart Rudderham
 * Created:     March 23, 2012
 * Last Edited: November 11, 2012
 */

#ifndef MOVEABLEOBJECT_HPP_INCLUDED
#define MOVEABLEOBJECT_HPP_INCLUDED

#include "Interfaces/GameObject.hpp"
#include "Math/MathFunctions.hpp"

class MoveableObject : virtual public GameObject {
    protected:
        Point2f position;                                                                   // The position of the object
        sf::Vector2f velocity;                                                              // The velocity of the object (i.e. the rate of change for the position)
        sf::Vector2f acceleration;                                                          // The acceleration of the object (i.e. the rate of change for the velocity)

        float angle;                                                                        // The direction the object is facing (in degrees, 0 = facing right, 90 = facing down)
        float rotationSpeed;                                                                // The speed that the object is rotating at (positive is clockwise, negative is
                                                                                            // counter-clockwise)
    public:
        MoveableObject( Point2f pos, float ang = 0 );                                       // ctor. Sets all speeds/accelerations to 0

        virtual ~MoveableObject();                                                          // dtor. Doesn't need to do anything, is virtual in case of children

        void EnableLocomotion();                                                            // Add the object to the MovementManager so it is able to move

        void DisableLocomotion();                                                           // Mark the object for removal from the MovementManager so it can
                                                                                            // no longer move under its own power (but other objects can move it)

        bool ShouldIgnore();                                                                // Predicate function to check if the MovementManager should
                                                                                            // move this object

        virtual void FrameUpdate( float deltaT ) = 0;                                       // Do updates that should only be done once per frame
                                                                                            // (e.g. rotate outline or cap velocity)

        virtual void TimeSliceUpdate( float deltaT ) = 0;                                   // Do updates that should only be done once per physics time slice
                                                                                            // (e.g. update the position)

        virtual void MoveBy( sf::Vector2f displacement ) = 0;                               // Move the object by the displacement vector

        inline void MoveTo( Point2f newPosition ) {                                         // Move the object to the specified location
            MoveBy( GetDirectionVector(position, newPosition) );
        }

        inline void UpdatePosition( float deltaT ) {                                        // Update the position based on the velocity
            MoveBy( velocity * deltaT );                                                    // and amount of time that has passed
        }

        inline void UpdateVelocity( float deltaT ) {                                        // Update the velocity based on the acceleration
            velocity += acceleration * deltaT;                                              // and amount of time that has passed
        }

        inline void UpdateAngle( float deltaT ) {                                           // Update the angle based on the rotation speed
            angle += rotationSpeed * deltaT;                                                // and amount of time passed
        }

        // Getters
        inline Point2f      GetPosition()      const { return position;      }
        inline sf::Vector2f GetVelocity()      const { return velocity;      }
        inline sf::Vector2f GetAcceleration()  const { return acceleration;  }
        inline float        GetAngle()         const { return angle;         }
        inline float        GetRotationSpeed() const { return rotationSpeed; }

        // Setters
        // We don't provide a SetPosition function because you should
        // either use MoveBy or MoveTo to adjust the position, since
        // these make sure anything else that depends on position
        // (e.g. collision outline) gets updated as well
        inline void SetVelocity     ( sf::Vector2f newVel ) { velocity     = newVel;    }
        inline void SetAcceleration ( sf::Vector2f newAcc ) { acceleration = newAcc;    }
        inline void SetAngle        ( float newAngle      ) { angle = newAngle;         }
        inline void SetRotationSpeed( float newSpeed      ) { rotationSpeed = newSpeed; }

        inline void ApplyImpulse( sf::Vector2f impulse ) {                                  // Apply an impulse to the velocity
            velocity += impulse;
        }

        inline void ApplyImpulse( float ang, float magnitude ) {                            // Apply an impulse based on a direction
            ApplyImpulse( GetDirectionVector( ang, magnitude ) );
        }

        inline void ApplyDampening( float factor ) {                                        // Apply a dampening factor to the velocity
            velocity.x *= factor;                                                           // Can be used to simulate friction
            velocity.y *= factor;
            rotationSpeed *= factor;
        }

        inline void ApplyRotation( float impulse ) {                                        // Apply an impulse to the rotation speed
            rotationSpeed += impulse;
        }

        inline void AdjustAngle( float delta ) {                                            // change the current angle without changing the speed
            angle += delta;
        }

        inline void CapVelocity( float maxSpeed ) {                                         // If the current velocity is greater than the given
            if( GetMagnitudeSquared(velocity) > maxSpeed * maxSpeed ) {                     // max speed then reduce the velocity
                velocity = GetUnitVector(velocity) * maxSpeed;
            }
        }
};

#endif // MOVEABLEOBJECT_HPP_INCLUDED
