/*
 * This header defines an abstract
 * base class for any object that
 * logs information
 *
 * Author:      Stuart Rudderham
 * Created:     September 3, 2012
 * Last Edited: October 19, 2012
 */

#ifndef LOGGABLEOBJECT_HPP_INCLUDED
#define LOGGABLEOBJECT_HPP_INCLUDED

class LoggableObject {
    private:
                                                        // No variables, each child class declares all the ValueLoggers they need
    public:
        LoggableObject();                               // ctor. Adds the object to the LoggingManager

        virtual void UpdateLoggers() = 0;               // update all the ValueLoggers associated with the object
};

#endif // LOGGINGOBJECT_HPP_INCLUDED
