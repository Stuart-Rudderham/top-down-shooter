/*
 * This file implements an abstract
 * base class for any object that
 * can move
 *
 * Author:      Stuart Rudderham
 * Created:     August 16, 2012
 * Last Edited: October 19, 2012
 */

#include "Interfaces/Logging/LoggableObject.hpp"
#include "Managers/Logging/LoggingManager.hpp"

using namespace sf;

// constructor
LoggableObject::LoggableObject()
{
    // Add the LoggableObject to the LoggingManger
    LoggingManager::Get().AddLoggableObject( this );
}
