/* 
 * This file holds all the external files used 
 * for storing media within an executable file 
 * 
 * Generated on January 12, 2013 at 16:55
 */ 

const ExternalFile TestMap = "Content/Maps/Test.xml"; 
const ExternalFile DefaultZombieDeadImage = "Content/Media/Characters/DefaultZombieDead.png"; 
const ExternalFile SoldierAliveImage = "Content/Media/Characters/SoldierAlive.png"; 
const ExternalFile SoldierDeadImage = "Content/Media/Characters/SoldierDead.png"; 
const ExternalFile ZombieAliveImage = "Content/Media/Characters/ZombieAlive.png"; 
const ExternalFile ZombieDeadImage = "Content/Media/Characters/ZombieDead.png"; 
const ExternalFile DebugTextFont = "Content/Media/Fonts/DebugText.ttf"; 
const ExternalFile CheckBoxImage = "Content/Media/GUI/CheckBox.png"; 
const ExternalFile LeftArrowImage = "Content/Media/GUI/LeftArrow.png"; 
const ExternalFile RightArrowImage = "Content/Media/GUI/RightArrow.png"; 
const ExternalFile PistolEquipSound = "Content/Media/Guns/Pistol/Pistol Equip.wav"; 
const ExternalFile PistolReloadSound = "Content/Media/Guns/Pistol/Pistol Reload.wav"; 
const ExternalFile PistolShotSound = "Content/Media/Guns/Pistol/Pistol Shot.wav"; 
const ExternalFile PistolImage = "Content/Media/Guns/Pistol/Pistol.png"; 
const ExternalFile SuperGunShotSound = "Content/Media/Guns/SuperGun/SuperGun Shot.wav"; 
const ExternalFile SuperGunImage = "Content/Media/Guns/SuperGun/SuperGun.png"; 
const ExternalFile SpawnPointImage = "Content/Media/Maps/SpawnPoint.png"; 
const ExternalFile TestMapBackgroundImage = "Content/Media/Maps/TestMapBackground.jpg"; 
const ExternalFile TreeImage = "Content/Media/Maps/Tree.png"; 
const ExternalFile MenuSound = "Content/Media/Music/Menu.ogg"; 
const ExternalFile TestFont = "Content/Media/Testing/Test.ttf"; 
const ExternalFile TestBMPImage = "Content/Media/Testing/TestBMP.bmp"; 
const ExternalFile TestPNGImage = "Content/Media/Testing/TestPNG.png"; 
const ExternalFile TestWAVSound = "Content/Media/Testing/TestWAV.wav"; 
