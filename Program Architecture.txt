Factory
    - Provides a contiguous block of memory for objects of a given type to be stored
    - Automatically checks for memory leaks when destroyed
    - It does not handle construction or destruction of objects, it merely provides an address where an object of type T can be created
    - Each class (including templated classes) will have its own private Factory, which is used by the overloaded "operator new" function
        - The "operator new" will handle calling the ctors and dtors
    - It is a global singleton
        - Created on demand, deleted automatically at program end
    - Is a fixed size, cannot grow or shrink
        - The size is determined by the class the Factory produces
    - Is created on demand, to avoid static initialization order issues
        - Since destructors are called in reverse order of constructors this means that there are also no static de-initialization  issues, ASSUMING that any static object that uses a Factory in its destructor *also* uses that Factory in its constructor. 
            - Hopefully this shouldn't be an issue, and if it is we will catch it because the Factory checks to see if there are any un-deleted objects when it is destroyed.
        - This also means that a Factory<T> isn't created if no objects of type T are created (on the heap)  

Interfaces
    - Provides a uniform interface for GameObjects that have the same functionality
        - For example, all objects that can collide, or all objects that can be drawn to the screen
    - The (virtual) base class for all Interfaces is the GameObject class
    - Interfaces should have a corresponding Manager that handles the updating of the GameObject
    - Some Interfaces automatically add the GameObject to the relevant Manager
        - These Interfaces should also check to make sure the GameObject has been removed from the Manager before destruction
            - Otherwise we get dangling pointers
    - Some Interfaces support manual removal of the GameObject from the corresponding Manager
        - These removal flags are kept in the GameObject class
        - They don't remove the GameObject right away, they only flag it for deferred removal at the end of the frame
            - Since the Manager will immediately start ignoring any marked GameObjects it is equivalent, however

Manager
    - Handles the updating of GameObjects that have the same Interface
        - A single Manager can handle multiple Interfaces, if desired
    - It is a global singleton
        - Created on demand, deleted automatically at program end
    - Any marked GameObjects are removed at the end of the frame
        - If a GameObject is marked halfway through the frame the Manager will immediately start ignoring the object

MediaCache
    - Provides transparent loading and reusing of media (e.g. pictures, sounds, etc...)
        - Makes sure that only one copy is actually loaded into memory and it shared between all objects that need it
    - All media is free'd when the Cache is destructed
    - It is a global singleton
        - Created on demand, deleted automatically at program end

Object Lifetime
    - GameObjects can be created at anytime
        - When created they may create other GameObjects, and so on
        - When created they are immediately added to any relevant Managers

    - GameObjects are only deleted at the end of a frame
        - This is because a GameObject must be removed from all relevant Managers before deletion and it is more efficient to do a batch removal at the end of the frame (O(n) time) rather than doing individual removals whenever a GameObject is destroyed (O(n^2) time)

    - When a GameObject is to be deleted it sets a flag, which "marks" it for deletion
        - This can happen at any time
        - Once this happens the GameObject is ignored by all Managers
            - It can still be accessed by other GameObjects, however